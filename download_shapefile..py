import requests
from bs4 import BeautifulSoup

# URL to the UEFA rankings page
url = "https://www.uefa.com/nationalassociations/uefarankings/club/?year=2024"

# Fetch the content from the URL
response = requests.get(url)
soup = BeautifulSoup(response.content, 'html.parser')

# Parse the team names and coefficients
teams = soup.select("table.rankings-table tbody tr")

# Extract the team names and coefficients
team_coefficients = {}
for team in teams:
    columns = team.find_all("td")
    if len(columns) > 2:
        team_name = columns[1].text.strip()
        coefficient = float(columns[2].text.strip())
        team_coefficients[team_name] = coefficient

# Generate the team_coefficient.py file
with open("team_coefficient.py", "w") as file:
    file.write("# Consolidated team list with UEFA coefficients\n\n")
    file.write("team_coefficients = {\n")
    for team, coefficient in team_coefficients.items():
        file.write(f'    "{team}": {coefficient},\n')
    file.write("}\n")

print("team_coefficient.py has been created with the updated coefficients.")
