import random
import time

pot1 = [("FC Liverpool", "England"),
        ("Manchester City", "England"),
        ("Chelsea", "England"),
        ("FC Barçelona", "Spain"),
        ("Real Madrid", "Spain"),
        ("Bayern München", "Germany"),
        ("Juventus", "Italy"),
        ("Paris Saint-Germain", "France")]
pot2 = [("Manchester United", "England"),
        ("Arsenal", "England"),
        ("Atletico Madrid", "Spain"),
        ("Villarreal", "Spain"),
        ("Borussia Dortmund", "Germany"),
        ("Ajax", "Netherlands"),
        ("Porto", "Portugal"),
        ("Zenit St Petersburg", "Russia")]
pot3 = [("Leverkusen", "Germany"),
        ("Schalke 04", "Germany"),
        ("AC Milan", "Italy"),
        ("Inter Milano", "Italy"),
        ("Marseille", "France"),
        ("AS Monaco", "France"),
        ("Shakhtar Donetsk", "Ukraine"),
        ("Fenerbahçe Istanbul", "Turkey")]
pot4 = [("Tottenham", "England"),
        ("Dynamo Kiev", "Ukraine"),
        ("CSKA Moscow", "Russia"),
        ("Olympiakos Piraeus", "Greece"),
        ("Benfica", "Portugal"),
        ("Galatasaray", "Turkey"),
        ("PSV Eindhoven", "Netherlands"),
        ("FC Copenhagen", "Denmark")]

group = []

for i in range(8):
    group.append([])

random.shuffle(pot1)
random.shuffle(pot2)
random.shuffle(pot3)
random.shuffle(pot4)

# Draw the first team
for m in range(8):
    group[m].append(pot1[m])

# Draw the second team
for n in range(8):
    first_country = group[n][0][1]
    second_country = pot2[0][1]
    if first_country != second_country:
        group[n].append(pot2[0])
        pot2.remove(pot2[0])
    else:
        second_country = pot2[1][1]
        if first_country != second_country:
            group[n].append(pot2[1])
            pot2.remove(pot2[1])

    # Draw the third team
for n in range(8):
    first_country = group[n][0][1]
    second_country = group[n][1][1]
    third_country = pot3[0][1]
    if first_country != third_country and second_country != third_country:
        group[n].append(pot3[0])
        pot3.remove(pot3[0])
    else:
        third_country = pot3[1][1]
        if first_country != third_country and second_country != third_country:
            group[n].append(pot3[1])
            pot3.remove(pot3[1])
        else:
            group[n].append(pot3[2])
            pot3.remove(pot3[2])

# Draw the fourth team
for n in range(8):
    first_country = group[n][0][1]
    second_country = group[n][1][1]
    third_country = group[n][2][1]
    fourth_country = pot4[0][1]
    if first_country != fourth_country and second_country != fourth_country and third_country != fourth_country:
        group[n].append(pot4[0])
        pot4.remove(pot4[0])
    else:
        fourth_country = pot4[1][1]
        if first_country != fourth_country and second_country != fourth_country and third_country != fourth_country:
            group[n].append(pot4[1])
            pot4.remove(pot4[1])
        else:
            group[n].append(pot4[2])
            pot4.remove(pot4[2])
    # print(group[n])                                                    # it shows the teams and its country in a list
    '''                                                                  it shows the teams one under the other --> '''
    # print("", group[n][0][0], "\n", group[n][1][0], "\n", group[n][2][0], "\n", group[n][3][0], "\n")

print("Group A".ljust(19), "Group B".ljust(19), "Group C".ljust(19), "Group D".ljust(19),
      "Group E".ljust(19), "Group F".ljust(19), "Group G".ljust(19), "Group D".ljust(19))

for n in range(8):
    time.sleep(1)
    print(group[n][0][0].ljust(20), end="")                         # it showed all the teams in pot1 in all the groups
print('')
for n in range(8):
    time.sleep(1)
    print(group[n][1][0].ljust(20), end="")                         # it showed all the teams in pot2 in all the groups
print('')
for n in range(8):
    time.sleep(1)
    print(group[n][2][0].ljust(20), end="")                         # it showed all the teams in pot3 in all the groups
print('')
for n in range(8):
    time.sleep(1)
    print(group[n][3][0].ljust(20), end="")                         # it showed all the teams in pot4 in all the groups

# -------------------------------------------- it shows all the teams in groups with delay, being four groups per line,
# ----------------------------------------------------------------------- but team1 Group E appears after team4 Group D
print()
print()
print("Group A".ljust(34), "Group B".ljust(34), "Group C".ljust(34), "Group D".ljust(34))
time.sleep(1)
print(group[0][0][0].ljust(35), end="")
time.sleep(1)
print(group[1][0][0].ljust(35), end="")
time.sleep(1)
print(group[2][0][0].ljust(35), end="")
time.sleep(1)
print(group[3][0][0].ljust(35))

time.sleep(1)
print(group[0][1][0].ljust(35), end="")
time.sleep(1)
print(group[1][1][0].ljust(35), end="")
time.sleep(1)
print(group[2][1][0].ljust(35), end="")
time.sleep(1)
print(group[3][1][0].ljust(35))

time.sleep(1)
print(group[0][2][0].ljust(35), end="")
time.sleep(1)
print(group[1][2][0].ljust(35), end="")
time.sleep(1)
print(group[2][2][0].ljust(35), end="")
time.sleep(1)
print(group[3][2][0].ljust(35))

time.sleep(1)
print(group[0][3][0].ljust(35), end="")
time.sleep(1)
print(group[1][3][0].ljust(35), end="")
time.sleep(1)
print(group[2][3][0].ljust(35), end="")
time.sleep(1)
print(group[3][3][0].ljust(35))

print()
print("Group E".ljust(34), "Group F".ljust(34), "Group G".ljust(34), "Group H".ljust(34))
time.sleep(1)
print(group[4][0][0].ljust(35), end="")
time.sleep(1)
print(group[5][0][0].ljust(35), end="")
time.sleep(1)
print(group[6][0][0].ljust(35), end="")
time.sleep(1)
print(group[7][0][0].ljust(35))

time.sleep(1)
print(group[4][1][0].ljust(35), end="")
time.sleep(1)
print(group[5][1][0].ljust(35), end="")
time.sleep(1)
print(group[6][1][0].ljust(35), end="")
time.sleep(1)
print(group[7][1][0].ljust(35))

time.sleep(1)
print(group[4][2][0].ljust(35), end="")
time.sleep(1)
print(group[5][2][0].ljust(35), end="")
time.sleep(1)
print(group[6][2][0].ljust(35), end="")
time.sleep(1)
print(group[7][2][0].ljust(35))

time.sleep(1)
print(group[4][3][0].ljust(35), end="")
time.sleep(1)
print(group[5][3][0].ljust(35), end="")
time.sleep(1)
print(group[6][3][0].ljust(35), end="")
time.sleep(1)
print(group[7][3][0].ljust(35))
# ---------------------------------------------------------------------------------------------------------------------

'''
# it shows all the teams in groups without delay, being four groups per line
print()
print()
print("Group A".ljust(35), "Group B".ljust(35), "Group C".ljust(35), "Group D".ljust(35))
print(group[0][0][0].ljust(35), group[1][0][0].ljust(35), group[2][0][0].ljust(35), group[3][0][0].ljust(35))
print(group[0][1][0].ljust(35), group[1][1][0].ljust(35), group[2][1][0].ljust(35), group[3][1][0].ljust(35))
print(group[0][2][0].ljust(35), group[1][2][0].ljust(35), group[2][2][0].ljust(35), group[3][2][0].ljust(35))
print(group[0][3][0].ljust(35), group[1][3][0].ljust(35), group[2][3][0].ljust(35), group[3][3][0].ljust(35))

print()
print()
print("Group D".ljust(35), "Group E".ljust(35), "Group F".ljust(35), "Group G".ljust(35))
print(group[4][0][0].ljust(35), group[5][0][0].ljust(35), group[6][0][0].ljust(35), group[7][0][0].ljust(35))
print(group[4][1][0].ljust(35), group[5][1][0].ljust(35), group[6][1][0].ljust(35), group[7][1][0].ljust(35))
print(group[4][2][0].ljust(35), group[5][2][0].ljust(35), group[6][2][0].ljust(35), group[7][2][0].ljust(35))
print(group[4][3][0].ljust(35), group[5][3][0].ljust(35), group[6][3][0].ljust(35), group[7][3][0].ljust(35))
'''