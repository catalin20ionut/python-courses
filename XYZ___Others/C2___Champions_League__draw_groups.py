# Champions League Draw
import random
# global variables
num_teams = 32
england = []
france = []
spain = []
germany = []
italy = []
portugal = []
russia = []
dutch = []
turkey = []
swiss = []
scotland = []
greece = []
poland = []
belgium = []

groupA = []
groupB = []
groupC = []
groupD = []
groupE = []
groupF = []
groupG = []
groupH = []
groups = [groupA, groupB, groupC, groupD, groupE, groupF, groupG, groupH]
countries1 = [england, france, spain, germany, italy, portugal, russia]
countries2 = [dutch, turkey, swiss, scotland, greece, poland, belgium]
used = []
loading = 0
tens = 10

# intro
print("\tUEFA Champions League Draw Simulator\n")
print("Enter teams to make your very own UEFA Champions League.")
input("Press the enter key to begin.")

# ask for teams
england += [input("\nPlease enter an English team: ")]
england += [input("Please enter an English team: ")]
england += [input("Please enter an English team: ")]
england += [input("Please enter an English team: ")]

france += [input("Please enter a French team: ")]
france += [input("Please enter a French team: ")]
france += [input("Please enter a French team: ")]
france += [input("Please enter a French team: ")]

spain += [input("Please enter a Spanish team: ")]
spain += [input("Please enter a Spanish team: ")]
spain += [input("Please enter a Spanish team: ")]
spain += [input("Please enter a Spanish team: ")]

germany += [input("Please enter a German team: ")]
germany += [input("Please enter a German team: ")]
germany += [input("Please enter a German team: ")]
germany += [input("Please enter a German team: ")]

italy += [input("Please enter an Italian team: ")]
italy += [input("Please enter an Italian team: ")]
italy += [input("Please enter an Italian team: ")]
italy += [input("Please enter an Italian team: ")]

portugal += [input("Please enter a Portuguese team: ")]
portugal += [input("Please enter a Portuguese team: ")]
portugal += [input("Please enter a Portuguese team: ")]

russia += [input("Please enter a Russian team: ")]
russia += [input("Please enter a Russian team: ")]

dutch += [input("Please enter a Dutch team: ")]

turkey += [input("Please enter a Turkish team: ")]

swiss += [input("Please enter a Swiss team: ")]

scotland += [input("Please enter a Scottish team: ")]

greece += [input("Please enter a Greek team: ")]

poland += [input("Please enter a Polish team: ")]

belgium += [input("Please enter a Belgiun team: ")]

# make copies
england1 = england[:]
france1 = france[:]
spain1 = spain[:]
germany1 = germany[:]
italy1 = italy[:]
portugal1 = portugal[:]
russia1 = russia[:]
dutch1 = dutch[:]
turkey1 = turkey[:]
swiss1 = swiss[:]
scotland1 = scotland[:]
greece1 = greece[:]
poland1 = poland[:]
belgium1 = belgium[:]

countries3 = [england1, france1, spain1, germany1, italy1, portugal1, russia1]
countries4 = [dutch1, turkey1, swiss1, scotland1, greece1, poland1, belgium1]

# create groups

while num_teams != 7:
    x = 0
    position = random.randrange(len(countries1))
    country = countries1[position]
    country1 = countries3[position]
    if country1:
        team = random.choice(country1)
        if team not in used:
            group = random.choice(groups)
            if len(group) < 4:
                for i in group:
                    if i not in country:
                        x += 0
                    else:
                        x += 1
                if x == 0:
                    group += [team]
                    num_teams -= 1
                    used += [team]
                    country1.remove(team)
                    loading += 1
                    if loading == tens:
                        print("\nLoading...\n")
                        tens += 10

while num_teams != 0:
    x = 0
    position = random.randrange(len(countries2))
    country = countries2[position]
    country1 = countries4[position]
    if country1:
        team = random.choice(country1)
        if team not in used:
            group = random.choice(groups)
            if len(group) < 4:
                for i in group:
                    if i not in country:
                        x += 0
                    else:
                        x += 1
                if x == 0:
                    group += [team]
                    num_teams -= 1
                    used += [team]
                    country1.remove(team)
                    loading += 1
                    if loading == tens:
                        print("\nLoading...\n")
                        tens += 10

# display groups
print("\nAssembling groups...\n")
input("Groups complete. Press the enter key to view.")

print("\nGroup A")
print(groupA[0])
print(groupA[1])
print(groupA[2])
print(groupA[3])
print()

print("\nGroup B")
print(groupB[0])
print(groupB[1])
print(groupB[2])
print(groupB[3])
print()
print("\nGroup C")
print(groupC[0])
print(groupC[1])
print(groupC[2])
print(groupC[3])
print()
print("\nGroup D")
print(groupD[0])
print(groupD[1])
print(groupD[2])
print(groupD[3])
print()
print("\nGroup E")
print(groupE[0])
print(groupE[1])
print(groupE[2])
print(groupE[3])
print()
print("\nGroup F")
print(groupF[0])
print(groupF[1])
print(groupF[2])
print(groupF[3])
print()
print("\nGroup G")
print(groupG[0])
print(groupG[1])
print(groupG[2])
print(groupG[3])
print()
print("\nGroup H")
print(groupH[0])
print(groupH[1])
print(groupH[2])
print(groupH[3])

# exit
input("\n\nPress the enter key to exit.")
