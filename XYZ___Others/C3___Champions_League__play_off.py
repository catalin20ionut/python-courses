import random
import time

pot1 = [("FC Liverpool", "A"),
        ("FC Barçelona", "B"),
        ("Bayern München", "C"),
        ("Manchester City", "D"),
        ("Juventus", "E"),
        ("Real Madrid", "F"),
        ("Paris Saint-Germain", "G"),
        ("Chelsea", "H")]
pot2 = [("Zenit St Petersburg", "A"),
        ("Manchester United", "B"),
        ("Atletico Madrid", "C"),
        ("Ajax", "D"),
        ("Borussia Dortmund", "E"),
        ("Arsenal", "F"),
        ("Villarreal", "G"),
        ("Porto", "H")]


group = []

for i in range(8):
    group.append([])

random.shuffle(pot1)
random.shuffle(pot2)

# Draw the first team
for m in range(8):
    group[m].append(pot1[m])

# Draw the second team
for n in range(8):
    first_in_group = group[n][0][1]
    second_in_group = pot2[0][1]
    if first_in_group != second_in_group:
        group[n].append(pot2[0])
        pot2.remove(pot2[0])
    else:
        second_in_group = pot2[1][1]
        if first_in_group != second_in_group:
            group[n].append(pot2[1])
            pot2.remove(pot2[1])

for n in range(8):
    time.sleep(1)
    print(f"{n + 1}. {group[n][0][0].ljust(20)} - ", end=" ")                               # it shows the team in pot1
    time.sleep(1)
    print(f"\r{n + 1}. {group[n][0][0].ljust(20)} - {group[n][1][0].ljust(20)}", end="\n")  # it shows the team in pot2
