import zipfile
import os

# Calea către arhiva descărcată
zip_file_path = "downloads/ne_110m_admin_0_countries.zip"

# Directorul în care vei extrage fișierul .shp
extract_dir = "/path/to/extract/directory"

# Asigură-te că directorul de extragere există
if not os.path.exists(extract_dir):
    os.makedirs(extract_dir)

# Extrage fișierul .shp din arhiva descărcată
with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
    zip_ref.extract("ne_110m_admin_0_countries.shp", extract_dir)

print("Fișierul a fost extras cu succes în directorul:", extract_dir)
