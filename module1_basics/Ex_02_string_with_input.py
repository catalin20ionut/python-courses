first_name = "John"
last_name = "Doe"
age = 25
print(f"Option 1: My full name is {first_name} {last_name} and I'm {age} years old.")
print("Option 2: My full name is {} {} amd I'm {} years old.".format("John", "Doe", 25))
print("Option 3: My full name is {0} {1} amd I'm {2} years old.".format("John", "Doe", 25))
print(f"Option 4: My full name is {first_name} {last_name} and I'm {age} years old.")


first_name2 = input("First name: ")
last_name2 = input("Last name: ")
age2 = input("Age: ")
print("Option 5: My full name is " + first_name2 + " " + last_name2 + " and I'm " + age2 + " years old.")
