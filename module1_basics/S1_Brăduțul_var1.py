# https://www.codesansar.com/python-programming-examples/generate-christmas-tree-pattern.htm
var = "+"
print(" "*15 + var + " "*15)
print(" "*14 + (var*3) + " "*14)
print(" "*13 + (var*5) + " "*13)
print(" "*12 + (var*7) + " "*12)
print(" "*11 + (var*9) + " "*11)
print(" "*10 + (var*11) + " "*10)
print(" "*9 + (var*13) + " "*9)
print(" "*8 + (var*15) + " "*8)
print(" "*7 + (var*17) + " "*7)
print((var*3).center(31))
print((var*3).center(31))
print((var*3).center(31))
