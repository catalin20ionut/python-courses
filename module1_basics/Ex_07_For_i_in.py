animals = ["dogs", "cat", "fish", "horse"]

for animal in animals:
    print("Situation 1.1 :", animal)                                         # list all the animals in the animals list

for animal in animals:
    if animal != "fish":
        print("Situation 2.1:", animal)                            # print all the animals in the list excepting "fish"

for animal in animals:
    if animal == "fish":
        continue
    print("Situation 2.1:", animal)                               # print all the animals in the list excepting "fish"


for i in range(0, len(animals)):
    print("Situation 1.2:", i, animals[i], sep=" >>> ")

my_list = [x for x in range(10)]
print(my_list)
print([x for x in range(10)])


