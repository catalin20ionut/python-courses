my_string1 = "bbbAAAaa"
print(my_string1.count("a"))

my_string2 = "bbbAAAaa"
y = my_string2.count("a")
print(f"The character \"a\" appears {y} times.")


def counting_how_many_times_a_character_appears_in_a_string(any_string, any_character):
    count = 0
    for i in range(0, len(any_string)):
        if any_string[i] == any_character:
            count += 1
    print(f"The character \"{any_character}\" appears {count} times.")

my_string3 = "bbbAAAaa"
my_character3 = "a"
counting_how_many_times_a_character_appears_in_a_string(my_string3, my_character3)

""" Java =>
public class Exercise_C8 {
    public static void countingHowManyTimesACharacterAppearsInAString(String anyString, char anyCharacter) {
            int count = 0;
            for (int i = 0; i < anyString.length(); i++) {
                if (anyString.charAt(i) == anyCharacter) {
                    count++;}
            }
            System.out.println("The character '"+ anyCharacter +"' appears " + count + " times.");
        }

    public static void main(String[] args) {
        countingHowManyTimesACharacterAppearsInAString("bbbAAAaa", 'a');
    }
}
"""