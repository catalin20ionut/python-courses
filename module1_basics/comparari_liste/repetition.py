from collections import Counter

list_a = [1, 3, "o", 7, 3, "私", "わたし", "١", "٢", "٣", "s", "o", 3, 7, 3]
element_count = Counter(list_a)                          # Folosim Counter pentru a număra frecvențele fiecărui element

# Afișăm elementele și frecvențele lor
for element, count in element_count.items():
    if count > 1:
        print(f"Elementul {element} apare de {count} ori.")



