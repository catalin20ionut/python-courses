list1 = [1, 5, 9, 2, 7, "a", "d", 8]
list2 = [1, "c", 7, 9, "e", 2,  5, "a", "d", 0]

# Transformăm listele în seturi pentru a simplifica operațiile de comparație
set1 = set(list1)
set2 = set(list2)
diff1 = set1 - set2                                                          # Elemente din list1 care nu sunt în list2
diff2 = set2 - set1                                                          # Elemente din list2 care nu sunt în list1
print("a1. Elementele din list1 care nu sunt în list2:", diff1)
print("a2. Elementele din list2 care nu sunt în list1:", diff2)


diff1 = [element for element in list1 if element not in list2]               # Elemente din list1 care nu sunt în list2
diff2 = [element for element in list2 if element not in list1]               # Elemente din list2 care nu sunt în list1
print("b1. Elementele din list1 care nu sunt în list2:", diff1)
print("b2. Elementele din list2 care nu sunt în list1:", diff2)
print("c1. Elementele din list1 care nu sunt în list2:", ", ".join(map(str, diff1)))
print("c2. Elementele din list2 care nu sunt în list1:", ", ".join(map(str, diff2)))
