"""
    8. Write a program that when completed will calculate the amount of paint needed to paint the walls and the ceiling
of a room. Your program should have variables for length, width and height of room. Assume that the room has doors and
windows that don't need painting. Also, the floor in the room is not painted. Use initial variables to enter the number
of doors and number of windows in the room and adjust the total square feet to be painted accordingly. Assume that each
door is 20 square feet and each window is 15 square feet. Suppose the paint covers 350 square feet per paint bucket.
Print in console how many paint buckets you need.                                                                   """
from math import ceil

length = float(input("Input the length of the room: "))
width = float(input("Input the width of the room: "))
height = float(input("Input the height of the room: "))
number_of_doors = int(input("How many doors does the room have? - "))
number_of_windows = int(input("How many windows does the room have? - "))
area_that_must_be_painted = length*height*2 + width*height*2 + width*length - number_of_doors*20 - number_of_windows*15
print(f"For this room we need {area_that_must_be_painted/350} paint buckets. "
      f"So we have to buy {ceil(area_that_must_be_painted/350)} paint buckets.")

""" Java =>
public class Exercise_8 {
    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);
        System.out.print("Input the length of the room: ");
        float length = input.nextFloat();

        System.out.print("Input the width of the room: ");
        float width = input.nextFloat();

        System.out.print("Input the height of the room: ");
        float height = input.nextFloat();

        System.out.print("How many doors does the room have? - ");
        int numberOfDoors = input.nextInt();

        System.out.print("How many windows does the room have? - ");
        int numberOfWindows = input.nextInt();

        float areaThatMustBePainted = length*height*2 + width*height*2
                + width*length - numberOfDoors*20 - numberOfWindows*15;

        System.out.println("For this room we need " + areaThatMustBePainted/350 + " paint buckets. " +
                "So we have to buy " + (int) Math.ceil(areaThatMustBePainted/350) + " paint buckets.");
    }
}
"""