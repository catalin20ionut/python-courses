def Fibonacci(n):
    if n < 0:
        print("Incorrect input")                                                          # First Fibonacci number is 0
    elif n == 0:
        return 0                                                                         # Second Fibonacci number is 1
    elif n == 1:
        return 1
    else:
        return Fibonacci(n-1)+Fibonacci(n-2)
print("Varianta 1:", Fibonacci(9))


def Fibonacci2(n):
    f = [0, 1]                                                      # Taking the first two fibonacci numbers as 0 and 1
    for i in range(2, n + 1):
        f.append(f[i - 1] + f[i - 2])
    return f[n]
print("Varianta 2:", Fibonacci2(9))
