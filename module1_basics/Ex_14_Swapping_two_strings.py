str1 = "Good"
str2 = "morning"
print("Strings before swapping: " + str1 + " " + str2)
concatenated_string = str1 + str2
str2 = str1[0: (len(concatenated_string) - len(str2))]                 # It extracts str2 from the concatenated string.
str1 = concatenated_string[len(str2):]                                 # It extracts str1 from the concatenated string.
print("Strings after swapping: " + str1 + " " + str2)
print(f"Strings after swapping: {str1} {str2}")

""" Java => Write a Java program to swap 2 strings.
public class Exercise_C11 {
    public static void main(String[] args) {
        String str1 = "Good", str2 = "morning";
        System.out.println("Strings before swapping: " + str1 + " " + str2);

        String concatenatedString = str1 + str2;                            
        str2 = str1.substring(0, (concatenatedString.length() - str2.length()));                    
        str1 = concatenatedString.substring(str2.length());            // It extracts str2 from the concatenated string.
        System.out.println("Strings after swapping: " + str1 + " " + str2);
    }  
}
"""