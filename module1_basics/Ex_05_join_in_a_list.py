given_numbers = ["1", "2", "3", "4"]
my_numbers = given_numbers[1:4]
number = ''.join(my_numbers)
print(f"The book contains {number} pages.")
print(f"The book contains {''.join(given_numbers[1:4])} pages.")

list1 = ["We", " are", " learning."]
print(''.join(list1))
