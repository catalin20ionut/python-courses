class BasicBurger:
    def __init__(self, meat_type="pork", price=5.5, bread_type="roll"):
        self.meat_type = meat_type
        self.price = price
        self.bread_type = bread_type

    def __str__(self):
        return f"For the standard Burger with {self.bread_type} and {self.meat_type} the price is {self.price}."


class ExtraBurger(BasicBurger):
    def __init__(self, ingredient_name, additional_price, meat_type="pork", price=5.5, bread_type="roll"):
        super().__init__(meat_type, price, bread_type)
        self.ingredient_name = ingredient_name
        self.additional_price = additional_price

    def finalPrice(self):
        return self.price + self.additional_price

    def __str__(self):
        return f"After adding the first additional ingredient {self.ingredient_name}" \
               f" the price is {self.finalPrice()} euro."


class BurgerWithAll(BasicBurger):
    def __init__(self, ingredient_name1, ingredient_name2, ingredient_name3, ingredient_name4,
                 additional_price1, additional_price2, additional_price3, additional_price4,
                 meat_type="pork", price=5.5, bread_type="roll"):
        super().__init__(meat_type, price, bread_type)
        self.ingredient_name1 = ingredient_name1
        self.ingredient_name2 = ingredient_name2
        self.ingredient_name3 = ingredient_name3
        self.ingredient_name4 = ingredient_name4
        self.additional_price1 = additional_price1
        self.additional_price2 = additional_price2
        self.additional_price3 = additional_price3
        self.additional_price4 = additional_price4

    def finalPriceWithAll(self):
        if self.ingredient_name4 != "-":
            return self.price + self.additional_price1 + self.additional_price2 + self.additional_price3 \
                   + self.additional_price4
        elif self.ingredient_name4 == "-" and self.ingredient_name3 != "-":
            return self.price + self.additional_price1 + self.additional_price2 + self.additional_price3
        elif self.ingredient_name4 == "-" and self.ingredient_name3 == "-" and self.ingredient_name2 != "-":
            return self.price + self.additional_price1 + self.additional_price2
        elif self.ingredient_name4 == "-" and self.ingredient_name3 == "-" and self.ingredient_name2 == "-" \
                and self.ingredient_name1 != "-":
            return self.price + self.additional_price1

    def __str__(self):
        return f"After adding the additional ingredients {self.ingredient_name1}, {self.ingredient_name2}, " \
               f"{self.ingredient_name3}, {self.ingredient_name4} the price is {self.finalPriceWithAll()} euro."

print(BasicBurger())
extra_burger = ExtraBurger("lettuce", 2.2)
extra_burger2 = ExtraBurger("ketchup", 3)
extra_burger3 = ExtraBurger("tomatoes", 2.5)
print(extra_burger)
print(extra_burger2)
print(extra_burger3)
print(extra_burger.finalPrice(), "euro")
burger_with_all = BurgerWithAll("lettuce", "ketchup", "tomatoes", "mustard", 2.2, 3, 2.5, 1.25)
print(burger_with_all)
