class Burger:
    def __init__(self, ingredient_name1, ingredient_name2, ingredient_name3, ingredient_name4,
                 additional_price1, additional_price2, additional_price3, additional_price4,
                 meat_type="pork", price=5.5, bread_type="roll"):
        self.meat_type = meat_type
        self.price = price
        self.bread_type = bread_type
        self.ingredient_name1 = ingredient_name1
        self.ingredient_name2 = ingredient_name2
        self.ingredient_name3 = ingredient_name3
        self.ingredient_name4 = ingredient_name4
        self.additional_price1 = additional_price1
        self.additional_price2 = additional_price2
        self.additional_price3 = additional_price3
        self.additional_price4 = additional_price4

    def BasicBurger(self):
        print(f"For the standard Burger with {self.bread_type} and {self.meat_type} the price is {self.price}.")

    def ExtraBurger(self):
        y = self.price + self.additional_price1
        print(f"After adding the first additional ingredient {self.ingredient_name1} the price is {y} euro.")

    def SuperBurger(self):
        if self.ingredient_name4 != "-":
            z = self.price + self.additional_price1 + self.additional_price2 + self.additional_price3 \
                   + self.additional_price4
        elif self.ingredient_name4 == "-" and self.ingredient_name3 != "-":
            z = self.price + self.additional_price1 + self.additional_price2 + self.additional_price3
        elif self.ingredient_name4 == "-" and self.ingredient_name3 == "-" and self.ingredient_name2 != "-":
            z = self.price + self.additional_price1 + self.additional_price2
        elif self.ingredient_name4 == "-" and self.ingredient_name3 == "-" and self.ingredient_name2 == "-" \
                and self.ingredient_name1 != "-":
            z = self.price + self.additional_price1

        my_list = [self.ingredient_name1, self.ingredient_name2, self.ingredient_name3, self.ingredient_name4]
        new_list = [x for x in my_list if "-" not in x]
        join_list = ' '.join(new_list)  # 3a.

        list2 = [' and '.join(new_list) if len(new_list) == 2
                 else f"{self.ingredient_name1}, {self.ingredient_name2} "
                      f"and {self.ingredient_name3}" if len(new_list) == 2
                 else f"{my_list[0]}, {my_list[1]}, {my_list[2]} and {my_list[3]}"]
        join_list2 = ' '.join(list2)
        print(f"3a. After adding the additional ingredients {join_list} the price is {z} euro.")
        print(f"3b. After adding the additional ingredients {join_list2} the price is {z} euro.")

        # 3c.
        if len(new_list) == 2:
            list2b = [' and '.join(new_list)]
            print(f"3c. After adding the additional ingredients {' '.join(list2b)} the price is {z} euro.")
        elif len(new_list) == 3:
            print(f"3c. After adding the additional ingredients {my_list[0]}, {my_list[1]} and {my_list[2]} the price "
                  f"is {z} euro.")
        else:
            print(f"3c. After adding the additional ingredients {my_list[0]}, {my_list[1]}, {my_list[2]} and "
                  f"{my_list[3]} the price is {z} euro.")

# doar "tomatoes" sau / și "mustard" pot avea valoare "-"
burger = Burger("lettuce", "ketchup", "tomatoes", "mustard", 2.2, 3, 2.5, 1.25)              # (7.7, 10.7, 13.2, 14.45)
burger.BasicBurger()
burger.ExtraBurger()
burger.SuperBurger()
