class BankAccount:
    def __init__(self, iban, balance, name, email, phone_number):
        self.iban = iban
        self.balance = balance
        self.name = name
        self.email = email
        self.phone_number = phone_number

    def DepositMoney(self, amount_deposited):
        self.balance += amount_deposited
        print(f"3. You deposited the amount of {amount_deposited} euro.")
        print(f"4. The total amount is {self.balance} euro.")

    def WithdrawMoney(self, withdrawn_amount):
        if withdrawn_amount <= self.balance:
            self.balance -= withdrawn_amount
            print(f"5. You withdrew the amount of {withdrawn_amount} euro.")
            print(f"6. After withdrawal the total amount is {self.balance} euro.")
        else:
            print("7. You don't have enough money in your account!")

contul_meu = BankAccount("ro1234", 100.20, "Gabriel", "gabi@gmail.com", "+040751148")
contul_meu.DepositMoney(10.12)
contul_meu.DepositMoney(25)
contul_meu.DepositMoney(55)
contul_meu.WithdrawMoney(25.32)
contul_meu.WithdrawMoney(65)
contul_meu.WithdrawMoney(100)
contul_meu.DepositMoney(55)
contul_meu.WithdrawMoney(100)


'''
package C04___07_07_2022;

public class Ex_4___BankAccount {
    private String nr;
    private double balance;
    private String name;
    private String email;
    private String phoneNumber;

    public Ex_4___BankAccount(){
        this("ro1234", 100.20, "Gabriel", "gabi@gmail.com", "+040751148");
        System.out.println("1. The thirst constructor: ");
    }

    public Ex_4___BankAccount(String iban, double balance, String name, String email, String phoneNumber){
        this.nr = iban;
        this.balance = balance;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        System.out.println("2. The second constructor: ");
    }

    public void DepositMoney (double amountDeposited){
        this.balance += amountDeposited;
        System.out.println("3. You deposited the amount of " + amountDeposited + " euro.");
        System.out.println("4. The total amount is " + this.balance + " euros.");
    }

    public void WithdrawMoney (double withdrawnAmount){
        if (withdrawnAmount <= this.balance){
            this.balance -= withdrawnAmount;
            System.out.println("5. You withdrew the amount of " + withdrawnAmount + " euro.");
            System.out.println("6. After withdrawal the total amount is " + this.balance + " euro.");
        }
        else {
            System.out.println("7. You don't have enough money in your account!");
        }
    }

    public static void main(String[] args) {
       Ex_4___BankAccount contulMeu = new Ex_4___BankAccount();
       contulMeu.DepositMoney(10.12);
       contulMeu.DepositMoney(25);
       contulMeu.DepositMoney(55);
       contulMeu.WithdrawMoney(25.32);
       contulMeu.WithdrawMoney(65);
       contulMeu.WithdrawMoney(100);
       contulMeu.DepositMoney(55);
       contulMeu.WithdrawMoney(100);
    }
}
'''
