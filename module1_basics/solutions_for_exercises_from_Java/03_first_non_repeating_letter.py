def first_non_repeating_character(str1):
    char_order = []
    ctr = {}
    for c in str1:
        if c in ctr:
            ctr[c] += 1
        else:
            ctr[c] = 1
            char_order.append(c)
    for c in char_order:
        if ctr[c] == 1:
            return "The first non-repeating character is \"" + c + "\"."
    return "Either all characters are repeating or string is empty."


print(first_non_repeating_character('abcdef'))
print(first_non_repeating_character('python'))
print(first_non_repeating_character('acuma'))
print(first_non_repeating_character('anna'))
print(first_non_repeating_character('   '))
