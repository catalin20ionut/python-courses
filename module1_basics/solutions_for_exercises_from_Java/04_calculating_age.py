from datetime import date


def calculate_age(birthdate):
    today = date.today()
    any_age = today.year - birthdate.year - ((today.month, today.day) < (birthdate.month, birthdate.day))
    return any_age

print(calculate_age(date(1980, 2, 29)))
