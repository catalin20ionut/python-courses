C = float(input("Temperatură in Celsius: "))
if C <= (-273.15):
    print("Nu există")
else:
    F = C * 1.8 + 32
    K = C + 273.15
    print(f"Temperatura este %.3F" 'F' % F, f"• Temperatura este %.3F" 'K' % K)


F = float(input("Temperatură in Fahrenheit: "))
if F <= (-459.67):
    print("Nu există")
else:
    C = (F - 32) * 5 / 9
    K = (F - 32) * 5 / 9 + 273.15
    print(f"Temperatura este %.3F" 'C' % C, f"• Temperatura este %.3F" 'K' % K)


K = float(input("Temperatură in Kelvin: "))
if K <= 0:
    print("Nu există")
else:
    C = K - 273.15
    print(f"Temperatura este %.3F" 'C' % C, f"• Temperatura este %.3F" 'F' % F)
