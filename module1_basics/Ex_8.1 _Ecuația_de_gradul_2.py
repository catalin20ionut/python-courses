print("Pentru o funcție de gradul 2 de forma f(x)=ax^2+bx+c:")
var_a = int(input("a = "))
var_b = int(input("b = "))
var_c = int(input("c = "))
var_delta = var_b**2 - 4*var_a*var_c

print("Delta = ", var_delta)
if var_delta == 0:
    var_y = -var_b/(2*var_a)
    print("Rădăcina 1 = Rădăcina 2 = ", var_y)
elif var_delta > 0:
    var_y = (-var_b + var_delta**(1/2))/(2*var_a)
    print("Rădăcina 1 = ", var_y)
    var_y = (-var_b - var_delta ** (1 / 2)) / (2 * var_a)
    print("Rădăcina 2 = ", var_y)
else:
    print("Soluțiile sunt numere imaginare")
