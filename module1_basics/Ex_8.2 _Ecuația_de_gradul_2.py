import math
a = int(input("Enter the coefficients of a: "))
b = int(input("Enter the coefficients of b: "))
c = int(input("Enter the coefficients of c: "))
# sau cele două variante de mai jos
# a, b, c = (int(value) for value in input("Enter the coefficients of a, b and c separated by commas: ").split(","))
# a, b, c = map(int, input("Enter the coefficients of a, b and c separated by commas: ").split(","))
d = b**2 - 4*a*c  # discriminant

if d < 0:
    print("This equation has no real solution")
elif d == 0:
    x = (-b + math.sqrt(b**2 - 4*a*c))/2 * a
    print("This equation has one solutions: ", x)
else:
    x1 = (-b + math.sqrt(b**2 - 4*a*c))/2 * a
    x2 = (-b - math.sqrt(b**2 - 4*a*c))/2 * a
    print("This equation has two solutions: ", x1, " and", x2)
