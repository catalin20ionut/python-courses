""" Count the digits of a number."""
number = 123
sum_of_digits = 0
for digit in str(number):
    sum_of_digits += int(digit)
print("• With for .............................................. ")
print(f"Situation 1: The sum of the digits for the number {number} is {sum_of_digits}.")     # sau print(sum_of_digits)

sum_of_digits = sum(int(digit) for digit in str(number))
print(f"Situation 2: The sum of the digits for the number {number} is {sum_of_digits}.")     # sau print(sum_of_digits)
print(f"Situation 3: The sum of the digits for the number {number} is"
      f" {sum(int(digit) for digit in str(number))}.")             # sau print(sum(int(digit) for digit in str(number)))


def sumDigits(numbers):
    sum1 = 0
    for digit2 in str(numbers):
        sum1 += int(digit2)
    return sum1
number2 = 1234
print("• With for in a function ................................... ")
print(f"Situation 4. The sum of the digits for the number {number2} is {sumDigits(number2)}.")


def sumDigits2(n):
    str_result = str(n)
    list_of_number = list(map(int, str_result.strip()))
    return sum(list_of_number)
n2 = 12345
print("• With strings, map, list, .strip() ......................... ")
print(f"Situation 5. The sum of the digits for the number {n2} is {sumDigits2(n2)}.")            # sau print(getSum(n))


def sumDigits3(no):
    digit_sum = 0
    while no != 0:
        digit_sum += no % 10
        no = no // 10
    return digit_sum
no3 = 123456

print("• With while ................................................. ")
print(f"Situation 6. The sum of the digits for the number {no3} is {sumDigits3(no3)}.")          # sau print(getSum(n03)


def sumDigits4(aantal):
    return 0 if aantal == 0 else int(aantal % 10) + sumDigits4(int(aantal / 10))
aantal4 = 1234567
print("• With if ..................................................... ")              # sau print(sumDigits4(aantal4))
print(f"Situation 7. The sum of the digits for the number {aantal4} is {sumDigits4(aantal4)}.")


def sumDigits5(broj):
    if broj == 0:
        print("The sum of the digits for the number 0 is 0.")
    else:
        return int(broj % 10) + sumDigits4(int(broj / 10))

print("• With if, input ............................................... ")
broj5 = int(input("Situation 8: ........................ Enter the number: "))
print(f"Situation 8. The sum of the digits for the number {broj5} is {sumDigits5(broj5)}.")
