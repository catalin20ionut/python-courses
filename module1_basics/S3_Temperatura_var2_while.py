while True:
    try:
        c = float(input("Temperature in grade Celsius: "))
        break
    except ValueError:
        print("Nice try")
        continue
k = float(c + 273.15)
k1 = format(k, ' 2f')
header1 = "Temperatura in grade Celsius"
header2 = "Temperatura in grade Kelvin"
print(f"  | {header1} |  {header2} | ")
print(f"  |                        {c} |                  {k1} |")
