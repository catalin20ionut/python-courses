from collections import Counter


def can_generate_a_palindrome(s):
    for x in Counter(s).values():
        if x % 2:
            return bool(0)
        else:
            return bool(1)

print(can_generate_a_palindrome("carrace"))
print(can_generate_a_palindrome("daily"))
print(can_generate_a_palindrome("ana"))
