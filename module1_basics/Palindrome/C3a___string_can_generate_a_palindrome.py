from collections import Counter


def permute_palindrome(s):
    return sum(1 for i in Counter(s).values() if i % 2) <= 1

print(permute_palindrome('carrace'))
print(permute_palindrome('daily'))
