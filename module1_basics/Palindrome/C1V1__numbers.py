import math


def isPalindrome(any_number):
    return int(any_number > 0) and ((any_number % 10) * (10 ** int(math.log(any_number, 10)))
                                    + isPalindrome(any_number // 10))
my_number = 2332
print(f'{my_number} --> {(my_number == isPalindrome(my_number))}')
print("1. Rest modulo to 10  :", my_number % 10)
print("2. Counting \"zeros\"   :", int(math.log(my_number, 10)))
print("3. Tens,thousands etc.:", 10 ** int(math.log(my_number, 10)))
print("4. Value type 3:      :", (my_number % 10) * (10 ** int(math.log(my_number, 10))))
print("5. Remained value     :", isPalindrome(my_number // 10))
print("6. Reverse value      :", (my_number % 10)*(10 ** int(math.log(my_number, 10))) + isPalindrome(my_number // 10))

my_number_as_a_string = "2332"
if my_number_as_a_string == my_number_as_a_string[::-1]:
    print(f"The number {my_number_as_a_string} is a palindrome.")
else:
    print(f"The number {my_number_as_a_string} is not a palindrome.")


def verifyIfItIsAPalindrome(any_number_as_a_string):
    if any_number_as_a_string == any_number_as_a_string[::-1]:
        print(f"The number {any_number_as_a_string} is a palindrome.")
    else:
        print(f"The number {any_number_as_a_string} is not a palindrome.")
verifyIfItIsAPalindrome("121121")
