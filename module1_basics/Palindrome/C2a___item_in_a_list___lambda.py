""" Using filter and lambda, make a list of the strings that are a palindrome from the original list. """
list_a = ["1", "22", "333", "4444", "56555", "6 horses", "days", "ana", "Hannah"]
palindrome = list(filter(lambda x: x == x[::-1], list_a))
palindrome2 = list(filter(lambda x: x == "".join(reversed(x)), list_a))
print("Palindromes:", palindrome)
print(f"Palindromes: {palindrome2}")
print("Palindromes:", list(filter(lambda x: x.lower() == x[::-1].lower(), list_a)))
print('Palindromes:')
for i in palindrome:
    print('------------ {:>12}'.format(i))
