# C1 --- if a string is a palindrome
# C2 --- if the items in a list are palindromes
# C3 --- if a string can generate a palindrome
# C4 --- if a list is palindrome
# C5 --- if the items in a dictionary are palindromes
# C6 --- if two strings are anagrams
def isPalindrome(any_string):
    if any_string == any_string[::-1]:
        print(f"{any_string} is palindrome.")
    else:
        print(f"{any_string} is not palindrome.")

my_string = "ana"
my_string1 = "Ana"
my_string2 = "race"
isPalindrome(my_string)
isPalindrome(my_string2)


def isPalindrome2(any_string):
    if any_string.lower() == any_string[::-1].lower():
        print(f"{any_string} is palindrome.")
    else:
        print(f"{any_string} is not palindrome.")

my_string3 = "Ana"
isPalindrome2(my_string3)
