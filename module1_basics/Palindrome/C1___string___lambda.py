print((lambda x: x == x[::-1])("ana"))
print("palindrome" if ((lambda x: x == x[::-1])("ana")) else "not palindrome")

my_string = "Hannah"
print(f"\"{my_string}\" is a palindrome."
      if ((lambda x: x == x[::-1])(my_string)) else f"\"{my_string}\" is not a palindrome.")

print()
print((lambda x: x == x[::-1])((lambda x: x.lower())("anA")))
print("palindrome" if ((lambda x: x.lower() == x[::-1].lower())("Ana")) else "not palindrome")
print(f"\"{my_string}\" is a palindrome."
      if ((lambda x: x.lower() == x[::-1].lower())(my_string)) else f"\"{my_string}\" is not a palindrome.")
print(f"\"{my_string}\" is a palindrome."
      if ((lambda x: x.upper() == x[::-1].upper())(my_string)) else f"\"{my_string}\" is not a palindrome.")
