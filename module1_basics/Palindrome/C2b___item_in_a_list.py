list_a = ["1", "22", "333", "4444", "56555", "6 horses", "days", "ana", "hannah"]


def verifying_items_whether_are_palindromes_or_not(any_list):
    for word in any_list:
        if word == word[::-1]:
            print(f"{word:9} ---  TRUE --- ", bool(1))
        else:
            print(f"{word:9} --- FALSE ---", bool(0))
verifying_items_whether_are_palindromes_or_not(list_a)

#######################################################################################################################
list_p = []


def verifyingPalindrome(any_list):
    for word in any_list:
        if word.lower() == word[::-1].lower():
            list_p.append(word)
    print(f"2. Display as a list -> {list_p}")
verifyingPalindrome(list_a)
