def isPalindrome(any_number):
    temp_number = any_number
    reversed_any_number = 0

    while any_number > 0:
        remainder = any_number % 10
        reversed_any_number = reversed_any_number * 10 + remainder
        any_number //= 10                               # floor division

    if temp_number == reversed_any_number:
        print(f'The number {temp_number} is a palindrome.')
    else:
        print(f'The number {temp_number} is not a palindrome.')

isPalindrome(121)
isPalindrome(1212)
