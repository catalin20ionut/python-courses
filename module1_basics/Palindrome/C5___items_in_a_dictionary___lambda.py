my_d = {0: "Racecar", 1: "anA", 2: "Mary", 5: "hannah"}
print("A. New dictionary where the values are palindromes -> new_dict =",
      dict(filter(lambda val: val[1].lower() == val[1][::-1].lower(), my_d.items())))

print("------------------------------------------------------------------------------------------------------")


def items_in_dict_are_palindromes(any_dict):
    for key in any_dict:
        if any_dict[key].lower() == any_dict[key][::-1].lower():                           # any_dict[key] is the value
            print(f"{key} : {any_dict[key]} -> is a palindrome item in the dictionary.")
        else:
            print(f"{key} : {any_dict[key]} -> is not a palindrome item in the dictionary.")

items_in_dict_are_palindromes(my_d)
print("------------------------------------------------------------------------------------------------------")


new_dict = {}


def items_in_dict_are_palindromes2(any_dict):
    for key in any_dict:
        if any_dict[key].lower() == any_dict[key][::-1].lower():
            # new_dict.update({f"{key}": f"{any_dict[key]}"})  # sau
            new_dict.update({int(f"{key}"): f"{any_dict[key]}"})
    print(f"C. New dictionary where the values are palindromes -> new_dict = {new_dict}")

items_in_dict_are_palindromes2(my_d)
