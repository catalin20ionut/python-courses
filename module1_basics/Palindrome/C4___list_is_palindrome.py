def check_palindrome_list(any_list):
    any_list_changed_to_a_string = ' '.join([str(elem) for elem in any_list])
    if any_list_changed_to_a_string == any_list_changed_to_a_string[::-1]:
        print("The list is a palindrome.")
    else:
        print("The list isn't a palindrome.")

my_list = [77, 1, 56, "abc", "cba", 65, 1, 77]
check_palindrome_list(my_list)
