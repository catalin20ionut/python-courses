list_a = ["1", "22", "333", "4444", "56555", "6 horses", "days", "ana", "hannah"]


def verifying_item_whether_are_palindromes(any_list):
    for myString in any_list:
        mid = (len(myString) - 1) // 2
        start = 0
        last = len(myString) - 1
        flag = 0

        while start < mid:
            if myString[start] == myString[last]:
                start += 1
                last -= 1
            else:
                flag = 1
                break

        if flag == 0:
            print("{:10} {:2} {}".format(myString, "-", bool(1)))
        else:
            print("{:10} - {}".format(myString, bool(0)))

verifying_item_whether_are_palindromes(list_a)
