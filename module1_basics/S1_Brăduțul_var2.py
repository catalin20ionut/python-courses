# https://www.codesansar.com/python-programming-examples/generate-christmas-tree-pattern.htm


def tree(n):
    for i in range(n):
        print(("+" * (i * 2 + 1)).center(n * 2 - 1))

tree(10)

