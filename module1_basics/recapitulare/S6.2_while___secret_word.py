secret_word = "elefant"
guess = " "
guess_count = 0
guess_limit = 3
still_have_guesses = True
message = "Guess an animal! - "

while guess != secret_word and still_have_guesses:
    if guess_count < guess_limit:
        guess = input(message)
        guess_count += 1
        if guess_count == 1:
            message = "It's a large one. - "
        if guess_count == 2:
            message = "i" \
                      "It's afraid of mice. - "
    else:
        still_have_guesses = False


if still_have_guesses:
    print("You win!")
else:
    print("You lose")
