try:
    number = int(input("Enter a number: "))
    number_divided_by_zero = number/0
except ZeroDivisionError as err:
    print("Division by 0 is impossible")
