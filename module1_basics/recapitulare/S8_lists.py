cars = ["Volvo", "Volvo", "Audi", "BMW", 5, False]
expensive_cars = ["Porsche", "Aston Martin", "Dacia"]

print("1:", cars[0])
print("2:", cars[2:])
print("3:", cars[1:3])
print("4:", cars[4:1:-1])  # backwards de index 3 la 1
print("5:", cars[::-1])  # loop through list backwards
cars[4] = True
print("6:", cars)

cars.extend(expensive_cars)
print("7:", cars)

cars.append("Mazda")
print("8:", cars)

cars.insert(1, "Dacia")
print("9:", cars)

cars.remove("BMW")
print("10:", cars)

print("11:", cars.index("Audi"))
print("12:", cars.count("Volvo"))

expensive_cars.sort()
sorted(expensive_cars)
print("13:", expensive_cars, '--- lista nesortată -> ["Porsche", "Aston Martin", "Dacia"]')

cars2 = cars.copy()
print("14:", cars2)

"""  pop()- înlătură ultimul element, pop(1) - înlătură elementul 1 """
print("15:", cars.pop(), "--- List devine:", cars)

cars.clear()
print("16:", cars)
