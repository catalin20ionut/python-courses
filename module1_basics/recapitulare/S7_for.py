for letter in "Andy":
    print("1:", letter)

cars = ["Volvo", "BMW", "Audi", "Dacie"]
for element in cars:
    print("2:", element)

for index in range(7):
    if index == 3:
        continue   # ignoră ce urmează, trimite codul înapoi sus
    print("3:", index)

for i in range(7):
    if i == 3:
        break
    print("4:", i)

for j in range(10, 15):
    print("5:", j)


for el in range(10, 25, 2):
    print("6.1:", el)

print("6.2:", [x for x in range(10, 25, 2)])

my_list = [w for w in range(10, 25, 2)]
print("6.3:", my_list)


for z in range(len(cars)):
    print("7:", cars[z])
