# A class is like defining your own data type.
# It can't represent a student in a string or list.
class Student:
    def __init__(self, given_name, given_major, given_gpa, given_is_on_probation):
        self.gpa = given_gpa
        self.name = given_name
        self.major = given_major
        self.is_on_probation = given_is_on_probation

    def on_honor_roll(self):
        if self.gpa > 3.5:
            return True
        else:
            return False


s1 = Student(3, 3, 3, True)
print(s1.on_honor_roll())
