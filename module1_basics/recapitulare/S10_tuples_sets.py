# tuples sunt imutabile, odată ce se declară, așa rămân
coordinates = (4, 5)
print("1:", coordinates)
print("2:", coordinates[0])

# list of tuples
coordinates_list = [(4, 5), (6, 7)]

# Sets
my_set = {1, 2}
my_list = [1, 1, 1, 2, 2, 3]
my_list_transformed = list(set(my_list))
print("3:", my_list_transformed)
