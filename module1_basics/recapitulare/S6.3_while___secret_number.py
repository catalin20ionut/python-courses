secret_number = 18
guessed_number = 10
while int(guessed_number) != int(secret_number):
    guessed_number = input("Ghicește un număr intre 0 si 20: ")
    if int(guessed_number) == int(secret_number):
        print("Felicitări!")
    elif int(guessed_number) < int(secret_number):
        print("Încearcă un număr mai mare!")
    else:
        print("Încearcă un număr mai mic!")


secret_number_2 = 3
guessed_number_2 = -3
while guessed_number_2 != secret_number_2:
    guessed_number_2 = int(input("2. Ghicește un număr intre 0 si 20: "))
    if guessed_number_2 == secret_number_2:
        print("2. Felicitări!")
    elif secret_number_2 > guessed_number_2:
        print("2. Încearcă un număr mai mare!")
    else:
        print("2. Încearcă un număr mai mic!")
