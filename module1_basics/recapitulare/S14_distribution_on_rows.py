values = [x / 7 for x in range(50)]

cols = 5
# Figure out how many rows are needed
rows, extra = divmod(len(values), cols)
if extra > 0:
    # That means we need one final partial row
    rows += 1

# And show each row in turn
for row in range(rows):
    line = ""
    for col in range(cols):
        i = col * rows + row
        if i < len(values):
            line += f"{(i +1):3d} -> {values[i]:3.9f}  "  # formatare
    print(line)
