num1 = int(input("Enter num1: "))
op = input("Enter operation: ")
num2 = int(input("Enter num2: "))

if op == "+":
    print(f"The result is {num1+num2}.")
elif op == "-":
    print(f"The result is {num1-num2}.")
else:
    print("Invalid operator")
