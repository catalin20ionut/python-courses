monthConversions = {"Jan": "January",
                    "Feb": "February",
                    "Mar": "March",
                    "Apr": "April"
                    }

noteElevi = {
    "Ionica": 10,
    "Cireșica": 9,
    "Covidel": 4,
}

print(noteElevi["Covidel"])
print(monthConversions["Jan"])
print(monthConversions.get("Jan"))
print(monthConversions.get("Dec", "Invalid Month"))
