def say_hi():
    print("1: Hi")
    return True
say_hi()


def say_hi_user(name):
    """This function is called with a string and prints a message"""
    name = name + " Surcică"
    print(f"2: Hi, {name}!")
say_hi_user("Silviu")


def cube(num):
    return num*num*num
print("3.1:", cube(2))
print("3.2:", cube(4))


def sum_of_numbers(*args, **kwargs):
    return sum(args) + sum(kwargs.values())
print("4:", sum_of_numbers(10, 11, 12, 13, a=1, b=2, c=3))
