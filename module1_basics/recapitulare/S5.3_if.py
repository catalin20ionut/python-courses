is_male = True
is_tall = False

if is_male or is_tall:
    print("1. male or tall or both")
else:
    print("1. neither male nor tall")

if is_male and is_tall:
    print("2. male or tall or both")
else:
    print("2. neither male nor tall")

if is_male and is_tall:
    print("3. tall male")
elif is_male and not is_tall:
    print("3. short male")
elif not is_male and is_tall:
    print("3. not a male but tall")
else:
    print("3. you are not a male and are not tall")
