from simple_colors import *

print(red("Hello"))
print(green("Hello", 'bold'))
print(green("Hello", 'italic'))
print(green("Hello", ['bold', 'underlined']))
print(green("Hello", ['italic', 'underlined']))

list1 = ["We", " are", " learning."]
print(blue(''.join(list1)))
print(blue(''.join(list1), 'underlined'))
print(green(''.join(list1), 'reverse'))

list2 = ["We", "are", "learning", "!"]
print(red(list2[0], 'underlined'), yellow(list2[1], 'underlined'), blue(list2[2], 'underlined') + magenta(list2[3]))
x = list2[2].join(list2[3])


given_numbers = ["1", "2", "3", "4"]
my_numbers = given_numbers[1:4]
number = ''.join(my_numbers)
print("The book contains", red(f"{number}"), "pages.")
print("The book contains", red(f"{number}", 'underlined'), "pages.")
