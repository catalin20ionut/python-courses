from S13_oop import Student  # generează automat False

# an object is an instance of a class
student1 = Student("Jim", "Business", 3.6, False)
student2 = Student("Pam", "Art", 2.5, True)


print("1:", student1.name)
print("2:", student2.name)
print("3:", student1.on_honor_roll())
print("4:", student2.on_honor_roll())
student2.gpa = 4
print("5:", student2.on_honor_roll())
