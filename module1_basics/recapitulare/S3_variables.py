character_name = "Andy"
character_age = 33.3
is_male = True

print("My \"name is \n" + character_name)
print(f"My age is {character_age}")
print(character_name.upper())
print(character_name.lower().islower())
print(len(character_name))
print(character_name[0])
