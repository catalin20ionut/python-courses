import math
from math import *

print("1:", 3 + 4.3)
print("2:", 10 % 3)      # restul împărțirii
print("3:", pow(2, 5))
print("4:", max(3, 5))
print("5:", round(3.5))  # rotunjește în sus de la .5 inclusiv
print("6:", ceil(3.3))   # valoarea întreagă superioară
print("7:", floor(3.9))  # valoarea întreagă inferioară
print("8:", sqrt(36))
print("9:", 2**3)

area = math.pi * 2
print("A:", area)
print("B:", '{:.3f}'.format(area))  # formatare, cifre după virgulă
print(f'C: {area:.3f}')             # formatare, cifre după virgulă

num = 10000000
# formatare număr
print(f"{num:,}")   # 10,000,000
print(f'{num:_}')   # 10_000_000
# print(f"{num:.}")   # it doesn't work
num2 = 10_000_001
print(num2)
