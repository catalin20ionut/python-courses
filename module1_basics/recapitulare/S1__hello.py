print('Hello\'s World')
a = 5
print(a, 2+1, 10, 11, sep="\n", end="?")
print('\n', type(1))
print(type([1]))

# design, formatare
print('{:26} {}'.format('one', 'two'))
print("greeting   -->", "Good afternoon!")
print("Begrüßung  -->", "     Guten Tag!")
print("begroeting -->", "      Goededag!")
print('{:10} {:3} {} '.format("greeting", "-->", "Good afternoon!"))                     # print(f'{variable_name:10}')
print('{:10} {:8} {} '.format("Begrüßung", "-->", "Guten Tag!"))
print('{:10} {:9} {} '.format("begroeting", "-->", "Goededag!"))
print('{:>29}'.format("The end"))                     # to the right

for x in range(1, 11):
    print('{0:2d} {1:3d} {2:4d}'.format(x, x*x, x*x*x))

print()
for x in range(1, 11):
    print(f'{x:2} {x * x:3} {x*x*x:4}')

myString1 = "This is"
myString2 = "an"
myString3 = "example."
print(myString1 + " " + myString2 + " " + myString3)
print(f"{myString1} {myString2} {myString3}")

book_name = "The Adventures of Huckleberry Finn"
print(book_name[0:18])
print(book_name.upper())
print(book_name.lower())

print(book_name[::-1])  # or
reversedString = ""
index = len(book_name)
while index > 0:
    reversedString += book_name[index - 1]
    index = index - 1
print(reversedString)

print(f"The length is: {len(book_name)}")
