txt = "successes"
all_freq = {}

for i in txt:
    if i in all_freq:
        all_freq[i] += 1
    else:
        all_freq[i] = 1
print(all_freq)

# """ sau """
# for i in txt:
#     all_freq[i] = all_freq.get(i, 0) + 1
# print(str(all_freq))


max_value = max(all_freq.values())
print(max_value)
print(list(all_freq.keys())[list(all_freq.values()).index(max_value)])
print("The letter", list(all_freq.keys())[list(all_freq.values()).index(max_value)], f"repeats for {max_value} times.")
