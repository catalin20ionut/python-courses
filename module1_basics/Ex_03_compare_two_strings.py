str1 = "This is Exercise 1"
str2 = "This is Exercise 2"

if str1 < str2:
    print("\"" + str1 + "\"" + " is less than " + "\"" + str2 + "\".")
elif str1 == str2:
    print("\"" + str1 + "\"" + " is equal to " + "\"" + str2 + "\".")
else:
    print("\"" + str1 + "\"" + " is greater than " + "\"" + str2 + "\".")

if str1 != str2:
    print("\"" + str1 + "\"" + " is different from " + "\"" + str2 + "\".")
else:
    print("\"" + str1 + "\"" + " is equal to " + "\"" + str2 + "\".")
