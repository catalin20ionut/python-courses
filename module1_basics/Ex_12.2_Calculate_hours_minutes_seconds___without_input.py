"""
Write a program that will have a variable seconds as integer. The program should compute and display the number of
hours, number of minutes and number of seconds in that seconds. For example if the initial value is 4205 seconds. We
should see the following in console.
Hours : 1
Minutes : 10
Seconds : 5
"""
from math import floor

seconds = 4205
hours = floor(seconds / 3600)
minutes = floor((seconds - hours * 3600) / 60)
sec = seconds - hours * 3600 - minutes * 60
print(f"Hours   : {hours}")
print(f"Minutes : {floor(minutes)}")
print(f"Seconds : {sec}")

"""
public class Exercise_9___without_Scanner {
    public static void main(String[] args) {
        int seconds = 4205;

        int hours = seconds / 3600;
        int minutes = (seconds - hours * 3600) / 60;
        int sec = seconds - hours * 3600 - minutes * 60;
        System.out.println("Hours   : " + hours);
        System.out.println("Minutes : " + minutes);
        System.out.println("Seconds : " + sec);
    }
}
"""