"""
    8. Write a program that when completed will calculate the amount of paint needed to paint the walls and the ceiling
of a room. Your program should have variables for length, width and height of room. Assume that the room has doors and
windows that don't need painting. Also, the floor in the room is not painted. Use initial variables to enter the number
of doors and number of windows in the room and adjust the total square feet to be painted accordingly. Assume that each
door is 20 square feet and each window is 15 square feet. Suppose the paint covers 350 square feet per paint bucket.
Print in console how many paint buckets you need.                                                                   """
from math import ceil

length = 20
width = 10
height = 9.5
number_of_doors = 1
number_of_windows = 1
area_that_must_be_painted = length*height*2 + width*height*2 + width*length - number_of_doors*20 - number_of_windows*15
print(f"For this room we need {area_that_must_be_painted/350} paint buckets. "
      f"So we have to buy {ceil(area_that_must_be_painted/350)} paint buckets.")

""" Java =>
public class Exercise_8___without_Scanner_Math {
    public static void main(String[] args) {
        float length = 20f;
        float width = 10f;
        float height = 9.5f;
        int numberOfDoors = 1;
        int numberOfWindows = 1;

        float areaThatMustBePainted = length*height*2 + width*height*2
                + width*length - numberOfDoors*20 - numberOfWindows*15;

        System.out.println("For this room we need " + areaThatMustBePainted/350 + " paint buckets.");
    }
}
"""