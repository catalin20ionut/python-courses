import re
import csv

all_records = []
missing_emails = []
middle_name = []
with open("people.csv", 'r') as f:
    reader = csv.DictReader(f)
    for element in reader:
        all_records.append(element)
    print(len(all_records))

    for element in all_records:
        if element['email'] == "":
            missing_emails.append(element)
    print(len(missing_emails))

    for element in all_records:
        if element['middle_name'] != "":
            middle_name.append(element)
    print(len(middle_name))

import csv
from functools import reduce

with open("people.csv") as file:
    reader = csv.reader(file)
    records = []
    for row in reader:
        records.append(row)
    no_email = reduce(lambda a, b: a + 1, map(lambda email: email.count, filter(lambda email: len(email[5]) == 0, records)), 0)

    print(len(records)-1)
    print(records[1])
    print(no_email)