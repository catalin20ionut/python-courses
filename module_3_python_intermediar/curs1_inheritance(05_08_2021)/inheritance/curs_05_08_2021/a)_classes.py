"""
Diferența dintre metodă şi atribut:
Atributul --------- este o variabilă, reprezintă o valoare un tip de dată , poate fi un string (şir de caractere),
------------------ un dicționar, o listă un număr. Atributul este o variabilă care este prezentă pe self.  <<<<<<<
Metoda ----------- este o funcţie specială în sensul că primește self ca parametru. Dacă nu primește self înseamnă
------------------ că nu este metodă; este doar o funcţie pe clasa respectivă. <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
Moștenirea ------- se foloseşte ca să se extindă modelul de bază. <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
Polimorfism ------ se folosește aceeași funcție pe metode diferite """


print('a) fără funcția def __str__ .....................................................................')


class Person:
    def __init__(self, n, a):
        self.name = n
        self.age = a  # Pentru atributele name şi age le dăm valoare atunci când apelăm funcţia.


p1 = Person("Marian", 28)
p2 = Person("Andrei", 25)
print(p1)
print(p2)
print(p1, p2)
# Rezultat (Output): <__main__.Person object at 0x000001CE93FFC460>
# __main__ >>>>>>>>>>>>>>> din ce fișier face parte, din main de unde a rulat scriptul
# .Person >>>>>>>>>>>>>>>> este un obiect din clasa Person
# 0x000001CE93FFC460 >>>>> aflat la adresa 0x000001CE93FFC460 care este un numar hexadecimal


print('b) ...............................................................................................')


class Person:
    def __init__(self, n, a):
        self.name = n
        self.age = a

    def __str__(self):
        return 'Person'


p1 = Person("Marian", 28)
p2 = Person("Andrei", 25)
print(p1, p2)
# ### Person apare de doua ori o data pentru p1 si o data pentru p2.
# ### A printat Person pentru că funcția mea returnează Person de fiecare dată.
# ### Deci trebuie format str pentru a identifica in mod unic fiecare obiect, pe care vrem sa-l printăm.


print('c) forma finala ..................................................................................')


class Person:
    def __init__(self, n, a):
        self.name = n
        self.age = a

    def __str__(self):
        return f'{self.name} is {self.age} years old'

'''Metoda __str__  care primește self; se apelează atunci când se face un string din obiectul person.'''
p1 = Person("Marian", 28)
p2 = Person("Andrei", 25)
print(p1)
print(p2)
print(p1, "---", p2)

print('d) ................................................................................................')


# pw='secret' poate sa nu fie scris, poate aparea doar pw
#     def __init__(self, n, a):
#         self.name = n
#         self.age = a
#         self.password = 'secret' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# sau
#     def __init__(self, n, a, pw):
#             self.name = n
#             self.age = a
#             self.password = pw


class User:
    def __init__(self, n, a, pw='secret'):
        self.name = n
        self.age = a
        self.password = pw

    def __str__(self):
        return f'{self.name} is {self.age} years old'

    def login(self, pw):
        if pw is self.password:
            print("Login successful")
        else:
            print("Login failed")


print('d1) .......... varianta cand apare si None pentru print')
# va aparea si "None None" pentru ca cu "(p1.login("zzz") printam si rezultatul functiei care returneaza None
p1 = User("Marian", 28)
p2 = User("Andrei", 25)
print(p1.login("zzz"), p2.login('secret'))

print('d2) .......... varianta finala ........................')
p1 = User("Marian", 28)
p2 = User("Andrei", 25)
p1.login("zzz")
p2.login('secret')
print(p1, '---------', p2)

print('d3) .......... merge si "zzz" ........................')
p1 = User("Marian", 28, 'zzz')
p2 = User("Andrei", 25)
p1.login("zzz")
p2.login('secret')
print(p1, '---------', p2)

# Observatie: ....................................................................................................
print('*Observatie:\"Daca era scris')
print('    def __init__(self, n, a)')
print('        self.name = n')
print('        self.age = a')
print('        self.password = \'secret\'')
print('........... punctul d3) nu ar fi functionat. Ar fi aparut eroare de mai jos.\"')

# class User:
#     def __init__(self, n, a):
#         self.name = n
#         self.age = a
#         self.password = 'secret'
#
#     def __str__(self):
#         return f'{self.name} is {self.age} years old'
#
#     def login(self, pw):
#         if pw is self.password:
#             print("Login successful")
#         else:
#             print("Login failed")
#
# p1 = User("Marian", 28, 'zzz')
# p2 = User("Andrei", 25)
# p1.login("zzz")
# p2.login('secret')
# print(p1, '---------', p2)
# # Output:
# # Traceback (most recent call last):
# # File "C:\Users\home\PycharmProjects\pythonProject\...", line 16, in <module>
# #     p1 = User("Marian", 28, 'zzz')
# # TypeError: __init__() takes 3 positional arguments but 4 were given
#  #..............................................................................................................


print("e_) inheritance ---- super(). --- ..................................................................")
print("e1) inheritance --- fără atribute în plus ..........................................................")


class User:
    def __init__(self, n, a, pw='secret'):
        self.name = n
        self.age = a
        self.password = pw

    def __str__(self):
        return f'{self.name} is an employee and he is {self.age} years old.'

    def login(self, pw):
        if pw is self.password:
            print("Login successful")
        else:
            print("Login failed")


class Employee(User):
    pass

print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> e1 varianta a')
employee = Employee("Silviu", 10)
print(employee)
employee.login('zzz')

print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> e1 varianta b')
employee = Employee("Silviu", 10, employee.login('zzz'))
print(employee)


print("e2) inheritance --- super(). --- inheritance with atributes in plus")


class User:
    def __init__(self, n, a, pw='secret'):
        self.name = n
        self.age = a
        self.password = pw

    def __str__(self):
        return f'{self.name} is an employee and he is {self.age} years old.'

    def login(self, pw):
        if pw is self.password:
            print("Login successful")
        else:
            print("Login failed")


class Employee(User):
    def __init__(self, name, salary, age, password='secret'):
        super().__init__(name, age, password)
        self.salary = salary

    def __str__(self):
        return f'{self.name} is an employee, he is {self.age} years old and his salary is {self.salary} pound.'

print('----- e2 modul1')
employee = Employee("Silviu", 1000, 20, 'zzz')
employee.login('zzz')
print(employee)
print('---- e2 modul 2')
employee_1 = Employee("Silviu", 1000, 20)
employee_1.login('zzz')
print(employee_1)


print("e2 modul 3 ------------------------------------- mosternirea stringului")
print("Formula pentru moștenirea stringului este:")
print('def __str__(self):')
print('    user_str = super().__str__()')
print('    return user_str + " His salary is " + str(self. salary) + " pound." ')
print('Rezultatul pentru mostenirea stringului: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>')


class User:
    def __init__(self, n, a, pw='secret'):
        self.name = n
        self.age = a
        self.password = pw

    def __str__(self):
        return f'{self.name} is an employee, he is {self.age} years old.'

    def login(self, pw):
        if pw is self.password:
            print("Login successful")
        else:
            print("Login failed")


class Employee(User):
    def __init__(self, name, salary, age, password='secret'):
        super().__init__(name, age, password)
        self.salary = salary

    def __str__(self):
        user_str = super().__str__()
        return user_str + " His salary is " + str(self. salary) + " pound."

employee = Employee("Silviu", 1000, 20, 'zzz')
employee.login('zzz')
print(employee)
print('--------------------------------------------------------------------')
print('--------------------------------------------------------------------')
print('f) inheritance --- la clasa copil se adaugă o nouă metodă; metoda "finance" ')


class User:
    def __init__(self, n, a, pw='secret'):
        self.name = n
        self.age = a
        self.password = pw

    def __str__(self):
        return f'{self.name} is an employee, he is {self.age} years old.'

    def login(self, pw):
        if pw is self.password:
            print("Login successful")
        else:
            print("Login failed")


class Employee(User):
    def __init__(self, name, salary, age, hours=180, password='secret'):
        super().__init__(name, age, password)
        self.salary = salary
        self.hours = hours

    def __str__(self):
        user_str = super().__str__()
        return user_str + " His salary per hour is " + str(self. salary) + " pound."

    def finance(self):
        return self.salary * self.hours

employee = Employee("Silviu", 10, 20, password='zzz')
print(employee)
print(employee.finance())
print('He works 180 hours on a month. So his salary per month is', employee.finance(), 'pound.')
print('He works 180 hours on a month. So his salary per month is', str(employee.finance()), '.')
print('--------------------------------------------------------------------')
print('f1) completion ---------------------------------------')


class Student(User):
    def __init__(self, name, age, scholarship):
        super().__init__(name, age)
        self.scholarship = scholarship

    def finance(self):
        return self.scholarship

student = Student('Andrei', 14, 250)
print(student.finance())
print("Andrei is a good scholar. His scholarship is", student.finance(), "Euro.")
print('------------------------------------------------------------')
print('f2) is instance -------------------------------------------------')
print(isinstance(student, Student))
print(isinstance(student, User))
print(isinstance(student, Employee))
