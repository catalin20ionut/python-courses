
class User:
    def __init__(self, pw='secret'):
        self.password = pw

    def login(self, pw):
        if pw is self.password:
            print("Login successful")
        else:
            print("Login failed")


class Bio:
    def __init__(self, age, pf):
        self.age = age
        self.profession = pf

    def description(self):
        return f"He is {self.age} years old and he is a {self.profession}."


class Profile(User):
    def __init__(self, name, pw='secret'):
        super().__init__(pw)
        self.name = name

    def profile_name(self):
        return self.name


class UserProfile(Bio, Profile):
    def __init__(self, name, facebook_link, age, pf):
        Profile.__init__(self, name)
        Bio.__init__(self, age, pf)
        self.facebook_link = facebook_link

    def usr_pfl(self):
        usr_pfl = super().description()
        return usr_pfl + "\n" + f'His facebook address is {self.facebook_link} and he is {self.age} years old.'

x = UserProfile('Andrei', 'fbk_link', 25, 'driver')
print(x.usr_pfl())

print('                            ')
print('''▼▼▼▼▼▼  Das is wichtig ▼▼▼▼▼▼
class UserProfile(Bio, Profile:''')
print('    def __init__(self, name, facebook_link, age, pf):')
print('        Profile.__init__(self, name)')
print('        Bio.__init__(self, age, pf)' + '\n' + '        self.facebook_link = facebook_link')
