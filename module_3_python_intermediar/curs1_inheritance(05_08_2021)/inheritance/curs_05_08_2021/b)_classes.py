print('a) -------------------')


class Animal:
    def __init__(self, name):
        self.name = name

    def make_a_sound(self):
        return " "


class Dog(Animal):
    def __init__(self, name):
        super().__init__(name)

    def make_a_sound(self):
        return "Ham, ham!"


class Cat(Animal):

    def make_a_sound(self):
        return "Miao, miao"


caine = Dog('Rex')
print(caine)
cat = Cat('Kitty')
print(cat)
print(caine.make_a_sound())
print(cat.make_a_sound())

print('b) -------------------')


class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def make_a_sound(self):
        return " "


class Dog(Animal):
    def __init__(self, name, age):
        super().__init__(name, age)

    def make_a_sound(self):
        return "Ham, ham!"


class Cat(Animal):

    def make_a_sound(self):
        return "Miao, miao!"


caine = Dog('Rex', 10)
cat = Cat('Kitty', 5)
print(caine.make_a_sound())
print(cat.make_a_sound())
print(cat.age)
print(caine.make_a_sound(), "I am " + str(caine.age) + ' years old.')

