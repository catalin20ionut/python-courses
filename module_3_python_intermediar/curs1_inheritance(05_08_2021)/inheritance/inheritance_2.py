class Animal:
	def makeNoise(self):
		raise NotImplementedError
		# pass


class Cat(Animal):
	def makeNoise(self):
		print("Meoooowwwww")


class Dog(Animal):
	def makeNoise(self):
		print("Woooooof")

d = Dog()
d.makeNoise()

c = Cat()
c.makeNoise()

a = Animal()
a.makeNoise()
