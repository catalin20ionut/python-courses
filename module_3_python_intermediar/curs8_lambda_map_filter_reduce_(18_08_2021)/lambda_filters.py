# ######################################################################################################
# #                                             FILTER Function                                        #
# ######################################################################################################
"""                  Filter function is used to filter out iterable elements. It returns a generator."""
""" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Syntax: filter (function, iterable)"""

""" Example 1"""
even_numbers = filter(lambda num: num % 2 == 0, range(1, 25))
print("Example 1:", list(even_numbers))
print("or >>>>>> ", list(filter(lambda num: num % 2 == 0, range(1, 25))))

""" Example 2"""
list_a = ["1", "22", "333", "4444", "56555", "6 horses", "days", "ana", "hannah"]
four_char_string = list(filter(lambda ax: len(ax) < 5, list_a))
print("Example 2:", four_char_string)

"""Example 3: Using filter and lambda, make a list of the strings that are a palindrome from the original list. """
palindrome = list(filter(lambda x: x == x[::-1], list_a))
print("Palindromes:", palindrome)


def verifying_item_whether_are_palindromes(any_list):
    for myString in any_list:
        mid = (len(myString) - 1) // 2
        start = 0
        last = len(myString) - 1
        flag = 0

        while start < mid:
            if myString[start] == myString[last]:
                start += 1
                last -= 1
            else:
                flag = 1
                break

        if flag == 0:
            print(myString, bool(1))
        else:
            print(myString, bool(0))


verifying_item_whether_are_palindromes(list_a)
