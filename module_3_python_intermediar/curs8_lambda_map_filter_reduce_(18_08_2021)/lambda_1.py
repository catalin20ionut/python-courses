# ################### LAMBDA FUNCTION #######################

# ## How we use lambda function (anonymous function)
# ## instead of writing simple functions: we can replace them by introducing a lambda function
# ## Lambda's syntax can be described lambda << arguments(parameters) >> : <<operation>>
# ## Lambda returns the result of operation by default
# ## Example: if we want a function that only adds two numbers instead of writing a whole function:

def add_numbers(a1, b1):
    return a1 + b1


s = add_numbers(2, 3)
print("1:", s)

# # or we can put the function in a variable to use it
addition = add_numbers
print("2:", addition(2, 3))

# ## we could use lambda:

addition2 = lambda a2, b2: a2 + b2
a = addition2(2, 3)
print("example 1 lambda:", a)


# ## - we don't have to give a name
# ## - we don't have to make a "return"

# ##  Another example of simple function which has got a parameter "x"
# and if we # give make the parameter 3 or a greater value it'll return True


def is_true(x):
    if x > 2:
        return True


print("4:", is_true(1))  # It will return None, because it is the value 1 is not greater than 3.
# Unless we use "else" the function will return by default None, in case the parameter is less than 3.


# The same thing using lambda

y = lambda y: True if y > 2 else False
print("Example 2a lambda:", y(1))
print("Example 2b lambda:", y(4))
