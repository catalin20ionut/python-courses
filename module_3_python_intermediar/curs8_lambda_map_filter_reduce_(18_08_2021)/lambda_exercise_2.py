""" 1. Being given the digit_list = [1, 2, 5, 9], create a list of their squares using map() and lambda. """

digit_list = [1, 2, 5, 9]
square_numbers = list(map(lambda s: s**2, digit_list))
print("result point 1.:", square_numbers)

""" 2. Being given list fruits = ["apples", "oranges", "grapes"] create a dictionary where the string is the key
 and the value is string's length. Use map and lambda. """
fruit = ["apple", "orange", "grape"]
print("result point 2.:", dict(list(map(lambda x: (x, len(x)), fruit))))

"""3. Using map and lambda show the items of a list in a dictionary that are even and odd! (even = par, odd = impar)"""
l = [1, 2, 3, 23, 54, 68, 39]
print("result point 3.:", dict(list(map(lambda x: (x, "even" if x % 2 == 0 else "odd"), l))))

""" 4a. Show the items of a dictionary one under the other!"""
s_s = {1: 'odd', 2: 'even', 3: 'odd', 23: 'odd', 54: 'even', 68: 'even', 39: 'odd'}
print('4. Show the items of a dictionary one under the other! .... ')
for key, value in s_s.items():
    print(key, ':', value)

print("4b with the current list: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ")
lis = [1, 2, 3, 23, 54, 68, 39]
dikt = (dict(list(map(lambda el: (el, "even" if el % 2 == 0 else "odd"), lis))))
print(dikt)
for key, value in dikt.items():
    print(key, 'is', value)

""" 5. without lambda """
print("5. without lambda: ")
li = [1, 2, 3, 23, 54, 68, 39, 102, 106]


def odd_or_even(no):
    for el in li:
        if el % 2 == 0:
            print(f'the number {el} is', "even")
        else:
            print(f'the number {el} is', "odd")

odd_or_even(li)

print("6: In the following rows lambda will be used >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
print("6a:", list(map(lambda el: "even" if el % 2 == 0 else "odd", li)))
print("6b:", list(map(lambda el: f'the number {el} is '"even" if el % 2 == 0 else f'the number {el} is '"odd", li)))
print("7a:", dict(list(map(lambda el: (el, "even" if el % 2 == 0 else "odd"), li))))

dkt = (dict(list(map(lambda el: (el, "even" if el % 2 == 0 else "odd"), li))))
print("7b:", dkt)

print("7c:")
for key, value in dikt.items():
    print(key, 'is', value)
