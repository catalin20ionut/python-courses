"""
1.Being given a list of User objects, try sorting them using:
    • registered_on
    • number_of_logins
    • last_seen
2.To do that, use either list.sort() or sorted(). Both support a keyword parameter key=callable.
3.Result of callable will be compared using comparison operators to determine order in list.
4.Use lambda as callable
"""

import time
import datetime


class User:
    def __init__(self, name):
        self.name = name
        self.registered_on = datetime.datetime.now()
        self.number_of_logins = 0
        self.last_seen = self.registered_on

    def login(self):
        self.number_of_logins += 1  # When the login is made, the number put by default on init increases from 0 to 1.
        self.last_seen = datetime.datetime.now()               # The hour when the user was accessed is updated

    def __str__(self):
        return f'{self.name} logins({self.number_of_logins}) last seen {self.last_seen}'

    """The function __repr__ return the string of the each elements of the list"""
    def __repr__(self):
        return f'{self.name} logins({self.number_of_logins}) last seen {self.last_seen}'

u1 = User("Catalin")
time.sleep(1)
u2 = User("Andrew")
time.sleep(0.2)
u3 = User("Mathew")
user_list = [u2, u3, u1]
print(u1)
time.sleep(2)
u1.login()
print(u1)
u2.login()
u2.login()
print(sorted(user_list, key=lambda user: user.registered_on))
print('number of logins:', sorted(user_list, key=lambda user: user.number_of_logins))
print(sorted(user_list, key=lambda user: user.last_seen, reverse=True))  # reversed sorted
print("shows the items of a list one under the other:""\n",
      "\n".join(map(str, sorted(user_list, key=lambda user: user.last_seen, reverse=True))))
