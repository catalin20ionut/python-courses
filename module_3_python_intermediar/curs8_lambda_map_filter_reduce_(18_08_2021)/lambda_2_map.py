# ##############################################################################################################
#                                                  The Function Map                                            #
# ##############################################################################################################
# - is a built-in function in Python
# - apply a certain function
# - it has two parameters. The first parameter has to be a function.
#                          The second one is an iterable, for example a list.
# It works applying a "for" in background: It applies for each element in iterable the certain function
# It returns the new list with the modified elements according to the function


fruit = ["apple", "banana", "strawberry"]
capitalized_list = list(map(lambda s: s.capitalize(), fruit))

print(capitalized_list)
print(fruit)


# the same thing not using lambda, but using :
def return_capitalized(s):
    return s.capitalize()

capitalized_fruit2 = list(map(return_capitalized, fruit))
print("without lambda, but with map:", capitalized_fruit2)

# Unless we use map, we should make a << for >>

capitalized_fruit_var3 = []
for el in fruit:
    capitalized_fruit_var3.append(el.capitalize())

print('without lambda and map      :', capitalized_fruit_var3)

#         What map does is that it applies the function to each element in the list << fruit >> in order to avoid
#   the writing of a << for >> loop. This << for >> loop is made automatic in the background by the function map.
#                     Map doesn't return a list, it returns a map object, that's why we must convert it to a list.
