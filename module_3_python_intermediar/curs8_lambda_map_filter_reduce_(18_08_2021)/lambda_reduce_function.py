# ######################################################################################################
# #                                             REDUCE Function                                        #
# ######################################################################################################

""" Reduce function is not built in. We need to import functools in order to use it.  It has always
two arguments. Syntax: reduce(function, iterable, initial==None)
Exercise 1: Create a list of bool values. Use reduce function and lambda to write a code that
takes the bool list and using << and >> compare them two by two and print out a final answer. """

import functools

list_bool = [True, False, True, True]
list_bool1 = [True, True, True, True]
list_bool2 = [False, False, True, True]
final_result = functools.reduce(lambda x, y: x and y, list_bool)
final_result1 = functools.reduce(lambda x, y: x and y, list_bool1)
final_result2 = functools.reduce(lambda x, y: x and y, list_bool2)
print(final_result)
print(final_result1)
print(final_result2)

""" See 18.08.2021 hour 2 min 10 sec 22 """
""" See 18.08.2021 hour 2 min 11 sec 55 """
print(functools.reduce(lambda x, y: x + y, [1, 4, 38]))
