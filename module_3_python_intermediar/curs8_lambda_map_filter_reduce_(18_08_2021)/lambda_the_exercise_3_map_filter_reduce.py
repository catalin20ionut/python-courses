"""
1. Given car list from example-04.py, find out the following using reduce-filter or map-filter-reduce:
2. What is the total cost of all Alfa Romeos on the list?
3. What is the count of all Fords on the list? Hint: use initial = 0"""

import functools
from functools import reduce


class Car:
    def __init__(self, make, model, price):
        self.make = make
        self.model = model
        self.price = price


cars = [
    Car("Ford", "Anglia", 300.0),
    Car("Ford", "Cortina", 700.0),
    Car("Alfa Romeo", "Stradale 33", 190.0),
    Car("Alfa Romeo", "Giulia", 500.0),
    Car("Citroën", "2CV", 75.0),
    Car("Citroën", "Dyane", 105.0),
       ]


""" 1. Let's find the price of the cheapest Citroën in the list """
c = reduce(min, map(lambda car: car.price, filter(lambda car: car.make == "Citroën", cars)))
print(f"1. The cheapest Citroën costs: {c}")

""" 2. Let's find the price of all Alfa Romeo cars in the list """
cost_alfa = functools.reduce(lambda a, b: a + b, (map(lambda car: car.price,
                                                      filter(lambda car: car.make == "Alfa Romeo", cars))))
print("2. All the Alfa Romeo cars cost:", cost_alfa, "Euro.")

""" 3. How many Fords are in the list? """
f = functools.reduce(lambda x, y: x+1, map(lambda car: car.make.count, filter(lambda car: car.make == "Ford", cars)), 0)
print(f"3a. The total number of Ford cars in the list is: {f}")
print(f"3b. In this list {f}", "Ford is." if f == 1 else "Fords are.")
""" Comment the 19th row and you will have << 3b. In this list 1 Fords is. >>"""
