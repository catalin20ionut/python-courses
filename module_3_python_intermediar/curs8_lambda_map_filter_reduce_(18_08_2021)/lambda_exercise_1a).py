"""
1.Being given a list of User objects, try sorting them using:
    • registered_on
    • number_of_logins
    • last_seen
2.To do that, use either list.sort() or sorted(). Both support a keyword parameter key=callable.
3.Result of callable will be compared using comparison operators to determine order in list.
4.Use lambda as callable
"""

import time
import datetime


class User:
    def __init__(self, name):
        self.name = name
        self.registered_on = datetime.datetime.now()
        self.number_of_logins = 0
        self.last_seen = self.registered_on

    def login(self):
        self.number_of_logins += 1  # When the login is made, the number put by default on init increases from 0 to 1.
        self.last_seen = datetime.datetime.now()               # The hour when the user was accessed is updated

    def __str__(self):
        return f'{self.name} logins({self.number_of_logins}) last seen {self.last_seen}'

u = User("Catalin")
print(u)
time.sleep(3)  # it make the result wait for 3 seconds
u.login()
print(u)
