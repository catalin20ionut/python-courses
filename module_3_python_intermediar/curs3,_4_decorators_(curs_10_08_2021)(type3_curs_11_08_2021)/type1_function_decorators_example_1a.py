print("The first step >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ")


def hello(name):
    return f"Hello {name}"

print(hello("Silviu"))
print("The second step >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ")


def decorator(f):
    def inner(n):
        print("Before call")
        result = f(n)
        print("After call")
        return result
    return inner


@decorator
def hello(name):
    return f"Hello {name}"

print(hello("Silviu"))
print("The 3rd step without decorator >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ")


def decorator(f):
    def inner(n):
        print("Before call")
        result = f(n)
        print("After call")
        return result
    return inner


def hello(name):
    return f"Hello {name}"

decorated_function = decorator(hello)
print(decorated_function("Silviu"))

print("or >>>>>>")

hello = decorator(hello)
print(hello("Silviu"))
# the rows 40, 41 and 45, 46 are the first variant and the second variant in lieu of decorators
print("The fourth step will be written in decorators_example_1b >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ")
