import datetime


def run_only_on_certain_second(seconds):
    def wrapper(func):
        def wrap():
            while True:
                second1 = None
                now = datetime.datetime.now()
                if second1 != now.second:
                    print(now.second)
                    second1 = now.second
                if now.second == seconds:
                    func()
                    break

        return wrap
    return wrapper


@run_only_on_certain_second(25)
def activate_program():
    print("program activated")


activate_program()
