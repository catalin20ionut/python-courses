import time


def timer(decorated_function):
    def wrapper(*args, **kwargs):
        t1 = time.time()
        decorated_function(*args, **kwargs)
        print(f"It took {time.time() - t1} to run")
    return wrapper


@timer
def power(x, y):
    for _ in range(10**6):
        x ** y

power(10, 5)  # It took x seconds to run
