class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def login(self, password, request):
        if password == self.password:
            request.authenticate(self)

    def __str__(self):
        return f"{self.username}"


class Request:
    def __init__(self):
        self.user = None

    def is_authenticated(self):
        if self.user:
            return True
        return False

    def authenticate(self, user):
        self.user = user


def login_required(func):
    def inner(request):
        if request.is_authenticated():
            return func(request)
        else:
            return "Login is required!"
    return inner


@login_required
def get_profile(request):
    return request.user


user = User("silviu", "pass")
request1 = Request()
request2 = Request()
user.login("pass", request1)

print(get_profile(request1))  # silviu
print(get_profile(request2))  # Login is required!
# Maybe It was required to write a function 27 -33
