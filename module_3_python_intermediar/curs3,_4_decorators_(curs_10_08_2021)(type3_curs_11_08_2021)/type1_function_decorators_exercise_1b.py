# write a decorator which shortens a long string
import random
import functools


def shortens_a_string(f):
    @functools.wraps(f)
    def inner(length, characters):
        result = f(length, characters)[:4]
        return result
    return inner


@shortens_a_string
def random_string(length, characters):
    result = ""
    for _ in range(length):
        result += random.choice(characters)
    return result

print(random_string(100, ["a", "A", "b", "c", "d", "E"]))
