# write a decorator which returns any string into lowercase and shortens a long string
import random
import functools


def lowercase(f):
    @functools.wraps(f)
    def inner(length, characters):
        result = f(length, characters).lower()
        return result
    return inner


def shortens_a_string(f):
    @functools.wraps(f)
    def inner(length, characters):
        result = f(length, characters)[:6]
        return result
    return inner


@lowercase
@shortens_a_string
def random_string(length, characters):
    result = ""
    for _ in range(length):
        result += random.choice(characters)
    return result

print(random_string(100, ["a", "A", "b", "c", "d", "E"]))
