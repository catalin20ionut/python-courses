import datetime

#  # I don't know what 4-12 is used for
# def short(param):
#     def wrapper_short(func_short):
#         def wrap(a, b):
#             print('short before')
#             result = func_short(a, b)[:param]
#             print('short after')
#             return result
#         return wrap
#     return wrapper_short


def run_only_on_second_30(f):
    def wrapper(*arg, **kwargs):
        while True:
            now = datetime.datetime.now()
            if now.second == 30:
                return f()
    return wrapper


@run_only_on_second_30
def activate_program():
    print("Program activated")

activate_program()
