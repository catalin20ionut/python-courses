import time


class Time:
    def __init__(self, f):
        self.f = f

    def __call__(self, *args, **kwargs):
        t1 = time.time()
        self.f(*args, **kwargs)
        print(f"It took {time.time() - t1} to run")


@Time
def power(x, y):
    for _ in range(10**6):
        x ** y

power(10, 5)  # It took x seconds to run
