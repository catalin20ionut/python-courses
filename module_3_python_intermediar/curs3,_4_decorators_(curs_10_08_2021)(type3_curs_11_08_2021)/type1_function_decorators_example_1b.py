import functools


def decorator(f):
    @functools.wraps(f)
    def inner(n):
        print("Before call")
        result = f(n)
        print("After call")
        return result
    return inner


@decorator
def hello(name):
    """Example of docstring. This function should print Hello and the name
        :param name
    """
    return f"Hello {name}"

print(hello("Silviu"))
print(hello.__name__)
print(hello.__doc__)

print("This example is without the decorator \"@functools.wraps(f)\" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")


def decorator(f):
    # @functools.wraps(f)
    def inner(n):
        print("Before call")
        result = f(n)
        print("After call")
        return result
    return inner


@decorator
def hello(name):
    """Example of docstring. This function should print Hello and the name
        :param name
    """
    return f"Hello {name}"

print(hello("Silviu"))
print(hello.__name__)
print(hello.__doc__)
