# another way to write for shortening a string and to make it lowercase
import random
import functools


def lowercase_and_shortens(f):
    @functools.wraps(f)
    def inner(length, characters):
        result = f(length, characters).lower()[:10]
        return result
    return inner


@lowercase_and_shortens
def random_string(length, characters):
    result = ""
    for _ in range(length):
        result += random.choice(characters)
    return result

print(random_string(100, ["a", "A", "b", "c", "d", "E", "UPPER"]))
