# this is the exercise 1 from function decorators, but it is written with classes
# See 10.08.2021 hour2 minute 31! ................ >>>>>>>>>>>>>>>>>>>>>>>>>>>>> "__call__" is very important!
import random


class Short:
    def __init__(self, f):
        print("init short")
        self.f = f
        self.__name__ = f.__name__
        self.__doc__ = f.__doc__

    def __call__(self, l, c):
        print("short call")
        return self.f(l, c)[:3]


class Lowercase:
    def __init__(self, f):
        print("init lowercase")
        self.f = f
        self.__name__ = f.__name__
        self.__doc__ = f.__doc__

    def __call__(self, l, c):
        print("lowercase call")
        return self.f(l, c).lower()


@Short
@Lowercase
def random_string(length, characters):
    """DOCSTRING"""
    result = ""
    for _ in range(length):
        result += random.choice(characters)
    return result


print(random_string(100, ["a", "A", "b"]))
print("The second print takes the name of the function in 31st row:", random_string.__name__)
print("See 32nd row!:", random_string.__doc__)
