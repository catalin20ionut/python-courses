# write a decorator which returns any string into lowercase
import random
import functools


def lowercase(f):
    @functools.wraps(f)
    def inner(length, characters):
        result = f(length, characters).lower()
        return result
    return inner


@lowercase
def random_string(length, characters):
    result = ""
    for _ in range(length):
        result += random.choice(characters)
    return result

print(random_string(15, ["a", "A", "b", "C", "D", "E"]))
