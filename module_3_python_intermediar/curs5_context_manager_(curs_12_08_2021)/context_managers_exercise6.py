#  # for requirements see hour 2 min 22

PASSWORD = "secret"


class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def reset_password(self):
        self.password = ""


class PasswordCheck(User):
    def __enter__(self):
        self.user = User(self.username, self.password)
        return self.user

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.password == PASSWORD:
            print(self.username)
        else:
            self.user.reset_password()
            print(self.user.password)


with PasswordCheck("Catalin", "secret") as user:
    print(user.password)

# In enter se creeaza un user si se intoarce
# Daca parola din PasswordCheck e gresita (diferita de PASSWORD)
# In exit parola este resetata si este printat usernameul
# Daca e parola corecta, atunci printam parola in exit