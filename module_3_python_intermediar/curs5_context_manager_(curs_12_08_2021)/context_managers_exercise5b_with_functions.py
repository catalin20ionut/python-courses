import contextlib
import datetime


@contextlib.contextmanager
def run_on_second(second):
    while True:
        timp = datetime.datetime.now()
        print(timp)
        if timp.second == second:
            print("It is the high time!")
            break
    yield

with run_on_second(30):
    print("The program was fulfilled!")
