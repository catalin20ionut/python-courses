class Lowercase:
    def __init__(self, phrase):
        self.phrase = phrase

    def __enter__(self):
        return self.phrase.lower()

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class Short:
    def __init__(self, phrase):
        self.phrase = phrase

    def __enter__(self):
        return self.phrase[:10]

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


with Short("AZi e JOI si MAINE E VINERI!") as phrase1:
    with Lowercase(phrase1) as phrase2:
        print(phrase2)
