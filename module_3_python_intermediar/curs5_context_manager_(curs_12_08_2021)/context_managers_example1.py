class FileOpen:
    def __init__(self, path, mode):
        self.path = path
        self.mode = mode
        self.file = None

    def __enter__(self):
        print("In enter")
        self.file = open(self.path, self.mode)
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('in exit', exc_type, exc_type, exc_tb)
        self.file.close()


with open('context_managers_example1.py', 'r') as f:
    print(len(f.readlines()))

with FileOpen('context_managers_example1.py', 'r') as f:
    print(len(f.readlines()))
