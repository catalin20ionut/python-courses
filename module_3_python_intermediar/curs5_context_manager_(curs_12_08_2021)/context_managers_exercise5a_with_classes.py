# it is from curs3, 4_decorators_(curs_10_08_2021)(type3_curs_11_08_2021) /
#                                                                  type3_parameter_decorators_exercise_3

import datetime


class RunOnSecond():
    def __init__(self, t):
        self.t = t

    def __enter__(self):
        while True:
            t1 = datetime.datetime.now()
            if t1.second == self.t:
                print("Program complete!")
                return

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

with RunOnSecond(30):
    print("Program is ok!")
