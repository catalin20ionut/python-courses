class Lowercase:
    def __init__(self, phrase):
        self.phrase = phrase

    def __enter__(self):
        return self.phrase.lower()

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(exc_val)
        print(dir(exc_tb))
        print(exc_type)


with Lowercase("Azi o zi buna sa MUNCESTI") as phrase:
    print(phrase)
    print(phrase[10])
    print(phrase[:100])
    print(phrase[100])
