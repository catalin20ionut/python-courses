# hour 2 min 43

class Database:
    connections = 0
    max_connections = 2

    def __init__(self):
        self.users = []

    def max_connections_reached(self):
        if self.max_connections == self.connections:
            print("Maximum connection was reached!!!")
            return True
        return False

    def save_user(self, user):
        if self.max_connections_reached():
            return
        self.connections += 1
        self.users.append(user)

    def get_users(self):
        if self.max_connections_reached():
            return
        self.connections += 1
        return self.users

    def disconnect(self):
        self.connections -= 1

database = Database()
# print("Part 1 ....................................................................................................")
# database.save_user("Silviu")
# print(database.get_users())
# database.save_user("Andrei")
# database.get_users()
print("Part two .....................................................................................................")
# Create a context manager with the name DatabaseInterface that makes sure it makes disconnect in the end using
# the "with" statement. The rows 53-60 were written before the requirement. I had to write the class rows 42-50


class DatabaseInterface:
    def __init__(self, database):
        self.database = database

    def __enter__(self):
        return self.database

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.database.disconnect()


with DatabaseInterface(database) as db:
    db.save_user("silviu")
with DatabaseInterface(database) as db:
    print(db.get_users())
with DatabaseInterface(database) as db:
    db.save_user("andrei")
with DatabaseInterface(database) as db:
    print(db.get_users())
print("Part 2a .......................................")
with DatabaseInterface(database) as db:
    database.save_user("Silviu2")
    database.save_user("Silviu3")
print(database.get_users())
