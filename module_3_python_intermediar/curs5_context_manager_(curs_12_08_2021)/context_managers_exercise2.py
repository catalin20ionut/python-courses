class Short:
    def __init__(self, phrase):
        self.phrase = phrase

    def __enter__(self):
        return self.phrase[:10]

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


with Short("Azi e JOI si MAINE E VINERI!") as phrase:
    print(phrase)
