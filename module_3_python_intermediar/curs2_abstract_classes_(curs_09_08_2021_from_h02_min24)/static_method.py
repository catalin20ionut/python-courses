import abc
import math


class Figure(abc.ABC):
    @abc.abstractmethod
    def circuit(self):
        pass

    @abc.abstractmethod
    def area(self):
        pass


class Rectangle(Figure):
    def __init__(self, lat, long):
        self.lat = lat
        self.long = long

    def circuit(self):
        return 2 * (self.lat + self.long)

    def area(self):
        return self.lat * self.long


class Circle(Figure):
    def __init__(self, r):
        self.r = r

    def circuit(self):
        return 2 * math.pi * self.r

    def area(self):
        return 2 * math.pi * self.r ** 2

    @staticmethod
    def get_pi():
        return math.pi

    @classmethod
    def generate_object(cls, r):
        print("print in generate_object:", cls)
        return cls(r)


r = Rectangle(1, 2)
print(r.area())

c = Circle(2)
print(c.area())
print("static method:", c.generate_object(2))
