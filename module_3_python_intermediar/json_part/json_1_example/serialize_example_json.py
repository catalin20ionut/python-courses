import json
with open("my_data.json", "w") as f:
    data = [{"a": 1, "b": 2}, {"x": True, "y": False}]  # uitate my _data.json
    json.dump(data, f, indent=2)  # uitate inca o data : pune in loc de 0 4


# data = [{"a":1, "b": 2}, {"x": True, "y": False}]
# with open("my_data.json", "w") as f:
#     json.dumps(data, f, indent=2)


x = json.dumps(data)
print(x)

with open("my_data.json") as f:
    print(json.load(f))

print(json.loads('[{"a": true}]'))
