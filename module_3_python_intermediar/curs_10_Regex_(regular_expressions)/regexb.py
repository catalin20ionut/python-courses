import re
# 1. Write regular expression which finds matches the first three word characters of a line Test it with:
# I. A Scandal in Bohemia
# II. The Red-Headed League
# III. A Case of Identity
# IV. The Boscombe Valley Mystery V. The Five Orange Pips

text = """
1. Write regular expression which finds matches the first three word characters of a line Test it with:
I. A Scandal in Bohemia
II. The Red-Headed League
III. A Case of Identity
IV. The Boscombe Valley Mystery 
V. The Five Orange Pips
"""

text = """
I. A Scandal in Bohemia
II. The Red-Headed League
III. A Case of Identity
IV. The Boscombe Valley Mystery
V. The Five Orange Pips
"""
text_lines = text.split('\n')
#print(text_lines)
# pattern = re.compile(r'\w{3}')
# print(pattern) #o optimizare
for index, line in enumerate(text_lines):
    found = re.search(r'\w{3}', line)
    if found:
        print(found.group())
    else:
        print(f'on line {index + 1} pattern not found')


