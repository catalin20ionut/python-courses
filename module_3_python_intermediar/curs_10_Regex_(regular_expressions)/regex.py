"""
https://regex101.com/

\. orice caracter mai putin newline
\d sau [0-9] - cifre de la 1 la 9
\s - space a\sb means "a b"
\t means tab
\n means newline
\w [A-Za-z0-9_]
[A-z] means [A-Za-z]
{4} means word having four characters
{2,5} lungimea cuvântului sa fie de 2, 3, 4, 5 litere
[OA]la meas Ola or Ala
[a-z]al means aal, bal, cal, dal ... zal
^ caret - semn de omisiune
Astazi este luni si eu Silviu predau Python.  Pentru a selecta Silviu si Python scriem
[^(Astazi)][A-Z][a-z]{5} - omitere Astazi cu litera mare litera mica de lungime 5
+ means unu sa mai multe
* zero or more repetition
$ means no more characters or digits after at the end :   if I put $ in ^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$ means
#### that after e, 8, d a can not put anything else. Unless I use $ I will have "92389f16f0de" in "92389f16f0des"
#### 377b1a2e-79c1-4c32-9ef2-92389f16f0de
#### 3e233770-9f67-4999-9944-563ce50f1678
#### 626ce1cd-4441-4957-bbae-5582096cf62d
? zero or 1 repetition
catalin1234ionut@gmail.com
(r'\w{3}', line) means the form from curs_10_Regex_(regular_expressions) a word of 3 characters and from where I take
the characters """

# # Solution 1
import re
print(re.search(r"[^\d]\w+", "my var"))
match = re.findall(r"[^\d]\w+", "my var io9")

if re.search(r"[^\d]\w+", "my_9"):
    print("string was found")
else:
    print("no match found")
print(match)


# # Solution 2 from slack
# import re
#
# match = re.search(r"[^\d]\w+", "my_var io9")
# print(match.group())
#
# match = re.findall(r"[^\d]\w+", "my_var io9")
# print(match)

# # Solution 3
# match = re.finditer(r"[^\d]\w+", "my_var io9")
# print(match)
# for element in match:
#     print(element.group())
#
# if re.search(r"[^\d]\w+", "my_9"):
#     print('string was found')
# else:
#     print('no match found')
