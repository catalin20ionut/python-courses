import re

lista = """
bjandourek0@stanford.edu
mtimothy1@deviantart.com
mdodding2@de-decms.com
kwessing3@linkedin.com
cstallion4@printfriendly.com
car@de.com
"""
text_lines = lista.split('\n')
result = {"Users": [], "Domains": []}


pattern = re.compile(r"([A-Za-z0-9._%+-]+)@([A-Za-z0-9.-]+\.[A-Z|a-z]{2,3})$")
for l1 in text_lines:
    match = re.search(pattern, l1)
    if match:
        result["Users"].append(match.groups())
        result["Domains"].append(match.groups())
        print(match.groups())


########### varianta din zoom
# import re
#
# emails = """
# bjandourek@stanford.edu
# mtimothy@deviantart.com
# mdodding2@de-decms.com
# car@de.de.com.ro"""
#
# emails = emails.split("\n")
# pattern = re.compile(r"(^[a-z0-9]+)@([a-z-]+\.[a-z]{2,3})$")
# data = {"users": [], "domains": []}
#
# for email in emails:
#     match = re.search(pattern, email)
#     if match:
#         x = match.groups()[0]
#         y = match.groups()[1]
#         data["users"].append(x)
#         data["domains"].append(y)
# print(data)