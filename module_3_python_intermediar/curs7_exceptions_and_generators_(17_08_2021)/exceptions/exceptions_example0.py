l = [1, 2, 10]
a = 1

for nr in l:
    try:
        print(nr / a)
    except ZeroDivisionError:
        print("Zero division is not accepted")
        a += 1
        print(nr / a)
    a -= 1
