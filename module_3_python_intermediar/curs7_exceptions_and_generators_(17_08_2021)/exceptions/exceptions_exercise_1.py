secret_password = "secret"


class PasswordTooShort(Exception):
    def __init__(self):
        m = "Password is too short"
        super().__init__(m)


class PasswordTooLong(Exception):
    def __init__(self):
        m = "Password is too long"
        super().__init__(m)


class InvalidPassword(Exception):
    def __init__(self):
        m = "Invalid password"
        super().__init__(m)


def validate_password():
    input_password = input("Please enter your password: ")
    if len(input_password) > 30:
        raise PasswordTooLong
    if len(input_password) < 3:
        raise PasswordTooShort
    if input_password != secret_password:
        raise InvalidPassword


try:
    validate_password()
except PasswordTooLong as e:
    print(e)
except PasswordTooShort as e:
    print(e)
except InvalidPassword as e:
    print(e)
