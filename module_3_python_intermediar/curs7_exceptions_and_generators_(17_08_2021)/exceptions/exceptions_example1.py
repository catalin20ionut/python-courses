# Custom exception by inheritance 1


class MyException(TypeError):
    pass

raise MyException("Something wrong happened")
