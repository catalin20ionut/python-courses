class NotFoundError(Exception):
    def __init__(self):
        m = "Resource was not found"
        super().__init__(m)

raise NotFoundError()
