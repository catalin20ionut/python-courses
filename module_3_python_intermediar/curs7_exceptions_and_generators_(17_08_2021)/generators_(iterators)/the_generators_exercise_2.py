""" Python intermediate page 71. THe requirements will be found at the 72nd page. """


def fibonacci():
    a = 0
    b = 1
    while True:
        yield a
        a, b = b, a + b


def elements(gen, max_elem):
    for iteration_number, element_from_gen in enumerate(gen()):
        if iteration_number < max_elem:
            yield element_from_gen
        else:
            break

for i in elements(fibonacci, 10):
    print(i)
