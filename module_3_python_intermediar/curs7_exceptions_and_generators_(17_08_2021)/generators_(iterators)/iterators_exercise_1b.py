class MyRange:
    def __init__(self, start, stop, step):
        self.start = start
        self.stop = stop
        self.step = step

    def __iter__(self):
        self.i = self.start
        return self

    def __next__(self):
        if self.i >= self.stop:
            raise StopIteration
        else:
            i = self.i
            self.i += self.step
        return i


for i in MyRange(-1, 10, 3):
    print(i)
