class MyRange:
    def __init__(self, start, stop, step):
        self.start = start
        self.stop = stop
        self.step = step

    def __iter__(self):
        self.i = self.start - self.step
        return self

    def __next__(self):
        self.i += self.step
        if self.i >= self.stop:
            raise StopIteration
        return self.i


for i in MyRange(-1, 100, 3):
    print(i)
