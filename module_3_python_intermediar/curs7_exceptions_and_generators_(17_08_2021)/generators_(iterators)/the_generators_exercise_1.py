print("a) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")


def my_gen():
    yield 1
    yield 2
    yield "aaa"
    yield 2.0

for i in my_gen():
    print(i)

print("b) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")


def my_range(start, stop, step=1):
    i = start - step
    while i <= stop - step:
        i += step
        yield i

for i in my_range(1, 10, 2):
    print(i)
