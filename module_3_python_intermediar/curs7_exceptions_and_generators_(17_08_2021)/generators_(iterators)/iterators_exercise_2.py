class FilterOddIterator:
    def __init__(self, l):
        self.l = l

    def __iter__(self):
        self.i = -1
        return self

    def __next__(self):
        self.i += 1
        if self.i == len(self.l):
            raise StopIteration
        if self.l[self.i] % 2 == 0:
            return self.l[self.i]
        # return ""  # if a use return"" I will have "0 empty rows 6 empty rows 10"

for i in FilterOddIterator([0, 1, 5, 6, 7, 10]):
    print(i)
