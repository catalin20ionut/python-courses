"""
Given file exercise-01.py
    1. Convert all calls to `print()` to proper logging
    2. configure logging to log simultaneously to two files and console, using the following formats and levels:
        1. File A: `timestamp - level - message` level DEBUG
        2. File B: `filename - funcname - line number` level INFO
        3. Console: `asctime [level]: message` level WARN
"""
import logging
import random
import functools

logger = logging.getLogger("cst")
logger.setLevel(logging.DEBUG)

handler1 = logging.FileHandler("file_A.log", "w+")
handler1.setLevel(logging.DEBUG)
handler1.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(message)s "))
logger.addHandler(handler1)

handler2 = logging.FileHandler("file_B.log", "w+")
handler2.setLevel(logging.INFO)
handler2.setFormatter(logging.Formatter("%(filename)s - %(funcName)s - %(lineno)d"))
logger.addHandler(handler2)

handler3 = logging.StreamHandler()
handler3.setLevel(logging.WARNING)
handler3.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s]: %(message)s"))
logger.addHandler(handler3)


def decorate(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        logger.info("INFO: calling a function")
        return func(*args, *kwargs)

    return wrapper


@decorate
def complex_calculation(a, b):
    logger.debug("DEBUG: performing complex calculation")
    result = a + b
    logger.debug(f"DEBUG: result={result}")
    return result


if __name__ == "__main__":
    for _ in range(10):
        result = complex_calculation(random.randint(0, 15), random.randint(0, 15))
        if result <= 10:
            logger.warning("WARN: a warning")
        elif result <= 20:
            logger.error("ERROR: an error")
        elif result <= 30:
            logger.critical("CRITICAL: a critical error")
