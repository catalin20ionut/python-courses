import random

EXCEPTIONS = [SyntaxError, TypeError, ZeroDivisionError, IndexError]

d = {SyntaxError.__name__: 0,
     TypeError.__name__: 0,
     ZeroDivisionError.__name__: 0,
     IndexError.__name__: 0}


def random_exception():
    raise random.choice(EXCEPTIONS)

for n in range(10):
    try:
        random_exception()
    except SyntaxError:
        d[SyntaxError.__name__] += 1
    except TypeError:
        d[TypeError.__name__] += 1
    except ZeroDivisionError:
        d[ZeroDivisionError.__name__] += 1
    except IndexError:
        d[IndexError.__name__] += 1

print(d)
