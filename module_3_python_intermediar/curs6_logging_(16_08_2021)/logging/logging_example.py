import logging

logger = logging.getLogger("custom")
logger.setLevel(logging.DEBUG)

handler1 = logging.StreamHandler()
handler1.setLevel(logging.DEBUG)
handler1.setFormatter(logging.Formatter("%(levelname)s  : %(lineno)d: %(message)s"))

logger.addHandler(handler1)

handler2 = logging.FileHandler("logging_message.log", 'w+')
handler2.setLevel(logging.WARNING)
handler2.setFormatter(logging.Formatter("%(levelname)s:%(name)s: %(message)s"))

logger.addHandler(handler2)


logger.debug("This a debug message. Poate fi orice")
logger.info("Another info message")
logger.warning("Yes, a warning")
logger.error("Error, not good")
logger.critical("Critical, we should do smth")
