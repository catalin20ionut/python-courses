class EshopCart:
    def __init__(self, buyer):
        self.buyer = buyer
        self.products = []
        self.total = 0.0

    def add_product(self, name, price):
        self.products.append(name)
        self.total += price

    def __len__(self):
        return len(self.products)

shopping1 = EshopCart("Silviu")
shopping1.add_product("eggs", 10)
print(len(shopping1))
