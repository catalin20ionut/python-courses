class EshopCart:
    def __init__(self, buyer):
        self.buyer = buyer
        self.products = []
        self.total = 0.0

    def add_product(self, name, price):
        self.products.append(name)
        self.total += price

    def __len__(self):
        return len(self.products)

    def __add__(self, other):
        for product in other.products:
            self.add_product(product, 0)
        self.total += other.total
        return self

    def __str__(self):
        return f"{self.buyer}'s cart with {len(self.products)}"


shopping1 = EshopCart("Silviu")
shopping1.add_product("eggs", 10)
print(len(shopping1))

shopping2 = EshopCart("maria")
shopping2.add_product("pastarnac", 1)

shopping3 = shopping1 + shopping2
# or
# shopping1 += shopping2
print(len(shopping3))
print(shopping3.total)
