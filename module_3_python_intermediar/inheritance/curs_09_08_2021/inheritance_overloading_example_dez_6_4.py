import copy


class EshopCart:
    def __init__(self, buyer):
        self.buyer = buyer
        self.products = []
        self.total = 0.0

    def add_product(self, name, price):
        self.products.append(name)
        self.total += price

    def __len__(self):
        return len(self.products)

    def __add__(self, other):
        mixed_cart = EshopCart(self.buyer)
        mixed_cart.products = copy.copy(self.products)
        mixed_cart.total = self.total
        for product in other.products:
            mixed_cart.products.append(product)
        mixed_cart.total += other.total
        return mixed_cart

    def __str__(self):
        return f"{self.buyer}'s cart with {len(self.products)}"

    # def __contains__(self, item):  # variant 1
    #     if item in self.products:
    #         return True
    #     else:
    #         return False

    def __contains__(self, item):  # variant 2
        for x in self.products:
            if x == item:
                return True
        return False

    # point 3

    def __ge__(self, other):
        if self.total >= other.total:
            return True
        return False

    def __lt__(self, other):
        if self.total < other.total:
            return True
        return False  # if "return False" was not written it would have not appeared "False" but "None"

    def __eq__(self, other):
        if self.total == other.total:
            return True
        return False

    # point 4 {self.__class__.__name__} name of the class
    def __str__(self):
        return f"{self.__class__.__name__}{{buyer:{self.buyer}, total: {self.total}, products: {len(self.products)}}}"



shopping1 = EshopCart("Silviu")
shopping1.add_product("eggs", 10)

shopping2 = EshopCart("maria")
shopping2.add_product("pastarnac", 1)

shopping3 = shopping1 + shopping2

print(len(shopping1))
print(shopping1.total)
print(shopping3)
print(len(shopping3))
print(shopping3.total)
print("cabbage" in shopping3)
print("eggs" in shopping3)
print("---- point 3 ------")
print(shopping1 >= shopping2)
print(shopping1 < shopping2)
print(shopping1 == shopping2)
print("---- point 4 ------")
print("Shopping1:", shopping1)
print("Shopping2:", shopping2)
print("Shopping3:", shopping3)
