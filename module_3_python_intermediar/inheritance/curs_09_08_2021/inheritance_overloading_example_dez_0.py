# the variant I had before writing dez_1 to dez 6_4. 6_4 is the final variant


class EshopCart:
    def __init__(self, buyer):
        self.buyer = buyer
        self.products = []
        self.total = 0.0

    def add_product(self, name, price):
        self.products.append(name)
        self.total += price

    def __len__(self):
        return len(self.products)

    def __add__(self, other):  # 2
        print(self, other)
        print(self.products, other.products)
        for product in other.products:
            # self.add_product(product, 0)
            self.products.append(product)  # varinta 2
        self.total += other.total
        return self

    # def __str__(self):
        # return f"{self.buyer}'s cart with {len(self.products)}"

    def _contains__(self, item):
        # if item in self.products: # varianata 1
        #     return True
        # else:
        #     return False
        for x in self.products:
            if x == item:
                return True
        return False

    # punctul 3
    def __ge__(self, other):
        if self.total >= other.total:
            return True

    def __lt__(self, other):
        if self.total < other.total:
            return True

    def __eg__(self, other):
        if self.total == other.total:
            return True

    # punctul 4 {self.__class__.__name__} denumire clasa
    def __str__(self):
        return f"{self.__class__.__name__}{{buyer:{self.buyer}, total: {self.total}, products: {len(self.products)}}}"


shopping1 = EshopCart("Silviu")
shopping1.add_product("eggs", 10)
# print(len(shopping1))
shopping2 = EshopCart("maria")  # 2
shopping2.add_product("pastarnac", 1)

shopping1 = shopping1 + shopping2
# shopping1 += shopping2

print(len(shopping1))
print(shopping1.total)
print(shopping1 >= shopping2)
print(shopping1 < shopping2)
print(shopping1 == shopping2)
print(shopping1)
