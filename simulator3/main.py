# ---------------------------------------------------------------------------------------------------------------------
# Point 1 - generates the file clasament_country_coefficients.py
import json
import os
from collections import Counter
from simulator3.country_coefficients import country_coefficients
from simulator3.fun import load_module_from_file, get_teams_from_module, writing_comments, \
    writing_variables_in_the_countries_files, import_data_and_writing_teams_of_CL_by_country, \
    import_data_and_writing_teams_of_UE_by_country, import_data_and_printing_the_teams_for_every_country, \
    creating_teams_of_CL
from simulator3.teams_of_CL_by_country import teams_of_CL_by_country

sorted_country_coefficients = sorted(country_coefficients, key=lambda x: x['coefficient'], reverse=True)
with open('clasament_country_coefficients.py', 'w', encoding='utf-8') as file:
    file.write('clasament_country_coefficients = [\n')
    for index, item in enumerate(sorted_country_coefficients, start=1):
        item_with_position = {**item, 'position': index}                            # ** is used to unpack dictionaries
        file.write(f'    {item_with_position},\n')
    file.write(']\n')
'''
# being written without 'position'
with open('clasament_country_coefficients.py', 'w', encoding='utf-8') as file:
    file.write('country_coefficients = [\n')
    for item in sorted_country_coefficients:
        file.write(f'    {item},\n')
    file.write(']\n')
'''
# ---------------------------------------------------------------------------------------------------------------------
# Point 2 - displays the ranking of countries based on the UEFA coefficients
print("\n Clasament uefa coeficienți")
for index, country in enumerate(sorted_country_coefficients, start=1):
    print(f"{index:>2}. {country['country']:<22}: {country['coefficient']:>7.3f}")
# ---------------------------------------------------------------------------------------------------------------------
# # Punctul 3
folder_path = r'C:\Users\home\PycharmProjects\pythonProject\Python_Courses\simulator3\countries'
teams_of_Co_wk_path = r'C:\Users\home\PycharmProjects\pythonProject\Python_Courses\simulator3\teams_of_Co_wk.py'
uefa_coefficients_path = r'C:\Users\home\PycharmProjects\pythonProject\Python_Courses\simulator3\uefa_coefficients.py'
countries_files = [f for f in os.listdir(folder_path) if f.endswith('.py') and f != '__init__.py']

teams_in_teams_of_co_wk = Counter([team['team'] for team in
                                getattr(load_module_from_file(teams_of_Co_wk_path), 'conference_wild_card_teams', [])])
duplicates_teams_of_Co_wk = {team: count for team, count in teams_in_teams_of_co_wk.items() if count > 1}
if duplicates_teams_of_Co_wk:
    print("3a. În fișierul teams_of_Co_wk.py")
    for team, count in duplicates_teams_of_Co_wk.items():
        print(f"Echipa {team} apare de {count} ori.")

teams_in_uefa_coefficients = Counter([team['team'] for team in
                                      getattr(load_module_from_file(uefa_coefficients_path), 'uefa_coefficients', [])])
duplicates_uefa_coefficients = {team: count for team, count in teams_in_uefa_coefficients.items() if count > 1}
if duplicates_uefa_coefficients:
    print("3b. În fișierul uefa_coefficients.py")
    for team, count in duplicates_uefa_coefficients.items():
        print(f"echipa {team} apare de {count} ori.")

# 3. Check for duplicates in 'countries' folder
teams_in_countries_folder = []
for file_name in countries_files:
    file_path = os.path.join(folder_path, file_name)
    module = load_module_from_file(file_path)
    teams = get_teams_from_module(module)
    teams_in_countries_folder.extend(teams)
team_counter = Counter(teams_in_countries_folder)
duplicates_countries = {team: count for team, count in team_counter.items() if count > 1}
if duplicates_countries:
    print("3b. În folderul countries")
    for team, count in duplicates_countries.items():
        print(f"echipa {team} apare de {count} ori.")

teams_in_countries_folder_set = set(teams_in_countries_folder)
teams_in_teams_of_co_wk_set = set(teams_in_teams_of_co_wk)
teams_in_uefa_coefficients_set = set(teams_in_uefa_coefficients)

missing_in_uefa_coefficients = \
    (teams_in_countries_folder_set | teams_in_teams_of_co_wk_set) - teams_in_uefa_coefficients_set
missing_in_countries_and_Co_wk =\
    teams_in_uefa_coefficients_set - (teams_in_countries_folder_set | teams_in_teams_of_co_wk_set)

if missing_in_uefa_coefficients:
    missing_teams = ', '.join(missing_in_uefa_coefficients)
    print(f"3a. Lipsește din uefa_coefficients.py: {missing_teams}")
if missing_in_countries_and_Co_wk:
    print(f"3b. Echipe lipsă în countries sau teams_of_Co_wk: {missing_in_countries_and_Co_wk}")

print(f"3c. În folderul countries sunt {len(teams_in_countries_folder)} echipe "
      f"cu \n{len(teams_in_teams_of_co_wk)} echipe din teams_of_Co_wk.py "
      f"fac {len(teams_in_countries_folder) + len(teams_in_teams_of_co_wk)} echipe.")
print(f"3c. În fișierul uefa_coefficients.py sunt {len(teams_in_uefa_coefficients)} echipe.")
# ---------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    writing_comments()                                                                                     # Punctul  4
    writing_variables_in_the_countries_files()                                                             # Punctul  5
    import_data_and_writing_teams_of_CL_by_country()                                                       # Punctul  6
    import_data_and_writing_teams_of_UE_by_country()                                                       # Punctul  7
    import_data_and_printing_the_teams_for_every_country()                                                 # Punctul  8
    # creating_teams_of_CL()                                                                                 # Punctul  9
    # creating_teams_of_UE()                                                                                 # Punctul 10
# Punctul  4 scrie comentarii în fișierele ___.py din folderul countries ----------------------------------------------
# Punctul  5 scrie variabilele în fișierele ___.py din folderul countries ---------------------------------------------
# Punctul  6 scrie în _teams_of_CL_by_country.py țările cu echipele participante numai în CL --------------------------
# Punctul  7 scrie în _teams_of_UE_by_country.py țările cu echipele participante numai în UE --------------------------
# Punctul  8 printează țările în ordine crescătoare cu echipele care participă în cupele CL și UE ---------------------
# Punctul  9 creează fișierului teams_of_CL.py în funcție de poziția țării în clasament_country_coefficients ----------
# Punctul 10 creează fișierului teams_of_UE.py în funcție de poziția țării în clasament_country_coefficients ----------
# ---------------------------------------------------------------------------------------------------------------------
# Create a dictionary to map countries to coefficients
coeff_dict = {entry['country']: entry['coefficient'] for entry in country_coefficients}

# Sort teams_of_CL_by_country by country coefficient in descending order
# If the country is not in coeff_dict, assign a coefficient of 0
teams_of_CL_sorted = sorted(teams_of_CL_by_country, key=lambda x: coeff_dict.get(x['country'], 0.0), reverse=True)

# Add position to each entry in the sorted list
for idx, entry in enumerate(teams_of_CL_sorted):
    entry['position'] = idx + 1

# Prepare the data in Python list format
output_lines = ["teams_of_CL = [\n"]
for entry in teams_of_CL_sorted:
    country = entry['country']
    teams = entry['teams']
    position = entry['position']
    # Write each dictionary entry in Python format
    output_lines.append(f"    {{'country': '{country}', 'teams': {teams}, 'position': {position}}},\n")

output_lines.append("]\n")
with open('teams_of_CL.py', 'w', encoding='utf-8') as f:
    f.writelines(output_lines)
