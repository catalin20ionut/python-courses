conference_wild_card_teams = [
    {"team": "Schalke", "coefficient": 25.0, "country": "Germany"},
    {"team": "Hamburger SV", "coefficient": 18.0, "country": "Germany"},
    {"team": "Bari", "coefficient": 15.0, "country": "Italy"},
    {"team": "Kaiserslautern", "coefficient": 14.0, "country": "Germany"},
    {"team": "Brescia", "coefficient": 12.0, "country": "Italy"},
    {"team": "Deportivo La Coruña", "coefficient": 10.0, "country": "Spain"},
    {"team": "Blackburn Rovers", "coefficient": 9.0, "country": "England"},
    {"team": "Sunderland", "coefficient": 8.5, "country": "England"},
    {"team": "Stoke", "coefficient": 8.0, "country": "England"},
    {"team": "Norwich City", "coefficient": 7.5, "country": "England"},
    {"team": "Reading", "coefficient": 6.5, "country": "England"},
    {"team": "Corvinul Hunedoara", "coefficient": 6.0, "country": "Romania"},
    {"team": "Torpedo Moscova", "coefficient": 6.0, "country": "Russia"},
    {"team": "Mordavia Saransk", "coefficient": 6.0, "country": "Russia"},
]
# uefa_coefficients = conference_wild_card_teams + countries
