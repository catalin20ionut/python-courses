teams = [
    "Ajax Amsterdam",  # 1
    "PSV Eindhoven",  # 2
    "AZ Alkmaar",  # 3
    "Twente Enschede",  # 4
    "Feyenoord Rotterdam",  # 5
    "Vitesse Arnheim",  # 6
    "Heerenveen",  # 7
    "NEC Nijmegen",  # 8
    "FC Utrecht",  # 9
    "Sparta Rotterdam",  # 10
    "PEC Zwolle",  # 11
    "Groningen",  # 12
    "Heracles Almelo",  # 13
    "Waalwijk",  # 14
    "Fortuna Sittard",  # 15
    "Willem II",  # 16
    "Cambuur",  # 17
    "Go Ahead Eagles",  # 18
    "VVV-Venlo",  # 19
    "Emmen",  # 20
]

var_CL = 3
var_UE = 4
CL = None
UE = None
CU = 4
string_CU = "Steaua Netherlands"
