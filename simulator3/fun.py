import importlib.util
import os
import random
import re
from clasament_country_coefficients import clasament_country_coefficients
from simulator3.teams_of_CL_by_country import teams_of_CL_by_country


def load_module_from_file(file_path):
    spec = importlib.util.spec_from_file_location("module.name", file_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module


def get_teams_from_module(module):
    ploegen = getattr(module, 'teams', [])
    # checking whether the elements in 'teams' are dictionaries or not.
    valid_teams = []
    for team in ploegen:
        if isinstance(team, dict) and 'team' in team:
            valid_teams.append(team['team'])
        elif isinstance(team, str):
            valid_teams.append(team)
        else:
            print(f"Warning: invalid element in: {team}")
    return valid_teams


def writing_comments():                                                       # !!! ->                          # {i+1}
    folder_path = 'countries'                                                 # the folder that contains the _.py files
    files = [f for f in os.listdir(folder_path) if f.endswith('.py')]         # it gets all the .py files in the folder

    for file_name in files:                                                             # it iterates through each file
        file_path = os.path.join(folder_path, file_name)
        try:
            with open(file_path, 'r', encoding='utf-8') as file:
                content = file.read()
        except UnicodeDecodeError:
            print(f"Error decoding {file_name}. Skipping file.")
            continue
        content = re.sub(r'#.*', '', content)                      # removing all old comments (# followed by anything)

        # caută în lista teams
        pattern = r'teams\s*=\s*\[(.*?)\]'
        match = re.search(pattern, content, re.DOTALL)
        if match:
            teams_content = match.group(1)
            local_teams = [team.strip().strip('"') for team in teams_content.split(',') if team.strip()]
            # creating the updated content, this is where the new text is written at # {i + 1}
            updated_teams_content = "\n    ".join(f'"{team}",  # {i+1}' for i, team in enumerate(local_teams))
            # adding the right square bracket - ] - to the end of the updated content
            pattern = r'(teams\s*=\s*\[)(.*?)(\])'
            replacement = f'\\1\n    {updated_teams_content}\n\\3'
            updated_content = re.sub(pattern, replacement, content, flags=re.DOTALL)

            # writing again in the file
            with open(file_path, 'w', encoding='utf-8') as file:
                file.write(updated_content)
        else:
            print(f"4. No teams list found in {file_name}") if file_name != '__init__.py' else None
    # print("4. S-au făcut schimbările în comentariile din fișierele din cadrul folderului countries.")
# ---------------------------------------------------------------------------------------------------------------------
#  bloc de stabilire a numărului de echipe pentru fiecare țară ce participă în Champions League, UEFA League  ---------


def find_country_info(country_name):
    for item in clasament_country_coefficients:
        if item['country'] == country_name:
            return item
    return None


def determine_var_CL(position, filename):
    if filename == 'liechtenstein.py':
        return 0
    if position == 1:
        return 5
    elif position <= 6:
        return 4
    elif position <= 12:
        return 3
    elif position <= 34:
        return 2
    else:
        return 1


def determine_var_UE(position):
    if position <= 5:
        return 5
    elif position <= 12:
        return 4
    elif position <= 41:
        return 3
    else:
        return 2


def determine_CL(filename):
    return 3 if filename == 'england.py' else None


def determine_UE(filename):
    return 6 if filename == 'england.py' else None


def get_random_CU(filecontent):
    regex_script = r'teams\s*=\s*\[([^\]]+)\]'
    ploegen = re.findall(regex_script, filecontent)
    if ploegen:
        team_list = ploegen[0].split(',')
        return random.randint(1, len(team_list)//3)
    return None


def writing_variables_in_the_countries_files():
    folder_path = r'C:\Users\home\PycharmProjects\pythonProject\Python_Courses\simulator3\countries'
    files = [f for f in os.listdir(folder_path) if f.endswith('.py') and f != '__init__.py']

    for file_name in files:
        file_path = os.path.join(folder_path, file_name)
        try:
            with open(file_path, 'r', encoding='utf-8') as file:
                file_content = file.read()
        except UnicodeDecodeError:
            print(f"Error decoding {file_name}. Skipping file.")
            continue

        # extrag numele de țară pentru position din clasament_country_coefficients.py și string_CU_val (replacement)
        country_name = file_name.replace('.py', '').title() \
            .replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
        country_name_for_string_CU = file_name.replace('.py', '').title() \
            .replace('_', ' ').replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')

        country_info = find_country_info(country_name)
        # # debugging line: print country_name to verify
        # print(f"Processing file: {file_name}, country name derived: {country_name}")
        if country_info:
            position = country_info['position']
            var_CL_val = determine_var_CL(position, file_name)
            var_UE_val = determine_var_UE(position)
            CL_val = determine_CL(file_name)
            UE_val = determine_UE(file_name)
            CU_val = get_random_CU(file_content)
        else:
            # # debugging line: print if country_name is not found
            # print(f"Country name '{country_name}' not found in coefficients.")
            var_CL_val, var_UE_val, CL_val, UE_val, CU_val, string_CU_val = 0, 0, None, None, 0, ''
        # update pattern to match everything after ']'
        pattern = r'\](.*)'
        replacement = (
            f']\n\n'
            f'var_CL = {var_CL_val}\n'
            f'var_UE = {var_UE_val}\n'
            f'CL = {CL_val}\n'
            f'UE = {UE_val}\n'
            f'CU = {CU_val}\n'
            f'string_CU = "Steaua {country_name_for_string_CU}"\n'
        )
        new_content = re.sub(pattern, replacement, file_content, flags=re.DOTALL)

        with open(file_path, 'w', encoding='utf-8') as file:
            file.write(new_content)
    # print("5. Toate fișierele .py au fost procesate.")
# ----------------------------------------------------------------------------------------------------------------------
# Definirea variabilelor globale pentru
# picking_teams_for_European_cups()
# import_data_and_writing_teams_of_CL_by_country()
# import_data_and_writing_teams_of_UE_by_country()
# import_data_and_printing_the_teams_for_every_country()
# creating_teams_of_CL(), creating_teams_of_UE()
teams = []
var_CL = 0
var_UE = 0
CL = 0
UE = 0
CU = 0
string_CU = ''


def picking_teams_for_European_cups():
    # aflam numărul echipelor din liga cu cele mai multe echipe --------------------------------
    directory = r'C:\Users\home\PycharmProjects\pythonProject\Python_Courses\simulator2\countries'
    files = [f for f in os.listdir(directory) if f.endswith('.py')]        # Lista fișierelor .py
    max_length = 0

    for file in files:
        filepath = os.path.join(directory, file)
        spec = importlib.util.spec_from_file_location("module.name", filepath)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        #  Verifică dacă atributul 'teams' există și dacă este o listă
        if hasattr(module, 'teams') and isinstance(module.teams, list):
            length = len(module.teams)
            if length > max_length:
                max_length = length
    most_teams = max_length+1                       # în cazul nostru -> most_teams = 20 + 1 = 21
    # -------------------------------------------------------------------------------------------
    pivot_CL = var_CL
    pivot_UE = var_CL + var_UE - 1

    teams_for_CL = []
    teams_for_UE = []

    if CL == UE and CL is not None and UE is not None:
        print("1. Acest lucru este imposibil.")

    elif CL is None and UE is None:
        if CU <= pivot_UE:
            teams_for_CL = teams[:pivot_CL]
            teams_for_UE = teams[pivot_CL:pivot_UE+1]
        elif most_teams > CU > pivot_UE:
            teams_for_CL = teams[:pivot_CL]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL]
            teams_for_UE = teams[pivot_CL:pivot_UE+1] + [string_CU]

    elif CL is None and UE <= pivot_CL:
        if CU <= pivot_UE+2:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1]
        elif most_teams > CU > pivot_UE+2:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [string_CU]

    elif CL is None and pivot_CL < UE <= pivot_UE:
        if CU <= pivot_UE:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+2]) if idx != UE - (pivot_CL+1)]
        elif most_teams > CU > pivot_UE:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+1]) if
                            idx != UE - (pivot_CL+1)] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+1]) if
                            idx != UE - (pivot_CL+1)] + [string_CU]

    elif CL is None and UE > pivot_UE:
        if CU <= pivot_UE:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[pivot_UE+1]] \
                if UE == pivot_UE+1else teams[pivot_CL:pivot_UE+1]
        elif most_teams > CU > pivot_UE and UE != CU:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[CU-1]]
        elif most_teams > CU > pivot_UE and UE == CU:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[pivot_UE+1]] \
                if UE == pivot_UE+1 else teams[pivot_CL:pivot_UE] + [teams[pivot_UE]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [string_CU]

    elif CL <= pivot_CL and UE is None:
        if CU <= pivot_UE+1:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+2]
        elif most_teams > CU > pivot_UE+1:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [string_CU]

    elif pivot_CL < CL <= pivot_UE and UE is None:
        if CU <= pivot_UE+1:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+2]) if idx != CL - (pivot_CL+1)]
        elif most_teams > CU > pivot_UE+1:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+1])
                            if idx != CL - (pivot_CL+1)] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+1])
                            if idx != CL - (pivot_CL+1)] + [string_CU]

    elif CL > pivot_UE and UE is None:
        if CU <= pivot_UE:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE+1]
        elif most_teams > CU > pivot_UE:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            if CL != CU:
                teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[CU-1]]
            elif CL == CU == pivot_UE+1:
                teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[pivot_UE+1]]
            elif CL == CU:
                teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[pivot_UE]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [string_CU]

    elif CL <= pivot_CL and UE <= pivot_CL:                             # CL <= 5, UE <= 5, pivot_CL == 5, pivot_UE == 9
        if CU <= pivot_UE+2:
            teams_for_CL = teams[:pivot_CL+2]
            teams_for_UE = teams[pivot_CL+2:pivot_UE+3]
        elif most_teams > CU > pivot_UE+2:
            teams_for_CL = teams[:pivot_CL+2]
            teams_for_UE = teams[pivot_CL+2:pivot_UE+2] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL+2]
            teams_for_UE = teams[pivot_CL+2:pivot_UE+2] + [string_CU]

    elif CL <= pivot_CL < UE <= pivot_UE:                           # CL <= 5, 5 < UE <= 9, pivot_CL == 5, pivot_UE == 9
        if CU <= pivot_UE+2:
            if UE == pivot_CL+1:
                teams_for_CL = teams[:pivot_CL+2]
                teams_for_UE = teams[pivot_CL+2:pivot_UE+3]
            elif pivot_CL+1 < UE <= pivot_UE:
                teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
                teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+3]) if
                                idx != UE - (pivot_CL+2)]
        elif most_teams > CU > pivot_UE+2:
            if UE == pivot_CL+1:
                teams_for_CL = teams[:pivot_CL+2]
                teams_for_UE = teams[pivot_CL+2:pivot_UE+2] + [teams[CU-1]]
            elif pivot_CL+1 < UE <= pivot_UE:
                teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
                teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+2]) if
                                idx != UE - (pivot_CL+2)] + [teams[CU-1]]
        elif CU == most_teams:
            if UE == pivot_CL+1:
                teams_for_CL = teams[:pivot_CL+2]
                teams_for_UE = teams[pivot_CL+2:pivot_UE+2] + [string_CU]
            elif pivot_CL+1 < UE <= pivot_UE:
                teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
                teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+2]) if
                                idx != UE - (pivot_CL+2)] + [string_CU]

    elif CL <= pivot_CL and UE > pivot_UE:                              # CL <= 5, UE > 10, pivot_CL == 5, pivot_UE == 9
        if CU <= pivot_UE+2:
            teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+3]) if idx != UE - (pivot_CL+2)] \
                if pivot_UE < UE <= pivot_UE+3 else teams[pivot_CL+1:pivot_UE+2]
        elif most_teams > CU > pivot_UE+2:
            if UE != CU:
                teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
                teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [teams[CU-1]]
            elif UE == CU:
                teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
                teams_for_UE = teams[pivot_CL+1:pivot_UE+2]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [string_CU]

    elif UE <= pivot_CL < CL <= pivot_UE:                           # 5 < CL <= 9, UE <= 5, pivot_CL == 5, pivot_UE == 9
        if CU <= pivot_UE+3 and CL == pivot_CL+1:
            teams_for_CL = teams[:pivot_CL+2]
            teams_for_UE = teams[pivot_CL+2:pivot_UE+3]
        elif CU <= pivot_UE+3 and pivot_CL+1 < CL <= pivot_UE:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+3]) if idx != CL - (pivot_CL+2)]
        elif most_teams > CU > pivot_UE+3 and CL == pivot_CL+1:
            teams_for_CL = teams[:pivot_CL+2]
            teams_for_UE = teams[pivot_CL+2:pivot_UE+2] + [teams[CU-1]]
        elif most_teams > CU > pivot_UE+3 and pivot_CL+1 < CL <= pivot_UE:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+2]) if idx != CL-(pivot_CL+2)] +\
                           [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+2]) if idx != CL-(pivot_CL+2)] + \
                           [string_CU]
    # 5 < CL <= 12, 5< UE <= 9, pivot_CL == 5, pivot_UE == 9
    elif pivot_CL < CL <= pivot_UE and pivot_CL < UE <= pivot_UE and CL != UE:
        if CU <= pivot_UE+3:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+3])
                            if idx != CL - (pivot_CL+1) and idx != UE - (pivot_CL+1)]
        elif most_teams > CU > pivot_UE+3:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+2])
                            if idx != CL - (pivot_CL+1) and idx != UE - (pivot_CL+1)] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+2])
                            if idx != CL - (pivot_CL+1) and idx != UE - (pivot_CL+1)] + [string_CU]

    elif pivot_UE > CL > pivot_CL > CU:                # 5 < CL <= 9, 10 <= UE ... CU <= 5, pivot_CL == 5, pivot_UE == 9
        if pivot_UE < UE <= pivot_UE+3:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+3])
                            if idx != CL - (pivot_CL+1) and idx != UE - (pivot_CL+1)]
        elif UE > pivot_UE+3:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+2])
                            if idx != CL - (pivot_CL+1)]
# am înlocuit 4 cu var_UE ....
    elif pivot_CL < CL <= pivot_UE < UE and CU < most_teams:        # 5 < CL <= 9, 10 > UE, pivot_CL == 5, pivot_UE == 9
        teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
        x = [team for team in teams if team not in teams_for_CL]
        teams_for_UE = (x[:var_UE] + [teams[CU-1]]) if teams[CU-1] not in (teams_for_CL + x[:var_UE]) else x[:var_UE+1]
    elif pivot_CL < CL <= pivot_UE < UE and CU == most_teams:
        teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
        x = [team for team in teams if team not in teams_for_CL]
        teams_for_UE = x[:var_UE] + [string_CU]

    elif CL > pivot_UE and UE <= pivot_CL and CU == most_teams:         # CL > 9, UE <= 5, pivot_CL == 5, pivot_UE == 9
        teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
        x = [team for team in teams if team not in teams_for_CL]
        teams_for_UE = x[:var_UE] + [string_CU]
    elif CL > pivot_UE and UE <= pivot_CL:
        if CU <= pivot_UE:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            if pivot_UE < CL <= pivot_UE+3:
                teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+3]) if idx != CL-(pivot_CL+2)]
            elif CL > pivot_UE+3:
                teams_for_UE = teams[pivot_CL+1:pivot_UE+2]
        elif most_teams > CL != CU > pivot_UE:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            x = [team for team in teams if team not in teams_for_CL]
            teams_for_UE =\
                (x[:var_UE] + [teams[CU-1]]) if teams[CU-1] not in (teams_for_CL + x[:var_UE]) else x[:var_UE+1]
        elif most_teams > CL == CU > pivot_UE:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            x = [team for team in teams if team not in teams_for_CL]
            teams_for_UE =\
                (x[:var_UE] + [teams[CU-1]]) if teams[CU-1] not in (teams_for_CL + x[:var_UE]) else x[:var_UE+1]

    elif most_teams > CL > pivot_UE and CU < most_teams:           # CL > = 10 ... UE > 6, pivot_CL == 5, pivot_UE == 9
        teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
        x = [team for team in teams if team not in teams_for_CL]
        teams_for_UE = (x[:var_UE] + [teams[CU-1]]) if teams[CU-1] not in (teams_for_CL + x[:4]) else x[:var_UE+1]
    elif CU == most_teams and most_teams > CL > pivot_UE:
        teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
        x = [team for team in teams if team not in teams_for_CL]
        teams_for_UE = x[:var_UE] + [string_CU]
    return teams_for_CL, teams_for_UE


def import_data_and_writing_teams_of_CL_by_country():
    countries_folder = 'countries'  # Folderul care conține fișierele .py
    output_file = 'teams_of_CL_by_country.py'  # Fișierul în care se vor scrie rezultatele

    with open(output_file, 'w', encoding='utf-8') as file:
        file.write('teams_of_CL_by_country = [\n')
        for filename in os.listdir(countries_folder):
            if filename.endswith('.py') and filename != '__init__.py':
                country_name = filename[:-3]                                                # without the extension .py
                module_name = f'countries.{country_name}'
                module = importlib.import_module(module_name)

                # Verifică dacă fișierul conține variabilele necesare
                if hasattr(module, 'teams') and hasattr(module, 'var_CL') and hasattr(module, 'var_UE') \
                        and hasattr(module, 'CL') and hasattr(module, 'UE') and hasattr(module, 'CU') \
                        and hasattr(module, 'string_CU'):

                    # Destructurarea variabilelor
                    global teams, var_CL, var_UE, CL, UE, CU, string_CU
                    teams = module.teams
                    var_CL = module.var_CL
                    var_UE = module.var_UE
                    CL = module.CL
                    UE = module.UE
                    CU = module.CU
                    string_CU = module.string_CU

                    teams_for_CL, teams_for_UE = picking_teams_for_European_cups()  # Apelează funcția

                    formatted_country_name = country_name.title() \
                        .replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                    formatted_teams_for_CL = [f"'{team}'" for team in teams_for_CL]
                    teams_str = f"[{', '.join(formatted_teams_for_CL)}]"
                    file.write(f"    {{'country': '{formatted_country_name}', 'teams': {teams_str}}},\n")
                else:
                    formatted_country_name = country_name.title() \
                        .replace('_', ' ').replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                    file.write(
                        f"    {{'country': '{formatted_country_name}', 'error': 'Missing necessary variables'}},\n")

        file.write(']\n')


def import_data_and_writing_teams_of_UE_by_country():
    countries_folder = 'countries'  # Folderul care conține fișierele .py
    output_file = 'teams_of_UE_by_country.py'  # Fișierul în care se vor scrie rezultatele

    with open(output_file, 'w', encoding='utf-8') as file:
        file.write('teams_of_UE_by_country = [\n')
        for filename in os.listdir(countries_folder):
            if filename.endswith('.py') and filename != '__init__.py':
                country_name = filename[:-3]  # Fără extensia .py
                module_name = f'countries.{country_name}'
                module = importlib.import_module(module_name)  # Importă modulul

                # Verifică dacă fișierul conține variabilele necesare
                if hasattr(module, 'teams') and hasattr(module, 'var_CL') and hasattr(module, 'var_UE') \
                        and hasattr(module, 'CL') and hasattr(module, 'UE') and hasattr(module, 'CU') \
                        and hasattr(module, 'string_CU'):

                    # Destructurarea variabilelor
                    global teams, var_CL, var_UE, CL, UE, CU, string_CU
                    teams = module.teams
                    var_CL = module.var_CL
                    var_UE = module.var_UE
                    CL = module.CL
                    UE = module.UE
                    CU = module.CU
                    string_CU = module.string_CU
                    teams_for_CL, teams_for_UE = picking_teams_for_European_cups()
                    formatted_country_name = country_name.title() \
                        .replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                    formatted_teams_for_UE = [f"'{team}'" for team in teams_for_UE]
                    teams_str = f'[{", ".join(formatted_teams_for_UE)}]'
                    file.write(f'    {{"country": "{formatted_country_name}", "teams": "{teams_str}"}},\n')
                else:
                    formatted_country_name = country_name.title() \
                        .replace('_', ' ').replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                    file.write(
                        f'    {{"country": "{formatted_country_name}", "error": "Missing necessary variables"}},\n')

        file.write(']\n')


def import_data_and_printing_the_teams_for_every_country():                     # var_CL, var_UE, CL, UE, CU, string_CU
    countries_folder = 'countries'                                                # Folderul care conține fișierele .py
    print("\n\033[32m8. Echipele participante\033[0m")

    for filename in os.listdir(countries_folder):
        if filename.endswith('.py') and filename != '__init__.py':
            country_name = filename[:-3]                                                            # Fără extensia .py
            module_name = f'countries.{country_name}'
            module = importlib.import_module(module_name)                                             # Importă modulul

            if hasattr(module, 'teams') and hasattr(module, 'var_CL') and hasattr(module, 'var_UE') \
                    and hasattr(module, 'CL') and hasattr(module, 'UE') and hasattr(module, 'CU') \
                    and hasattr(module, 'string_CU'):
                # Destructurarea variabilelor
                global teams, var_CL, var_UE, CL, UE, CU, string_CU
                teams = module.teams
                var_CL = module.var_CL
                var_UE = module.var_UE
                CL = module.CL
                UE = module.UE
                CU = module.CU
                string_CU = module.string_CU

                # Apelează funcția
                teams_for_CL, teams_for_UE = picking_teams_for_European_cups()

                pivot_CL = var_CL
                pivot_UE = var_CL + var_UE - 1

                formatted_country_name = country_name.title() \
                    .replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                print(f"{formatted_country_name:<22} - teams_for_CL "
                      f"- {', '.join(teams_for_CL):<101} <= pivot CL - {pivot_CL} ")
                print(f"{formatted_country_name:<22} - teams_for_UE "
                      f"- {', '.join(teams_for_UE):<101} <= pivot UE - {pivot_UE}")

            else:
                formatted_country_name = country_name.title()\
                    .replace('_', ' ').replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                print(f"{formatted_country_name:<22} - Fișierul {filename} nu conține variabilele necesare.")


# def creating_teams_of_CL():
#     teams_of_CL = {}
#     for entry in teams_of_CL_by_country:
#         country_9 = entry['country']
#         teams_9 = entry['teams']
#         if country_9 in clasament_country_coefficients:    # verifică existența țării în clasament_country_coefficients
#             position = clasament_country_coefficients[country_9]['position']                    # extrage poziția țării
#             teams_of_CL[country_9] = {'teams': teams_9, 'position': position}  # crearea și atribuirea unei noi intrări
#             # în dicționar      În dicționarul teams_of_CL, se adaugă o nouă intrare cu cheia country_9 (numele țării).
#             #                              Valoarea asociată acestei chei este un nou dicționar care conține două chei:
#             # 'teams':                                care are ca valoare lista de echipe teams_9 asociată acelei țări.
#             # 'position':                                           care are ca valoare poziția țării extrasă anterior.
#     sorted_teams_of_CL = dict(sorted(teams_of_CL.items(), key=lambda item: item[1]['position']))  # sortăm după poziție
#     '''
#     # Creăm fișierul teams_of_CL.py
#     with open('teams_of_CL.py', 'w', encoding='utf-8') as file_9:
#         file_9.write('teams_of_CL = {\n')
#         for country_9, data_9 in sorted_teams_of_CL.items():
#             file_9.write(f"    '{country_9}': {{'teams': {data_9['teams']}, 'position': {data_9['position']}}},\n")
#         file_9.write('}\n')
#     '''
#     with open('teams_of_CL.py', 'w', encoding='utf-8') as file_9:
#         file_9.write('teams_of_CL = [\n')
#         for country_9, data_9 in sorted_teams_of_CL.items():
#             team_list = data_9["teams"]
#             first_line = ', '.join(f'"{team}"' for team in team_list[:len(team_list) // 2])
#             if len(f'    {{"country": "{country_9}", '
#                    f'"teams": {data_9["teams"]}, "position": {data_9["position"]}}},\n') > 120:
#                 file_9.write(f'     {{"country": "{country_9}", "teams": [{first_line},\n                         ')
#                 for team in team_list[len(team_list) // 2:]:
#                     file_9.write(f'"{team}", ')
#                 file_9.seek(file_9.tell() - 2)  # elimină ultima virgulă; cursorul se mută cu două spații înapoi
#                 file_9.write(f'], "position": {data_9["position"]}}},\n')
#             else:
#                 file_9.write(f'     {{"country": "{country_9}", '
#                              f'"teams": {data_9["teams"]}, "position": {data_9["position"]}}},\n')
#         file_9.write(']\n')
#     return teams_of_CL

def creating_teams_of_CL():
    teams_of_CL = []

    # Verificăm dacă teams_of_CL_by_country este gol
    if not teams_of_CL_by_country:
        print("Lista teams_of_CL_by_country este goală. Nu se vor crea date în fișier.")
        with open('teams_of_CL.py', 'w', encoding='utf-8') as file_9:
            file_9.write('teams_of_CL = []\n')
        return teams_of_CL

    # Parcurge lista de dicționare din teams_of_CL_by_country
    for entry in teams_of_CL_by_country:
        country_9 = entry['country']
        teams_9 = entry['teams']

        # Verifică dacă țara există în clasament_country_coefficients
        if country_9 in clasament_country_coefficients:
            position = clasament_country_coefficients[country_9]['position']  # Extrage poziția țării

            # Adaugă o nouă intrare în teams_of_CL
            teams_of_CL[country_9] = {'teams': teams_9, 'position': position}

    # Sortăm după poziție
    sorted_teams_of_CL = dict(sorted(teams_of_CL.items(), key=lambda item: item[1]['position']))

    # Creăm fișierul teams_of_CL.py în formatul dorit
    with open('teams_of_CL.py', 'w', encoding='utf-8') as file_9:
        file_9.write('teams_of_CL = [\n')

        for country_9, data_9 in sorted_teams_of_CL.items():
            team_list = data_9["teams"]

            # Pregătim lista de echipe cu ghilimele duble
            teams_str = '[' + ', '.join(f'"{team}"' for team in team_list) + ']'

            # Construim linia completă
            line = f"    {{'country': '{country_9}', 'teams': {teams_str}, 'position': {data_9['position']}}},\n"

            # Verificăm dacă linia este prea lungă
            if len(line) > 120:
                # Împărțim echipele în două linii
                half_length = len(team_list) // 2
                first_half = ', '.join(f'"{team}"' for team in team_list[:half_length])
                second_half = ', '.join(f'"{team}"' for team in team_list[half_length:])

                # Construim liniile separate
                first_line = f"    {{'country': '{country_9}', 'teams': [{first_half},\n"
                second_line = f"                  {second_half}, 'position': {data_9['position']}}},\n"

                file_9.write(first_line)
                file_9.write(second_line)
            else:
                # Scriem linia completă
                file_9.write(line)

        file_9.write(']\n')  # Închide lista în fișier

    return teams_of_CL


