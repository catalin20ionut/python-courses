""" Metoda Factory creează obiecte fără a fi nevoie de a specifica clasa.
Clasele îndeplinesc aceeași interfață, dar diferă atunci când sunt utilizate. Fabricile sunt adesea folosite atunci când
este configurația care decide pe care ar trebui să o alegem - alegerea se face dinamic.

Factory method creates objects without the need to specify class. Classes fulfill the same interface, but they are
different when they are used. Factories are often used when it’s the config, which it decides which one we should pick
– the pick is made dynamically.

Die Factory-Methode erstellt Objekte, ohne die Klasse angeben zu müssen. Klassen haben dieselbe Schnittstelle; sie
unterscheiden sich jedoch in der Verwendung. Fabriken werden oft verwendet sein, wenn es die Konfiguration gibt, die
entscheidet, welche wir wählen sollten - die Wahl ist dynamisch. """


class RoyalMail:
    cost = 15.0
    duration = 3

    def __init__(self, package):
        self.package = package

    def ship(self):
        print(f"Shipping {self.package} in {self.duration} days for {self.cost} via {self.__class__.__name__}")


class ShadyCourier:
    cost = 1.0
    duration = 30

    def __init__(self, package):
        self.package = package

    def ship(self):
        print(
            (
                f"Shipping {self.package} in {self.duration} days "
                f"for {self.cost} via {self.__class__.__name__}"
            )
        )


class OnlineStore:
    _companies = {"RoyalMail": RoyalMail, "ShadyCourier": ShadyCourier}

    def process_shipment(self, package, company="RoyalMail"):
        return self._companies[company](package)


if __name__ == "__main__":
    store = OnlineStore()
    fast_shipment = store.process_shipment("Vase")
    fast_shipment.ship()

    slow_shipment = store.process_shipment("Flamingo", "ShadyCourier")
    slow_shipment.ship()
