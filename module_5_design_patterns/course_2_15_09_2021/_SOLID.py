"""
https://medium.com/backticks-tildes/the-s-o-l-i-d-principles-in-pictures-b34ce2f1e898
Principiile SOLID
S - Single responsibility principle - principiul responsabilități unice
O - Open-closed principle
L - Liskov substitution principle
I - Interface segregation principle
D - Dependency inversion principle - principiul inversiei dependințelor
autor: Robert C. Martin "Design Principles and Design Patterns"

1) Single responsibility principle --- every module, class or function in a computer program should have responsibility
over a single part of that program's functionality, which it should encapsulate. No other module can be responsible for
the same functionality. Gather together those things that change for the same reason, and separate those things that
change for different reasons.
2) Open/close principle - each module should be:
closed to modification - clearly defined interface, fixed functionality
open to extension ----------- allows you to make changes by inheritance
This principle aims to extend a Class’s behaviour without changing the existing behaviour of that Class. This is to
avoid causing bugs wherever the Class is being used.
3) Liskov substitution principle - objects of a superclass should be replaceable with objects of its subclasses without
breaking the application. This rule applies to inheritance; the characteristics of the base class object should be
preserved in the child class object (e.g. method parameters, invariants, exceptions)
Polimorfism - într-o ierarhie de moșteniri obiecte diferite pot răspunde diferit la apelul aceleași funcții (în clasa
animal câinele, pisica și calul scot sunete diferite - funcția sound) (15.09.2021 1.2 06:00)
4) Interface segregation principle - interfaces should be as specific as possible
clients only have to know about the methods that are of interest to them
Firstly, let’s define the terms used here more simply

When a Class is required to perform actions that are not useful, it is wasteful and may produce unexpected bugs if the
Class does not have the ability to perform those actions. A Class should perform only actions that are needed to fulfil
its role. Any other action should be removed completely or moved somewhere else if it might be used by another Class in
the future.
Goal:      This principle aims at splitting a set of actions into smaller sets so that a Class executes ONLY the set of
actions it requires.

5) Dependency inversion principle -
high-level modules should not depend on low-level modules; both should depend on abstractions
abstractions should not depend on details; details (concrete implementations) should depend on abstractions

High-level Module(or Class): Class that executes an action with a tool.
Low-level Module (or Class): The tool that is needed to execute the action
Abstraction: Represents an interface that connects the two Classes.
Details: How the tool works
This principle says a Class should not be fused with the tool it uses to execute an action. Rather, it should be fused
to the interface that will allow the tool to connect to the Class. It also says that both the Class and the interface
should not know how the tool works. However, the tool needs to meet the specification of the interface.

Goal
This principle aims at reducing the dependency of a high-level Class on the low-level Class by introducing an interface.
"""
