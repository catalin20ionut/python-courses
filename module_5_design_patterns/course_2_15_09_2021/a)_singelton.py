""" Design patterns can be defined as well defined solutions to common problems. They are universal to all platforms and
languages. Design patterns should be treated as a suggestion of a solution for those issues. They are handy for naming
structures that naturally occur in code: adapters, decorators, builders.

Design patterns are not copy-paste solutions.
They do not solve problems by themselves.
Forcefully applying pattern to a code it does not fit always.
Design patterns are not rulebooks, you can take a concept and modify it to your needs.

Modelele de design înseamnă soluții bine definite pentru problemele care pot apărea des. Sunt universale pentru toate
platformele și limbaje. Ar trebui să fie tratate ca o sugestie a unei soluții pentru aceste probleme. Modelele de design
nu sunt soluții copy-paste. Nu rezolvă problemele de la sine. Nu merg întotdeauna în fiecare caz.

Se împart în trei categorii:
creational - Singleton, Builder, Factory, Abstract Factory, Dependency injection
structural - Adapter, Bridge, Facade, Decorator, Proxy, Flyweight
behavioral - Chain of responsibility, Command, Interpreter, Iterator, Strategy, Observer

creational - how to create objects
structural - how to describe structures of related objects (descriu structurile obiectelor)
behavioral (behavioral) - how to program the behavior and responsibility of related objects. (asociate)


Designmodelle bedeuten wohldefinierte Lösungen für Probleme, die häufig auftreten können. Sie sind universell für alle
Plattformen und Programmiersprachen. Sie sollten als Vorschlag für eine Lösung dieser Probleme betrachtet werden.
Designvorlagen sind keine Copy-Paste-Lösungen. Sie lösen keine Probleme von alleine. Sie funktionieren nicht immer in
jedem Fall.
Es gibt in drei Kategorien:
kreativ - Singleton, Builder, Factory, Abstract Factory, Dependency Injection
strukturell - Adapter, Brücke, Fassade, Dekorateur, Proxy, Fliegengewicht
Verhalten - Verantwortungskette, Befehl, Dolmetscher, Iterator, Strategie, Beobachter

https://refactoring.guru/design-patterns/creational-patterns
https://www.geeksforgeeks.org/__new__-in-python/

"""


class Singleton:
    _instance = None

    def __init__(self, name=''):
        print(f'Instance initialization name: {name}')
        self.name = name

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            print('Creating a new instance')
            cls._instance = object.__new__(cls)
        print('Returning existing instance')
        return cls._instance

if __name__ == "__main__":                                                                      # 2.1 19:00 15.09.2021
    obj_1 = Singleton("obj_1")
    print('obj_1 name: ', obj_1.name)
    obj_2 = Singleton("obj_2")
    print("id of obj_1: ", id(obj_1))
    print("id of obj_2: ", id(obj_2))
    print('obj_1 name: ', obj_1.name)
    print('obj_2 name: ', obj_2.name)

    obj_1 = Singleton("obj_1")
    obj_1.name = "obj_2"

""" O clasă poate avea doar o singură instanţă. Când creem obiecte noi ele reprezintă o referinţă la aceeaşi instanţă.
Nu se mai alocă memorie pentru ele."""
""" Se apelează mai întâi __new__. Am creat instanţa cu cls.instance, apoi o returnăm şi apoi abia se intră în __init__.
Ne aloca memorie pentru o noua instanță. Ce facem aici, este să ne asigurăm că alocăm memorie pentru instanța noastră,
doar o singura dată; la  instanțieri ulterioare, vom returna direct cls._instance care este deja creată.
Toate clasele de Python moștenesc din clasa object.
Cls. --- ne apelează secția de __new__ din clasa object.              New trebuie să apeleze instanţa creată de noi."""


# # # a) Alocăm acelaşi id pentru obiecte diferite. • We assign the same id for different objects.
# # # Wir weisen verschidenen Objekten die selbe Id zu. • Wij wijzen dezelfe id aan verschillende objecten toe.
#

# # ''' https://refactoring.guru/design-patterns/creational-patterns '''
# # ''' https://www.geeksforgeeks.org/__new__-in-python/ '''
# #
# # # # a) Alocăm acelaşi id pentru obiecte diferite. • We assign the same id for different objects.
# # # # Wir weisen verschidenen Objekten die selbe Id zu. • Wij wijzen dezelfe id aan verschillende objecten toe.
# #
# # # class MyClass:
# # #     def __init__(self):
# # #         pass
# # #
# # # my_obj_1 = MyClass()
# # # my_obj_2 = MyClass()
# # # my_obj_3 = my_obj_2
# # # print(id(my_obj_1))
# # # print(id(my_obj_2))
# # # print(id(my_obj_3))
# # # print(id(my_obj_1), id(my_obj_2), id(my_obj_3))
# #
# # '''O clasă care poate avea doar o singură instanţă.
# # Când creem obiecte noi ele reprezintă o referinţă la aceeaşi instanţă.
# # Nu se mai alocă memorie pentru ele.'''
# # class Singleton:
# #     _instance = None  # protected variable, a global one
# #
# #     def __init__(self, name=''):
# #         print(f'Instance initialization name: {name}')
# #         self.name = name
# #
# #
# #     def __new__(cls, *args, **kwargs):  # Se apelează mai întâi __new__. Am creat instanţa cu cls.instance,
# #                                         # apoi o returnăm şi apoi abia se intră îŋ __init__ .
# #         # ne aloca memorie pentru o noua instanta
# #         # ce facem aici, este sa ne asiguram ca alocam memorie pentru instanta noastra, doar o singura data
# #         # la  instantieri ulterioare, vom returna direct cls._instance care este deja creat
# #         if cls._instance is None:
# #             print('Creating a new instance')
# #             cls._instance = object.__new__(cls)   # Toate clasele de Pytho moştenesc din clasa object.
# #                                                   # Cls. --- ne apelează secţia de __new__ din clasa object.
# #                                                   # New trebuie să apeleze instanţa creată de noi.
# #
# #         print('Returning existing instance')
# #         return cls._instance
# #
# # # Obiectul obj_1 este cls.instance. Obj_2 apelează mai întâi pe __new__, dar cls.instance nu mai este in None,
# # # astfel returnează obiectul creat de mine anterior, de aceea obj_1 şi obj_2 pontează către aceeaşi adresăde
# # # memorie, apoi merge pe __init__ self.name = name. Se modifică numele instanţei anterioare.
# # if __name__ == "__main__":
# #     # putem considera ca obj_1 si obj_2 sunt aliasuri pentru acelasi obiect
# #     obj_1 = Singleton("obj_1")
# #     print('obj_1 name: ', obj_1.name)
# #     obj_2 = Singleton("obj_2")
# #     print("id of obj_1: ", id(obj_1))
# #     print("id of obj_2: ", id(obj_2))
# #     print('obj_1 name: ', obj_1.name)
# #     print('obj_2 name: ', obj_2.name)
# #
# #
# #     obj_1 = Singleton("obj_1")
# #     obj_1.name = "obj_2"
# #
# #
# #
# # # exemplu apelare metoda cu args si kwargs
# # # def my_sum(a, b):
# # #     return a + b
# # #
# # # print(my_sum(3, 4))
# # # print(my_sum(a=3, b=4))
