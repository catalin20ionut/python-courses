""" Builder is a very useful pattern, allowing us to construct objects from smaller pieces.Sometimes we need
to create many objects which behave in a similar way. Reusing code is a good idea. Builder helps us use common objects
and mix and match them. """


class Message:
    def __init__(self, text):
        self._text = text

    def __str__(self):
        return self._text


class Capitalized:
    def __init__(self, msg):
        self._msg = msg

    def __str__(self):
        return str(self._msg).capitalize()


class WithExclamation:
    def __init__(self, msg):
        self._msg = msg

    def __str__(self):
        return str(self._msg) + "!"


class WithQuestion:
    def __init__(self, msg):
        self._msg = msg

    def __str__(self):
        return str(self._msg) + "?"


m = Message("really")
happy = Capitalized(WithExclamation(m))
print(str(happy))  # sau print(str(Capitalized(WithExclamation(m)))) pentru rândurile 34 and 35
confused = Capitalized(WithQuestion(m))
happy_and_confused = Capitalized(WithQuestion(WithExclamation(m)))
print(str(confused))
print(str(happy_and_confused))
