""" Decoratorii adaugă funcționalități noi funcției fără să o schimbe. Decorators add new features to the function
without changing it. Dekorateure fügen der Funktion neue Features hinzu, ohne sie zu ändern."""
import datetime
import time


def my_decorator(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print("Time necessary to run our method: ", end - start)
        return result

    return wrapper


@my_decorator
def my_sum(a, b):
    return a + b


@my_decorator
def my_dif(a, b):
    return a - b


print(my_sum(3, 4))
print(my_dif(3, 4))


# 2) exemplu din trecut: decorator cu parametri conține parametru pentru decorator
def run_only_on_certain_second_5(seconds):
    # parametrul este funcția decorată
    def wrapper(func):
        # parametru este parametrul funcției decorate, poate fi și *args, **kwargs
        def wrap(name):
            second1 = None
            while True:
                now = datetime.datetime.now()
                if second1 != now.second:
                    print(now.second)
                    second1 = now.second
                if now.second == seconds:
                    func(name)
                    break

        return wrap

    return wrapper


@run_only_on_certain_second_5(45)
def activate_program(name):
    print(f"It's time! {name}")


activate_program("Alexandru")
