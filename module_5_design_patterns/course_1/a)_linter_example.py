"""
Commands in terminal:
git --version >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> version of git bash
pip install pylint >>>>>>>>>>>>>>>>>>>>>>>>> in case pylint is not installed
python -m pylint a)_linter_example.py >>>>>> applying pylint
"""
import time


class TimerDecorator:
    """
        Doing something
    """
    def __init__(self, func):
        """
            Constructor for doing this and that
        """
        self.times = []
        self.func = func

    def __call__(self, *args, **kwargs):
        """
            Class for doing this and that
        """
        start_time = time.time()
        result = self.func(*args, **kwargs)
        delta_time = time.time() - start_time
        self.times.append(delta_time)
        print(f"Function {self.func.__name__} has been executed in {delta_time} seconds with "
              f"the following arguments: {args} {kwargs}")
        return result

    def __str__(self):
        return"My obj"


@TimerDecorator
def calculate_product_a_million_times(small_number):
    """
        Constructor for doing this and that
    """
    for _ in range(1_000_000):
        large_number = 4 ** 39
        _ = large_number * small_number + small_number ** 39


if __name__ == """__main__""":
    calculate_product_a_million_times(10)
