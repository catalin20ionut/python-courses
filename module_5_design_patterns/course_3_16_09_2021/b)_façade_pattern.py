"""
Façade oferă o interfață care simplifică execuția unei operațiuni sau integrează mai multe interfețe într-una singură.
Façade provides an interface that simplifies the execution of an operation or integrates multiple interfaces into one.
Façade bietet eine Schnittstelle, die die Ausführung einer Operation vereinfacht oder mehrere Schnittstellen in eine
integriert. """


class Toast:
    def prepare(self, time):
        return f"Making toast. It will take {time}"


class Juice:
    def pour(self, glasses_nb):
        return f"Pouring {glasses_nb} glasses of juice."


class Eggs:
    def fry(self):
        return f"Frying eggs"


class AddIcedVodka:
    def pour_iced_vodka(self):
        return 'Pouring a big glass of iced vodka :D.'


class Breakfast:
    def __init__(self):
        # aici facem compunere
        self.toast = Toast()
        self.juice = Juice()
        self.eggs = Eggs()
        self.iced_vodka = AddIcedVodka()

    def make(self, time, glasses_nb):
        print(self.toast.prepare(time))
        print(self.juice.pour(glasses_nb))
        print(self.eggs.fry())
        print(self.iced_vodka.pour_iced_vodka())
        print("Breakfast is ready!")


if __name__ == "__main__":
    Breakfast().make("5 minutes.", 2)
