class ZeroToN:
    def __init__(self, n):
        self.n = n

    def __iter__(self):        # called when iterator is created; declarăm, inițializăm pe i care este contorul nostru
        self.i = -1
        return self

    def __next__(self):
        self.i += 1
        if self.i < self.n:
            return self.i
        else:
            raise StopIteration              # aruncăm eroarea aceasta pentru ca bucla for să știe când să se oprească


my_obj = ZeroToN(10)

print(my_obj)
for elem in my_obj:
    print(elem)

print('=========================================')

# my_range = range(10)
# my_range_iter = my_range.__iter__()
# print(my_range_iter.__next__().__next__())
# print(my_range_iter.__next__())
# print(my_range_iter.__next__())
