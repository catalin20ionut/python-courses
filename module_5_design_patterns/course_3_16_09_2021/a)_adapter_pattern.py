"""  Ne dă posibilitatea de a comunica cu clase care nu adaptate pentru acest lucru. E adesea folosit pentru a se adapta
la biblioteci externe care nu se pot modifica. ............................................. It gives us the opportunity
to communicate with classes that are not adapted for this. It is often used to adapt to external libraries that can't be
modified. .......................... Es gibt uns die Möglichkeit, mit Klassen zu kommunizieren, die dafür nicht geeignet
sind. Es wird häufig verwendet, um sich an externe Programmbibliotheken anzupassen, die nicht geändert werden können."""


class Container:
    def __init__(self, elem: int):
        self.elements = list(range(elem))

    def __repr__(self):                        # avem __repr__ aici, pentru ca mai jos sa putem printa lista de obiecte
        return f"<Container ({len(self.elements)} elem)>"


class DictAdapter:
    def __init__(self, d: dict):
        self.d = d

    def __repr__(self):
        return f"<Container ({len(self.d)} elem)>"

print(Container(30))
containers = [Container(10), Container(2), DictAdapter({"A": 1, "C": 45, "M": 47})]
print(containers)
