""" 7. Write a Python program to calculate the sum of the positive integers
of n+(n-2)+(n-4)... (until n-x =< 0) both recursive and iterative.
Test Data:
sum_series(6) -> 12
sum_series(10) -> 30 """


def sum_series_iterative_1(n):
    x = 2
    result = n

    while n - x > 0:
        result += (n - x)
        x += 2
    return result


def sum_series_iterative_2(n):
    result = 0
    while n > 0:
            result += n
            n -= 2
    return result


# n  = 5
# 5 + sum_series_recursive(3)
# 5 + 3 + sum_series_recursive(1)
# 5 + 3 + 1 + sum_series_recursive(-1)
# 9

def sum_series_recursive(n):
    if n <= 0:
        return 0
    else:
        return n + sum_series_recursive(n - 2)


print('Iterative version:')
print(sum_series_iterative_1(6))
# 10 + 8 + 6 + 4 + 2 = 30
print(sum_series_iterative_1(10))
# 11 + 9 + 7 + 5 + 3 + 1 = 36
print(sum_series_iterative_2(11))


print('Recursive version:')
print(sum_series_recursive(6))
print(sum_series_recursive(10))
print(sum_series_recursive(5))
