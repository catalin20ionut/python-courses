""" 6. Write a Python program for sequential search.
Sequential Search : In computer science, linear search or sequential search is a method
for finding a particular value in a list that checks each element in sequence until the
desired element is found or the list is exhausted. The list need not be ordered.
Test Data :
Sequential_Search([11, 23, 58, 31, 56, 77, 43,12, 65, 19], 31) -> (True, 3) """


def sequential_search(lst, nr):
    for index, elem in enumerate(lst):
        if elem == nr:
            return True, index
    return False, -1

print(sequential_search([11, 23, 58, 31, 56, 77, 43, 12, 65, 19], 91))


# varianta a-II-a
def sequential_search(my_list, searched_num):
    position = 0
    found = False
    while position < len(my_list) and not found:
        if my_list[position] == searched_num:
            found = True
        else:
            position += 1
    return found, position


print(sequential_search([11, 23, 58, 31, 56, 77, 43, 12, 65, 19], 1))
