""" 8. Write a Python program to calculate the value of 'a' to the power 'b' recursive and iterative.
Test Data :
(power(3,4) -> 81
a^0 = 1
1^a = 1
0^a = 0
Example: 3^4 = 3 * 3 * 3 * 3
Note: Find 2 or more possibilities to do this """


def power_iter_1(base, exponent):
    if exponent == 0:
        return 1
    elif base == 1:
        return 1
    elif base == 0:
        return 0
    else:
        result = base
        for i in range(exponent-1):
            result *= base
    return result

# sau


def power_iter_2(a, b):
    power = 0
    result = 1
    if b == 0:
        return 1
    if b == 1:
        return a
    while power < b:
        power += 1
        result = result * a
    return result


def power_rec(base, exponent):
    if exponent == 0:
        return 1
    elif base == 1:
        return 1
    elif base == 0:
        return 0
    else:
        return base * power_rec(base, exponent - 1)


print(power_iter_1(3, 4))
print(power_rec(5, 4))
print(power_iter_2(5, 2))
