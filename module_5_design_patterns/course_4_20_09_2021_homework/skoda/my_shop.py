from vehicle.skoda import Skoda

my_skoda = Skoda('black')
print(my_skoda)

print('Changing color: ')
my_skoda.color = 'blue'
print(my_skoda)

# print('Trying to change price: ')
# my_skoda.set_price(20_000)
# print(my_skoda)
# print(my_skoda.get_price())
#
#
# print('Trying to change price: ')
# my_skoda.set_price(27_000)
# print(my_skoda)
# print(my_skoda.get_price())

# =======================

# daca ati decomentat functiile cu @property si @price.setter, se lucreaza asa:
print('Trying to change price: ')
my_skoda.price = 20_000
print(my_skoda)
print(my_skoda.price)

print('Trying to change price: ')
my_skoda.price = 27_000
print(my_skoda)
print(my_skoda.price)
