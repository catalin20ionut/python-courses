class Skoda:
    def __init__(self, color: str):
        self.color = color
        # membru privat, poate fi modificat direct numai din interiorul clasei
        self.__price = 30_000

    # def set_price(self, new_price):
    #     if new_price >= 25_000:
    #         self.__price = new_price
    #
    # def get_price(self):
    #     return self.__price

    # In loc de set_price si get_price, putem folosi o alta versiune, mai Pythonica:
    @property
    def price(self):
        # echivalent cu get_price
        return self.__price

    @price.setter
    def price(self, new_price):
        # echivalent cu set_price
        if new_price >= 25_000:
            self.__price = new_price

    def __str__(self):
        return f'Color: {self.color}, price: {self.__price}'
