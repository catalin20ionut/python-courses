"""
9.Insert an element in a list at a certain index without using “insert” python function and then using python functions.
Test Data : insert_element([1, 2, 5, 6, 8, 0], 2, 3) → [1, 2, 3, 5, 6, 8, 0] """


def insert_number_in_list(my_list, index, nb):
    my_list_1 = my_list[0:index]
    my_list_1.append(nb)
    my_list_2 = my_list[index:]
    # my_new_list = my_list_1 + my_list_2
    # return my_new_list
    my_list_1.extend(my_list_2)
    return my_list_1
# index:   0  1  2  3  4  5
my_list = [1, 2, 5, 6, 8, 0]
x = insert_number_in_list(my_list, 2, 3)
print(x)


# varianta a-II-a
def insert_number_in_list_1(my_list, index, nb):                                        # lista inițială nu se modifică
    return my_list[:index] + [nb] + my_list[index:]                                     # sau ..........................


def insert_number_in_list_2(my_l, ind, nr):
    new_l = []
    for i in range(len(my_l)):
        if i == ind:
            new_l.append(nr)
        new_l.append(my_l[i])
    return new_l

# index:   0  1  2  3  4  5
my_list = [1, 2, 5, 6, 8, 0]
print('my_list: ', my_list)
print('insert_number_in_list_1: ', insert_number_in_list_1(my_list, 0, 99))           # aici se modifică lista inițială
my_list.insert(5, 778)
print(my_list)
print('insert_number_in_list_2: ', insert_number_in_list_2(my_list, 4, 99))
