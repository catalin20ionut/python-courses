import matplotlib.pyplot as plt
import geopandas as gpd
import cartopy.crs as ccrs

# Se folosește o proiecție specifică pentru hărți pentru a afișa harta lumii
fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree()})

# Se încarcă datele hărții lumii
world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))

# Se afișează harta lumii
world.plot(ax=ax, color='lightgray')

# Se configurează aspectul hărții
ax.set_title('Harta Lumii')
ax.set_extent([-180, 180, -90, 90], crs=ccrs.PlateCarree())
ax.coastlines()

# Se afișează harta
plt.show()
