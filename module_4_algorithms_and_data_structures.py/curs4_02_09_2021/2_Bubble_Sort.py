"""
https://www.youtube.com/watch?v=xli_FI7CuzA&ab_channel=MichaelSambol
https://www.youtube.com/watch?v=lyZQPjUT5B4&ab_channel=AlgoRythmics
Sortarea ----------------------------------------- înseamnă aranjarea într-o anumită ordine a elementelor dintr-o listă.
Bubble sort compară elementul din stânga cu cel din dreapta prin parcurgerea de la stânga la dreapta a listei. Dacă ele-
mentul din stânga este mai mare, atunci cele două elemente își inversează poziţiile. Dacă elementul din stânga este mai
mic, atunci elementul din dreapta continuă parcurgerea.
Bubble sort este de complexitate O(n^2) Quadratic Complexity. Complexitatea memoriei este O(1) complexitate fixă.   """
# index    0, 1, 2, 3, 4, 5, 6
# length   1, 2, 3, 4, 5, 6, 7
my_list = [2, 8, 5, 3, 9, 4, 1]


def bubble_sort(any_list):                               # if we want a separated list:  any_list_new = any_list.copy()
    n = len(any_list)
    for i in range(1, n):                                # i este lungimea (length)
        for j in range(0, n-1):                          # j este indexul (index)
            if any_list[j] > any_list[j+1]:
                # any_list[j], any_list[j+1] = any_list[j+1], any_list[j]  # 18th and 19th rows or the 20th - 20nd rows
                ''' See insertion sort!
                any_list[j], any_list[j - 1] = any_list[j - 1], any_list[j]
                j = j - 1 '''
                aux = any_list[j]
                any_list[j] = any_list[j+1]
                any_list[j+1] = aux
                # print("Evolution of the bubble sort in my_list: ", my_list)

print("My list before the bubble sort:", my_list)
bubble_sort(my_list)
print("My list after the bubble sort: ", my_list)


"""          Sorting means arranging elements in a certain order in a list. Bubble sort compares the left element to the
right element by traversing the list from left to right. If the element on the left is larger, then positions of the two
elements will be swapped. If the element on the left is smaller, then the element on the right continues the looping.
Bubble sort is O(n^2) Quadratic Complexity. Memory complexity is O(1) fixed complexity. 

      Sortierung bedeutet Anordnung der Elementen in einer Liste in einer bestimmten Reihenfolge. Bubble Sort vergleicht
das linke Element mit dem rechten Element, während die Liste von links nach rechts durchlaufen wird. Wenn das linke Ele-
ment größer ist, werden die Positionen der beiden Elemente vertauscht. Wenn das linke Element kleiner ist, setzt das 
rechte Element das Durchlauf der Schleife fort.
Bubble sort ist O(n^2) Quadratische Komplexität. Speicherkomplexität ist O(1) feste Komplexität.                     """