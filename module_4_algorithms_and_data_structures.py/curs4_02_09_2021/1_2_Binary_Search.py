""" Bisect ne ajută să aflăm indexul la care putem introduce un număr într-o listă astfel încât lista să rămână sortată.
Bisect nu trebuie instalat. """

import bisect
# index:   0  1  2  3  4  5  6
my_list = [1, 2, 2, 2, 4, 7, 9]

print(bisect.bisect(my_list, 3))
print(bisect.bisect_left(my_list, 2))
print(bisect.bisect_right(my_list, 2))
print(bisect.bisect_left(my_list, 3))
print(bisect.bisect(my_list, 15))
