""" ################################################## Binary Search ################################################
Binary search - se aplică pentru liste sortate. Computational complexity - logaritmic O(log n)"""

# index:     0  1  2  3  4  5   6   7   8   9  10  11  12  13  14  15  16
test_list = [1, 3, 4, 6, 7, 8, 10, 13, 14, 18, 19, 21, 24, 37, 40, 45, 71]


def binary_search(my_list, x):
    start = 0
    end = len(my_list) - 1

    while start <= end:
        mid = int((start + end) / 2)                                                # sau >>> mid = (start + end) // 2
        if my_list[mid] == x:
            return mid                                         # searched_number > element at mid, ignore the left half
        elif my_list[mid] > x:
            end = mid - 1                                                              # else, we ignore the right half
        else:
            start = mid + 1
    else:
        return 'The sought number was not found'
print("The sought number is at the index:", binary_search(test_list, 19))
