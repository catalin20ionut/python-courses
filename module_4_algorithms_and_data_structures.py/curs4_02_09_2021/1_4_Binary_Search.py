import bisect

# index:     0  1  2  3  4  5   6   7   8   9  10  11  12  13  14  15  16
test_list = [1, 3, 4, 6, 7, 8, 10, 13, 14, 18, 19, 21, 24, 37, 40, 45, 71]


def find(my_list, x):
    i = bisect.bisect_left(my_list, x)
    if i != len(my_list) and my_list[i] == x:
        return i

    raise ValueError('The number you were looking for was not found')
print(find(test_list, 10))
print(find(test_list, 12))                                                       # dă eroare fiindcă nu este in listă
