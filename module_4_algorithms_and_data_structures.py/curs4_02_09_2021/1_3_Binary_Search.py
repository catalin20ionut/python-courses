import bisect

# index:   0  1  2  3  4  5  6
my_list = [1, 2, 2, 2, 4, 7, 9]

my_str_list = ['aaa', 'c', 'd', 'g', 'z']

# obs: bisect.insort works with lists of: int and float numbers and strings

print(my_list)
bisect.insort(my_list, 3)
print(my_list)

bisect.insort_left(my_list, 2)
print(my_list)

bisect.insort_right(my_list, 2)
print(my_list)

print(my_str_list)
bisect.insort(my_str_list, 'aa')
print(my_str_list)

bisect.insort(my_str_list, 'J')
print(my_str_list)

bisect.insort(my_list, 7.8)
print(my_list)

bisect.insort(my_str_list, '0')
print(my_str_list)
