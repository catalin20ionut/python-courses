"""
https://www.youtube.com/watch?v=JU767SDMDvA&ab_channel=MichaelSambol
https://www.youtube.com/watch?v=ROalU379l3U&ab_channel=AlgoRythmics
    Lista se parcurge de la stânga la dreapta. Algoritmul constă în compararea fiecărui element din listă cu elementele
de la dreapta la stânga din stânga elementului care se compară. Elementele din stânga elementului care se compară sunt
aranjate în mod crescător. Elementul care se compară va fi introdus în poziția corectă astfel ca elementele până la ur-
mătorul element care se va compară să rămână în ordine crescătoare, adică elementul care se compară se mută la stânga
până ce se va găsi un element mai mic sau egal. Este asemănător cu modul în care oamenii își aranjează cărțile.
Insertion sort este de complexitate O(n^2) Quadratic Complexity. Complexitatea memoriei este O(1) complexitate fixă. """

my_list = [2, 8, 5, 700, 500, 400, 200, 300, 3, 9, 4, 1]


def insertion_sort(any_list):
    for i in range(1, len(any_list)):
        j = i
        while j > 0 and any_list[j-1] > any_list[j]:
            any_list[j], any_list[j-1] = any_list[j-1], any_list[j]
            j = j-1
            # print("Evolution of the insertion sort in my_list: ", my_list)
print("My list before the insertion sort:", my_list)
insertion_sort(my_list)
print("My list after the insertion sort: ", my_list)


''' Am observat că funcționează și dacă scriu codul de mai jos (fără j)
def insertion_sort(any_list):
    for i in range(1, len(any_list)):
        # j = i
        while i > 0 and any_list[i-1] > any_list[i]:
            any_list[i], any_list[i-1] = any_list[i-1], any_list[i]
            i = i-1
            # print("Evolution of the insertion sort in my_list: ", my_list)
print("My list before the insertion sort:", my_list)
insertion_sort(my_list)
print("My list after the insertion sort: ", my_list)
'''

"""
The list is traversed from left to right. The algorithm consists of comparing each element in the list with the elements
from right to left of the element that is compared. The elements that are in the left of the element that is compared 
are arranged in ascending order. The element that is compared will be inserted in the correct position so that the ele-
ments till to the next element that is to be compared will remain in ascending order, that means the element that is 
compared moves to the left until a smaller or equal element is found. It works similarly to the way in which the people 
arrange the playing cards in their hands.
Insertion sort is O(n^2) Quadratic Complexity. Memory complexity is O(1) fixed complexity.



Die Liste wird von links nach rechts durchlaufen. 
Jedes Element in der Liste wird mit den vorherigen Elementen verglichen. Der Vergleich erfolgt von rechts nach links des
Elements, das verglichen wird. Die Elemente, die sich befinden, links vom dem Element, das verglichen wird, sind in auf-
steigender Reihenfolge angeordnet. Unser Element wird an der richtigen Position eingefügt, so dass die Elemente bis zum
nächsten Element, das verglichen wird, in aufsteigender Reihenfolge bleiben. Das heißt, dass unser Element sich nach 
links bewegt, bis ein kleineres oder gleiches Element gefunden wird. Es ist ähnlich wie beim Sortieren von Spielkarten.
Insertion sort ist O(n^2) Quadratische Komplexität. Speicherkomplexität ist O(1) feste Komplexität.                 """