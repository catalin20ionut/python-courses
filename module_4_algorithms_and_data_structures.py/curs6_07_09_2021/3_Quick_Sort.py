""" Video prezentare algoritm: https://youtu.be/Hoixgm4-P4M
https://www.youtube.com/watch?v=ywWBy6J5gz8&ab_channel=AlgoRythmics
Este un algoritm recursiv. Computational complexity O(n * log n) - Linear Logarithmic Complexity. Este unul dintre cei
mai rapizi algoritmi de sortare.
Se selectează un pivot și se împarte lista in două subliste: elemente mai mici decât pivotul și elemente mai mari decât
pivot-ul. Se alege alt pivot si se reia funcția."""


def quick_sort(array, start, end):                                  # parametri: lista, index de început, index de final
    if start >= end:                                                            # condiția de terminare a recursivității
        return

    p = partition(array, start, end)                                                      # p vine din funcția partition
    quick_sort(array, start, p-1)
    quick_sort(array, p+1, end)


def partition(array, start, end):
    pivot = array[start]                                                       # pivot-ul este primul element din listă
    low = start + 1                                                                   # low este de la pivot in dreaptă
    high = end

    while True:
        # If the current value we're looking at is larger than the pivot
        # it's in the right place (right side of pivot) and we can move left,
        # to the next element.
        # We also need to make sure we haven't surpassed the low pointer, since that
        # indicates we have already moved all the elements to their correct side of the pivot
        while low <= high and array[high] >= pivot:
            high = high - 1

        # Opposite process of the one above
        while low <= high and array[low] <= pivot:
            low = low + 1

        # We either found a value for both high and low that is out of order
        # or low is higher than high, in which case we exit the loop
        if low <= high:
            array[low], array[high] = array[high], array[low]                                       # aici se face swap
            # The loop continues
        else:
            # We exit out of the loop
            break

    array[start], array[high] = array[high], array[start]

    return high

my_array = [2, 8, 5, 3, 9, 4, 1]
quick_sort(my_array, 0, len(my_array) - 1)
print(my_array)
