"""
https://www.youtube.com/watch?v=4VqmGXwpLqc&ab_channel=MichaelSambol

 Se împarte lista în mai multe subunități până când se ajunge la unități singulare care apoi se unesc din nou, sortate.
 Merge sort este un algoritm recursiv. Complexitate computațională ------ O(n * log n) - Linear Logarithmic Complexity.
 Complexitatea memoriei este O(n) -- complexitate lineară. Memoria folosită de algoritm crește proporțional cu dimensi-
 unea input-ului nostru. Nu este o funcție built-in. Putem instala biblioteci care au mai mulți algoritmi de sortare.
 De exemplu biblioteca Pysort: https://pypi.org/ .
 Despre stabilitatea algoritmilor: https://www.baeldung.com/cs/stable-sorting-algorithms
"""


def merge_sort(array, left_index, right_index):
    if left_index >= right_index:                                                               # condiția de terminare
        return

    middle = (left_index + right_index) // 2                               # împărțim lista in două (calculam mijlocul)
    merge_sort(array, left_index, middle)                                                    # pentru partea din stânga
    merge_sort(array, middle + 1, right_index)                                              # pentru partea din dreapta
    merge(array, left_index, right_index, middle)                                             # apelăm funcția de merge


def merge(array, left_index, right_index, middle):
    # Make copies of both arrays we're trying to merge
    # The second parameter is non-inclusive, so we have to increase by 1                     # se împarte lista în două
    left_copy = array[left_index:middle + 1]               # luăm până la middle+1 pentru că limita maximă este exclusă
    right_copy = array[middle + 1:right_index + 1]                                   # la fel și pentru right_index + 1

    # Initial values for variables that we use to keep
    # track of where we are in each array
    left_copy_index = 0
    right_copy_index = 0
    sorted_index = left_index

    # Go through both copies until we run out of elements in one
    while left_copy_index < len(left_copy) and right_copy_index < len(right_copy):

        # If our left_copy has the smaller element, put it in the sorted
        # part and then move forward in left_copy (by increasing the pointer)
        # Compară elementul de la index-ul [0] din lista din stânga cu elem[0] de la cea din dreapta.
        if left_copy[left_copy_index] <= right_copy[right_copy_index]:
            array[sorted_index] = left_copy[left_copy_index]
            left_copy_index = left_copy_index + 1
        # Opposite from above
        else:
            array[sorted_index] = right_copy[right_copy_index]
            right_copy_index = right_copy_index + 1

        # Regardless of where we got our element from
        # move forward in the sorted part
        sorted_index = sorted_index + 1

    # We ran out of elements either in left_copy or right_copy
    # so we will go through the remaining elements and add them
    # dacă rămân elemente nesortate fie în lista din stânga, fie in cea din dreapta
    while left_copy_index < len(left_copy):
        array[sorted_index] = left_copy[left_copy_index]
        left_copy_index = left_copy_index + 1
        sorted_index = sorted_index + 1

    while right_copy_index < len(right_copy):
        array[sorted_index] = right_copy[right_copy_index]
        right_copy_index = right_copy_index + 1
        sorted_index = sorted_index + 1


# index: 0  1  2  3  4  5  6
my_array = [2, 8, 5, 3, 9, 4, 1]
print("List before the merge sort:", my_array)
merge_sort(my_array, 0, len(my_array) - 1)
print("List after the merge sort: ", my_array)
