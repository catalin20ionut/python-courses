"""
https://www.youtube.com/watch?v=g-PGLbMth_g&ab_channel=MichaelSambol

Este un algoritm recursiv. Complexitatea este O(n^2) - Quadratic Complexity. Funcționează găsind cel mai mic element
din lista nesortată și îl mută la început.

În timpul fiecărei iterații se selectează cel mai mic element din compartimentul nesortat și se mută în compartimentul
sortat făcându-se schimb între elemente. Se ține evidența elementului minim curent și elementului curent.

During each iteration we'll select the smallest item from the unsorted partition and move it to the sorted partition.
The items will be swapped. We'll keep track of the current minimum and current item.                                """
my_list = [2, 8, 5, 3, 9, 4, 1]


def selection_sort(all_lists):
    for i in range(0, len(all_lists) - 1):
        j_min = i
        for j in range(i + 1, len(all_lists)):
            if all_lists[j] < all_lists[j_min]:
                j_min = j        # nu dăm return pentru că nu e necesar, funcția selection_sort modifică lista inițială
        if j_min != i:
            all_lists[i], all_lists[j_min] = all_lists[j_min], all_lists[i]

print("Initial list             :", my_list)
selection_sort(my_list)
print("List after selection sort:", my_list)
