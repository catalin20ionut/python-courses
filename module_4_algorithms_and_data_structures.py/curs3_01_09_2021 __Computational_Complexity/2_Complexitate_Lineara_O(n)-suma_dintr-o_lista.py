""" ##################################### Linear Complexity --- O(n) ##################################################
Timpul şi memoria cresc direct proporţional cu mărimea datelor introduse.
Time and/or memory increase in direct proportion to the size of data input.
Zeit und/oder Speicherplatz erhöhen direkt proportional zur Größe der Dateneingabe.
Exemplu de algoritm cu complexitate liniară: iteraţia într-o listă element cu element
                                             single iteration loop over list elements
                                             die einzelne Iterationsschleife über Listenelemente """

import random
import time


def calc_sum(given_array):
    my_sum = 0
    for elem in given_array:
        my_sum += elem
    return my_sum

my_array = [1, 2, 3, 4, 5, 6, 7]
print(calc_sum(my_array))

start_time = time.time()
[calc_sum(my_array) for i in range(40_000)]                    # asta l-am pus eu ca să fie tot cu 40000 de repetiţii
end_time = time.time()
print("Time elapsed for my_array: ", end_time-start_time)


random_array = [random.randrange(0, 10) for _ in range(40_000)]
random_array_2 = [random.randrange(0, 10) for _ in range(4_000_000)]

start_time_r = time.time()
print(calc_sum(random_array))
end_time_r = time.time()
print("Time elapsed for random array 1: ", end_time_r-start_time_r)

start_time_r2 = time.time()
print(calc_sum(random_array_2))
end_time_r2 = time.time()
print("Time elapsed for random array 2: ", end_time_r2-start_time_r2)
