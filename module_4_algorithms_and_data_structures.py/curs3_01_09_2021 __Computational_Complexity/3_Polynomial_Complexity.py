""" ######################## Polynomial Complexity O(n^c) ♦   Quadratic Complexity O(n^2) ##########################
Timpul de rulare creşte proporţional cu pătratul, cubul input-ului. ... The running time increases in proportion to the
square, the cube of the input. ........... Die Laufzeit erhöht proportional zum Quadrat, der dritten Potenz der Eingabe.
O(n^2)-------------------- Quadratic Complexity ------- bubble sort, insertion sort, selection sort ... a part of O(n^c)
O(n^c) ------------------ Polynomial Complexity ------------------------------------------------------------ nested sort
"""
import random
import time

my_2d_arr_1 = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]


def calc_sum(my_array):
    my_sum = 0
    for i in range(len(my_array)):
        for j in range(len(my_array[i])):
            my_sum += my_array[i][j]
    return my_sum
''' Calculează suma tuturor elementelor din şir.
len de my_2d_arr_1 este 3. << i >> se plimbă orizontal şi va fi pe rând:
<j> ia pe rând fiecare element a lui << i >>. Pentru i[0] va fi pe rând: 1, 2, 3 apoi j pt i[1] va fi 4,5,6 etc
adaugă la suma fiecare element începând cu index [0][0], [0][1] etc. '''
print("Suma este:", calc_sum(my_2d_arr_1))

random_array_1 = [[random.randrange(0, 10) for i in range(2_000)] for j in range(2_000)]
# print("2.:", random_array_1)                                                    # ca să vezi cum arată, pune range(10)

start_time = time.time()
end_time = time.time()
print("Time elapsed for 'my_2d_arr_1' is:", end_time-start_time)


start_time_r = time.time()
print("Sum of random_array_1 is:", calc_sum(random_array_1))
end_time_r = time.time()
print("Time elapsed for \"random_array_1\" is:", end_time_r-start_time_r)
