""" ##################################### COMPUTATIONAL COMPLEXITY / COMPLEXITATE DE CALCUL #######################
    Complexitatea computaţională este o metodă prin care comparăm eficienţa algoritmului. Măsoară timpul şi resursele de
memorie pe care un algoritm le va folosi pentru realizarea unei sarcini. Sunt trei cazuri: worst case, average case,
best case. Un site https://www.toptal.com/developers/sorting-algorithms

########################################################################################################################
                            Clasificarea algoritmilor în funcţie de complexitate:
                        O(1) < O(log n) < O(n) < O(n * log n) < O(n^2) < O(2^n) < O(n!)
O(1) ------------------------- Fixed Complexity ------------------------------ când luăm primul element dintr-un şir,
--------------------------------------------------------------------------- operaţia de adăugare, crearea unei variabile
O(log n) --------------- Logarithmic Complexity ---------------------------------------------------------- Binary search
O(n) ------------------------ Liniar Complexity --- iteraţia într-o listă element cu element (suma unei liste de numere)
O(n * log n) ---- Linear Logarithmic Complexity ------------------------------------------------- quick sort, merge sort
O(n^2)-------------------- Quadratic Complexity ------- bubble sort, insertion sort, selection sort ... a part of O(n^c)
O(n^c) ------------------ Polynomial Complexity ------------------------------------------------------------ nested sort
O(c^n) ----------------- Exponential Complexity ---------------------------------------------------- Fibonacci recursive
O(n!) -------------------- Factorial Complexity -------------------------------------------- Travelling salesman problem
########################################################################################################################
################################################# O(1) - Fixed Complexity ##############################################
Fixed Complexity O(1) --- Best case -------------------- Timpul de rulare şi memoria nu depind de input-ul din algoritm.
------------------------------ Time and memory do not depend on the amount of input data. (cantitatea de date introduse)
Exemple: --------------------------- când luăm primul element dintr-un şir, operaţia de adăugare, crearea unei variabile
------------------------------------------ getting the first element of an array, add operation, creating a new variable
"""
import random
import time


def do_nothing(my_array):
    a = 3                                                                                         # creating a variable
    return a, my_array[0]                                                   # returning the first element from an array

given_array = [1, 2, 3, 4, 5, 6, 7]
start_time = time.time()
[do_nothing(given_array) for _ in range(1000)]                                 # îi dăm să facă operaţia de 1000 de ori
end_time = time.time()
print('Time elapsed for given_array: ', end_time-start_time)

# Creăm un şir de 40000 de numere şi îl dăm ca parametru pentru a demonstra că nu creşte timpul de rulare în funcţie de
# dimensiunea lui.
random_array = [random.randrange(0, 10) for i in range(40_000)]
# print(random_array)

start_time_r = time.time()
[do_nothing(random_array) for _ in range(1000)]
end_time_r = time.time()
print("Time elapsed for random_array: ", end_time_r-start_time_r)
