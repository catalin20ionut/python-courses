# punem un counter ###
counter = 0                     # facem o variabila counter ca sa numaram totusi ca Python-ul face apel la


def inf_recursion(number):      # funcția recursiva de 995 de ori
    global counter              # pentru ca am definit variabila counter in afara functiei, ea nu o sa fie
    print(counter)              # găsită de Py când ii spunem sa o printeze decât daca ii spunem înainte
    counter += 1                # ca e <<global counter>>. Defapt global <<numele variabilei>>
    if number != 0:             # ex: pt o variabila cu numele << student >> am zice << global student >>
        inf_recursion(number)   # in interiorul funcției pt ca Py sa caute numele variabilei in afara ei
    return 0                    # ##########################################################################

inf_recursion(2)                # ################################################################################
