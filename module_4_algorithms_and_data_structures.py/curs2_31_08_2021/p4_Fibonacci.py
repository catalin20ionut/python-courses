""" Calculate recursively and iteratively the number in Fibonacci sequence at a certain position """
# position     0 1 2 3 4 5 6 7
# fib sequence 0 1 1 2 3 5 8 13

import sys
sys.setrecursionlimit(2000)


def fib_iterative(n):
    if n == 0:
        return 0
    first_no, second_no = 0, 1                      # putem să definim două variabile deodată dacă le scriem cu virgulă
    for i in range(2, n + 1):
        first_no, second_no = second_no, first_no + second_no
    return second_no


def fib_recursive(n):
    if n < 1:
        return 0
    if n == 1:
        return 1
    return fib_recursive(n-2) + fib_recursive(n-1)

print("Fibonacci iterative:", fib_iterative(0))
print("Fibonacci recursive:", fib_recursive(0))
print('Fibonacci iterative:', fib_iterative(1_000))
print('Fibonacci recursive:', fib_recursive(30))  # it lasts too long time for 1000

# ########################################## varianta 1 de la basic Python #############################################


def Fibonacci(n):
    if n < 0:
        print("Incorrect input")
    elif n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return Fibonacci(n-1)+Fibonacci(n-2)
print("Varianta 1_basic:", Fibonacci(0), "and", Fibonacci(10))


# ############################################# varianta 2 de la basic Python #########################################
def fibonacci(n):
    f = [0, 1]
    for i in range(2, n + 1):
        f.append(f[i - 1] + f[i - 2])
    return f[n]
print("Varianta 2_basic:", fibonacci(0), "and", fibonacci(10))
