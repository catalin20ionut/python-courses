def my_sum(a, b):
    return a + b


def my_sum_new(a: int, b: int) -> int: 		         # putem să îi dăm Python-ului Type hints: dacă introducem altceva,
    return a + b			                         # Python ne zice << expected int>>, dar va calcula rezultatul.


def my_sum_for_strings(a: str, b: str) -> str:     # este folositor pentru operator când citeşte codul si il foloseşte
    return a + b

print(my_sum(2, 3))
print(my_sum('a', 'b'))
print(type(my_sum_new(2, 3)))
print(my_sum_for_strings('a', 'b'))
