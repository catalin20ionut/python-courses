"""
3.
Initialize total to zero
Initialize counter to zero
Input the first grade
while the user has not as yet entered the sentinel
    add this grade into the running total
    add one to the grade counter
    input the next grade (possibly the sentinel)
if the counter is not equal to zero
    set the average to the total divided by the counter
    print the average
else
    print 'no grades were entered'
"""


print('variant 1')
total = 0
counter = 0
sentinel = 11

grade = int(input("Enter the grade: "))
while grade != sentinel:
    total += grade
    counter += 1
    grade = int(input("Enter the next grade: "))
if counter != 0:
    average = total / counter
    print(average)
else:
    print("No grades were entered !!!")


print('variant 2 you can also use "float" in lieu of "int"')
total = 0
counter = 0
sentinel = 11

try:
    grade = float(input("Enter the grade: "))
    while grade != sentinel:
        total += grade
        counter += 1
        grade = float(input("Enter the next grade: "))
    if counter != 0:
        average = total / counter
        print(average)
    else:
        print("No grades were entered !!!")
except ValueError:
    print("not a valid input.")
