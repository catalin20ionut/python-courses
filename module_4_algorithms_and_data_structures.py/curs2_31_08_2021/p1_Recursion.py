"""  Recursivitatea este un proces prin care o funcţie care se cheamă pe ea însăși
------------------- este formată de condiția de terminare și ce se întâmplă in funcţie cu apelul recursiv
In Python este un proces greoi si ocupă mult spațiu.

5! = 5 * 4 * 3 * 2 * 1                                     Final condition for factorial
n! = n * (n - 1) * (n - 2) * ... * 1                       0! = 1
n! = n * (n-1)!                                            1! = 1
Condiția de terminare în cazul factorialelor este când se ajunge la 1!
"""


# ############################################### Infinite loops ######################################################
import sys


def inf_recursion(number):      # dacă chemăm funcţia cu <<0>> nu se va întâmplă nimic, funcţia returnează 0
    if number != 0:             # dacă apelam funcţia cu orice alt număr in afara de 0, se apelează funcţia
        inf_recursion(number)   # la infinit și Python-ul sesisează infinite loop-ul si dă eroarea:
    return 0                    # << RecursionError: maximum recursion depth exceeded in comparison >>
                                # by default sunt 995 de apeluri recursive pe care Python poate să le facă
inf_recursion(0)                # limita poate fi marita la 999_999_999 de apeluri recursive prin

sys.setrecursionlimit(999_999_999)
""" importăm sys și setăm recursion limit la 999_999_999, care este maxim-ul posibil cu underscore putem delimita 
numerele în Python ca să fie mai ușor de citit """