""" Sunt 3 metode: metoda recursiva, metoda iterativa si recursiv dinamic (dynamic recursion) """
# am folosit https://pythontutor.com/visualize.html#mode=display ca sa analizam pas cu pas
# ############################# Calculul factorial prin metoda recursiva ##############################################
import sys
sys.setrecursionlimit(9_999_999)


def factorial(n):
    """Calculates factorial.
    Args:
        n: the natural number that is the input for the algorithm.
    Returns:
        factorial of number n.
    """                                                                                        # chemam funcția cu n = 5
    if n == 0:                                      # verifică daca n = 0, răspunsul este nu așa că trece în << else >>
        return 1
    else:                                    # și returnează 5; cheamă din nou funcția "factorial" cu n = n-1 adică ...
        return n * factorial(n-1)            # ......................... n = 4 și acum apelează funcția cu factorial(4)
print("recursive:", factorial(5))

# factorial(5)                    programul trebuie să "țină minte" valoarea de dinainte și să apeleze funcția .........
# 5 * factorial(4)                până ajunge la factorial de 0 adică 0!, apoi pornește de acolo înapoi si face calculul
# 5 * 4 * factorial(3)
# 5 * 4 * 3 * factorial(2)
# 5 * 4 * 3 *  2 * factorial(1)
# 5 * 4 * 3 *  2 *  1 * factorial(0)
# 5 * 4 * 3 *  2 *  1 * 1

# ################################## Calculul factorial prin metoda iterativa #########################################


def factorial(n):
    """Calculates factorial in an iterative manner.
         Args:
             n: the natural number that is the input for the algorithm.
         Returns:
             factorial of number n.
         """
    result = 1
    for i in range(1, n+1):                                              # range-ul este 1-5(inclusiv) și le ia pe rând.
        result *= i                                         # pentru i = 1 result = 1(result) înmulțit cu 1(i) apoi ...
    return result         # pentru i = 2 result va fi result = 1(result) * 2(i); pt i = 3 result = 2(result) * 3(i) etc.

print("iterative:", factorial(5))

""" Diferența dintre recursiv si iterativ:
metoda iterativă folosește o buclă (for, while) iar cea recursivă - funcția se apelează pe ea însăși
metoda recursiva foloseşte foarte multă memorie RAM aşa că durează mult timp (in Python); aceasta problema poate fi
rezolvată prin implementarea unei variante dinamice. În varianta dinamică, există un parametru numit << memory >>. Acest
parametru va ţine minte valorile deja calculate de către program. """

# ####################################################### Dynamic recursion ###########################################
"""Calculates a factorial by using dynamic programming.
         Args:
             n: the natural number that is the input for the algorithm.
             memory: the result dictionary will be updated with each function call.
         Returns:
             factorial of number n.
         """


def factorial(n, memory={0: 1, 1: 1}):
    if n in memory:                              # dacă n-ul pe care îl cerem noi când chemăm funcția există în memorie,
        return memory[n]                         # ...... o să returneze valoarea pentru key [n] din dicționarul memory
    else:                                        # dacă nu există încă:
        memory[n] = n * factorial(n-1)           # îl memorează în <<memory>> cu valoarea calculului n * cheamă din nou
        return memory[n]            # funcția cu valoarea n-1 și verifică din nou dacă există în memorie până când va da
                                    # de factorial(1) pe care îl are in dicționar, apoi completează dicționarul cu toate
print("dynamic recursive:", factorial(5))                                            # factorialele calculate până la 5


# O funcție recursivă cu adunare (adună toate numerele de la 0 până la parametrul dat); exercițiu de la Claudiu ########

def tri_recursion(k):
    if k > 0:                                           # condiția de terminare este ca parametrul sa fie mai mare ca 0
        result = k + tri_recursion(k-1)                 # funcția se cheamă pe ea însăși și decrementează parametrul ...
        print("A number in the array:", result)         # .................... cu 1 la fiecare iterație până ajunge la 0
    else:
        result = 0
    return result
print("Recursion Example Result:", tri_recursion(7))


def func(x):                                          # face același lucru ca cel de mai sus, dar iterativ, nu recursiv
    res = 0
    for i in range(x+1):
        res += i
    return res
print("Claudiu, metoda iterativă:", func(7))
