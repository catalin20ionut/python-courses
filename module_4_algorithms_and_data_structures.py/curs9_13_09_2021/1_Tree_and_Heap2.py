from heapq import heappush, heappop
import random

""" https://www.techbeamers.com/python-heapq/
https://www.w3schools.com/python/ref_random_seed.asp
https://www.geeksforgeeks.org/str-vs-repr-in-python/    difference between str and repr """
# tree = arbore
"""    A tree is a structure made of nodes. The first node of a tree is called root. The root has child nodes, which we 
call children. If the node has no children, it is called a leaf. The sequence connected by edges is called a path.   
• folosim seed() pentru a avea de fiecare dată aceeași listă generată random. Dacă nu folosim funcția asta, se ia timpul
 curent """

random.seed(10)


def heap_sort(iterable):                                                             # containerul pentru heap-ul nostru
    h = []
    for value in iterable:
        heappush(h, value)                                                              # adaugă câte un element in heap
        print('h: ', h)
    print('Heap is full :)')
    return [heappop(h) for i in range(len(h))]

random_list = random.sample(range(100), 10)
print('random_list: ', random_list)
sorted_list = heap_sort(random_list)
print('sorted_list', sorted_list)
