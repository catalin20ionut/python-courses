""" Queue sau deque este de tip FIFO (First In First Out): Primul element adăugat este și primul care iese. Funcțiile
folosite sunt .insert() și .pop(). Se mai folosesc și .appendleft() și .popleft(). Deque se consideră mai rapid decât
lista normală la operațiile de .append() si .pop()              vezi: https://www.geeksforgeeks.org/deque-in-python/ """


class MyQueue:
    def __init__(self, initial_list=None):
        if initial_list is None:
            initial_list = []
        self.queue = initial_list

    def insert(self, element):
        self.queue.append(element)

    def delete(self):
        return self.queue.pop(0)       # pop ia implicit ultimul index, noi am specificat de zero ca sa il ia pe primul

    def __str__(self):
        return f" The elements in the queue are:{self.queue}"

    def __len__(self):
        return len(self.queue)

    def is_empty(self):
        return False if self.queue else True


my_queue = MyQueue(['person_0'])
my_queue.insert('person_1')
my_queue.insert('person_2')
my_queue.insert('person_3')
print(my_queue)
print(my_queue.delete())  # afișează elementul scos
print(my_queue)
