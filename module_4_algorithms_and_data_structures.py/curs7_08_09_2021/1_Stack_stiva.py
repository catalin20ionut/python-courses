""" Stiva este de tip LIFO: Last In, First Out. I se mai spune LIFO Q
Funcția pentru a:
                -	introduce elemente in stiva este push()
                -	scoate elemente dintr-o stiva: pop()
  Nu există clasă desemnată direct in Python pentru stivă, folosim lista din Python ca sa “simulăm” funcționalitatea de
stivă: adăugăm elementele pe rând și le scoatem doar începând cu ultimul adăugat (exemplul folosit este o cutie în care
adăugăm farfurii: nu putem scoate prima farfurie pusă in cutie, trebuie să începem cu ultima adăugată)              """


class MyStack:
    def __init__(self, initial_list=None):
        if initial_list is None:
            initial_list = []
        self.stack = initial_list

    def push(self, element):
        self.stack.append(element)

    def pop(self):
        return self.stack.pop()

    def __str__(self):
        return f"the elements in the stack are: {self.stack}"

    def __len__(self):
        return len(self.stack)

    def is_empty(self):
        return False if self.stack else True


my_stack = MyStack(['plate_0', 'plate_00'])
my_stack.push('plate_1')
my_stack.push('plate_2')
my_stack.push('plate_3')
print(my_stack)
print(my_stack.pop())
print(my_stack)
my_stack2 = MyStack()
print(my_stack2.is_empty())
print(my_stack.is_empty())
my_stack2.push("blue_plate")
print(len(my_stack2))
