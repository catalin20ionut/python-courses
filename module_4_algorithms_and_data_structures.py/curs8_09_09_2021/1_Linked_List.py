class LinkedElement:
    def __init__(self, data, next=None):
        self.data = data
        self.next = next                                                 # next este o referință către următorul obiect.

    def __str__(self):
        next_str = str(self.next) if self.next else ""             # dacă există următorul element, elementul curent ...
        return f'{self.data} {next_str}'                           # ....................... are next, altfel next e nul


class LinkedList:
    def __init__(self):
        self.__first = None                                                                                    # privat

    def append(self, data: int):
        new_linked_element = LinkedElement(data)    # dacă lista este goală, noul element creat va deveni primul element
        if self.__first is None:
            self.__first = new_linked_element
        else:
            last_element = self.get_last_element()                                  # obținem ultimul element din listă
            last_element.next = new_linked_element           # face conexiunea dintre ultimul element găsit în listă ...
# ................................................................. și noul element creat prin next-ul ultimului element

    def get_last_element(self):                              # funcția care returnează ultimul element din listă
        if self.__first is None:
            return None
        current_element = self.__first
        while current_element.next is not None:
            current_element = current_element.next
        return current_element

# sau
#     def get_last_element(self):
#         if self.__first is None:
#             return None
#         current_element = self.__first
#         while True:
#             if current_element.next is None:
#                 return current_element
#             current_element = current_element.next

    def __str__(self):
        return str(self.__first)

# pseudocod pentru funcția de __len__()
# linked_list_obj
# current_element = linked_list_obj.first_element
# counter = 1
# while current_element.next != None:
# increment counter
# current_element = current_element.next
# return counter

    def __len__(self):
        if self.__first is None:
            return 0
        current_element = self.__first
        counter = 1
        while current_element.next is not None:
            counter += 1
            current_element = current_element.next
        return counter

    def delete(self):                                         # funcția delete șterge primul element din listă când  ...
        if self.__first is None:                              # ... este apelată ca să păstreze structura de linked list
            return            # dacă nu există nici un element în listă nu se întâmplă nimic, iese din funcția .delete()
        self.__first = self.__first.next    # dacă nu e goală: primul element devine următorul element și primul dispare

    def extend(self, other_linked_list):                                       # funcție care unește doua "linked lists"
        if self.__first is None:
            self.__first = other_linked_list.get_first_element()
        last_element = self.get_last_element()                                     # obținem ultimul element din lista 1
        last_element.next = other_linked_list.get_first_element()            # next-ul ultimului element din lista 1 ...
# ............................................ devine primul element din lista 2 obținut cu funcția .get_first_element()

    def get_first_element(self):    # facem o funcție care returnează primul element pentru că __first l-am făcut privat
        return self.__first

my_linked_list = LinkedList()
my_linked_list.append(1)
my_linked_list.append(2)
my_linked_list.append(3)
print('my_linked_list: ', my_linked_list)
print('Length of the list: ', len(my_linked_list))
my_linked_list.delete()
print('After deleting first element:')
print('my_linked_list: ', my_linked_list)
print('Length of the list: ', len(my_linked_list))

my_new_linked_list = LinkedList()
my_new_linked_list.append(4)
my_new_linked_list.append(5)
print('my_new_linked_list: ', my_new_linked_list)

my_linked_list.extend(my_new_linked_list)
print('my_linked_list: ', my_linked_list)
