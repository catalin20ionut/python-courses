class DoublyLinkedElement:
    def __init__(self, data, prev=None, next=None):
        self.data = data
        self.next = next                                                  # next este un alt obiect de tip LinkedElement
        self.prev = prev

    def __str__(self):
        prev_str = "<->" if self.prev else ""
        next_str = str(self.next) if self.next else ""
        return f'{prev_str} {self.data} {next_str}'


class DoublyLinkedList:
    def __init__(self):
        self.__first = None

    def append(self, data: int, ):
        new_linked_element = DoublyLinkedElement(data)
        if self.__first is None:
            self.__first = new_linked_element       # dacă lista este goală, noul element creat va deveni primul element
        else:
            last_element = self.get_last_element()
            last_element.next = new_linked_element
            new_linked_element.prev = last_element                                # am obținut ultimul element din listă

    def get_last_element(self):
        if self.__first is None:
            return None
        current_element = self.__first
        # while True:
        #     if current_element.next is None:
        #         return current_element
        #     current_element = current_element.next
        # sau
        while current_element.next is not None:
            current_element = current_element.next
        return current_element

    def __str__(self):
        return str(self.__first)

    # pseudocod pentru funcția de __len__()
    # linked_list_obj
    # current_element  = linked_list_obj.first_element
    # counter = 1
    # while current_element.next != None:
    #    increment counter
    #    current_element = current_element.next
    # return counter

    def __len__(self):
        if self.__first is None:
            return 0
        current_element = self.__first
        counter = 1
        while current_element.next is not None:
            counter += 1
            current_element = current_element.next
        return counter

    def delete(self, data: int):         # am dat ca parametru data ca să putem șterge elementul cu data pe care îl vrem
        if self.__first is None:
            return
        if data == self.__first.data:                                # dacă elementul pe care il vrem șters este primul:
            print("Deleting the first element!")                     # ca să se șteargă primul element, îi spunem că ...
            self.__first = self.__first.next                         # ................. noul first e ce era next-ul lui
            self.__first.prev = None                           # trebuie să spunem că primul element nu mai are previous
        # dacă vrem să putem șterge orice element din Double Linked List(în funcție de data pe care îl dăm ca parammetru
        else:
            current_element = self.__first                                # mergem din element în element până găsim ...
            # ........... elementul al cărui data este cel pe care il vrem șters, dat ca parametru când apelăm .delete()
            while True:                                                           # (73) am găsit ......................
                if current_element is not None and current_element.data == data:  # .... elementul pe care îl vrem șters
                    if current_element.next:             # dacă nu e ultimul element, raportăm totul la elementul curent
                        current_element.prev.next = current_element.next
                        current_element.next.prev = current_element.prev
                        break  # trebuie să punem break ca să se oprească din while atunci când a șters elementul căutat
                    else:                                  # dacă e ultimul element din listă, cel pe care îl vrem șters
                        current_element.prev.next = None
                        break
                if current_element is None:
                    raise ValueError('Element is not in our list!!')
                current_element = current_element.next

    def extend(self, other_linked_list):
        if self.__first is None:
            self.__first = other_linked_list.get_first_element()
        last_element = self.get_last_element()
        last_element.next = other_linked_list.get_first_element()
        last_element.next.prev = last_element

    def get_first_element(self):
        return self.__first


my_linked_list = DoublyLinkedList()
my_linked_list.append(1)
my_linked_list.append(2)
my_linked_list.append(3)

print('my_linked_list: ', my_linked_list)
print('Length of the list: ', len(my_linked_list))

my_linked_list.delete(1)
print('\nAfter deleting first element: ')
print('my_linked_list: ', my_linked_list)
print('Length of the list: ', len(my_linked_list), '\n')

my_new_linked_list = DoublyLinkedList()
my_new_linked_list.append(4)
my_new_linked_list.append(5)
print('my_new_linked_list: ', my_new_linked_list)

my_linked_list.extend(my_new_linked_list)
print('my_linked_list: ', my_linked_list)

my_linked_list.delete(2)
print(my_linked_list)
my_linked_list.delete(4)
print(my_linked_list)
my_linked_list.delete(5)


print(my_linked_list)
my_linked_list.delete(10)         # programul va arunca o eroare; e normal, deoarece vrem să ștergem un număr pentru ...
# ...... care nu avem element corespunzător în listă. Codul erorii va apărea la început, înaintea print-urilor, deși ...
# ............................................................................... eroarea se aruncă după acele printuri.
