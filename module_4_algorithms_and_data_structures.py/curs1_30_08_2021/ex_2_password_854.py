print("variant 1")
a = int(input("Read code: "))
while a != 854:
    a = int(input("Read code: "))
print("code accepted")

print("variant 2 is quite strange")
a = int(input("Read code: "))
while a != 854:
    a = int(input("Read code: "))
    if a == 854:
        print("code accepted")

print("variant 3a strange, inelegant")
a = input()
while a:
    if a == '854':
        print("code accepted")
        break
    else:
        a = input()

print("variant 3b strange")
a = int(input("Read code: "))
while a:
    if a == 854:
        print("code accepted")
        break
    else:
        a = int(input("Read code: "))
