MORSE_CODE = {'.-': 'A', '-...': 'B', '-.-.': 'C', '-..': 'D',
              '.': 'E', '..-.': 'F', '--.': 'G', '....': 'H', '..': 'I',
              '.---': 'J', '-.-': 'K', '.-..': 'L', '--': 'M', '-.': 'N',
              '---': 'O', '.--.': 'P', '--.-': 'Q', '.-.': 'R', '...': 'S',
              '-': 'T', '..-': 'U', '...-': 'V', '.--': 'W',
              '-..-': 'X', '-.--': 'Y', '--..': 'Z', '-----': '0', '.----': '1',
              '..---': '2', '...--': '3', '....-': '4', '.....': '5', '-....': '6',
              '--...': '7', '---..': '8', '----.': '9', '.-.-.-': '.', '--..--': ',', '..--..': '?',
              '.----.': "'", '-.-.--': '!', '-..-.': '/', '-.--.': '(', '-.--.-': ')', '.-...': '&',
              '---...': ':', '-.-.-.': ';', '-...-': '=', '.-.-.': '+', '-....-': '-', '..--.-': '_',
              '.-..-.': '"', '...-..-': '$', '.--.-.': '@', '...---...': 'SOS'}

# 1 spațiu, separăm literele
# 3 spații, separam cuvintele


def decode_morse(morse_code):
    morse_code = morse_code.strip()                                                     # am împărțit per cuvinte morse
    morse_words = morse_code.split('   ')
    decoded_str = ''                                                                      # parcurgem per cuvinte morse
    for morse_word in morse_words:
        morse_letters = morse_word.split(' ')                                # parcurgem per literele unui cuvânt morse
        for morse_letter in morse_letters:
            letter = MORSE_CODE[morse_letter]                           # sau >>> letter = MORSE_CODE.get(morse_letter)
            decoded_str += letter
        decoded_str += ' '

    decoded_str = decoded_str.strip()
    return decoded_str


def decode_morse_2(morse_sequence):
    words = []                                                                            # parcurgem per cuvinte morse
    for morse_word in morse_sequence.split('   '):
        word = ''.join(MORSE_CODE.get(morse_char, '') for morse_char in morse_word.split(' '))
        if word:
            words.append(word)
    return ' '.join(words)


print('variant 1:', decode_morse('.... . -.--   .--- ..- -.. .'))
print('variant 2:', decode_morse_2('.... . -.--   .--- ..- -.. .'))
