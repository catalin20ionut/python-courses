"""https://pythontutor.com/visualize.html#mode=edit"""

""" •  Un algoritm este o formulă pentru rezolvarea unei probleme de calcul. Un algoritm reprezintă o serie de operații 
efectuate folosind diferite structuri de date pentru a rezolva o problemă de programare................ An algorithm is 
a formula for solving a computational problem. An algorithm represents a series of operations carried out using various
data structures to solve a programming problem. ...................... Ein Algorithmus ist eine Formel zur Lösung eines 
Rechenproblems. Ein Algorithmus repräsentiert eine Reihe von Operationen unter Verwendung verschiedener Datenstrukturen 
durchgeführt, um ein Programmierproblem zu lösen. 
Pașii pentru realizarea unui algoritm:                                       The procedure for developing an algorithm:
1. Formularea problemei sau sarcinii                                                      1. Problem / task formulation
2. Determinarea datelor de intrare                                                        2. Determining the input data
3. Determinarea rezultatului                                                                  3. Determining the result
4. Găsirea unei metode pentru a finaliza sarcina                               4. Finding a method to complete the task
5. Exprimarea algoritmului prin utilizarea metodei selectate   5. Expressing the algorithm by using the selected method
6. Analiza si testarea algoritmului                                              6. Analysing and testing the algorithm
7. Evaluarea eficacității algoritmului                                 7. Evaluating the effectiveness of the algorithm

Die Schritte zur Entwicklung eines Algorithmus:
1. Formulierung des Problems oder der Aufgabe 
2. Bestimmung der Eingangsdaten
3. Ermittlung des Ergebnisses
4. Finden eines Weges, um die Aufgabe zu beenden (auszuführen, abzuschließen)
5. Ausdrücken des Algorithmus unter Verwendung des ausgewählten Verfahrens
6. Algorithmenanalyse und -Test
7. Bewertung der Wirksamkeit des Algorithmus

1. determinarea a ceea ce vrem să facă algoritmul nostru (sortarea unui șir de numere în ordine crescătoare)
2. va fi o listă cu numere aleatoare, random
3. va fi o listă cu numere sortate
4. vom găsi o metodă de sortare (Bubble Sort, Insertion Sort, Merge Sort, Selection Sort, Quick Sort etc)
5. scriem codul (de Python)
6. 7. ------

Un algoritm poate fi reprezentat prin mai multe feluri:
a) limbajul natural,
b) cod,
c) pseudocod,
d) diagrama bloc.

Algorithms can be expressed in many types:        Algorithmen können in vielen Arten ausgedrückt werden, einschließlich:
a) natural language (verbal description),                                a) Natürliche Sprache (mündliche Beschreibung),
b) code,                                                                                       b) (der) Programmiercode,
c) pseudocode,                                                                                            c) Pseudocode,
d) block diagram. [daı'əgræm]                                                                    d) (das) Blockdiagramm.

Pseudocodul este o combinație între limbajul natural și cod. .............................. Pseudocode is a combination 
of natural language and code. ........................ Pseudocode ist eine Kombination von natürlicher Sprache und Code.
1. If student's grade is greater than or equal to 60
    Print "passed"
else
    Print "failed"

grade = 100
if grade >= 60:
    print("passed")
else:
    print("failed") ... Output: passed
    
2. Set total to zero
Set grade counter to one
While grade counter is less than or equal to ten
    Input the next grade
    Add the grade into the total
Set the class average to the total divided by ten
Print the class average.

total = 0
grade_counter = 1
while grade_counter <= 10:
    grade = int(input("Enter the next grade: "))
    total += grade
    grade_counter += 1
class_average = total / 10
print(class_average)


O diagramă bloc este o reprezentare grafică a unui algoritm pentru o anumită sarcină. Ne ajută să citim instrucțiunile
și să determinăm fluxul algoritmului. .............................................................. A block diagram is 
a graphic representation of an algorithm for a specific task.It help us to read the instructions and determine the flow 
of the algorithm. .................................. Ein Blockdiagramm ist eine grafische Darstellung eines Algorithmus 
für eine bestimmte Aufgabe. Es hilft uns, die Anweisungen zu lesen und wir können den Ablauf des Algorithmus bestimmen.

• Cercurile marchează începutul şi finalul algoritmului. ........................... The circles indicate the beginning
and end of the algorithm. ................................ Die Kreise zeigen den Anfang und das Ende des Algorithmus an.
• Săgeata sau conectorul indică ordinea în care instrucţiunile se execută. .............................................
............ The arrow or the connector indicates the order in which the instructions will be executed in the algorithm. 
......... Der Pfeil oder der Verbinder gibt die Reihenfolge an, in der die Anweisungen im Algorithmus ausgeführt werden.
• Caseta de operaţii (dreptunghi) în care se stochează instrucţiuni. .......... The operation (rectangle [rεҝȶ'æŋgl] box
 that stores instructions. ................................ Die Operationsbox (das Rechteck), die Anweisungen speichert.
• Caseta de intrare și ieșire (paralelogram) se foloseşte pentru introducerea și ieșirea datelor. ..................... 
...................... The input and output box (parallelogram [pærә'lεlәgræm]) serves for the input and output of data.
.................................... Das Ein- und Ausgabefeld (das Parallelogramm) dient der Ein- und Ausgabe von Daten.
• Caseta condițională (romb) verifică dacă condiția este adevărată sau falsă. ..........................................
.................................... Conditional box (rhombus [rɔmbәs]) verifies whether the condition is true or false.
........................ Bedingungskaste (die Raute oder das Rhombus) überprüft, ob die Bedingung wahr oder falsch ist.
"""

""" The lines below are from  https://www.unf.edu/~broggio/cop2221/2221pseu.htm 
An algorithm is a procedure for solving a problem in terms of the actions to be executed and the order in which 
those actions are to be executed. An algorithm is merely the sequence of steps taken to solve a problem. The steps are 
normally "sequence," "selection, " "iteration," and a case-type statement.

In C, "sequence statements" are imperatives. The "selection" is the "if then else" statement, and the iteration is
satisfied by a number of statements, such as the "while," " do," and the "for," while the case-type statement is 
satisfied by the "switch" statement.

Pseudocode is an artificial and informal language that helps programmers develop algorithms. Pseudocode is a 
"text-based" detail (algorithmic) design tool.

The rules of Pseudocode are reasonably straightforward. All statements showing "dependency" are to be indented. 
These include while, do, for, if, switch. Examples below will illustrate this notion.

The following examples will be used as exercises:

1. If student's grade is greater than or equal to 60

    Print "passed"
else
    Print "failed"

2. Set total to zero
Set grade counter to one
While grade counter is less than or equal to ten
    Input the next grade
    Add the grade into the total
Set the class average to the total divided by ten
Print the class average.

3.
Initialize total to zero
Initialize counter to zero
Input the first grade
while the user has not as yet entered the sentinel
    add this grade into the running total
    add one to the grade counter
    input the next grade (possibly the sentinel)
if the counter is not equal to zero
    set the average to the total divided by the counter
    print the average
else
    print 'no grades were entered'

4.initialize passes to zero
initialize failures to zero
initialize student to one
while student counter is less than or equal to ten
    input the next exam result
    if the student passed
        add one to passes
    else
        add one to failures
add one to student counter
print the number of passes
print the number of failures
if eight or more students passed
    print "raise tuition"


                                        Some Keywords That Should be Used

For looping and selection, The keywords that are to be used include Do While...EndDo; Do Until...End do; Case...EndCase;
If...Endif; Call ... with (parameters); Call; Return ....; Return; When; Always use scope terminators for loops and
iteration. As verbs, use the words Generate, Compute, Process, etc. Words such as set, reset, increment, compute,
calculate, add, sum, multiply, ... print, display, input, output, edit, test , etc. with careful indentation tend to
foster desirable pseudocode.

Do not include data declarations in your pseudocode."""
