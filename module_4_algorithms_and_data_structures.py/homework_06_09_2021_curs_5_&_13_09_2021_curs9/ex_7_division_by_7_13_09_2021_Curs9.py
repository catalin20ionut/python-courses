'''7. Write a program in which:
-  Write a method in which we generate first m numbers divisible by n. In this case, we want to get in a list and print the first 500 numbers divisible by 7.
-  save all of those numbers in a file, one number per each row. The file will be called „file_numbers.txt"
In the next steps, we're interested in using as little memory as possible.
-  create a generator which does exactly the same thing as above: generate the first 500 numbers divisible by 7 (the number of numbers generated is parametrizable) and save it to a file called "file_numbers_new.txt"
-  read the numbers from file ("file_numbers_new.txt") and compute their average without keeping the entire list in memory.
'''

def generate_numbers(total_n, nr):
    my_list = []
    num = 0
    counter = 1
    while counter <= total_n:
        if num % nr == 0:
            my_list.append(num)
            counter += 1
        num += 1

    return my_list


def numbers_generator(total_n, nr):
    num = 0
    counter = 1
    while counter <= total_n:
        if num % nr == 0:
            yield num
            counter += 1
        num += 1


# primul subpunct + al doilea
my_list_for_file = generate_numbers(500, 7)
print('my_list_for_file: ', my_list_for_file)

with open("file_numbers.txt", "w+") as file:
    for elem in my_list_for_file:
        file.write(f"{elem} \n")

# al treilea subpunct
with open("file_numbers_new.txt", "w+") as file:
    for elem in numbers_generator(500, 7):
        file.write(f"{elem} \n")


with open("file_numbers_new.txt", "r") as file_object:
    iterable_object = []
    nb_sum = 0
    nb_cnt = 0
    for element in file_object:
        numbers = int(element)
        nb_sum += numbers
        nb_cnt += 1
    average = nb_sum / nb_cnt
    print(average)