""" 6. Write the elements of a list to a file in the following way: each row will contain one number from the list. Do
 this thing both in an iterative and recursive way.
Method calls :                                                    write_elem_using_recursion(filename, [1, 2, 5, 6, 8])
                                                                   write_elem_iterative(filename, [1, 2, 5, 6, 8]) """
test_list = [1, 2, 5, 6, 8]


def write_elem_iterative(filename, my_list):
    with open(f"{filename}", "w") as f:
        for elements in my_list:
            f.write(str(f"{elements}\n"))


# using recursion:
def save(value, output=''):
    if len(value) == 0:
        return output
    else:
        element = value[0]
        if type(element) == int:
            output += f"{str(element)} \n"

        remaining_val = value[1:]
        return save(remaining_val, output=output)


def write_elem_using_recursion(filename, mylist):
    with open(f"{filename}.txt", "w") as f:
        f.write(save(mylist))


# sau

def write_elem_recursively(f, my_list):
    if len(my_list) == 0:
        return
    f.write(str(my_list[0]) + '\n')
    my_list.pop(0)
    write_elem_recursively(f, my_list)


def write_elem_using_recursion_1(filename, my_list):
    with open(filename, 'w') as f:
        write_elem_recursively(f, my_list)


filename_1 = 'iterative.txt'
filename_2 = 'recursive.txt'
filename_3 = 'recursive_1.txt'
write_elem_iterative(filename_1, test_list)
write_elem_using_recursion(filename_2, test_list)
write_elem_using_recursion_1(filename_3, test_list)
