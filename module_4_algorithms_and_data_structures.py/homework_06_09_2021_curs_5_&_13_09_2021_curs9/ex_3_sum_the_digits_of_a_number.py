'''3.  Write a Python program to get the sum of a non-negative integer
(there are multiple ways to do that).
Test Data:
sum_digits(345) -> 12
sum_digits(45) -> 9
'''
# ######### the first way -- a
number = 253
sum_of_digits = 0
for digit in str(number):
  sum_of_digits += int(digit)

# ########### the first way --- b
number = 253
sum_of_digits = 0
sum_of_digits = sum(int(digit) for digit in str(number))
print(sum_of_digits)


def sum_digits(number):
  total = 0
  for char in str(number):
    total += int(char)
  return total


# sau
# 345
def sum_digits_with_wile_loop(n):
  s = 0
  while n:
    s += n % 10
    n //= 10
  return s


# sau
number = 345
sum_of_digits = sum(int(digit) for digit in str(number))
print(sum_of_digits)

print(sum_digits(345))

test_data = str(input("Enter three digits:   ") or "427")


# sau

def sum_digits_1(digits):
  sum_list = []
  total_sum = 0
  digits = digits[0] + "" + digits[1] + "" + digits[2]
  for elem in digits:
    sum_list.append(int(elem))
  for j in sum_list:
    total_sum += j
  return total_sum


# sau

def sum_digits_recursive(n):
  if n == 0:
    return 0
  else:
    return n % 10 + sum_digits_recursive(int(n / 10))


# sau

def sum_digits1(nr):
  nr = str(nr)
  if nr == '':
    return 0
  else:
    return int(nr[0]) + sum_digits1(nr[1:])


# 5 + sum_digits_recursive(34)
# 5 + 4 + sum_digits_recursive(3)
# 5 + 4 + 3
print(sum_digits(345))
print(sum_digits_with_wile_loop(56))
print(sum_digits_1(test_data))
print(sum_digits(45))
print(sum_digits_recursive(345))
print(sum_digits1(345))