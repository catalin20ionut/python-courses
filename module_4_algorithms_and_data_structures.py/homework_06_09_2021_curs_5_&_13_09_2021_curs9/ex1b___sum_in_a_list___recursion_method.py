""" ------------------------- sum in a list using the recursion method with a parameter --------------------------- """
import math


def sum_in_a_list_recursion_method(any_list):
    if len(any_list) == 0:
        return 0
    else:
        return any_list[0] + sum_in_a_list_recursion_method(any_list[1:])
my_list = [1, 5, 9]
print("1. The sum of my_list in the recursion_method is:", sum_in_a_list_recursion_method(my_list))


# the second way to write omitting else:
def sum_in_a_list_recursion_method(other_list):
    if len(other_list) == 0:
        return 0
    return other_list[0] + sum_in_a_list_recursion_method(other_list[1:])
my_list2 = [1, 5, 9]
print("2. The sum of my_list2 in the recursion_method is:", sum_in_a_list_recursion_method(my_list2))


# the third way to write using lambda
# Ich habe schlecht geschrieben, und habe ich beobachtet, dass ich anstatt "len(lists)" auch "lists" schreiben kann.
def sum_in_a_list_recursion_method(lists):
    return lists[0] + sum_in_a_list_recursion_method(lists[1:]) if len(lists) else 0
my_list3 = [1, 5, 9]
print("3. The sum of my_list3 in the recursion_method is:", sum_in_a_list_recursion_method(my_list3))


# the fourth method ››› using two parameters
def sum_in_a_list(the_list, i):
    if i == 0:
        return the_list[i]
    else:
        return the_list[i] + sum_in_a_list(the_list, i - 1)

my_list4 = [1, 5, 9]
print("4. The sum of my_list4 in the recursion_method is:", sum_in_a_list(my_list4, len(my_list4)-1))

# the fifth method
my_list5 = [1, 2, 3, 4, 5, 6]
print("5. The sum of my_list5 is:", sum(my_list5))

# the sixth way, import math was made; it's at the top of the file
my_numbers = [1, 2, 3, 4, 5, 6]
print("6a. The sum of my_numbers is:", math.fsum(my_numbers))
print("6b. The sum of my_numbers is:", int(math.fsum(my_numbers)))


