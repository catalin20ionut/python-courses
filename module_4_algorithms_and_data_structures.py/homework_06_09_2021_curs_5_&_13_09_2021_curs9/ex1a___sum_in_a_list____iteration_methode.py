"""
1. Write a Python program (recursive and iterative) to calculate the sum of a list of numbers by hand and using
Python functions. Test Data: [1, 5, 9]

---------------------------- sum in a list using the iteration method with a parameter --------------------------- """

# input
my_list = []
n = int(input("Enter number of elements: "))
print("My elements are: ")

for i in range(0, n):
    elements = int(input())
    my_list.append(elements)
print("my_list is:", my_list)


def sum_in_a_list_in_iteration_method(any_list):
    total1 = 0
    for val in any_list:
        total1 += val
    return total1
print("1. The sum of my_list in the iteration method is:", sum_in_a_list_in_iteration_method(my_list))


# the second way when the input is not used
def my_list_in_iteration_method(a_list):
    total2 = 0
    for val in a_list:
        total2 += val
    return total2
''' return total, for val in my_list (total is total + val) ---- Ich muss die Loesung unbedigt finden'''
my_list2 = [1, 5, 9]
print("2. The sum of my_list2 in the iteration method is:", my_list_in_iteration_method(my_list2))


# the third way "while loop" and without function
my_list3 = [1, 5, 9]
total3 = 0
i = 0
while i < len(my_list3):
    total3 = total3 + my_list3[i]
    i += 1
print("3. The sum of my_list3 in the iteration method is:", total3)


# the 4 way with "while loop" and with function
my_list4 = [1, 5, 9]


def sum_my_list(test_data):
    total4 = 0
    index = 0
    while index < len(test_data):
        total4 = total4 + test_data[index]
        index += 1
    return total4
print("4. The sum of my_list4 in the iteration method is:", sum_my_list(my_list4))


# the fifth way
my_list5 = [1, 5, 9, 10]


def sum_in_a_simple_list(lists):
    for indexes in range(0, len(lists)):
        return sum(my_list5)
print("5. The sum of my_list5 in the iteration method is:", sum_in_a_simple_list(my_list5))
