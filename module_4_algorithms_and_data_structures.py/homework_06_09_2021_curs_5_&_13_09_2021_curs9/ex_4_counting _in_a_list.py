''' 4. Count the occurrences of each item and display them in a dictionary. Also, as a second method, solve this using Python functions.
Test Data:  [11, 45, 8, 11, 23, 45, 23, 45, 89]
Expected Result: {11: 2, 45: 3, 8: 1, 23: 2, 89: 1} '''


# ## varianta 1
counts = dict()
my_list = [11, 45, 8, 11, 23, 45, 23, 45, 89]
for i in my_list:
    counts[i] = counts.get(i, 0) + 1
print(counts)

# ## varianta 2
import bisect
from collections import Counter

test_data = [11, 45, 8, 11, 23, 45, 23, 45, 89]
data_sorted = sorted(test_data)


# def count_with_binary_s(my_list):
#     my_dict = {}
#     counter = 0
#     for nr in my_list:
#         counter = 0 if nr not in my_dict.keys() else counter
#         index = bisect.bisect_left(my_list, nr)
#         if index != len(my_list) and my_list[index] == nr:
#             counter += 1
#         my_dict[nr] = counter
#     return my_dict
# print(count_with_binary_s(data_sorted))

# ### varianta 3
# my_counter = dict(map(lambd@ a@fara3@ x: (x, test_data.count(x)), test_data))
# print(my_counter)

# # varianta 4
# def count_occurrences(my_var):
#     my_dict = {}
#     my_unique_list = set(my_var)
#     for n in my_unique_list:
#         counter = my_var.count(n)
#         my_dict[n] = counter
#     return my_dict
# print(count_occurrences(test_data))

# # varianta 5
# def number_counter(n):
#     dict_x = {}
#     for number in n:
#         x = 1
#         if number not in dict_x:
#             dict_x[number] = x
#         else:
#             x = n.count(number)
#             dict_x[number] = x
#     return dict_x
# y = number_counter(test_data)
# print(y)
# #print(number_counter(test_data))


# ## varianta 6
# def count_numbers(lst):
#     list_dict = {}
#     for elem in lst:
#         if elem in list_dict:
#             list_dict[elem] += 1
#         else:
#             list_dict[elem] = 1
#     return list_dict
#     # for key, value in list_dict.items():
#     #     print("%d : %d" % (key, value))
# print(count_numbers(test_data))


#  ## varianta 7
# my_counter_dict = dict(Counter(test_data))
# print(my_counter_dict)