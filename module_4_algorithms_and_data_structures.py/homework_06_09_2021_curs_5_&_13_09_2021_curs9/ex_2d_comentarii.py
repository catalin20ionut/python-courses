import copy
my_list = [1, 5, 3, 6, 8, 3, 0]

# shallow copy
my_list_1 = my_list
print('1. my_list: ', my_list)
print('1. my_list_1: ', my_list)

my_list[0] = 99
print('2. my_list: ', my_list)
print('2. my_list_1: ', my_list)

my_list_2 = my_list.copy()
my_list[1] = 73
print('3. my_list: ', my_list)
print('3. my_list_2: ', my_list_2)
print('=========================')

# my_new_list = [1, 2, [3, 4], [5, 6]]
my_new_list = [1, 2, [3, [4, 9, 13]], [5, 6]]

print('4. my_new_list: ', my_new_list)
my_new_list_1 = my_new_list


# my_new_list_2 = my_new_list.copy() --- nu face deepcopy la elementele sub liste
# varianta aceasta rezolva problema
my_new_list_2 = copy.deepcopy(my_new_list)
my_new_list[2][0] = 33

print('5. my_new_list_2: ', my_new_list_2)
print('5. my_new_list: ', my_new_list)
