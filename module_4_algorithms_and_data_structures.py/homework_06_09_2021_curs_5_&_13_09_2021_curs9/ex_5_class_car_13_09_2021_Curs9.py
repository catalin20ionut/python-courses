""" 5.  Create a car class which has as members:
- brand
- model
- year
a) Implement __str__() method to show its brand, model, and year and print this information to the console
b) Adapt bubble sort, insertion sort methods discussed in class to sort a list of car objects by year.
c) Use Python built in method to sort these objects by year and then by brand alphabetically. (tip: use "key" parameter
in built in sorted() method to achieve that )
d) Use sorted() method directly to sort our list of objects by year and then by brand alphabetically without the "key"
parameter (tip: implement methods __eq__ __gt__ and __ls__ in class Car to achieve that)
Example code:
car1 = Car("Alfa Romeo", "33 SportWagon", 1988)
car2 = Car("Chevrolet", "Cruz Hatchback", 2011)
car3 = Car("Corvette", "C6 Couple", 2004)
car4 = Car("Lincoln", "Navigator SUV", 2015)
car5 = Car("Cadillac", "Seville Sedan", 1995)
car_array = [car1, car2, car3, car4, car5] """

# pentru 5 '''https://realpython.com/lessons/how-and-when-use-str/'''


class Car:
    def __init__(self, brand, model, year):
        self.brand = brand
        self.model = model
        self.year = year

    def __str__(self):
        return f"{self.brand} {self.model}, year of manufacture {self.year}"

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        if isinstance(other, Car):
            return self.year == other.year

        return False

    def __gt__(self, other):
        if isinstance(other, Car):
            return self.year > other.year

        return False

    def __ls__(self, other):
        if isinstance(other, Car):
            return self.year < other.year

        return False


def bubble_sort(l):
    n = len(l)
    for i in range(1, n):
        for j in range(0, n - 1):
            if l[j].year > l[j + 1].year:
                l[j], l[j + 1] = l[j + 1], l[j]


def insertion_sort(l):
    for i in range(1, len(l)):
        j = i
        while j > 0 and l[j - 1] > l[j]:
            l[j], l[j - 1] = l[j - 1], l[j]
            j -= 1


def bubble_sort_2(l, comparison_function):
    n = len(l)
    for i in range(1, n):
        for j in range(0, n - 1):
            if comparison_function(l[j], l[j + 1]):
                l[j], l[j + 1] = l[j + 1], l[j]


car1 = Car("Alfa Romeo", "33 SportWagon", 1988)
car2 = Car("Chevrolet", "Cruz Hatchback", 2011)
car3 = Car("Corvette", "C6 Couple", 2004)
car4 = Car("Lincoln", "Navigator SUV", 2015)
car5 = Car("Cadillac", "Seville Sedan", 1995)
car_array = [car1, car2, car3, car4, car5]
print('punctul a)')
print(car_array)
# punctul b
print('punctul b)')
car_array_1 = car_array.copy()
bubble_sort(car_array_1)
print('using bubble sort')
print(car_array_1)

car_array_2 = car_array.copy()
bubble_sort_2(car_array_2, lambda car_a, car_b: car_a.year > car_b.year)
print('using bubble sort 2')
print(car_array_2)
# aceeași idee se aplica si pentru insertion sort
# print(car_array)
# insertion_sort(car_array)
# print(car_array)
print('punctul c)')
car_array_3 = car_array.copy()
print(car_array_3)
car_array_3.sort(key=lambda car: car.brand)
print("sorting by brand", car_array_3)
print("sorted by brand again", sorted(car_array, key=lambda car: car.brand))
print('punctul d)')
# funcționează doar dacă implementăm funcțiile __gt__, __eq__, __ls__
car_array_4 = car_array.copy()
print(car_array_4)
car_array_4.sort()
print("sorting by brand", car_array_4)
print("sorted by brand again", sorted(car_array))
