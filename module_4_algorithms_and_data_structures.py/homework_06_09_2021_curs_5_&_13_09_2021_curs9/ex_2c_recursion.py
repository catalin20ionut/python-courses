import copy
my_list = [1, 2, [3, 4], [5, 6]]
my_list_1 = [1, 2, [3, [4, 9, 13]], [5, 6]]


def sum_list_recursion(lists):
    if len(lists) == 0:
        return 0
    else:
        if type(lists[-1]) is list:
            return sum_list_recursion(lists.pop()) + sum_list_recursion(lists)
        else:
            return lists.pop() + sum_list_recursion(lists)

# varianta aceasta nu este de ajuns
# my_list_copy = my_list.copy()
# print("1a. Sum of my_list with \"name of the list\".copy() -->", sum_list_recursion(my_list_copy))
# print("1b. Sum of my_list with \"name of the list\".copy() -->", sum_list_recursion(my_list_1.copy()))

# varianta aceasta, rezolva problema, totuși 1a și 1b trebuie comentate (16, 17, 18)
my_list_copy = copy.deepcopy(my_list)
print("2a. Sum of my_list with copy.deepcopy(\"name of the list\") -->", sum_list_recursion(my_list_copy))
print("2b. Sum of my_list with copy.deepcopy(\"name of the list\") -->", sum_list_recursion(copy.deepcopy(my_list_1)))


# Solution 3 is the best!
my_list = [1, 2, [3, 4], [5, 6]]
my_list_1 = [1, 2, [3, [4, 9, 13]], [5, 6]]


def nested_recursion(the_lists):
    total = 0
    for element in the_lists:
        if isinstance(element, list):
            total += nested_recursion(element)
        else:
            total += element
    return total
print("3a. The best solution:", nested_recursion(my_list))
print("3b. The best solution:", nested_recursion(my_list_1))
