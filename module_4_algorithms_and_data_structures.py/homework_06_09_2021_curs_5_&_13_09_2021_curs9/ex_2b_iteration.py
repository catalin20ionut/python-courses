""" 2. Write a Python program of recursion and iterative list sum. Test Data: [1, 2, [3,4], [5,6]]

---------------------------- sum in a list using the iteration method with a parameter --------------------------- """

my_list = [1, 2, [3, 4], [5, 6]]
test_data = [1, 2, [3, [4, [10, 20]], [5, [6, 5]]]]


# this function doesn't work for a list in a list with another list inside, only my_list available
def iterative_sum(my_lists):
    total = 0
    for elem in my_lists:
        # if type(elem) == type([]): # sau
        # if type(elem) == list:  # sau
        if isinstance(elem, list):
            for sub_elem in elem:
                total += sub_elem
        else:
            total += elem
    return total

print('1. Iterative method: ', iterative_sum(my_list))


# Solution 2
# this function doesn't work for a list in a list with another list inside, only my_list available
def iterative_sum_2(the_list):
    my_final_list = []
    for elem in the_list:
        if isinstance(elem, list):
            for sub_elem in elem:
                my_final_list.append(sub_elem)
        else:
            my_final_list.append(elem)
        if type(elem) is int:
            my_final_list.append(elem)
        else:
            for sub_elem in elem:
                my_final_list.append(sub_elem)

    print("2.", my_final_list)
    return sum(my_final_list)
print('2. Iterative method:', iterative_sum_2(my_list))

# # Solution 3, "Solution 3" must be commented for Solution 4
# # It works with list in list in another list aso.
# i3 = 0
# total3 = 0
# while i3 < len(test_data):
#     element3 = test_data[i3]
#     if isinstance(element3, list):
#         test_data += element3
#     else:
#         total3 += element3
#     i3 += 1
# print("3. Iterativ:", total3)


# Solution 4
# It works with list in list in another list aso.
test_data4 = [1, 2, [3, [4, 10, [10, 20]], [5, 10, [6, 5, 10]]]]


def iterative_sum_3(the_lists):
    i = 0
    total2 = 0
    while i < len(the_lists):
        element4 = the_lists[i]
        if isinstance(element4, list):
            the_lists += element4
        else:
            total2 += element4
        i += 1
    return total2
print("4. Iterativ:", iterative_sum_3(test_data))
print("4. Iterativ:", iterative_sum_3(test_data4))
