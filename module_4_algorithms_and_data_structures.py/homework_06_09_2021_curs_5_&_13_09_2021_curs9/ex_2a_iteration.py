my_list1 = [1, 2, [3, 4], [5, 6, 9]]
my_list2 = [[1, 2], [3, 4], [5, 6, 9]]


def calc_sum(my_array):
    my_sum = 0
    for i in range(len(my_array)):
        for j in range(len(my_array[i])):
            my_sum += my_array[i][j]
    return my_sum
print(calc_sum(my_list2))  # it works only for lists of type [[], [], ... []]
# print(calc_sum(my_list1))  # it doesn't work
