# korean_transliterator.py

# Mapează caracterele latine la consoane și vocale Hangeul
latin_to_consonant_map = {
    'b': 'ㅂ', 'g': 'ㄱ', 'd': 'ㄷ', 's': 'ㅅ', 'n': 'ㄴ', 'm': 'ㅁ',
    'r': 'ㄹ', 'p': 'ㅍ', 't': 'ㅌ', 'k': 'ㅋ', 'h': 'ㅎ', 'j': 'ㅈ',
    'l': 'ㄹ'
}

latin_to_vowel_map = {
    'a': 'ㅏ', 'e': 'ㅔ', 'i': 'ㅣ', 'o': 'ㅗ', 'u': 'ㅜ'
}

# Consoanele finale
final_consonants = {
    'n': 'ㄴ', 'ng': 'ㅇ', 'r': 'ㄹ', 'm': 'ㅁ', 's': 'ㅅ'
}

# Liste cu ordinea consoanelor, vocalelor și consoanelor finale
initial_consonants = 'ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ'
vowels = 'ㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣ'
final_consonants_list = 'ㄱㄲㄳㄴㄵㄶㄷㄸㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅃㅄㅅㅆㅈㅉㅊㅋㅌㅍㅎ'


def latin_to_hangeul(text):
    syllables = []
    initial_consonant = None
    vowel = None
    final_consonant = None

    i = 0
    while i < len(text):
        char = text[i].lower()

        # Verifică dacă caracterul este o consoană inițială
        if char in latin_to_consonant_map:
            if initial_consonant and vowel:
                # Finalizează silaba anterioară
                syllable = chr(0xAC00 +
                               (initial_consonants.index(initial_consonant) * 21 * 28) +
                               (vowels.index(vowel) * 28) +
                               (final_consonants_list.index(final_consonant) if final_consonant else 1))
                syllables.append(syllable)
                initial_consonant = latin_to_consonant_map[char]
                vowel = None
                final_consonant = None
            else:
                initial_consonant = latin_to_consonant_map[char]

        # Verifică dacă caracterul este o vocală
        elif char in latin_to_vowel_map:
            vowel = latin_to_vowel_map[char]

        # Verifică dacă caracterul este o consoană finală
        elif char in final_consonants:
            final_consonant = final_consonants[char]

        else:
            # Dacă întâlnește un caracter care nu poate fi transliterat, îl adaugă ca atare
            if initial_consonant and vowel:
                syllable = chr(0xAC00 +
                               (initial_consonants.index(initial_consonant) * 21 * 28) +
                               (vowels.index(vowel) * 28) +
                               (final_consonants_list.index(final_consonant) if final_consonant else 0))
                syllables.append(syllable)
                initial_consonant = None
                vowel = None
                final_consonant = None
            syllables.append(char)

        i += 1

    # Finalizează ultima silabă
    if initial_consonant and vowel:
        syllable = chr(0xAC00 +
                       (initial_consonants.index(initial_consonant) * 21 * 28) +
                       (vowels.index(vowel) * 28) +
                       (final_consonants_list.index(final_consonant) if final_consonant else 0))
        syllables.append(syllable)

    return ''.join(syllables)


# Exemplu de utilizare
if __name__ == "__main__":
    latin_text = "il li han"
    hangeul_text = latin_to_hangeul(latin_text)
    print(f"Litere latine: {latin_text}")
    print(f"Caractere Hangeul: {hangeul_text}")
