import random

# Definim echipele
teams = [f"Echipa {i + 1}" for i in range(20)]


# Funcție pentru a simula un meci între două echipe și a returna scorul
def simulate_match(team1, team2):
    score1 = random.randint(0, 5)  # Generăm un scor între 0 și 5 pentru echipa 1
    score2 = random.randint(0, 5)  # Generăm un scor între 0 și 5 pentru echipa 2
    return score1, score2


# Funcție pentru a actualiza statisticile și punctele fiecărei echipe
def update_stats(teams_stats, score1, score2, team1, team2):
    if score1 == score2:
        teams_stats[team1]['points'] += 1
        teams_stats[team2]['points'] += 1
        teams_stats[team1]['draws'] += 1
        teams_stats[team2]['draws'] += 1
    elif score1 > score2:
        teams_stats[team1]['points'] += 3
        teams_stats[team1]['wins'] += 1
        teams_stats[team2]['losses'] += 1
    else:
        teams_stats[team2]['points'] += 3
        teams_stats[team2]['wins'] += 1
        teams_stats[team1]['losses'] += 1

    teams_stats[team1]['played'] += 1
    teams_stats[team2]['played'] += 1


# Funcție pentru a afișa clasamentul
def display_standings(teams_stats, round_num):
    sorted_teams = sorted(teams_stats.items(), key=lambda x: x[1]['points'], reverse=True)
    print(f"\nClasament după etapa {round_num}:")
    print("Poz. | Echipa      | J | V | E | I | P")
    for i, (team, stats) in enumerate(sorted_teams, start=1):
        print(
            f"{i:2}. {team:10} | {stats['played']:1} | {stats['wins']:1} | {stats['draws']:1} | {stats['losses']:1} | {stats['points']:2}")
    print()


# Inițializăm statisticile echipei
teams_stats = {team: {'played': 0, 'wins': 0, 'draws': 0, 'losses': 0, 'points': 0} for team in teams}

# Generăm meciurile pentru etapele 1-19
matches = [(teams[i], teams[j]) for i in range(len(teams)) for j in range(i + 1, len(teams))]
random.shuffle(matches)  # Amestecăm meciurile pentru a le distribui pe etape

# Selectăm meciurile pentru etapele 1-19
first_half_matches = matches[:19 * (len(teams) // 2)]

# Inversăm meciurile pentru etapele 20-38
second_half_matches = [(match[1], match[0]) for match in first_half_matches]

# Combinăm ambele părți ale campionatului
all_matches = first_half_matches + second_half_matches

# Simulăm etapele
num_matches_per_round = len(teams) // 2  # 10 meciuri pe etapă
num_rounds = len(all_matches) // num_matches_per_round

# Variabile pentru a ține evidența echipei care a jucat deja într-o etapă
played_teams = set()

for round_num in range(1, num_rounds + 1):
    print(f"\nEtapa {round_num}:")
    round_matches = []

    # Selectăm meciurile pentru etapa curentă, asigurându-ne că fiecare echipă joacă doar un meci
    for match in all_matches:
        if len(round_matches) == num_matches_per_round:
            break
        team1, team2 = match
        if team1 not in played_teams and team2 not in played_teams:
            round_matches.append(match)
            played_teams.add(team1)
            played_teams.add(team2)

    # Simulăm meciurile din etapa curentă
    for team1, team2 in round_matches:
        score1, score2 = simulate_match(team1, team2)
        update_stats(teams_stats, score1, score2, team1, team2)
        print(f"{team1} vs {team2} - {score1}:{score2}")

    # După fiecare etapă, afișăm clasamentul
    display_standings(teams_stats, round_num)

    # Resetăm echipele pentru etapa următoare
    played_teams.clear()

# Afișăm clasamentul final
print("Clasament final:")
sorted_teams = sorted(teams_stats.items(), key=lambda x: x[1]['points'], reverse=True)
print("Poz. | Echipa      | J | V | E | I | P")
for i, (team, stats) in enumerate(sorted_teams, start=1):
    print(
        f"{i:2}. {team:10} | {stats['played']:1} | {stats['wins']:1} | {stats['draws']:1} | {stats['losses']:1} | {stats['points']:2}")
