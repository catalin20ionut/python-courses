""" 1. Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17. Bonus: Can you do this in one pass? """


def checkingSum(any_list, k):
    for i in range(len(any_list)):
        for j in range(i+1, len(any_list)):
            if any_list[i] + any_list[j] == k:
                return True
    return bool(0)

my_list = [10, 15, 3, 7]
print(checkingSum(my_list, 10))
