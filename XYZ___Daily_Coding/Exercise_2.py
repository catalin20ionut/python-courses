"""
2. Given an array of integers, return a new array such that each element at index i of the new array is the product of
all the numbers in the original array except the one at i. For example, if our input was [1, 2, 3, 4, 5], the expected
output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].          """


def productList(any_list):
    if len(any_list) == 1:
        return "The productList cannot be formed!"
    else:
        product_list = []
        product = 1
        for number in any_list:
            product *= number
        for number in any_list:
            product_list.append(int(product / number))
        return product_list


my_list1 = [5]
my_list2 = [1, 2, 3, 4, 5]
my_list3 = [5, 4, 3, 2, 1]
my_list4 = [3, 2, 1]
print(productList(my_list1))
print(productList(my_list2))
print(productList(my_list3))
print(productList(my_list4))
