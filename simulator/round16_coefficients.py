# Echipele și coeficienții din șaisprezecimi care au terminat pe prima poziție în grupe
round16_pos1_teams_coefficients = [
    ('Inter Milan', 130.0, 'position: 1'),
    ('Ajax Amsterdam', 100.0, 'position: 2'),
    ('FC Porto', 85.0, 'position: 3'),
    ('AS Roma', 73.0, 'position: 4'),
    ('Bayer Leverkusen', 72.0, 'position: 5'),
    ('Zenit Saint Petersburg', 60.0, 'position: 6'),
    ('CSKA Moscow', 45.0, 'position: 7'),
    ('Rangers', 35.0, 'position: 8'),
]
# Echipele și coeficienții din șaisprezecimi care au terminat pe prima poziție în grupe
round16_pos2_teams_coefficients = [
    ('Juventus Torino', 121.0, 'position: 1'),
    ('Borussia Dortmund', 110.0, 'position: 2'),
    ('AC Milan', 93.0, 'position: 3'),
    ('Shakhtar Donetsk', 85.0, 'position: 4'),
    ('Red Bull Salzburg', 81.0, 'position: 5'),
    ('Rosenborg', 50.0, 'position: 6'),
    ('Celtic Glasgow', 40.0, 'position: 7'),
    ('Hertha Berlin', 38.0, 'position: 8'),
]
