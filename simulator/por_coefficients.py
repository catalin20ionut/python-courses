# Echipele și coeficienții pentru play-off - champions path
por_teams_coefficients = [
    ('Red Bull Salzburg', 81.0, 'position: 1'),
    ('Olympiacos Piraeus', 73.0, 'position: 2'),
    ('FC Basel', 50.0, 'position: 3'),
    ('Rosenborg', 50.0, 'position: 4'),
    ('Celtic Glasgow', 40.0, 'position: 5'),
    ('Slavia Prague', 39.5, 'position: 6'),
    ('Steaua București', 25.0, 'position: 7'),
    ('Dudelange', 12.0, 'position: 8'),
]
