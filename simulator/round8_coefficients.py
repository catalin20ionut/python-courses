# Echipele și coeficienții din optimi
round8_teams_coefficients = [
    ('Inter Milan', 130.0, 'position: 1'),
    ('Ajax Amsterdam', 100.0, 'position: 2'),
    ('FC Porto', 85.0, 'position: 3'),
    ('Red Bull Salzburg', 81.0, 'position: 4'),
    ('AS Roma', 73.0, 'position: 5'),
    ('Bayer Leverkusen', 72.0, 'position: 6'),
    ('Rosenborg', 50.0, 'position: 7'),
    ('CSKA Moscow', 45.0, 'position: 8'),
]
