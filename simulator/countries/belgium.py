teams = [
    "Anderlecht Bruxelles",
    "Standard Liège",
    "Club Brugge",
    "Sporting Charleroi",
    "Royal Antwerp",
    "Racing Genk",
    "Gent",
    "Zulte Waregem",
    "Cercle Brugge",
    "KV Mechelen",
    "KV Oostende",
    "Sint-Truidense",
    "KV Kortrijk",
    "Eupen",
    "OH Leuven",
    "Sporting Lokeren"
]