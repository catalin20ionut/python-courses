import random
import importlib.util


# # Funcția de alegere a scorului, neutilizat
# def choosing_score(home_coef, away_coef):
#     coef_diff = home_coef - away_coef
#     if coef_diff > 60:
#         home_score = random.choices([3, 4, 5, 6], [0.65, 0.20, 0.10, 0.05])[0]
#         away_score = random.randint(0, 1)
#     elif coef_diff > 40:
#         home_score = random.choices([2, 3, 4], [0.60, 0.25, 0.15])[0]
#         away_score = random.randint(0, 2)
#     else:
#         home_score = random.randint(0, 3)
#         away_score = random.randint(0, 3)
#     return home_score, away_score


def simulate_1leg_matches(team1, team2):
    # Scorul după 90 de minute
    score1_90 = random.randint(0, 3)
    score2_90 = random.randint(0, 3)

    if score1_90 != score2_90:
        # O echipă a câștigat în 90 de minute
        return f"{team1:<25} vs {team2:<25} - {score1_90}:{score2_90}", team1 if score1_90 > score2_90 else team2

    # Prelungiri
    score1_ET = score1_90 + random.randint(0, 2)
    score2_ET = score2_90 + random.randint(0, 2)

    if score1_ET != score2_ET:
        # O echipă a câștigat în prelungiri
        return f"{team1:<25} vs {team2:<25} - {score1_90}:{score2_90} {score1_ET}:{score2_ET}", \
               team1 if score1_ET > score2_ET else team2

    # Penalty-uri
    score1_pen = random.randint(1, 5)
    score2_pen = random.randint(1, 5)

    while score1_pen == score2_pen or (abs(score1_pen - score2_pen) > 2 and max(score1_pen, score2_pen) <= 5) \
            or (abs(score1_pen - score2_pen) > 1 and max(score1_pen, score2_pen) > 5):
        if max(score1_pen, score2_pen) <= 5:
            score1_pen = random.randint(1, 5)
            score2_pen = random.randint(1, 5)
        else:
            score1_pen += random.randint(0, 1)
            score2_pen += random.randint(0, 1)

    return f"{team1:<25} vs {team2:<25} - {score1_90}:{score2_90} {score1_ET}:{score2_ET} {score1_pen}:{score2_pen}", \
           team1 if score1_pen > score2_pen else team2


def simulate_matches(team1, team2, is_runners_up=False):
    if is_runners_up:
        team1_name, team1_coef = team1, 0
        team2_name, team2_coef = team2, 0
    else:
        try:
            team1_name, team1_coef, *_ = team1
            team2_name, team2_coef, *_ = team2
            team1_coef = float(team1_coef)
            team2_coef = float(team2_coef)
        except ValueError as e:
            print(f"ValueError: {e}. Team1: {team1}, Team2: {team2}")
            return "Invalid team data."

    # Funcție pentru a simula scorul cu probabilități ajustate
    def choosing_score(home_coef, away_coef):
        coef_diff = home_coef - away_coef
        if coef_diff > 50:
            home_score = random.choices([3, 4, 5, 6], [0.05, 0.05, 0.45, 0.45])[0]
            away_score = random.randint(0, 1)
        elif coef_diff > 30:
            home_score = random.choices([2, 3, 4], [0.1, 0.25, 0.65])[0]
            away_score = random.randint(0, 2)
        else:
            home_score = random.randint(0, 3)
            away_score = random.randint(0, 3)
        return home_score, away_score

    # Simulăm meciurile tur retur
    score1_tur, score2_tur = choosing_score(team1_coef, team2_coef)
    score1_retur, score2_retur = choosing_score(team2_coef, team1_coef)

    result_tur = f"{team1_name:<30} vs {team2_name:<30} - {score1_tur}:{score2_tur}"
    result_retur = f"{score1_retur}:{score2_retur}"

    # Calculăm scorul total
    total_score1 = score1_tur + score1_retur
    total_score2 = score2_tur + score2_retur

    # Verificăm câștigătorul după scorul total
    if total_score1 > total_score2:
        qualified_team = team1_name
    elif total_score2 > total_score1:
        qualified_team = team2_name

    # pentru egalități în cele două manșe
    elif total_score1 == total_score2 and score1_tur == score2_tur and score1_retur < score1_tur:
        qualified_team = team2_name
    elif total_score1 == total_score2 and score1_tur == score2_tur and score1_retur > score1_tur:
        qualified_team = team1_name
    elif total_score1 == total_score2 and score1_tur == score2_tur and score1_retur == score1_tur:
        score1_et = random.randint(0, 2)
        score2_et = random.randint(0, 2)

        total_score1_et = score1_retur + score1_et
        total_score2_et = score2_retur + score2_et

        if total_score1_et > score1_retur and total_score1_et - score1_retur >= total_score2_et - score2_retur:
            qualified_team = team1_name
        elif total_score1_et > score1_retur and total_score1_et - score1_retur < total_score2_et - score2_retur:
            qualified_team = team2_name
        elif total_score1_et == score1_retur and total_score1_et - score1_retur < total_score2_et - score2_retur:
            qualified_team = team2_name
        else:
            # penalty shoot-out, lovituri de departajare
            while True:
                score1_pen = random.randint(1, 6)
                score2_pen = random.randint(1, 6)

                # Verificăm condițiile pentru loviturile de departajare
                if score1_pen <= 5 and score2_pen <= 5:
                    if abs(score1_pen - score2_pen) <= 2:
                        break
                else:
                    if abs(score1_pen - score2_pen) == 1:
                        break

            if score1_pen > score2_pen:
                qualified_team = team1_name
            else:
                qualified_team = team2_name

            return f"{result_tur}  {result_retur} ({total_score1_et}:{total_score2_et}, " \
                   f"{score1_pen}:{score2_pen}) Qualified team: {qualified_team}"

        return f"{result_tur}  {result_retur} ({total_score1_et}:{total_score2_et})     " \
               f" Qualified team: {qualified_team}"

    elif total_score1 == total_score2 and score1_retur > score2_tur:
        qualified_team = team1_name
    else:
        qualified_team = team2_name

    return f"{result_tur}  {result_retur}            Qualified team: {qualified_team}"


def import_attribute_from_module(file_path, attribute_name):
    spec = importlib.util.spec_from_file_location("module.name", file_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    if hasattr(module, attribute_name):
        return getattr(module, attribute_name)
    else:
        raise AttributeError(f"Modulul din {file_path} nu are atributul '{attribute_name}'.")


def create_and_print_urns(coefficients, urn_count=2):
    if urn_count not in {2, 4}:
        raise ValueError("Numărul de urne trebuie să fie 2 sau 4.")

    chunk_length = len(coefficients) // urn_count
    urns = [coefficients[i * chunk_length: (i + 1) * chunk_length] for i in range(urn_count)]
    if urn_count == 2:
        header = " ".join([f" {'Urna ' + str(i + 1):<48}" for i in range(urn_count)])
    else:
        header = " ".join([f" {'Urna ' + str(i + 1):<38}" if i < 2
                           else f"{'Urna ' + str(i + 1):<39}" for i in range(urn_count)])

    print(f"\n{header}")

    for x in range(max(len(urn) for urn in urns)):
        row_info = []
        for i, urn in enumerate(urns):
            if x < len(urn):
                index = x + i * chunk_length + 1
                if urn_count == 2:
                    team_info = f"{index:>2}. {urn[x][0]:<30} - {urn[x][1]:>5.1f}"
                else:
                    team_info = f"{index:>2}. {urn[x][0]:<24} - {urn[x][1]:>5.1f}"
            else:
                team_info = ""
            row_info.append(team_info)

        if urn_count == 2:
            print(" ".join([f"{info:<50}" for info in row_info]))
        else:
            print(" ".join([f"{info:<39}" for info in row_info]))
