earnings_before_round_of_16 = [
    ('Inter Milan                 ', 55000000),
    ('Juventus Torino             ', 45000000),
    ('Real Madrid                 ', 45000000),
    ('Zenit Saint Petersburg      ', 40000000),
    ('Crvena Zvezda               ', 40000000),
    ('PSV Eindhoven               ', 35000000),
    ('AC Milan                    ', 35000000),
    ('Red Bull Salzburg           ', 35000000),
    ('CSKA Moscow                 ', 35000000),
    ('Hertha Berlin               ', 35000000),
    ('AS Roma                     ', 35000000),
    ('Shakhtar Donetsk            ', 35000000),
    ('Borussia Dortmund           ', 35000000),
    ('Anderlecht Bruxelles        ', 35000000),
    ('Dinamo Zagreb               ', 30500000),
    ('Red Bull Salzburg           ', 30500000),
    ('FC Copenhagen               ', 30000000),
    ('Olympiacos Piraeus          ', 30000000),
    ('Galatasaray                 ', 30000000),
    ('FC Barcelona                ', 30000000),
    ('Liverpool                   ', 30000000),
    ('Atlético Madrid             ', 30000000),
    ('Villarreal                  ', 30000000),
    ('Manchester United           ', 30000000),
    ('PSV Eindhoven               ', 29000000),
    ('Benfica                     ', 29000000),
    ('Manchester City             ', 25000000),
    ('Liverpool                   ', 25000000),
    ('Real Madrid                 ', 25000000),
    ('FC Barcelona                ', 25000000),
    ('Bayern Munich               ', 25000000),
    ('Paris Saint-Germain         ', 25000000),
    ('Atlético Madrid             ', 25000000),
    ('Inter Milan                 ', 25000000),
    ('Juventus Torino             ', 25000000),
    ('Arsenal                     ', 25000000),
    ('Borussia Dortmund           ', 25000000),
    ('Manchester United           ', 25000000),
    ('Ajax Amsterdam              ', 25000000),
    ('AS Monaco                   ', 25000000),
    ('AC Milan                    ', 25000000),
    ('Olympique Marseille         ', 25000000),
    ('FC Porto                    ', 25000000),
    ('Shakhtar Donetsk            ', 25000000),
    ('Villarreal                  ', 25000000),
    ('AS Roma                     ', 25000000),
    ('Bayer Leverkusen            ', 25000000),
    ('Zenit Saint Petersburg      ', 25000000),
    ('Galatasaray                 ', 25000000),
    ('Anderlecht Bruxelles        ', 25000000),
    ('CSKA Moscow                 ', 25000000),
    ('Hertha Berlin               ', 25000000),
    ('Paris Saint-Germain         ', 25000000),
    ('Bayer Leverkusen            ', 25000000),
    ('Manchester City             ', 25000000),
    ('Olympique Marseille         ', 25000000),
    ('FC Porto                    ', 20000000),
    ('Ajax Amsterdam              ', 20000000),
    ('Rangers                     ', 20000000),
    ('Arsenal                     ', 20000000),
    ('Celtic Glasgow              ', 20000000),
    ('AS Monaco                   ', 15000000),
    ('Bayern Munich               ', 15000000),
    ('Olympiacos Piraeus          ', 10000000),
    ('FC Basel                    ', 5000000),
    ('Standard Liège              ', 4000000),
    ('BATE Borisov                ', 3000000),
    ('Anorthosis Famagusta        ', 3000000),
    ('Crvena Zvezda               ', 3000000),
    ('Maccabi Tel Aviv            ', 3000000),
    ('Ballkani Suharekë           ', 3000000),
    ('Dinamo Kiev                 ', 2500000),
    ('Sparta Prague               ', 2500000),
    ('Grasshoppers Zürich         ', 2500000),
    ('Steaua București            ', 2500000),
    ('Slavia Prague               ', 2500000),
    ('Celtic Glasgow              ', 2500000),
    ('Spartak Moscow              ', 2500000),
    ('Maribor Branik              ', 1500000),
    ('Sheriff Tiraspol            ', 1500000),
    ('Ludogorets Razgrad          ', 1500000),
    ('Ferencváros Budapest        ', 1500000),
    ('Vardar Skopje               ', 1500000),
    ('Flamurtari Vlorë            ', 1500000),
    ('Žalgiris Vilnius            ', 1500000),
    ('Havnar Bóltfelag            ', 1500000),
    ('Shakhter Karagandy          ', 1500000),
    ('The New Saints              ', 1500000),
    ('Fenerbahçe                  ', 1000000),
    ('CFR Cluj                    ', 1000000),
    ('Rangers                     ', 1000000),
    ('Sliema Wanderers            ', 500000),
    ('Legia Warszawa              ', 500000),
    ('Rosenborg                   ', 500000),
    ('Slovan Bratislava           ', 500000),
    ('Malmö                       ', 500000),
    ('Dudelange                   ', 500000),
    ('Fimleikafélag Hafnarfjörður ', 500000),
    ('Shamrock Rovers             ', 500000),
    ('Dinamo Tbilisi              ', 500000),
    ('Linfield Belfast            ', 500000),
    ('Neftçi Baku                 ', 500000),
    ('Riga FC                     ', 500000),
    ('Budućnost Podgorica         ', 500000),
    ('Željezničar Sarajevo        ', 500000),
    ('Pyunik Yerevan              ', 500000),
    ('Flora Tallinn               ', 500000),
    ('HJK Helsinki                ', 500000),
    ('Europa FC                   ', 250000),
    ('FC Santa Coloma             ', 100000),
    ('La Fiorita                  ', 100000),
]
