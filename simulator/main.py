from country_coefficients import country_coefficients  # pentru Punctul 1
from clasament_country_coefficients import clasament_country_coefficients  # pentru Punctul 2, 3
import os  # pentru Punctul 3
import random  # pentru Punctul 6

from teams_of_cl import teams_of_cl  # pentru Punctul 6
from uefa_coefficients import uefa_coefficients  # pentru Punctul 7
from fqr_coefficients import fqr_teams_coefficients  # pentru Punctul 7
from sqr_coefficients import sqr_teams_coefficients  # pentru Punctul 8
from tqr_coefficients import tqr_teams_coefficients  # pentru Punctul 9
from por_coefficients import por_teams_coefficients  # pentru Punctul 10
import time  # pentru Punctul 12
from fun import simulate_1leg_matches, simulate_matches, import_attribute_from_module, create_and_print_urns
# import pygame  # Punctul 17
from simulator.earnings_group_stage import earnings_group_stage  # Punctul 18
#
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 1 - generează fișierul clasament_country_coefficients.py


def write_sorted_coefficients_countries():        # Funcția pentru a scrie coeficienții ordonați în fișierul specificat
    sorted_countries = sorted(country_coefficients.items(), key=lambda x: x[1], reverse=True)
    output_file = 'clasament_country_coefficients.py'
    try:
        with open(output_file, 'w') as file:
            file.write('clasament_country_coefficients = {\n')
            for position_1, (country_1, uefa_coefficient) in enumerate(sorted_countries, start=1):
                country_str = f"'{country_1}':"
                coefficient_str = f"{uefa_coefficient:.3f}"
                position_str = f"{position_1}"
                country_padding = ' ' * (26 - len(country_str))
                coefficient_padding = ' ' * (8 - len(coefficient_str))
                position_padding = ' ' * (3 - len(position_str))
                file.write(f"    {country_str}{country_padding}{{'coefficient':{coefficient_padding}{coefficient_str}, "
                           f"'position':{position_padding}{position_str}}},\n")
            file.write('}\n')
        print(f"1a. Datele au fost scrise cu succes în fișierul {output_file}.")
    except IOError as e_1:
        print(f"Eroare: Nu s-a putut scrie în fișierul {output_file}. Detalii: {e_1}")

if __name__ == "__main__":
    write_sorted_coefficients_countries()
# ---------------------------------------------------------------------------------------------------------------------
print(" ")
print(" Clasament uefa coeficienți")


# Punctul 2 - afișează clasament clasamentul țărilor după coeficienții uefa
def print_country_coefficients(coefficients_dict):
    for country_2, data in sorted(coefficients_dict.items(), key=lambda x: x[1]['position']):
        print(f"{data['position']:2d}. {country_2:22} {data['coefficient']:7.3f}")

print_country_coefficients(clasament_country_coefficients)
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 3 - creează fișierul teams_of_cl.py
# Preia din folderul countries pe baza clasamentului din clasament_country_coefficients.py numărul de echipe necesar.


def import_teams(file_source_3):
    return import_attribute_from_module(file_source_3, 'teams')


def write_sorted_coefficients():
    countries_dir = 'C:\\Users\\home\\PycharmProjects\\pythonProject\\Python_Courses\\simulator\\countries'
    output_file = 'teams_of_cl.py'
    try:
        with open(output_file, 'w', encoding='utf-8') as file:
            file.write('teams_of_cl = {\n')
            for country_3, info in clasament_country_coefficients.items():
                position_2 = info['position']
                num_teams = 0
                if 1 <= position_2 <= 4:
                    num_teams = 4
                elif 5 <= position_2 <= 6:
                    num_teams = 3
                elif 7 <= position_2 <= 15:
                    num_teams = 2
                elif 16 <= position_2 <= 55:
                    num_teams = 1

                country_file = os.path.join(countries_dir, f"{country_3.lower().replace(' ', '_')}.py")
                if os.path.exists(country_file):
                    try:
                        teams_1 = import_teams(country_file)
                        teams_1 = list(teams_1)
                        file.write(
                            f"    '{country_3}': {{'teams': {teams_1[:num_teams]}, 'position': {position_2}}},\n")
                    except AttributeError as e_3:
                        print(e_3)
            file.write('}\n')
        print(f" 3. Datele au fost scrise cu succes în fișierul {output_file}.", "\n")
    except IOError as e_3:
        print(f"Eroare: Nu s-a putut scrie în fișierul {output_file}. Detalii: {e_3}")


if __name__ == "__main__":
    write_sorted_coefficients()
# ----------------------------------------------------------------------------------------------------------------------
print(" 4. Echipele participante")


def writing_teams():
    try:
        from teams_of_cl import teams_of_cl
        for country_4, data in teams_of_cl.items():    # Parcurgem dicționarul teams_of_cl și afișăm informațiile dorite
            position3 = data['position']
            teams_2 = ", ".join(data['teams'])
            if position3 <= 9:
                position_str = f" {position3}"                       # Afișăm numerele de la 1 la 9 cu un spațiu în față
            else:
                position_str = str(position3)
            print(f"{position_str}. {country_4.ljust(22)}: {teams_2}")
    except ImportError as e_4a:
        print(f"Eroare: Nu s-a putut importa modulul teams_of_cl. Detalii: {e_4a}")
    except AttributeError as e_4b:
        print(f"Eroare: Modulul 'teams_of_cl' nu conține atributul 'items'. Detalii: {e_4b}")

if __name__ == "__main__":
    writing_teams()
# ----------------------------------------------------------------------------------------------------------------------
# Punctul 5 - afișează câte echipe participă


def counting_teams():
    try:
        from teams_of_cl import teams_of_cl
        total_teams = 0
        # Parcurgem dicționarul teams_of_cl și adunăm numărul de echipe pentru fiecare țară
        for country_5, data in teams_of_cl.items():
            teams_count = len(data['teams'])
            total_teams += teams_count
        print(f"*5. Total echipe participante: {total_teams}")
    except ImportError as e_5a:
        print(f"Eroare: Nu s-a putut importa modulul teams_of_cl. Detalii: {e_5a}")
    except AttributeError as e_5b:
        print(f"Eroare: Modulul 'teams_of_cl' nu conține atributul 'items'. Detalii: {e_5b}")


if __name__ == "__main__":
    counting_teams()
# ----------------------------------------------------------------------------------------------------------------------
# Punctul 6 - preliminary round
# Extragem echipele de la pozițiile 52, 53, 54 și 55
preliminary_round_teams = [52, 53, 54, 55]
selected_teams_for_the_preliminary_round = {pos: team for country, details in teams_of_cl.items() for pos, team in
                  enumerate(details['teams'], start=details['position']) if pos in preliminary_round_teams}

print("\n\033[31mPreliminary round:\033[0m")
for i, position in enumerate(preliminary_round_teams, start=1):                          # Afișăm echipele participante
    print(f"{i}. {selected_teams_for_the_preliminary_round[position]}")
print()

# Extragem echipele în ordinea pozițiilor
teams_pr = [selected_teams_for_the_preliminary_round[position] for position in preliminary_round_teams]


def simulate_match_pr(team1pr, team2pr):
    return simulate_1leg_matches(team1pr, team2pr)

semifinal1_result_pr, winner1 = simulate_match_pr(teams_pr[0], teams_pr[1])
semifinal2_result_pr, winner2 = simulate_match_pr(teams_pr[2], teams_pr[3])
print("Semifinal 1:", semifinal1_result_pr)
print("Semifinal 2:", semifinal2_result_pr)
final_result_pr, final_winner_pr = simulate_match_pr(winner1, winner2)
print("Final:      ", final_result_pr)
print("Winner:     ", final_winner_pr)

# --------------------------------------------------------------------------------------------------------------------
# Punctul 7 - the first qualifying round
# Extragem echipele de la pozițiile 18 până la 52 inclusiv, cu excepția echipei din Liechtenstein
positions_for_fqr = list(range(18, 52))
fqr_teams = {pos: team for country, details in teams_of_cl.items() for pos, team in enumerate(details['teams'],
             start=details['position']) if pos in positions_for_fqr and country != 'Liechtenstein'}
fqr_teams[52] = final_winner_pr                                         # Adăugăm câștigătoarea din "preliminary round"
teams_1qr = list(fqr_teams.values())                                                           # Lista finală de echipe

print("\n\033[31mFirst round:\033[0m")                                               # printarea echipelor participante
for i, team in enumerate(teams_1qr, start=1):
    print(f"{i}. {team}")
print()

# Scriem echipele în fișierul fqr_coefficients.py
with open('fqr_coefficients.py', 'w', encoding='utf-8') as file_2:
    file_2.write("teams_1qr = [\n")
    for team in teams_1qr:
        file_2.write("    '{}',\n".format(team))
    file_2.write("]\n")

# Importăm coeficienții UEFA
# Creăm o listă pentru echipele din prima rundă de calificare și coeficienții lor
teams_with_coefficients = [(team, uefa_coefficients[team]['coefficient']) for team in teams_1qr if team in
                           uefa_coefficients]

# Sortăm echipele în funcție de coeficient în ordine descrescătoare
sorted_teams_with_coefficients = sorted(teams_with_coefficients, key=lambda x: x[1], reverse=True)

# Scriem echipele și coeficienții în fișierul fqr_coefficients.py
with open('fqr_coefficients.py', 'w', encoding='utf-8') as f:
    f.write("# Echipele și coeficienții pentru prima rundă de calificare\n")
    f.write("fqr_teams_coefficients = [\n")
    for position, (team, coefficient) in enumerate(sorted_teams_with_coefficients, start=1):
        f.write(f"    ('{team}', {coefficient}, 'position: {position}'),\n")
    f.write("]\n")


# Funcție pentru a importa lista de echipe și coeficienții lor din fișierul fqr_coefficients.py
def import_fqr_teams_coefficients(file_location):
    return import_attribute_from_module(file_location, 'fqr_teams_coefficients')


# Funcție pentru a împărți echipele în două urne și a le afișa
def create_and_print_urns_fr(coefficients):
    create_and_print_urns(coefficients, urn_count=2)


if __name__ == "__main__":
    file_path = 'fqr_coefficients.py'
    try:
        fqr_teams_coefficients_list = import_fqr_teams_coefficients(file_path)
        create_and_print_urns_fr(fqr_teams_coefficients_list)
    except (ImportError, AttributeError, IOError) as e_7:
        print(f"Eroare: {e_7}")


def simulate_matches_first_qualifying_round(team1, team2):
    return simulate_matches(team1, team2)

list_of_qualified_teams_in_the_second_round = []


def run_first_qualifying_round():
    # Sortăm echipele după coeficient
    fqr_teams_coefficients.sort(key=lambda x: x[1], reverse=True)

    # Împărțim echipele în două urne
    urna1_fqr = fqr_teams_coefficients_list[:len(fqr_teams_coefficients_list)//2]
    urna2_fqr = fqr_teams_coefficients_list[len(fqr_teams_coefficients_list)//2:len(fqr_teams_coefficients_list)]

    # Amestecăm echipele din urna2 pentru a crea meciuri random
    random.shuffle(urna2_fqr)

    # Creăm meciurile
    matches = list(zip(urna1_fqr, urna2_fqr))

    print("\n\033[31mFirst qualifying round results:\033[0m")
    for index, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches_first_qualifying_round(team1, team2)
        if index < 10:
            print(f" {index}. {result}")  # Adăugăm un spațiu pentru primele 9 meciuri
        else:
            print(f"{index}. {result}")  # Nu adăugăm spațiu pentru celelalte meciuri
        list_of_qualified_teams_in_the_second_round.append(result.split("Qualified team: ")[1].strip())
    # Returnăm lista cu echipele calificate
    return list_of_qualified_teams_in_the_second_round


if __name__ == "__main__":
    list_of_qualified_teams_in_the_second_round = run_first_qualifying_round()
# ----------------------------------------------------------------------------------------------------------------------
# Punctul 8 - the second qualifying round
positions = [15, 16, 17]  # Pozițiile dorite
extracted_teams = []      # Lista în care vom adăuga echipele extrase

# Iterăm prin fiecare poziție dorită
for position in positions:
    # Găsește țara cu poziția specificată
    country = next((key for key, value in teams_of_cl.items() if value['position'] == position), None)

    if country:
        # Extrage lista de echipe pentru țara găsită
        teams_list = teams_of_cl[country]['teams']

        # Verifică dacă lista de echipe nu este goală și indexul este valid
        if teams_list:
            # Extrage echipa aflată la poziția 0 din lista de echipe
            team_at_index = teams_list[0]
            # Adaugă echipa în lista de echipe extrase
            extracted_teams.append(team_at_index)

# Afișează lista de echipe extrase
print("3 -", extracted_teams)

the_list_with_the_teams_in_the_second_round = extracted_teams + list_of_qualified_teams_in_the_second_round
print("\n\033[32m The teams in the second qualifying round - champions path:\033[0m")
for i, team in enumerate(the_list_with_the_teams_in_the_second_round, start=1):
    if i < 10:
        print(f" {i}. {team}")
    else:
        print(f"{i}. {team}")

# Importăm coeficienții UEFA
# Creăm o listă pentru echipele din rundă a doua de calificare champions path și coeficienții lor
teams_with_coefficients_sr_cp = [(team, uefa_coefficients[team]['coefficient']) for team in
                              the_list_with_the_teams_in_the_second_round if team in uefa_coefficients]

# Sortăm echipele în funcție de coeficient în ordine descrescătoare
sorted_teams_with_coefficients_sr_cp = sorted(teams_with_coefficients_sr_cp, key=lambda x: x[1], reverse=True)

# Scriem echipele și coeficienții în fișierul sqr_coefficients.py
with open('sqr_coefficients.py', 'w', encoding='utf-8') as f:
    f.write("# Echipele și coeficienții pentru rundă a doua de calificare champions path\n")
    f.write("sqr_teams_coefficients = [\n")
    for position, (team, coefficient) in enumerate(sorted_teams_with_coefficients_sr_cp, start=1):
        f.write(f"    ('{team}', {coefficient}, 'position: {position}'),\n")
    f.write("]\n")


# Funcție pentru a importa lista de echipe și coeficienții lor din fișierul sqr_coefficients.py
def import_sqr_teams_coefficients(file_location_2):
    return import_attribute_from_module(file_location_2, 'sqr_teams_coefficients')


# Funcție pentru a împărți echipele în două urne și a le afișa
def create_and_print_urns_sr(coefficients):
    create_and_print_urns(coefficients, urn_count=2)


if __name__ == "__main__":
    file_path_2 = 'sqr_coefficients.py'
    try:
        sqr_teams_coefficients_list = import_sqr_teams_coefficients(file_path_2)
        create_and_print_urns_sr(sqr_teams_coefficients_list)
    except (ImportError, AttributeError, IOError) as e_8:
        print(f"Eroare: {e_8}")


def simulate_matches_second_qualifying_round_cp(team1, team2):
    return simulate_matches(team1, team2)


list_of_qualified_teams_in_the_third_round_cp = []


def run_second_qualifying_round_cp():
    # Sortăm echipele după coeficient
    sqr_teams_coefficients.sort(key=lambda x: x[1], reverse=True)

    # Împărțim echipele în două urne
    urna1_sqr = sqr_teams_coefficients_list[:len(sqr_teams_coefficients_list)//2]
    urna2_sqr = sqr_teams_coefficients_list[len(sqr_teams_coefficients_list)//2:len(sqr_teams_coefficients_list)]

    # Amestecăm echipele din urna2 pentru a crea meciuri random
    random.shuffle(urna2_sqr)

    # Creăm meciurile
    matches = list(zip(urna1_sqr, urna2_sqr))

    print("\n\033[31mSecond qualifying round results - champions path:\033[0m")
    for index, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches_second_qualifying_round_cp(team1, team2)
        if index < 10:
            print(f" {index}. {result}")  # Adăugăm un spațiu pentru primele 9 meciuri
        else:
            print(f"{index}. {result}")  # Nu adăugăm spațiu pentru celelalte meciuri
        list_of_qualified_teams_in_the_third_round_cp.append(result.split("Qualified team: ")[1].strip())
    # Returnăm lista cu echipele calificate
    return list_of_qualified_teams_in_the_third_round_cp


if __name__ == "__main__":
    list_of_qualified_teams_in_the_third_round_cp = run_second_qualifying_round_cp()


selected_teams_sqr_runners_up = []
for country, details in teams_of_cl.items():
    if 10 <= details['position'] <= 15:
        selected_teams_sqr_runners_up.append(details['teams'][1])

print("\n\033[31mRunners-up in the second qualifying round:\033[0m")
# Afișează echipele numerotate de la 1 la 6
for i, team in enumerate(selected_teams_sqr_runners_up, start=1):
    print(f"{i}. {team}")


def simulate_matches_second_qualifying_round_runners_up(team1, team2):
    return simulate_matches(team1, team2, is_runners_up=True)

list_of_qualified_teams_in_the_third_round_runners_up = []


def run_second_qualifying_round_runners_up():
    # Împărțim echipele în două urne
    urna1_sqr_runners_up = selected_teams_sqr_runners_up[:len(selected_teams_sqr_runners_up)//2]
    urna2_sqr_runners_up = selected_teams_sqr_runners_up[len(selected_teams_sqr_runners_up)//2:]

    # Amestecăm echipele din urna2 pentru a crea meciuri random
    random.shuffle(urna2_sqr_runners_up)

    # Creăm meciurile
    matches_sqr_runners_up = list(zip(urna1_sqr_runners_up, urna2_sqr_runners_up))

    print("\n\033[31mSecond qualifying round results - runners-up:\033[0m")
    for index, (team1, team2) in enumerate(matches_sqr_runners_up, start=1):
        result = simulate_matches_second_qualifying_round_runners_up(team1, team2)
        print(f"{index}. {result}")
        list_of_qualified_teams_in_the_third_round_runners_up.append(result.split("Qualified team: ")[1].strip())
    # Returnăm lista cu echipele calificate
    return list_of_qualified_teams_in_the_third_round_runners_up


if __name__ == "__main__":
    list_of_qualified_teams_in_the_third_round_runners_up = run_second_qualifying_round_runners_up()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 9 - the third qualifying round
selected_teams_tqr_runners_up = [] + list_of_qualified_teams_in_the_third_round_runners_up
for country, details in teams_of_cl.items():
    if 7 <= details['position'] <= 9:
        selected_teams_tqr_runners_up.append(details['teams'][1])

print("\n\033[31mRunners-up in the third qualifying round:\033[0m")
# Afișează echipele numerotate de la 1 la 6
for i, team in enumerate(selected_teams_tqr_runners_up, start=1):
    print(f"{i}. {team}")

print("\n\033[32m The teams in the third qualifying round - champions path:\033[0m")
for i, team in enumerate(list_of_qualified_teams_in_the_third_round_cp, start=1):
    if i < 10:
        print(f" {i}. {team}")
    else:
        print(f"{i}. {team}")


def simulate_matches_third_qualifying_round_runners_up(team1, team2):
    return simulate_matches(team1, team2, is_runners_up=True)

list_of_qualified_teams_in_the_playoff_round_runners_up = []


def run_third_qualifying_round_runners_up():
    # Împărțim echipele în două urne
    urna1_tqr_runners_up = selected_teams_tqr_runners_up[:len(selected_teams_tqr_runners_up)//2]
    urna2_tqr_runners_up \
        = selected_teams_tqr_runners_up[len(selected_teams_tqr_runners_up)//2:len(selected_teams_tqr_runners_up)]

    # Amestecăm echipele din urna2 pentru a crea meciuri random
    random.shuffle(urna2_tqr_runners_up)

    # Creăm meciurile
    matches_tqr_runners_up = list(zip(urna1_tqr_runners_up, urna2_tqr_runners_up))

    print("\n\033[31mThird qualifying round results - runners-up:\033[0m")
    for index, (team1, team2) in enumerate(matches_tqr_runners_up, start=1):
        result = simulate_matches_third_qualifying_round_runners_up(team1, team2)
        print(f"{index}. {result}")
        list_of_qualified_teams_in_the_playoff_round_runners_up.append(result.split("Qualified team: ")[1].strip())
    # Returnăm lista cu echipele calificate
    return list_of_qualified_teams_in_the_playoff_round_runners_up


if __name__ == "__main__":
    list_of_qualified_teams_in_the_playoff_round_runners_up = run_third_qualifying_round_runners_up()


# Importăm coeficienții UEFA
# Creăm o listă pentru echipele din rundă a doua de calificare champions path și coeficienții lor
teams_with_coefficients_tr_cp = [(team, uefa_coefficients[team]['coefficient']) for team in
                                 list_of_qualified_teams_in_the_third_round_cp if team in uefa_coefficients]

# Sortăm echipele în funcție de coeficient în ordine descrescătoare
sorted_teams_with_coefficients_tr_cp = sorted(teams_with_coefficients_tr_cp, key=lambda x: x[1], reverse=True)

# Scriem echipele și coeficienții în fișierul sqr_coefficients.py
with open('tqr_coefficients.py', 'w', encoding='utf-8') as f:
    f.write("# Echipele și coeficienții pentru rundă a treia de calificare champions path\n")
    f.write("tqr_teams_coefficients = [\n")
    for position, (team, coefficient) in enumerate(sorted_teams_with_coefficients_tr_cp, start=1):
        f.write(f"    ('{team}', {coefficient}, 'position: {position}'),\n")
    f.write("]\n")


# Funcție pentru a importa lista de echipe și coeficienții lor din fișierul sqr_coefficients.py
def import_tqr_teams_coefficients(file_location_3):
    return import_attribute_from_module(file_location_3, 'tqr_teams_coefficients')


# Funcție pentru a împărți echipele în două urne și a le afișa
def create_and_print_urns_tr(coefficients):
    create_and_print_urns(coefficients, urn_count=2)


if __name__ == "__main__":
    file_path_3 = 'tqr_coefficients.py'
    try:
        tqr_teams_coefficients_list = import_tqr_teams_coefficients(file_path_3)
        create_and_print_urns_sr(tqr_teams_coefficients_list)
    except (ImportError, AttributeError, IOError) as e_9:
        print(f"Eroare: {e_9}")


def simulate_matches_third_qualifying_round_cp(team1, team2):
    return simulate_matches(team1, team2)

list_of_qualified_teams_in_the_playoff_round_cp = []


def run_third_qualifying_round_cp():
    # Sortăm echipele după coeficient
    tqr_teams_coefficients.sort(key=lambda x: x[1], reverse=True)

    # Împărțim echipele în două urne
    urna1_tqr = tqr_teams_coefficients_list[:len(tqr_teams_coefficients_list)//2]
    urna2_tqr = tqr_teams_coefficients_list[len(tqr_teams_coefficients_list)//2:len(tqr_teams_coefficients_list)]

    # Amestecăm echipele din urna2 pentru a crea meciuri random
    random.shuffle(urna2_tqr)

    # Creăm meciurile
    matches = list(zip(urna1_tqr, urna2_tqr))

    print("\n\033[31mThird qualifying round results - champions path:\033[0m")
    for index, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches_third_qualifying_round_cp(team1, team2)
        if index < 10:
            print(f" {index}. {result}")  # Adăugăm un spațiu pentru primele 9 meciuri
        else:
            print(f"{index}. {result}")  # Nu adăugăm spațiu pentru celelalte meciuri
        list_of_qualified_teams_in_the_playoff_round_cp.append(result.split("Qualified team: ")[1].strip())
    # Returnăm lista cu echipele calificate
    return list_of_qualified_teams_in_the_playoff_round_cp


if __name__ == "__main__":
    list_of_qualified_teams_in_the_playoff_round_cp = run_third_qualifying_round_cp()

# ---------------------------------------------------------------------------------------------------------------------
# Punctul 10 - the play-off round
selected_teams_por_runners_up = [] + list_of_qualified_teams_in_the_playoff_round_runners_up
for country, details in teams_of_cl.items():
    if details['position'] == 6:
        selected_teams_por_runners_up.append(details['teams'][2])

print("\n\033[31mRunners-up in the play-off round:\033[0m")
for i, team in enumerate(selected_teams_por_runners_up, start=1):
    print(f"{i}. {team}")


def simulate_matches_playoff_round_runners_up(team1, team2):
    return simulate_matches(team1, team2, is_runners_up=True)


list_of_qualified_teams_in_the_group_round_runners_up = []


def run_playoff_round_runners_up():
    # Împărțim echipele în două urne
    urna1_por_runners_up = selected_teams_por_runners_up[:len(selected_teams_por_runners_up)//2]
    urna2_por_runners_up \
        = selected_teams_por_runners_up[len(selected_teams_por_runners_up)//2:len(selected_teams_por_runners_up)]

    # Amestecăm echipele din urna2 pentru a crea meciuri random
    random.shuffle(urna2_por_runners_up)

    # Creăm meciurile
    matches_por_runners_up = list(zip(urna1_por_runners_up, urna2_por_runners_up))

    print("\n\033[31m2a.Play-off round results - runners-up:\033[0m")
    for index, (team1, team2) in enumerate(matches_por_runners_up, start=1):
        result = simulate_matches_playoff_round_runners_up(team1, team2)
        print(f"{index}. {result}")
        list_of_qualified_teams_in_the_group_round_runners_up.append(result.split("Qualified team: ")[1].strip())
    # Returnăm lista cu echipele calificate
    return list_of_qualified_teams_in_the_group_round_runners_up


if __name__ == "__main__":
    list_of_qualified_teams_in_the_group_round_runners_up = run_playoff_round_runners_up()


selected_teams_por_cp = [] + list_of_qualified_teams_in_the_playoff_round_cp
for country, details in teams_of_cl.items():
    if 12 <= details['position'] <= 14:
        selected_teams_por_cp.append(details['teams'][0])

print("\n\033[31m3a. The teams in the play-off round - champions path::\033[0m")
for i, team in enumerate(selected_teams_por_cp, start=1):
    print(f"{i}. {team}")

# Importăm coeficienții UEFA
# Creăm o listă pentru echipele din rundă a doua de calificare champions path și coeficienții lor
teams_with_coefficients_por_cp = [(team, uefa_coefficients[team]['coefficient']) for team in
                                 selected_teams_por_cp if team in uefa_coefficients]

# Sortăm echipele în funcție de coeficient în ordine descrescătoare
sorted_teams_with_coefficients_por_cp = sorted(teams_with_coefficients_por_cp, key=lambda x: x[1], reverse=True)

# Scriem echipele și coeficienții în fișierul sqr_coefficients.py
with open('por_coefficients.py', 'w', encoding='utf-8') as f:
    f.write("# Echipele și coeficienții pentru play-off - champions path\n")
    f.write("por_teams_coefficients = [\n")
    for position, (team, coefficient) in enumerate(sorted_teams_with_coefficients_por_cp, start=1):
        f.write(f"    ('{team}', {coefficient}, 'position: {position}'),\n")
    f.write("]\n")


# Funcție pentru a importa lista de echipe și coeficienții lor din fișierul sqr_coefficients.py
def import_por_teams_coefficients(file_location_4):
    return import_attribute_from_module(file_location_4, 'por_teams_coefficients')


# Funcție pentru a împărți echipele în două urne și a le afișa
def create_and_print_urns_por(coefficients):
    create_and_print_urns(coefficients, urn_count=2)


if __name__ == "__main__":
    file_path_4 = 'por_coefficients.py'
    try:
        por_teams_coefficients_list = import_por_teams_coefficients(file_path_4)
        create_and_print_urns_por(por_teams_coefficients_list)
    except (ImportError, AttributeError, IOError) as e_10:
        print(f"Eroare: {e_10}")


def simulate_matches_playoff_round_cp(team1, team2):
    return simulate_matches(team1, team2)

list_of_qualified_teams_in_the_group_round_cp = []


def run_playoff_round_cp():
    # Sortăm echipele după coeficient
    por_teams_coefficients.sort(key=lambda x: x[1], reverse=True)

    # Împărțim echipele în două urne
    urna1_por = por_teams_coefficients_list[:len(por_teams_coefficients_list)//2]
    urna2_por = por_teams_coefficients_list[len(por_teams_coefficients_list)//2:len(por_teams_coefficients_list)]

    # Amestecăm echipele din urna2 pentru a crea meciuri random
    random.shuffle(urna2_por)

    # Creăm meciurile
    matches = list(zip(urna1_por, urna2_por))

    print("\n\033[31m4a.Play-off round results - champions path:\033[0m")
    for index, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches_playoff_round_cp(team1, team2)
        if index < 10:
            print(f" {index}. {result}")  # Adăugăm un spațiu pentru primele 9 meciuri
        else:
            print(f"{index}. {result}")  # Nu adăugăm spațiu pentru celelalte meciuri
        list_of_qualified_teams_in_the_group_round_cp.append(result.split("Qualified team: ")[1].strip())
    # Returnăm lista cu echipele calificate
    return list_of_qualified_teams_in_the_group_round_cp


if __name__ == "__main__":
    list_of_qualified_teams_in_the_group_round_cp = run_playoff_round_cp()
# ---------------------------------------------------------------------------------------------------------------
# Punctul 11 - the group round
automatic_champions_group = []
for country, details in teams_of_cl.items():
    if 1 <= details['position'] <= 11:
        automatic_champions_group.append(details['teams'][0])

automatic_runners_up_group = []
for country, details in teams_of_cl.items():
    if 1 <= details['position'] <= 6:
        automatic_runners_up_group.append(details['teams'][1])

automatic_third_group = []
for country, details in teams_of_cl.items():
    if 1 <= details['position'] <= 5:
        automatic_third_group.append(details['teams'][2])

automatic_fourth_group = []
for country, details in teams_of_cl.items():
    if 1 <= details['position'] <= 4:
        automatic_fourth_group.append(details['teams'][3])


automatic_selected_teams_group = automatic_champions_group + automatic_runners_up_group \
                                 + automatic_third_group + automatic_fourth_group
list_of_qualified_teams_in_the_group = list_of_qualified_teams_in_the_group_round_cp \
                                       + list_of_qualified_teams_in_the_group_round_runners_up\
                                       + automatic_selected_teams_group
print("\n\033[31m The teams in the group round:\033[0m")
for i, team in enumerate(list_of_qualified_teams_in_the_group, start=1):
    if i < 10:
        print(f" {i}. {team}")
    else:
        print(f"{i}. {team}")
# ---------------------------------------------------------------------------------------------------------------------
# # Punctul 12 - afișare urne

# Importăm coeficienții UEFA
# Creăm o listă pentru echipele din rundă a doua de calificare champions path și coeficienții lor
teams_with_coefficients_group = [(team, uefa_coefficients[team]['coefficient'], uefa_coefficients[team]['country'])
                                 for team in list_of_qualified_teams_in_the_group if team in uefa_coefficients]

# Sortăm echipele în funcție de coeficient în ordine descrescătoare
sorted_teams_with_coefficients_group = sorted(teams_with_coefficients_group, key=lambda x: x[1], reverse=True)

# Scriem echipele și coeficienții în fișierul group_coefficients.py
with open('group_coefficients.py', 'w', encoding='utf-8') as f:
    f.write("# Echipele și coeficienții pentru grupe\n")
    f.write("group_teams_coefficients = [\n")
    for position, (team, coefficient, country) in enumerate(sorted_teams_with_coefficients_group, start=1):
        f.write(f"    ('{team}', {coefficient}, 'position: {position}', '- {country}'),\n")
    f.write("]\n")


# Funcție pentru a importa lista de echipe și coeficienții lor din fișierul sqr_coefficients.py
def import_group_teams_coefficients(file_location_group):
    return import_attribute_from_module(file_location_group, 'group_teams_coefficients')


# Funcție pentru a împărți echipele în 4 urne și a le afișa
def create_and_print_urns_group(coefficients):
    create_and_print_urns(coefficients, urn_count=4)

if __name__ == "__main__":
    file_path_group = 'group_coefficients.py'
    try:
        list_of_qualified_teams_in_the_group = import_group_teams_coefficients(file_path_group)
        create_and_print_urns_group(list_of_qualified_teams_in_the_group)
    except (ImportError, AttributeError, IOError) as e_11:
        print(f"Eroare: {e_11}")
print()
print()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 12 - extragere grupe


# Extragerea grupei în patru urne
def split_into_pots(group_teams):
    pot_1, pot_2, pot_3, pot_4 = [], [], [], []

    for team, coeff, pos, country in group_teams:
        pos_num = int(pos.split(":")[1].strip())
        if pos_num <= 8:
            pot_1.append((team, country))
        elif 9 <= pos_num <= 16:
            pot_2.append((team, country))
        elif 17 <= pos_num <= 24:
            pot_3.append((team, country))
        elif 25 <= pos_num <= 32:
            pot_4.append((team, country))

    return pot_1, pot_2, pot_3, pot_4


# Validarea grupelor
def validate_groups(groups):
    for group in groups:
        countries = [team[1] for team in group]
        if len(countries) != len(set(countries)):
            return False
    return True


def draw_teams(pot1, pot2, pot3, pot4):
    while True:
        temp_groups = [[] for _ in range(8)]

        random.shuffle(pot1)
        random.shuffle(pot2)
        random.shuffle(pot3)
        random.shuffle(pot4)

        all_pots = [pot1, pot2, pot3, pot4]
        valid_draw = True

        for current_pot in all_pots:
            for x in range(8):
                added = False
                for j in range(len(current_pot)):
                    team, country = current_pot[j]
                    if all(country != team_country for _, team_country in temp_groups[x]):
                        temp_groups[x].append(current_pot.pop(j))
                        added = True
                        break
                if not added:
                    valid_draw = False
                    print("trebuie refăcut")
                    break
            if not valid_draw:
                break

        if valid_draw and validate_groups(temp_groups):
            return temp_groups


def print_groups(groups):
    print("Group A".ljust(39), "Group B".ljust(39), "Group C".ljust(39), "Group D".ljust(39))
    for idx in range(4):
        for j in range(4):
            if j < 3:
                print(groups[j][idx][0].ljust(40), end="")
                time.sleep(1)
            else:
                print(groups[j][idx][0].ljust(39), end="")
                time.sleep(1)
        print()

    print()
    print("Group E".ljust(39), "Group F".ljust(39), "Group G".ljust(39), "Group H".ljust(39))
    for x in range(4):
        for j in range(4, 8):
            if j < 7:
                print(groups[j][x][0].ljust(40), end="")
                time.sleep(1)
            else:
                print(groups[j][x][0].ljust(39), end="")
                time.sleep(1)
        print()


def groups_to_dict(groups):
    group_names = ["Group A", "Group B", "Group C", "Group D", "Group E", "Group F", "Group G", "Group H"]
    return {group_names[x]: [team for team, _ in group] for x, group in enumerate(groups)}

# ---------------------------------------------------------------------------------------------------------------------
# Punctul 12+ - group phase


# Inițializarea clasamentului
def init_standings(teams):
    return {team: {
        "played": 0, "won": 0, "drawn": 0, "lost": 0,
        "goals_scored": 0, "goals_conceded": 0,
        "goal_difference": 0, "points": 0,
        "head_to_head": {opponent: {"points": 0, "goal_difference": 0} for opponent in teams if opponent != team}
    } for team in teams}


# actualizarea clasamentului
def update_standings(standings, team1, team2, score1, score2):
    standings[team1]["played"] += 1
    standings[team2]["played"] += 1

    if score1 > score2:
        standings[team1]["won"] += 1
        standings[team2]["lost"] += 1
        standings[team1]["points"] += 3
        standings[team1]["head_to_head"][team2]["points"] += 3
    elif score1 < score2:
        standings[team2]["won"] += 1
        standings[team1]["lost"] += 1
        standings[team2]["points"] += 3
        standings[team2]["head_to_head"][team1]["points"] += 3
    else:
        standings[team1]["drawn"] += 1
        standings[team2]["drawn"] += 1
        standings[team1]["points"] += 1
        standings[team2]["points"] += 1
        standings[team1]["head_to_head"][team2]["points"] += 1
        standings[team2]["head_to_head"][team1]["points"] += 1

    standings[team1]["goals_scored"] += score1
    standings[team1]["goals_conceded"] += score2
    standings[team2]["goals_scored"] += score2
    standings[team2]["goals_conceded"] += score1

    standings[team1]["goal_difference"] = standings[team1]["goals_scored"] - standings[team1]["goals_conceded"]
    standings[team2]["goal_difference"] = standings[team2]["goals_scored"] - standings[team2]["goals_conceded"]

    standings[team1]["head_to_head"][team2]["goal_difference"] += (score1 - score2)
    standings[team2]["head_to_head"][team1]["goal_difference"] += (score2 - score1)

all_team_list = []


def print_standings(group_name, standings):
    sorted_teams = sorted(standings.items(), key=lambda x: (
        x[1]['points'],
        x[1]['goal_difference']
    ), reverse=True)

    print(f"---------------------------------------------------------------")
    print(f"{group_name:>63}")
    print(f"#  {'Team':<35} {'G':>2} {'W':>2} {'D':>2} {'L':>2} {'GF':>2} {'GA':>2} {'GD':>3} {'P':>2}")

    # team_list = []

    for idx, (team, stats) in enumerate(sorted_teams, start=1):
        winnings = (stats['won'] * 10_000_000) + (stats['drawn'] * 5_000_000)
        print(
            f"{idx:}. {team:<35} {stats['played']:>2} {stats['won']:>2} {stats['drawn']:>2} {stats['lost']:>2} "
            f"{stats['goals_scored']:>2} {stats['goals_conceded']:>2} {stats['goal_difference']:>3} "
            f"\033[31m{stats['points']:>2}\033[0m  {winnings:>10}€"
        )

qualified_1st_teams_in_round16 = []
qualified_2nd_teams_in_round16 = []


def generate_matches_and_earnings(group_dict):
    match_results_dict = {group: {leg: [] for leg in range(1, 7)} for group in group_dict}
    team_earnings = []

    for group, teams in group_dict.items():
        standings = init_standings(teams)
        legs = [
            [(0, 1), (2, 3)],  # Leg 1
            [(2, 0), (3, 1)],  # Leg 2
            [(0, 3), (1, 2)],  # Leg 3
            [(3, 0), (2, 1)],  # Leg 4
            [(1, 0), (3, 2)],  # Leg 5
            [(0, 2), (1, 3)],  # Leg 6
        ]

        for leg_num, leg in enumerate(legs, start=1):
            for (home_idx, away_idx) in leg:
                home_team = teams[home_idx]
                away_team = teams[away_idx]
                home_score = random.randint(0, 3)
                away_score = random.randint(0, 3)
                match_results_dict[group][leg_num].append(f"{home_team:<25} vs {away_team:<25} "
                                                          f"{home_score} - {away_score}")
                update_standings(standings, home_team, away_team, home_score, away_score)

        print_standings(group, standings)
        sorted_standings = sorted(standings.items(), key=lambda x: (
            x[1]['points'],
            x[1]['goal_difference']
        ), reverse=True)

        qualified_1st_teams_in_round16.append(sorted_standings[0][0])
        qualified_2nd_teams_in_round16.append(sorted_standings[1][0])
        print()

        for leg in range(6):
            for match in match_results_dict[group][leg + 1]:
                print(f"{leg + 1}. {match}")

        # Calculăm câștigurile pentru echipele din grup
        for team, stats in sorted_standings:
            winnings = (stats['won'] * 10_000_000) + (stats['drawn'] * 5_000_000)
            team_earnings.append((team, winnings))

    # Salvăm lista de câștiguri în fișierul teams_info.py
    with open('earnings_group_stage.py', 'w', encoding='utf-8') as fg:
        fg.write("# Lista echipei și câștigurile lor\n")
        fg.write("earnings_group_stage = [\n")
        for team, earnings_group in team_earnings:
            fg.write(f"    ('{team}', {earnings_group}),\n")
        fg.write("]\n")

    return match_results_dict, qualified_1st_teams_in_round16, qualified_2nd_teams_in_round16

if __name__ == "__main__":
    file_path_group = 'group_coefficients.py'
    generated_groups = {}
    try:
        list_of_qualified_teams_in_the_group = import_group_teams_coefficients(file_path_group)
        pot_one, pot_two, pot_three, pot_four = split_into_pots(list_of_qualified_teams_in_the_group)
        drawn_groups = draw_teams(pot_one, pot_two, pot_three, pot_four)
        print_groups(drawn_groups)
        generated_groups = groups_to_dict(drawn_groups)

    except (ImportError, AttributeError, IOError, ValueError) as e:
        print(f"Error: {e}")
        generated_groups = {}

    if generated_groups:
        match_results, qualified_1st_teams_in_round16, qualified_2nd_teams_in_round16 \
            = generate_matches_and_earnings(generated_groups)
        print("\nThe qualified teams in the round of 16 are:")
        print("Teams qualified in first place:")
        for crt, team in enumerate(qualified_1st_teams_in_round16, start=1):
            print(f" {crt}. {team}")

        print("\nTeams qualified in second place:")
        for crt, team in enumerate(qualified_2nd_teams_in_round16, start=1):
            print(f" {crt}. {team}")

# ---------------------------------------------------------------------------------------------------------------------
# Punctul 14 - round of 16

# Importăm coeficienții UEFA
teams_with_coefficients_round16_pos1 = [(team, uefa_coefficients[team]['coefficient']) for team in
                              qualified_1st_teams_in_round16 if team in uefa_coefficients]
teams_with_coefficients_round16_pos2 = [(team, uefa_coefficients[team]['coefficient']) for team in
                              qualified_2nd_teams_in_round16 if team in uefa_coefficients]

sorted_teams_with_coefficients_round16_pos1 =\
    sorted(teams_with_coefficients_round16_pos1, key=lambda x: x[1], reverse=True)

sorted_teams_with_coefficients_round16_pos2 =\
    sorted(teams_with_coefficients_round16_pos2, key=lambda x: x[1], reverse=True)

# Scriem echipele și coeficienții în fișierul round16_coefficients.py
with open('round16_coefficients.py', 'w', encoding='utf-8') as f:
    f.write("# Echipele și coeficienții din șaisprezecimi care au terminat pe prima poziție în grupe\n")
    f.write("round16_pos1_teams_coefficients = [\n")
    for position, (team, coefficient) in enumerate(sorted_teams_with_coefficients_round16_pos1, start=1):
        f.write(f"    ('{team}', {coefficient}, 'position: {position}'),\n")
    f.write("]\n")

    f.write("# Echipele și coeficienții din șaisprezecimi care au terminat pe prima poziție în grupe\n")
    f.write("round16_pos2_teams_coefficients = [\n")
    for position, (team, coefficient) in enumerate(sorted_teams_with_coefficients_round16_pos2, start=1):
        f.write(f"    ('{team}', {coefficient}, 'position: {position}'),\n")
    f.write("]\n")


def import_attribute_from_module(module_name, attribute_name):
    # Funcția import_attribute_from_module trebuie să fie definită
    module = __import__(module_name)
    return getattr(module, attribute_name)


def import_round16_pos1_teams_coefficients(file_location_round16_pos1):
    return import_attribute_from_module(file_location_round16_pos1, 'round16_pos1_teams_coefficients')


def import_round16_pos2_teams_coefficients(file_location_round16_pos2):
    return import_attribute_from_module(file_location_round16_pos2, 'round16_pos2_teams_coefficients')


def create_and_print_urns_round16(pos1_coefficients, pos2_coefficients):
    # Funcție pentru a afișa urnele
    print("\n Urna 1 - round of 16                           Urna 2 - round of 16")
    for x, (team1, team2) in enumerate(zip(pos1_coefficients, pos2_coefficients), start=1):
        rank1 = f" {x}. {team1[0].ljust(30)} - {str(team1[1]).rjust(5)}"
        rank2a = f" {x + 8}. {team2[0].ljust(30)} - {str(team2[1]).rjust(5)}"
        rank2b = f"{x + 8}. {team2[0].ljust(30)} - {str(team2[1]).rjust(5)}"
        if x < 2:
            print(rank1 + "     " + rank2a)
        else:
            print(rank1 + "     " + rank2b)


def simulate_matches_round16(team1, team2):
    return simulate_matches(team1, team2)

list_of_qualified_teams_in_the_round8 = []


def run_round16():
    try:
        file_location_round16_pos1 = 'round16_coefficients'
        round16_pos1_teams_coefficients_list = import_round16_pos1_teams_coefficients(file_location_round16_pos1)
    except (ImportError, AttributeError, IOError) as e_r16:
        print(f"Eroare: {e_r16}")
        return []

    try:
        file_location_round16_pos2 = 'round16_coefficients'
        round16_pos2_teams_coefficients_list = import_round16_pos2_teams_coefficients(file_location_round16_pos2)
    except (ImportError, AttributeError, IOError) as e_r16:
        print(f"Eroare: {e_r16}")
        return []

    # Sortăm echipele după coeficient
    round16_pos1_teams_coefficients_list.sort(key=lambda x: x[1], reverse=True)
    round16_pos2_teams_coefficients_list.sort(key=lambda x: x[1], reverse=True)

    # Afișăm urnele
    create_and_print_urns_round16(round16_pos1_teams_coefficients_list, round16_pos2_teams_coefficients_list)

    # Împărțim echipele în două urne
    urna1_round16 = round16_pos1_teams_coefficients_list
    urna2_round16 = round16_pos2_teams_coefficients_list

    # Amestecăm echipele din urna2 pentru a crea meciuri random
    random.shuffle(urna2_round16)

    # Creăm meciurile
    matches = list(zip(urna1_round16, urna2_round16))

    print("\n\033[31mRound of 16:\033[0m")
    for index, (team1, team2) in enumerate(matches, start=1):
        result_round16 = simulate_matches_round16(team1, team2)
        if index < 10:
            print(f" {index}. {result_round16}")  # Adăugăm un spațiu pentru primele 9 meciuri
        else:
            print(f"{index}. {result_round16}")  # Nu adăugăm spațiu pentru celelalte meciuri
        list_of_qualified_teams_in_the_round8.append(result_round16.split("Qualified team: ")[1].strip())

    # Returnăm lista cu echipele calificate
    return list_of_qualified_teams_in_the_round8

if __name__ == "__main__":
    list_of_qualified_teams_in_the_round8 = run_round16()
    print("\nEchipele calificate în round of 8")
    for current_number, team in enumerate(list_of_qualified_teams_in_the_round8, start=1):
        print(f"{current_number}. {team}")

# --------------------------------------------------------------------------------------------------------------------
# Punctul 15 - round of 8

# Importăm coeficienții UEFA
teams_with_coefficients_round8 = [(team, uefa_coefficients[team]['coefficient']) for team in
                              list_of_qualified_teams_in_the_round8 if team in uefa_coefficients]

sorted_teams_with_coefficients_round8 = sorted(teams_with_coefficients_round8, key=lambda x: x[1], reverse=True)

# Scriem echipele și coeficienții în fișierul round8_coefficients.py
with open('round8_coefficients.py', 'w', encoding='utf-8') as f:
    f.write("# Echipele și coeficienții din optimi\n")
    f.write("round8_teams_coefficients = [\n")
    for position, (team, coefficient) in enumerate(sorted_teams_with_coefficients_round8, start=1):
        f.write(f"    ('{team}', {coefficient}, 'position: {position}'),\n")
    f.write("]\n")

# # ca și cum s-ar scrie
# from round8_coefficients import round8_teams_coefficients as list_of_qualified_teams_in_the_round8


def import_round8_teams_coefficients(file_location_round8):
    return import_attribute_from_module(file_location_round8, 'round8_teams_coefficients')

# Folosim funcțiile definite pentru a importa și redenumi atributul
file_location_round8r = 'round8_coefficients'
list_of_qualified_teams_in_the_round8 = import_round8_teams_coefficients(file_location_round8r)


def simulate_matches_round8(team1, team2):
    return simulate_matches(team1, team2)


list_of_qualified_teams_in_the_round4 = []


def run_round8():
    matches = [
        (list_of_qualified_teams_in_the_round8[0], list_of_qualified_teams_in_the_round8[1]),
        (list_of_qualified_teams_in_the_round8[2], list_of_qualified_teams_in_the_round8[3]),
        (list_of_qualified_teams_in_the_round8[4], list_of_qualified_teams_in_the_round8[5]),
        (list_of_qualified_teams_in_the_round8[6], list_of_qualified_teams_in_the_round8[7])
    ]

    print("\n\033[31mRound of 8:\033[0m")
    for index, (team1, team2) in enumerate(matches, start=1):
        result_round8 = simulate_matches_round8(team1, team2)
        print(f"{index}. {result_round8}")
        list_of_qualified_teams_in_the_round4.append(result_round8.split("Qualified team: ")[1].strip())

    return list_of_qualified_teams_in_the_round4

if __name__ == "__main__":
    list_of_qualified_teams_in_the_round4 = run_round8()
    print("\nEchipele calificate în semifinale")
    for current_number, team in enumerate(list_of_qualified_teams_in_the_round4, start=1):
        print(f"{current_number}. {team}")

# --------------------------------------------------------------------------------------------------------------------
# Punctul 15 - round of 4

# Importăm coeficienții UEFA
teams_with_coefficients_round4 = [(team, uefa_coefficients[team]['coefficient']) for team in
                                  list_of_qualified_teams_in_the_round4 if team in uefa_coefficients]

sorted_teams_with_coefficients_round4 = sorted(teams_with_coefficients_round4, key=lambda x: x[1], reverse=True)

# Scriem echipele și coeficienții în fișierul round4_coefficients.py
with open('round4_coefficients.py', 'w', encoding='utf-8') as f:
    f.write("# Echipele și coeficienții din semifinale\n")
    f.write("round4_teams_coefficients = [\n")
    for position, (team, coefficient) in enumerate(sorted_teams_with_coefficients_round4, start=1):
        f.write(f"    ('{team}', {coefficient}, 'position: {position}'),\n")
    f.write("]\n")


# # ca și cum s-ar scrie
# from round4_coefficients import round4_teams_coefficients as list_of_qualified_teams_in_the_round4


def import_round4_teams_coefficients(file_location_round4):
    return import_attribute_from_module(file_location_round4, 'round4_teams_coefficients')

# Folosim funcțiile definite pentru a importa și redenumi atributul
file_location_round4r = 'round4_coefficients'
list_of_qualified_teams_in_the_round4 = import_round4_teams_coefficients(file_location_round4r)


def simulate_matches_round4(team1, team2):
    return simulate_matches(team1, team2)


list_of_qualified_teams_in_the_round2 = []


def run_round4():
    matches = [
        (list_of_qualified_teams_in_the_round4[0], list_of_qualified_teams_in_the_round4[1]),
        (list_of_qualified_teams_in_the_round4[2], list_of_qualified_teams_in_the_round4[3])
    ]

    print("\n\033[31mSemifinals:\033[0m")
    for index, (team1, team2) in enumerate(matches, start=1):
        result_round4 = simulate_matches_round8(team1, team2)
        print(f"{index}. {result_round4}")
        list_of_qualified_teams_in_the_round2.append(result_round4.split("Qualified team: ")[1].strip())

    return list_of_qualified_teams_in_the_round2


if __name__ == "__main__":
    list_of_qualified_teams_in_the_round2 = run_round4()
    print("\nEchipele calificate în finală")
    for current_number, team in enumerate(list_of_qualified_teams_in_the_round2, start=1):
        print(f"{current_number}. {team}")

# --------------------------------------------------------------------------------------------------------------------
# Punctul 16 - final


def simulate_matches_round4(team1, team2):
    return simulate_1leg_matches(team1, team2)

list_of_qualified_teams_in_the_round1 = []


def run_round2():
    matches = [(list_of_qualified_teams_in_the_round2[0], list_of_qualified_teams_in_the_round2[1])]

    print("\n\033[31mFinala:\033[0m")
    for index, (team1, team2) in enumerate(matches, start=1):
        result_round2 = simulate_matches_round4(team1, team2)
        match_text, qualified_team = result_round2
        print(f"{index}. {match_text}")
        list_of_qualified_teams_in_the_round1.append(qualified_team)

    return list_of_qualified_teams_in_the_round1


if __name__ == "__main__":
    list_of_qualified_teams_in_the_round1 = run_round2()
    print(f"\nChampions League Winner: {list_of_qualified_teams_in_the_round1[0]}")
# --------------------------------------------------------------------------------------------------------------------
# # Punctul 17 - artificii, fireworks
#
# # Initialize Pygame
# pygame.init()
#
# # Constants
# SCREEN_WIDTH = 800
# SCREEN_HEIGHT = 600
# WHITE = (255, 255, 255)
# BLACK = (0, 0, 0)
# RED = (255, 0, 0)
# BLUE = (0, 0, 255)
# GREEN = (0, 255, 0)
# YELLOW = (255, 255, 0)
# FPS = 60
#
# # Create the screen
# screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
# pygame.display.set_caption("Champions League Winner")
#
# # Set up font
# font = pygame.font.Font(None, 74)
# small_font = pygame.font.Font(None, 48)
#
#
# # Function to draw fireworks
# def draw_fireworks():
#     for _ in range(50):
#         x = random.randint(0, SCREEN_WIDTH)
#         y = random.randint(0, SCREEN_HEIGHT)
#         color = random.choice([RED, BLUE, GREEN, YELLOW])
#         pygame.draw.circle(screen, color, (x, y), random.randint(5, 10))
#
#
# # Main loop
# running = True
# clock = pygame.time.Clock()
# while running:
#     for event in pygame.event.get():
#         if event.type == pygame.QUIT:
#             running = False
#
#     screen.fill(BLACK)
#
#     # Draw fireworks
#     draw_fireworks()
#
#     # Draw text
#     # main_text = font.render("Champions League Winner", True, WHITE)
#     # team_text = small_font.render(list_of_qualified_teams_in_the_round1[0], True, WHITE)
#     team_text = small_font.render("Champions League Winner", True, WHITE)
#     main_text = font.render(list_of_qualified_teams_in_the_round1[0], True, WHITE)
#
#     # Calculate positions
#     main_text_rect = main_text.get_rect(center=(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 20))
#     team_text_rect = team_text.get_rect(center=(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 + 40))
#
#     # Blit the text
#     screen.blit(main_text, main_text_rect)
#     screen.blit(team_text, team_text_rect)
#
#     pygame.display.flip()
#     clock.tick(FPS)
#
# pygame.quit()
# --------------------------------------------------------------------------------------------------------------------
# # # Punctul 18 - câștigul fiecărei echipe
#
# # Definirea listelor cu echipe și premiile corespunzătoare pentru fiecare etapă
# pr_teams_for_prizes = teams_pr
# # winner1, winner2, final_winner_pr
# runnerup_pr = winner2 if final_winner_pr == winner1 else winner1
# fqr_teams_for_prizes = teams_1qr
# sqr_teams_for_prizes = the_list_with_the_teams_in_the_second_round + selected_teams_sqr_runners_up
# tqr_teams_for_prizes = list_of_qualified_teams_in_the_third_round_cp + selected_teams_tqr_runners_up
# por_teams_for_prizes = selected_teams_por_cp + selected_teams_por_runners_up
# group_teams_for_prizes = [team for team, _, _, _ in list_of_qualified_teams_in_the_group]
#
# round16_teams_for_prizes = qualified_1st_teams_in_round16 + qualified_2nd_teams_in_round16
# round8_teams_for_prizes = [team for team, _, _ in list_of_qualified_teams_in_the_round8]
# round4_teams_for_prizes = [team for team, _, _ in list_of_qualified_teams_in_the_round4]
# round2_teams_for_prizes = list_of_qualified_teams_in_the_round2
# runnerup_cl = list_of_qualified_teams_in_the_round2[0] \
#     if list_of_qualified_teams_in_the_round2[1] == list_of_qualified_teams_in_the_round1[0] \
#     else list_of_qualified_teams_in_the_round2[1]
# round1_teams_for_prizes = list_of_qualified_teams_in_the_round1
#
# prizes = {
#     "pr_teams_for_prizes": 100000,
#     "final_winner_pr": 250000,
#     "runnerup_pr": 150000,
#     "fqr_teams_for_prizes": 500000,
#     "sqr_teams_for_prizes": 1000000,
#     "tqr_teams_for_prizes": 1500000,
#     "por_teams_for_prizes": 2500000,
#     "group_teams_for_prizes": 25000000,
#     "round16_teams_for_prizes": 15000000,
#     "round8_teams_for_prizes": 25000000,
#     "round4_teams_for_prizes": 40000000,
#     "runnerup_cl": 55000000,
#     "round1_teams_for_prizes": 120000000
# }
#
# # Calculul premiilor pentru fiecare echipă
# team_prizes = {}
#
#
# # Funcție pentru a adăuga premiul la echipe
# def add_prize(teams, prize_value):
#     for team in teams:
#         if team in team_prizes:
#             team_prizes[team] += prize_value
#         else:
#             team_prizes[team] = prize_value
#
# # Adăugarea premiilor pentru echipele din fiecare categorie
# add_prize(teams_pr, prizes["pr_teams_for_prizes"])
# add_prize([final_winner_pr], prizes["final_winner_pr"])
# add_prize([runnerup_pr], prizes["runnerup_pr"])
# add_prize(fqr_teams_for_prizes, prizes["fqr_teams_for_prizes"])
# add_prize(sqr_teams_for_prizes, prizes["sqr_teams_for_prizes"])
# add_prize(tqr_teams_for_prizes, prizes["tqr_teams_for_prizes"])
# add_prize(por_teams_for_prizes, prizes["por_teams_for_prizes"])
# add_prize(group_teams_for_prizes, prizes["group_teams_for_prizes"])
#
# # Adăugarea premiilor din earnings_group_stage
# if isinstance(earnings_group_stage, list):
#     for team, earnings in earnings_group_stage:
#         add_prize([team], earnings)
#
# # Adăugarea premiilor pentru alte etape
# add_prize(round16_teams_for_prizes, prizes["round16_teams_for_prizes"])
# add_prize(round8_teams_for_prizes, prizes["round8_teams_for_prizes"])
# add_prize(round4_teams_for_prizes, prizes["round4_teams_for_prizes"])
# add_prize([runnerup_cl], prizes["runnerup_cl"])
# add_prize(round1_teams_for_prizes, prizes["round1_teams_for_prizes"])
#
# # Sortarea echipei în funcție de câștigurile totale
# sorted_teams_earnings = sorted(team_prizes.items(), key=lambda x: x[1], reverse=True)
#
# # Scrierea rezultatelor în fișierul prizes.py
# with open('prizes.py', 'w', encoding='utf-8') as f:
#     f.write('teams_prizes = [\n')
#     for team, prize in sorted_teams_earnings:
#         f.write(f"    ('{team:<28}', {prize}),\n")
#     f.write(']\n')
#
#
# # total premii este afișat după premiilor aferente fiecărei echipe
# total_prize = 0
# for k, (team, prize) in enumerate(sorted_teams_earnings, start=1):
#     formatted_prize = f"{prize:,.0f}".replace(",", ".")
#     print(f"{k:>2}. {team:<28}- {formatted_prize:>14} €")
#     total_prize += prize
#
# formatted_total_prize = f"{total_prize:,.0f}".replace(",", ".")
# print(f"**. Total premii:                 {formatted_total_prize:>14} €")

# -------------------------------------------------------------------------


# ----------------------------------------------------------------
"""
# total premii este afișat înaintea premiilor aferente fiecărei echipe
total_prize = 0  # Inițializează variabila pentru suma totală

# Calculează suma totală a premiilor
for _, (_, prize) in enumerate(sorted_teams_earnings, start=1):
    total_prize += prize

formatted_total_prize = f"{total_prize:,.0f}".replace(",", ".")
print(f"**. Total premii:                 {formatted_total_prize:>14} €")

# Afișează fiecare echipă și premiul său
for k, (team, prize) in enumerate(sorted_teams_earnings, start=1):
    formatted_prize = f"{prize:,.0f}".replace(",", ".")
    print(f"{k:>2}. {team:<28}- {formatted_prize:>14} €")
"""
# ---------------------------------------
"""
# Definirea listelor cu echipe și premiile corespunzătoare pentru fiecare etapă
pr_teams_for_prizes = teams_pr
runnerup_pr = winner2 if final_winner_pr == winner1 else winner1
fqr_teams_for_prizes = teams_1qr
sqr_teams_for_prizes = the_list_with_the_teams_in_the_second_round + selected_teams_sqr_runners_up
tqr_teams_for_prizes = list_of_qualified_teams_in_the_third_round_cp + selected_teams_tqr_runners_up
por_teams_for_prizes = selected_teams_por_cp + selected_teams_por_runners_up
group_teams_for_prizes = [team for team, _, _, _ in list_of_qualified_teams_in_the_group]

round16_teams_for_prizes = qualified_1st_teams_in_round16 + qualified_2nd_teams_in_round16
round8_teams_for_prizes = [team for team, _, _ in list_of_qualified_teams_in_the_round8]
round4_teams_for_prizes = [team for team, _, _ in list_of_qualified_teams_in_the_round4]
round2_teams_for_prizes = list_of_qualified_teams_in_the_round2
runnerup_cl = list_of_qualified_teams_in_the_round2[0] \
    if list_of_qualified_teams_in_the_round2[1] == list_of_qualified_teams_in_the_round1[0] \
    else list_of_qualified_teams_in_the_round2[1]
round1_teams_for_prizes = list_of_qualified_teams_in_the_round1

prizes = {
    "pr_teams_for_prizes": 100000,
    "final_winner_pr": 250000,
    "runnerup_pr": 150000,
    "fqr_teams_for_prizes": 500000,
    "sqr_teams_for_prizes": 1000000,
    "tqr_teams_for_prizes": 1500000,
    "por_teams_for_prizes": 2500000,
    "group_teams_for_prizes": 25000000,
    "round16_teams_for_prizes": 15000000,
    "round8_teams_for_prizes": 25000000,
    "round4_teams_for_prizes": 40000000,
    "runnerup_cl": 55000000,
    "round1_teams_for_prizes": 120000000
}

# Calculul premiilor pentru fiecare echipă
team_prizes = {}

# Funcție pentru a adăuga premiul la echipe
def add_prize(teams, prize_value):
    for team in teams:
        if team in team_prizes:
            team_prizes[team] += prize_value
        else:
            team_prizes[team] = prize_value

# Adăugarea premiilor pentru echipele din fiecare categorie
add_prize(pr_teams_for_prizes, prizes["pr_teams_for_prizes"])
add_prize([final_winner_pr], prizes["final_winner_pr"])
add_prize([runnerup_pr], prizes["runnerup_pr"])
add_prize(fqr_teams_for_prizes, prizes["fqr_teams_for_prizes"])
add_prize(sqr_teams_for_prizes, prizes["sqr_teams_for_prizes"])
add_prize(tqr_teams_for_prizes, prizes["tqr_teams_for_prizes"])
add_prize(por_teams_for_prizes, prizes["por_teams_for_prizes"])
add_prize(group_teams_for_prizes, prizes["group_teams_for_prizes"])

# Crearea unui dicționar pentru câștigurile complete înainte de optimile de finală
earnings_before_round_of_16 = {}

# Adăugarea premiilor înainte de meciurile din grupă
for team, prize in prizes_before_group_matches:
    earnings_before_round_of_16[team] = prize

# Adăugarea câștigurilor din etapa grupelor
for team, earnings in earnings_group_stage:
    if team in earnings_before_round_of_16:
        earnings_before_round_of_16[team] += earnings
    else:
        earnings_before_round_of_16[team] = earnings

# Sortarea premiilor înainte de optimile de finală
sorted_earnings_before_round_of_16 = sorted(earnings_before_round_of_16.items(), key=lambda x: x[1], reverse=True)

# Scrierea rezultatelor în fișierul earnings_before_round_of_16.py
with open('earnings_before_round_of_16.py', 'w', encoding='utf-8') as f:
    f.write('earnings_before_round_of_16 = [\n')
    for team, earnings in sorted_earnings_before_round_of_16:
        f.write(f"    ('{team:<28}', {earnings}),\n")
    f.write(']\n')

# Total premii este afișat după premiilor aferente fiecărei echipe
print("Premii înainte de round of 16")
total_prize_before_round_of_16 = 0
for k, (team, prize) in enumerate(sorted_earnings_before_round_of_16, start=1):
    formatted_prize = f"{prize:,.0f}".replace(",", ".")
    print(f"{k:>2}. {team:<28}- {formatted_prize:>14} €")
    total_prize_before_round_of_16 += prize

# Adăugarea premiilor pentru alte etape
add_prize(round16_teams_for_prizes, prizes["round16_teams_for_prizes"])
add_prize(round8_teams_for_prizes, prizes["round8_teams_for_prizes"])
add_prize(round4_teams_for_prizes, prizes["round4_teams_for_prizes"])
add_prize([runnerup_cl], prizes["runnerup_cl"])
add_prize(round1_teams_for_prizes, prizes["round1_teams_for_prizes"])

# Sortarea echipei în funcție de câștigurile totale
sorted_teams_earnings = sorted(team_prizes.items(), key=lambda x: x[1], reverse=True)

# Scrierea rezultatelor în fișierul prizes.py
with open('prizes.py', 'w', encoding='utf-8') as f:
    f.write('teams_prizes = [\n')
    for team, prize in sorted_teams_earnings:
        f.write(f"    ('{team:<28}', {prize}),\n")
    f.write(']\n')

# Total premii este afișat după premiilor aferente fiecărei echipe
total_prize = 0
for k, (team, prize) in enumerate(sorted_teams_earnings, start=1):
    formatted_prize = f"{prize:,.0f}".replace(",", ".")
    print(f"{k:>2}. {team:<28}- {formatted_prize:>14} €")
    total_prize += prize

formatted_total_prize = f"{total_prize:,.0f}".replace(",", ".")
print(f"**. Total premii:                 {formatted_total_prize:>14} €")

"""
# # # Punctul 18 - câștigul fiecărei echipe
#
# # Definirea listelor cu echipe și premiile corespunzătoare pentru fiecare etapă
# pr_teams_for_prizes = teams_pr
# # winner1, winner2, final_winner_pr
# runnerup_pr = winner2 if final_winner_pr == winner1 else winner1
# fqr_teams_for_prizes = teams_1qr
# sqr_teams_for_prizes = the_list_with_the_teams_in_the_second_round + selected_teams_sqr_runners_up
# tqr_teams_for_prizes = list_of_qualified_teams_in_the_third_round_cp + selected_teams_tqr_runners_up
# por_teams_for_prizes = selected_teams_por_cp + selected_teams_por_runners_up
# group_teams_for_prizes = [team for team, _, _, _ in list_of_qualified_teams_in_the_group]
#
# round16_teams_for_prizes = qualified_1st_teams_in_round16 + qualified_2nd_teams_in_round16
# round8_teams_for_prizes = [team for team, _, _ in list_of_qualified_teams_in_the_round8]
# round4_teams_for_prizes = [team for team, _, _ in list_of_qualified_teams_in_the_round4]
# round2_teams_for_prizes = list_of_qualified_teams_in_the_round2
# runnerup_cl = list_of_qualified_teams_in_the_round2[0] \
#     if list_of_qualified_teams_in_the_round2[1] == list_of_qualified_teams_in_the_round1[0] \
#     else list_of_qualified_teams_in_the_round2[1]
# round1_teams_for_prizes = list_of_qualified_teams_in_the_round1
#
# prizes = {
#     "pr_teams_for_prizes": 100000,
#     "final_winner_pr": -100000,
#     "runnerup_pr": 150000,
#     "fqr_teams_for_prizes": 500000,
#     "sqr_teams_for_prizes": 1000000,
#     "tqr_teams_for_prizes": 1500000,
#     "por_teams_for_prizes": 2500000,
#     "group_teams_for_prizes": 25000000,
#     "round16_teams_for_prizes": 15000000,
#     "round8_teams_for_prizes": 25000000,
#     "round4_teams_for_prizes": 40000000,
#     "runnerup_cl": 55000000,
#     "round1_teams_for_prizes": 120000000
# }
#
# # Calculul premiilor pentru fiecare echipă
# team_prizes = {}
#
#
# # Funcție pentru a adăuga premiul la echipe
# def add_prize(teams, prize_value):
#     for team in teams:
#         if team in team_prizes:
#             team_prizes[team] += prize_value
#         else:
#             team_prizes[team] = prize_value
#
# # Adăugarea premiilor pentru echipele din fiecare categorie
# add_prize(teams_pr, prizes["pr_teams_for_prizes"])
# add_prize([final_winner_pr], prizes["final_winner_pr"])
# add_prize([runnerup_pr], prizes["runnerup_pr"])
# add_prize(fqr_teams_for_prizes, prizes["fqr_teams_for_prizes"])
# add_prize(sqr_teams_for_prizes, prizes["sqr_teams_for_prizes"])
# add_prize(tqr_teams_for_prizes, prizes["tqr_teams_for_prizes"])
# add_prize(por_teams_for_prizes, prizes["por_teams_for_prizes"])
# add_prize(group_teams_for_prizes, prizes["group_teams_for_prizes"])
#
# # Adăugarea premiilor din earnings_group_stage
# if isinstance(earnings_group_stage, list):
#     for team, earnings in earnings_group_stage:
#         add_prize([team], earnings)
#
# # Adăugarea premiilor pentru alte etape
# add_prize(round16_teams_for_prizes, prizes["round16_teams_for_prizes"])
# add_prize(round8_teams_for_prizes, prizes["round8_teams_for_prizes"])
# add_prize(round4_teams_for_prizes, prizes["round4_teams_for_prizes"])
# add_prize([runnerup_cl], prizes["runnerup_cl"])
# add_prize(round1_teams_for_prizes, prizes["round1_teams_for_prizes"])
#
# # Sortarea echipei în funcție de câștigurile totale
# sorted_teams_earnings = sorted(team_prizes.items(), key=lambda x: x[1], reverse=True)
#
# # Scrierea rezultatelor în fișierul prizes.py
# with open('prizes.py', 'w', encoding='utf-8') as f:
#     f.write('teams_prizes = [\n')
#     for team, prize in sorted_teams_earnings:
#         f.write(f"    ('{team:<28}', {prize}),\n")
#     f.write(']\n')
#
#
# # total premii este afișat după premiilor aferente fiecărei echipe
# total_prize = 0
# for k, (team, prize) in enumerate(sorted_teams_earnings, start=1):
#     formatted_prize = f"{prize:,.0f}".replace(",", ".")
#     print(f"{k:>2}. {team:<28}- {formatted_prize:>14} €")
#     total_prize += prize
#
# formatted_total_prize = f"{total_prize:,.0f}".replace(",", ".")
# print(f"**. Total premii:                 {formatted_total_prize:>14} €")
# # Definirea listelor cu echipe și premiile corespunzătoare pentru fiecare etapă

# Definirea listelor cu echipe și premiile corespunzătoare pentru fiecare etapă
pr_teams_for_prizes = teams_pr
runnerup_pr = winner2 if final_winner_pr == winner1 else winner1
fqr_teams_for_prizes = teams_1qr
sqr_teams_for_prizes = the_list_with_the_teams_in_the_second_round + selected_teams_sqr_runners_up
tqr_teams_for_prizes = list_of_qualified_teams_in_the_third_round_cp + selected_teams_tqr_runners_up
por_teams_for_prizes = selected_teams_por_cp + selected_teams_por_runners_up
group_teams_for_prizes = [team for team, _, _, _ in list_of_qualified_teams_in_the_group]

round16_teams_for_prizes = qualified_1st_teams_in_round16 + qualified_2nd_teams_in_round16
round8_teams_for_prizes = [team for team, _, _ in list_of_qualified_teams_in_the_round8]
round4_teams_for_prizes = [team for team, _, _ in list_of_qualified_teams_in_the_round4]
round2_teams_for_prizes = list_of_qualified_teams_in_the_round2
runnerup_cl = list_of_qualified_teams_in_the_round2[0] \
    if list_of_qualified_teams_in_the_round2[1] == list_of_qualified_teams_in_the_round1[0] \
    else list_of_qualified_teams_in_the_round2[1]
round1_teams_for_prizes = list_of_qualified_teams_in_the_round1

prizes = {
    "pr_teams_for_prizes": 100000,
    "final_winner_pr": 250000,
    "runnerup_pr": 150000,
    "fqr_teams_for_prizes": 500000,
    "sqr_teams_for_prizes": 1000000,
    "tqr_teams_for_prizes": 1500000,
    "por_teams_for_prizes": 2500000,
    "group_teams_for_prizes": 25000000,
    "round16_teams_for_prizes": 15000000,
    "round8_teams_for_prizes": 25000000,
    "round4_teams_for_prizes": 40000000,
    "runnerup_cl": 55000000,
    "round1_teams_for_prizes": 120000000
}

# Calculul premiilor pentru fiecare echipă
team_prizes = {}


# Funcție pentru a adăuga premiul la echipe
def add_prize(teams, prize_value):
    for team in teams:
        if team in team_prizes:
            team_prizes[team] += prize_value
        else:
            team_prizes[team] = prize_value

# Adăugarea premiilor pentru echipele din fiecare categorie
add_prize(pr_teams_for_prizes, prizes["pr_teams_for_prizes"])
add_prize([final_winner_pr], prizes["final_winner_pr"])
add_prize([runnerup_pr], prizes["runnerup_pr"])
add_prize(fqr_teams_for_prizes, prizes["fqr_teams_for_prizes"])
add_prize(sqr_teams_for_prizes, prizes["sqr_teams_for_prizes"])
add_prize(tqr_teams_for_prizes, prizes["tqr_teams_for_prizes"])
add_prize(por_teams_for_prizes, prizes["por_teams_for_prizes"])
add_prize(group_teams_for_prizes, prizes["group_teams_for_prizes"])

# Adăugarea premiilor din earnings_group_stage
if isinstance(earnings_group_stage, list):
    for team, earnings in earnings_group_stage:
        add_prize([team], earnings)

# Adăugarea premiilor pentru alte etape
add_prize(round16_teams_for_prizes, prizes["round16_teams_for_prizes"])
add_prize(round8_teams_for_prizes, prizes["round8_teams_for_prizes"])
add_prize(round4_teams_for_prizes, prizes["round4_teams_for_prizes"])
add_prize([runnerup_cl], prizes["runnerup_cl"])
add_prize(round1_teams_for_prizes, prizes["round1_teams_for_prizes"])

# Sortarea echipei în funcție de câștigurile totale
sorted_teams_earnings = sorted(team_prizes.items(), key=lambda x: x[1], reverse=True)

# Scrierea rezultatelor în fișierul prizes.py
with open('prizes.py', 'w', encoding='utf-8') as f:
    f.write('teams_prizes = [\n')
    for team, prize in sorted_teams_earnings:
        f.write(f"    ('{team:<28}', {prize}),\n")
    f.write(']\n')

# Total premii este afișat după premiilor aferente fiecărei echipe
total_prize = 0
for k, (team, prize) in enumerate(sorted_teams_earnings, start=1):
    formatted_prize = f"{prize:,.0f}".replace(",", ".")
    print(f"{k:>2}. {team:<28}- {formatted_prize:>14} €")
    total_prize += prize

formatted_total_prize = f"{total_prize:,.0f}".replace(",", ".")
print(f"**. Total premii:                 {formatted_total_prize:>14} €")
