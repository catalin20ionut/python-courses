# Echipele și coeficienții din semifinale
round4_teams_coefficients = [
    ('Inter Milan', 130.0, 'position: 1'),
    ('Red Bull Salzburg', 81.0, 'position: 2'),
    ('Bayer Leverkusen', 72.0, 'position: 3'),
    ('CSKA Moscow', 45.0, 'position: 4'),
]
