# Echipele și coeficienții pentru rundă a treia de calificare champions path
tqr_teams_coefficients = [
    ('Red Bull Salzburg', 81.0, 'position: 1'),
    ('Olympiacos Piraeus', 73.0, 'position: 2'),
    ('FC Basel', 50.0, 'position: 3'),
    ('Rosenborg', 50.0, 'position: 4'),
    ('Anorthosis Famagusta', 19.5, 'position: 5'),
    ('Fimleikafélag Hafnarfjörður', 19.0, 'position: 6'),
    ('Dinamo Tbilisi', 15.0, 'position: 7'),
    ('Dudelange', 12.0, 'position: 8'),
    ('Ballkani Suharekë', 12.0, 'position: 9'),
    ('Budućnost Podgorica', 10.0, 'position: 10'),
]
