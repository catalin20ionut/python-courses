import unittest
from game import Enemy, Player

# 12:13 03.08.2021 1.


class EnemyInitTest(unittest.TestCase):
    def test_enemy_init(self):
        e = Enemy("orc")
        self.assertEqual(e.name, "orc")
        self.assertEqual(e.lives, 1)


class EnemyDamageTest(unittest.TestCase):
    def test_enemy_takes_damage(self):
        e = Enemy("goblin")
        e.receive_damage(1)
        self.assertEqual(e.lives, 0)


class PlayerMovement(unittest.TestCase):
    def setUp(self):
        self.player = Player("Link")

    def test_move_north(self):
        self.player.reset_position()
        self.player.move_north()
        self.assertEqual(self.player.position_xy, (0, 1))

    def test_move_east(self):
        self.player.reset_position()
        self.player.move_east()
        self.assertEqual(self.player.position_xy, (1, 0))

    def test_pretty_position(self):
        self.player.move_south()
        self.player.move_south()
        self.player.move_west()
        self.player.move_west()
        self.assertEqual(
            self.player.pretty_position(), "Link went 2 steps south and 2 steps west"
        )

'''
1. Add a test suite containing player movement tests.
2. Add a new test case called PlayerDamage with the following functions: 
    2a) setUp which creates a single Player instance
and a single Enemy instance 
    2b) test_player_attack in which the self.player.attack_enemy(self.enemy) is tested
3. Add PlayerDamage to both playerSuite and enemySuite.'''

enemySuite = unittest.TestSuite()  # point 1
enemySuite.addTests(
    [
        EnemyInitTest("test_enemy_init"),
        EnemyDamageTest("test_enemy_takes_damage"),
    ]
)

enemySuite = unittest.TestSuite()
player_movement_suite = unittest.TestSuite()

player_movement_suite.addTests([PlayerMovement("test_pretty_position"), PlayerMovement("test_move_east")])
player_movement_suite.addTests([EnemyInitTest("test_enemy_init"), EnemyDamageTest("test_enemy_takes_damage")])
player_movement_suite.addTest(PlayerMovement("test_move_north"))
# enemy_suite = unittest.TestCase()  they're for tomorrow
# enemy_suite.addTests([EnemyInitTest("test_player_attack"), EnemyDamageTest("test_enemy_who_attacks_player_loses")])


class PlayerDamage(unittest.TestCase):
    def setUp(self):  # 2a)
        self.single_player = Player("single_player")
        self.single_enemy = Enemy("single_enemy")

    def test_player_attack(self):  # 2b)
        result = self.single_player.attack_enemy(self.single_enemy)
        self.assertEqual(result, "single_enemy lost!")

    def test_enemy_attack(self):  # 3
        result = self.single_enemy.attack_player(self.single_player)
        self.assertEqual(result, None)

    def test_enemy_attacks_player_loses(self):
        self.single_enemy.attack = 3
        result = self.single_enemy.attack_player(self.single_player)
        self.assertEqual(result, "single_player lost!")
