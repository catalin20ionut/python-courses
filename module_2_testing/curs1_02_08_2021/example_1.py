import unittest

# very important site where you can learn about testing from.
# https://docs.pytest.org/en/latest/contents.html

my_list = [1, 2, 3, 4, 100]


def even_mean(l):
    s = 0
    nr = 0
    for element in l:
        if element % 2 == 0:
            s += element
            nr += 1
    return s / nr

# print(even_mean(my_list))


class PositiveTestCase(unittest.TestCase):
    def test_list_with_3_one_is_even(self):
        result = even_mean([1, 2, 3])
        self.assertEqual(result, 2.0)
        self.assertIsInstance(result, float)

    def test_list_with_3_two_are_even(self):
        result = even_mean([1, 2, 4])
        self.assertEqual(result, 3.0)
        self.assertIsInstance(result, float)

    def test_list_with_one_negative_all_are_even(self):
        result = even_mean([-20, -2, 7, 9, 10])
        self.assertEqual(result, -4.0)
        self.assertIsInstance(result, float)

# the following two tests are not necessary

    def test_list_with_4_all_are_even(self):
        result = even_mean([2, 4, 6, 8])
        self.assertEqual(result, 5.0)
        self.assertIsInstance(result, float)

    def test_list_with_n_5_are_even(self):
        result = even_mean([2, 4, 6, 8, 9, 11, 23, 35, 47, 80])
        self.assertEqual(result, 20.0)
        self.assertIsInstance(result, float)


class NegativeTestCase(unittest.TestCase):
    @unittest.expectedFailure
    def test_without_even_numbers(self):
        result = even_mean([1, 3, 5])
        self.assertEqual(result, 0)

    @unittest.skip("Not ready yet")
    def test_is_not_ready(self):
        self.fail("This should not be run")

    def test_is_not_ready_1(self):
        self.assertEqual(1, 1)

# The test will fail. If this decorator is used the test will be ignored.
# Unless the decorator is used the test will fail. <<<<<<<<<<<<<<<<<<<<<
