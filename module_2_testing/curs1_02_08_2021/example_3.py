import time
import unittest

'''
You have been given a DatabaseInterface class and a test case for it, however there are several things for you to fix:
1. Each of the tests takes a lot of time, because of DatabaseInterface.__init__. Fix that without removing the sleep 
call. Hint: maybe it is enough to clear the database before each test?
2. A test for get_record is missing. Add it.
3. One of the tested methods is not implemented yet. Skip the test related to it using unittest.skip(), or handle the
error using self.assertRaises()'''


class DatabaseInterface:
    def __init__(self):
        # Connecting to database usually takes time.
        # We are simulating it here using sleep.
        time.sleep(2)
        self.records = []
        self.reset_records()

    def reset_records(self):
        self.records = [
            {"title": "Raiders of the Lost Ark", "year": 1981},
            {"title": "Jaws", "year": 1975},
        ]

    def add_record(self, record):
        if (
            not isinstance(record, dict)
            or "title" not in record
            or "year" not in record
        ):
            return False
        self.records.append(record)
        return True

    def get_record(self, idx):
        return self.records[idx]

    def remove_record(self, idx):
        raise NotImplementedError


class TestDatabaseInterface(unittest.TestCase):
    def setUp(self):
        self.dbi = DatabaseInterface()

    def test_connection_init(self):
        dbi = DatabaseInterface()
        self.assertEqual(len(dbi.records), 2)

    def test_add_record(self):
        dbi = DatabaseInterface()
        seventh_seal = {"title": "Seventh Seal", "year": 1957}
        response = dbi.add_record(seventh_seal)
        self.assertEqual(dbi.records[-1], seventh_seal)
        self.assertTrue(response)

    def test_add_malformed_record(self):
        dbi = DatabaseInterface()
        response = dbi.add_record({"not a good record": 1})
        self.assertFalse(response)

    def test_record_removal(self):
        with self.assertRaises(NotImplementedError):
            self.dbi.remove_record(0)
       # result = self.dbi.get_record(1)
       # self.assertEqual(result, {"tile": "Jaws", "year": 1975})
       # self.assertIsInstance(result, dict)
       # self.assertTrue(type(result) is dict)
        # dbi = DatabaseInterface()



    # def test_get_record(self):
    #     self.dbi.get_record(1)
    #     self.dbi.get_record()
