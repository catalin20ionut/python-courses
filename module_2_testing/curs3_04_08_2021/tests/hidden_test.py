def test_hidden():
    assert True is not False

# If I had saved "hidden.py" the "pytest tests" would have found only two tests.
# If I had written "assert True is False" I would have had 2 tests passed and 1 test failed
