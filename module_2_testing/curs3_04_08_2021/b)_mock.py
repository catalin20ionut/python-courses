import unittest
from unittest.mock import MagicMock, patch
# it is the example_1 in curs1_02_08_20021

my_list = [1, 2, 3, 4, 100]


def is_even(nr):
    return nr % 2 == 0


def even_mean(l):
    s = 0
    nr = 0
    for element in l:
        if is_even(element):
            s += element
            nr += 1
    return s / nr

print("------------------")
print(even_mean(my_list))
print("------------------")


class PositiveTestCase(unittest.TestCase):
    def test_list_with_3_one_is_even(self):
        false_mock = MagicMock()
        false_mock.return_value = True
        with patch('b)_mock.is_even', false_mock):
            result = even_mean([1, 2, 3])
        self.assertEqual(result, 2.0)
        self.assertIsInstance(result, float)
