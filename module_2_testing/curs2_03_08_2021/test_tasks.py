from tasks import Task, Tasks
import unittest
import datetime


class TasksTestCase(unittest.TestCase):
    def setUp(self):
        self.my_tasks = Tasks()

    def test_today_returns_list(self):
        result = self.my_tasks.today()
        self.assertIsInstance(result, list)

    def test_today_should_have_1_task(self):
        task = Task("first")
        self.my_tasks.add_task(task)
        result = self.my_tasks.today()
        self.assertIsInstance(result[0], Task)

    def test_today_with_2_tasks_should_have_1_tasks(self):
        task1 = Task("first")
        task2 = Task("second", datetime.date.today() + datetime.timedelta(days=1))
        self.my_tasks.add_task(task2)
        self.my_tasks.add_task(task1)
        result = self.my_tasks.today()
        self.assertIsInstance(result[0], Task)
        self.assertTrue(result[0].date == datetime.date.today())

# This is the code for the first step. It is the same with
""" def setUp(self):
        self.tasks = Tasks()

    def test_today_returns_list(self):
        result = self.tasks.today()
        self.assertIsInstance(result, list)"""
# class TasksTestCase(unittest.TestCase):
#     def test_today_returns_list(self):
#         tasks = Tasks()
#         result = tasks.today()
#         self.assertIsInstance(result, list)
