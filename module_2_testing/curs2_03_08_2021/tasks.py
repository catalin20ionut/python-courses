import datetime


class Task:
    def __init__(self, title, date=None):
        self.title = title
        if date is None:
            self.date = datetime.date = datetime.date.today()
        else:
            self.date = date


class Tasks:
    def __init__(self):
        self.tasks = []

    def add_task(self, new_task):
        assert isinstance(new_task, Task)
        self.tasks.append(new_task)

    def done(self, task):
        assert isinstance(task, Task)
        if task in self.tasks:
            self.tasks.remove(task)

    # the following code is for the first unittest. It is the one with #
    # def today(self):
    #     """Today returns a list of today's tasks"""
    #     return []

    def today(self):
        """Today returns a list of today's tasks"""
        today_tasks = []
        for t in self.tasks:
            if t.date == datetime.date.today():
                today_tasks.append(t)
        return today_tasks

    def tomorrow(self):
        """Today returns a list of tomorrow's tasks"""
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        return [t for t in self.tasks if t.date == tomorrow]
