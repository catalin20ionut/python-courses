from timeit import timeit

setup_code = "from math import pow"

code = """
def make_operation():
    return [pow(i, i+1) for i in range(100)]
make_operation()
"""
print(timeit(stmt=code, setup=setup_code, number=5*10**5))