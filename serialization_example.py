import pickle
import datetime

try:
    with open('data.txt','rb') as f:
        data = pickle.load(f)
        print(data)
except FileNotFoundError:
        data = datetime.datetime.now()
        print(f"Time was not saved:{data}")
        pickle.dump(data, f)