import sys
import time


for i in range(20):
    if i % 2 == 0:
        time.sleep(1)
        print(f"{i}", end="")
        sys.stdout.flush()
        print(f"\r{i}", end="")


for i in range(21):
    time.sleep(1)
    print(f"Remained time: {20-i}", end="")
    sys.stdout.flush()
    print(f"\rRemained time: {20-i}", end="")
    if f"{20-i}" == "0":
        print("\nboom")
        file1 = exec(open("explosion.py").read())
        ''' # if you want to open the file, comment 20th row and uncomment from 22nd row
        file1 = open("explosion.py")
        print(file1.read())
        file1.close()
        '''
