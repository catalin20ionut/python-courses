matches = [
    ("echipa1", "echipa2"),
    ("echipa1", "echipa3"),
    ("echipa1", "echipa4"),
    ("echipa1", "echipa5"),
    ("echipa1", "echipa6"),
    ("echipa1", "echipa7"),
    ("echipa1", "echipa8"),
    ("echipa2", "echipa3"),
    ("echipa2", "echipa4"),
    ("echipa2", "echipa5"),
    ("echipa2", "echipa6"),
    ("echipa2", "echipa7"),
    ("echipa2", "echipa8"),
    ("echipa3", "echipa4"),
    ("echipa3", "echipa5"),
    ("echipa3", "echipa6"),
    ("echipa3", "echipa7"),
    ("echipa3", "echipa8"),
    ("echipa4", "echipa5"),
    ("echipa4", "echipa6"),
    ("echipa4", "echipa7"),
    ("echipa4", "echipa8"),
    ("echipa5", "echipa6"),
    ("echipa5", "echipa7"),
    ("echipa5", "echipa8"),
    ("echipa6", "echipa7"),
    ("echipa6", "echipa8"),
    ("echipa7", "echipa8")
]

# Liste pentru a ține evidența meciurilor din fiecare coloană
columns = [[] for _ in range(7)]

# Seturi pentru a ține evidența echipelor din fiecare coloană
team_sets = [set() for _ in range(7)]

for match in matches:
    team1, team2 = match

    # Găsește o coloană în care echipele nu se repetă
    for col, team_set in zip(columns, team_sets):
        if team1 not in team_set and team2 not in team_set:
            col.append(match)
            team_set.update([team1, team2])
            break

# Afișează rezultatele
for i, col in enumerate(columns, start=1):
    print(f"Coloana {i}:")
    for match in col:
        print(f"  {match[0]} - {match[1]}")
