import random
teams_of_CL_LP = [
    {"team": "Manchester City", "coefficient": 152.0, "country": "England", "position": 1},
    {"team": "Liverpool", "coefficient": 146.0, "country": "England", "position": 2},
    {"team": "Real Madrid", "coefficient": 145.0, "country": "Spain", "position": 3},
    {"team": "FC Barcelona", "coefficient": 135.0, "country": "Spain", "position": 4},
    {"team": "Bayern Munich", "coefficient": 134.0, "country": "Germany", "position": 5},
    {"team": "Paris Saint-Germain", "coefficient": 132.0, "country": "France", "position": 6},
    {"team": "Atlético Madrid", "coefficient": 130.0, "country": "Spain", "position": 7},
    {"team": "Inter Milan", "coefficient": 130.0, "country": "Italy", "position": 8},
    {"team": "Juventus Torino", "coefficient": 121.0, "country": "Italy", "position": 9},
    {"team": "Chelsea", "coefficient": 113.0, "country": "England", "position": 10},
    {"team": "Arsenal", "coefficient": 112.0, "country": "England", "position": 11},
    {"team": "Borussia Dortmund", "coefficient": 110.0, "country": "Germany", "position": 12},
    {"team": "Tottenham Hotspur", "coefficient": 104.0, "country": "England", "position": 13},
    {"team": "Manchester United", "coefficient": 102.0, "country": "England", "position": 14},
    {"team": "Ajax Amsterdam", "coefficient": 100.0, "country": "Netherlands", "position": 15},
    {"team": "AS Monaco", "coefficient": 94.0, "country": "France", "position": 16},
    {"team": "Olympique Marseille", "coefficient": 93.0, "country": "France", "position": 17},
    {"team": "AC Milan", "coefficient": 93.0, "country": "Italy", "position": 18},
    {"team": "PSV Eindhoven", "coefficient": 90.5, "country": "Netherlands", "position": 19},
    {"team": "Shakhtar Donetsk", "coefficient": 85.0, "country": "Ukraine", "position": 20},
    {"team": "FC Porto", "coefficient": 85.0, "country": "Portugal", "position": 21},
    {"team": "Benfica", "coefficient": 82.0, "country": "Portugal", "position": 22},
    {"team": "Red Bull Salzburg", "coefficient": 81.0, "country": "Austria", "position": 23},
    {"team": "Villarreal", "coefficient": 80.0, "country": "Spain", "position": 24},
    {"team": "Dinamo Kiev", "coefficient": 80.0, "country": "Ukraine", "position": 25},
    {"team": "Olympique Lyonnais", "coefficient": 76.0, "country": "France", "position": 26},
    {"team": "Olympiacos Piraeus", "coefficient": 73.0, "country": "Greece", "position": 27},
    {"team": "AS Roma", "coefficient": 73.0, "country": "Italy", "position": 28},
    {"team": "Bayer Leverkusen", "coefficient": 72.0, "country": "Germany", "position": 29},
    {"team": "Zenit Saint Petersburg", "coefficient": 60.0, "country": "Russia", "position": 30},
    {"team": "Galatasaray", "coefficient": 55.0, "country": "Turkey", "position": 31},
    {"team": "FC Basel", "coefficient": 50.0, "country": "Switzerland", "position": 32},
    {"team": "Crvena Zvezda", "coefficient": 50.0, "country": "Serbia", "position": 33},
    {"team": "Fenerbahçe", "coefficient": 50.0, "country": "Turkey", "position": 34},
    {"team": "Club Brugge", "coefficient": 44.0, "country": "Belgium", "position": 35},
    {"team": "Shamrock Rovers", "coefficient": 19.0, "country": "Ireland", "position": 36},
]


# Funcție pentru a verifica dacă o grupă respectă regulile
def is_valid_group(group):
    country_count = {}
    for team in group:
        country = team['country']
        if country not in country_count:
            country_count[country] = 0
        country_count[country] += 1
        if country_count[country] > 2:
            return False
    return True


# Crearea grupelor
def create_groups(teams, num_groups):
    groups = [[] for _ in range(num_groups)]
    remaining_teams = sorted(teams, key=lambda x: x['position'])

    for team in remaining_teams:
        for group in groups:
            if len(group) < 9 and is_valid_group(group + [team]):
                group.append(team)
                break
    return groups

# Crearea și afișarea grupelor
num_groups2 = 4
groups2 = create_groups(teams_of_CL_LP, num_groups2)

# Afișarea grupelor
for i, group2 in enumerate(groups2):
    print(f"Group {i+1}:")
    for team2 in group2:
        print(f"{team2['team']} ({team2['country']})")
    print()
# -------------------------------------------------------------------------
''' de aici in jos nu este bine'''


# Funcție pentru a verifica dacă o echipă poate fi aleasă pentru o echipă dată
def can_choose_for_team(team, chosen_teams):
    for chosen_team in chosen_teams:
        if team['country'] == chosen_team['country']:
            return False
    return True


# Funcție pentru a alege adversari pentru fiecare echipă conform regulilor
def choose_opponents(groups):
    opponents = {team['team']: [] for group in groups for team in group}

    for idx, group in enumerate(groups):
        current_group_teams = group

        for team in current_group_teams:
            chosen_teams = []

            # Alegem 2 echipe din propria grupă
            same_group_teams = [t for t in current_group_teams if t['team'] != team['team']]
            if len(same_group_teams) >= 2:
                chosen_teams.extend(random.sample(same_group_teams, 2))

            # Alegem 2 echipe din grupele restante
            for j, other_group in enumerate(groups):
                if idx != j:
                    available_teams = [t for t in other_group if can_choose_for_team(t, chosen_teams)]
                    if len(available_teams) >= 2:
                        selected_teams = random.sample(available_teams, 2)
                    else:
                        selected_teams = available_teams
                    chosen_teams.extend(selected_teams)

                    # Dacă am selectat deja 8 echipe, ieșim din buclă
                    if len(chosen_teams) >= 8:
                        break

            # Asigurăm că am găsit exact 8 echipe pentru fiecare echipă
            if len(chosen_teams) < 8:
                print(f"Atenție: Nu s-au găsit suficiente echipe pentru {team['team']}")
            else:
                # Selectăm exact 8 echipe
                opponents[team['team']] = chosen_teams[:8]

    return opponents


# Funcție pentru a afișa alegerea adversarilor
def print_opponents(opponents):
    for team, chosen_teams in opponents.items():
        print(f"{team}:")
        for chosen_team in chosen_teams:
            print(f"- {chosen_team['team']} ({chosen_team['country']})")
        print()


# Funcție pentru a verifica dacă grupa este validă
def is_valid_group(group):
    country_count = {}
    for team in group:
        country = team['country']
        if country not in country_count:
            country_count[country] = 0
        country_count[country] += 1
        if country_count[country] > 2:
            return False
    return True


# Funcție pentru a crea grupele
def create_groups(teams, num_groups):
    groups = [[] for _ in range(num_groups)]
    remaining_teams = sorted(teams, key=lambda x: x['position'])

    for team in remaining_teams:
        for group in groups:
            if len(group) < 9 and is_valid_group(group + [team]):
                group.append(team)
                break
    return groups


# Crearea grupelor
num_groups2 = 4
groups2 = create_groups(teams_of_CL_LP, num_groups2)

# Afișarea grupelor
for i, group2 in enumerate(groups2):
    print(f"Group {i + 1}:")
    for team2 in group2:
        print(f"{team2['team']} ({team2['country']})")
    print()

# Alegerea adversarilor
opponents2 = choose_opponents(groups2)

# Afișarea alegerea adversarilor
print_opponents(opponents2)
