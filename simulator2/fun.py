import importlib.util
import os
import random
import re
from simulator2.clasament_country_coefficients import clasament_country_coefficients
from simulator2.teams_of_CL_by_country import teams_of_CL_by_country
from simulator2.teams_of_UE_by_country import teams_of_UE_by_country


def import_teams_from_file(file_path):                              # importă listele din folderul countries
    spec = importlib.util.spec_from_file_location("module.name", file_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return getattr(module, 'teams', [])


def writing_comments():                                                           # !!! ->                    # {i + 1}
    folder_path = 'countries'                                                     # Folderul care conține fișierele .py
    files = [f for f in os.listdir(folder_path) if f.endswith('.py')]           # Obține toate fișierele .py din folder

    for file_name in files:                                                              # Iterează prin fiecare fișier
        file_path = os.path.join(folder_path, file_name)
        try:
            with open(file_path, 'r', encoding='utf-8') as file:
                content = file.read()
        except UnicodeDecodeError:
            print(f"Error decoding {file_name}. Skipping file.")
            continue
        content = re.sub(r'#.*', '', content)                 # Îndepărtează toate comentariile vechi (# urmat de orice)

        # caută în lista teams
        pattern = r'teams\s*=\s*\[(.*?)\]'
        match = re.search(pattern, content, re.DOTALL)
        if match:
            teams_content = match.group(1)
            local_teams = [team.strip().strip('"') for team in teams_content.split(',') if team.strip()]
            # creează conținutul actualizat, aici se scrie ce textul nou la # {i + 1}
            updated_teams_content = "\n    ".join(f'"{team}",  # {i + 1}' for i, team in enumerate(local_teams))

            # Adaugă ']' la sfârșitul conținutului actualizat
            pattern = r'(teams\s*=\s*\[)(.*?)(\])'
            replacement = f'\\1\n    {updated_teams_content}\n\\3'
            updated_content = re.sub(pattern, replacement, content, flags=re.DOTALL)

            # Scrie înapoi în fișier
            with open(file_path, 'w', encoding='utf-8') as file:
                file.write(updated_content)
        else:
            print(f"4. No teams list found in {file_name}") if file_name != '__init__.py' else None
    print("4. S-au făcut schimbările în comentariile din fișierele din cadrul folderului countries.")
# ---------------------------------------------------------------------------------------------------------------------
#  bloc de stabilire a numărului de echipe pentru fiecare țară ce participă în Champions League, UEFA League  ---------


def determine_var_CL(position, filename):                   # condițiile de participare pentru Champions League, var_CL
    if filename == 'liechtenstein.py':
        return 0
    if position == 1:
        return 5
    elif position <= 6:
        return 4
    elif position <= 12:
        return 3
    elif position <= 34:
        return 2
    else:
        return 1


def determine_var_UE(position):                                   # condițiile de participare pentru UEFA League, var_UE
    if position <= 5:
        return 5
    elif position <= 12:
        return 4
    elif position <= 41:
        return 3
    else:
        return 2


def determine_CL(filename):                           # se scrie ce echipă a câștigat Champions League în sezonul trecut
    return 3 if filename == 'england.py' else None


def determine_UE(filename):                                  # se scrie ce echipă a câștigat UE League în sezonul trecut
    return 6 if filename == 'england.py' else None


def get_random_CU(filecontent):                                             # returnează un număr random pentru cupă CU
    regex_script = r'teams\s*=\s*\[([^\]]+)\]'
    ploegen = re.findall(regex_script, filecontent)
    if ploegen:
        team_list = ploegen[0].split(',')
        # return random.randint(1, len(team_list)-1)
        return random.randint(1, len(team_list)//3)
    return None


# scrie în fișierele din countries  valorile pentru var_CL, var_UE, CL, UE, CU, string_CU
def writing_variables_in_the_countries_files():
    folder_path = r'C:\Users\home\PycharmProjects\pythonProject\Python_Courses\simulator2\countries'
    files = [f for f in os.listdir(folder_path) if f.endswith('.py') and f != '__init__.py']

    for file_name in files:
        file_path = os.path.join(folder_path, file_name)
        try:
            with open(file_path, 'r', encoding='utf-8') as file:
                file_content = file.read()
        except UnicodeDecodeError:
            print(f"Error decoding {file_name}. Skipping file.")
            continue

        # extrag numele de țară pentru position din clasament_country_coefficients.py și string_CU_val (replacement)
        country_name = file_name.replace('.py', '').title() \
            .replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
        country_name_for_string_CU = file_name.replace('.py', '').title() \
            .replace('_', ' ').replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
        # # Debugging line: print country_name to verify
        # print(f"Processing file: {file_name}, country name derived: {country_name}")
        if country_name in clasament_country_coefficients:
            position = clasament_country_coefficients[country_name]['position']
            var_CL_val = determine_var_CL(position, file_name)
            var_UE_val = determine_var_UE(position)
            CL_val = determine_CL(file_name)
            UE_val = determine_UE(file_name)
            CU_val = get_random_CU(file_content)
        else:
            # # Debugging line: print if country_name is not found
            # print(f"Country name '{country_name}' not found in coefficients.")
            var_CL_val, var_UE_val, CL_val, UE_val, CU_val, string_CU_val = 0, 0, None, None, 0, ''
        # Update pattern to match everything after ']'
        pattern = r'\](.*)'
        replacement = (
            f']\n\n'
            f'var_CL = {var_CL_val}\n'
            f'var_UE = {var_UE_val}\n'
            f'CL = {CL_val}\n'
            f'UE = {UE_val}\n'
            f'CU = {CU_val}\n'
            f'string_CU = "Steaua {country_name_for_string_CU}"\n'
        )

        new_content = re.sub(pattern, replacement, file_content, flags=re.DOTALL)        # înlocuiește vechiul conținut

        with open(file_path, 'w', encoding='utf-8') as file:
            file.write(new_content)
        # Debugging lines:
    #     print(f"Modificările au fost aplicate în {file_name}")
    # print("Toate fișierele .py au fost procesate.")
# ----------------------------------------------------------------------------------------------------------------------
# Definirea variabilelor globale pentru
# picking_teams_for_European_cups()
# import_data_and_writing_teams_of_CL_by_country()
# import_data_and_writing_teams_of_UE_by_country()
# import_data_and_printing_the_teams_for_every_country()
# creating_teams_of_CL(), creating_teams_of_UE()
teams = []
var_CL = 0
var_UE = 0
CL = 0
UE = 0
CU = 0
string_CU = ''


def picking_teams_for_European_cups():
    # aflam numărul echipelor din liga cu cele mai multe echipe --------------------------------
    directory = r'C:\Users\home\PycharmProjects\pythonProject\Python_Courses\simulator2\countries'
    files = [f for f in os.listdir(directory) if f.endswith('.py')]        # Lista fișierelor .py
    max_length = 0

    for file in files:
        filepath = os.path.join(directory, file)
        spec = importlib.util.spec_from_file_location("module.name", filepath)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        #  Verifică dacă atributul 'teams' există și dacă este o listă
        if hasattr(module, 'teams') and isinstance(module.teams, list):
            length = len(module.teams)
            if length > max_length:
                max_length = length
    most_teams = max_length+1                       # în cazul nostru -> most_teams = 20 + 1 = 21
    # -------------------------------------------------------------------------------------------
    pivot_CL = var_CL
    pivot_UE = var_CL + var_UE - 1

    teams_for_CL = []
    teams_for_UE = []

    if CL == UE and CL is not None and UE is not None:
        print("1. Acest lucru este imposibil.")

    elif CL is None and UE is None:
        if CU <= pivot_UE:
            teams_for_CL = teams[:pivot_CL]
            teams_for_UE = teams[pivot_CL:pivot_UE+1]
        elif most_teams > CU > pivot_UE:
            teams_for_CL = teams[:pivot_CL]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL]
            teams_for_UE = teams[pivot_CL:pivot_UE+1] + [string_CU]

    elif CL is None and UE <= pivot_CL:
        if CU <= pivot_UE+2:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1]
        elif most_teams > CU > pivot_UE+2:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [string_CU]

    elif CL is None and pivot_CL < UE <= pivot_UE:
        if CU <= pivot_UE:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+2]) if idx != UE - (pivot_CL+1)]
        elif most_teams > CU > pivot_UE:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+1]) if
                            idx != UE - (pivot_CL+1)] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+1]) if
                            idx != UE - (pivot_CL+1)] + [string_CU]

    elif CL is None and UE > pivot_UE:
        if CU <= pivot_UE:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[pivot_UE+1]] \
                if UE == pivot_UE+1else teams[pivot_CL:pivot_UE+1]
        elif most_teams > CU > pivot_UE and UE != CU:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[CU-1]]
        elif most_teams > CU > pivot_UE and UE == CU:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[pivot_UE+1]] \
                if UE == pivot_UE+1 else teams[pivot_CL:pivot_UE] + [teams[pivot_UE]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL] + [teams[UE-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [string_CU]

    elif CL <= pivot_CL and UE is None:
        if CU <= pivot_UE+1:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+2]
        elif most_teams > CU > pivot_UE+1:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL+1]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [string_CU]

    elif pivot_CL < CL <= pivot_UE and UE is None:
        if CU <= pivot_UE+1:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+2]) if idx != CL - (pivot_CL+1)]
        elif most_teams > CU > pivot_UE+1:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+1])
                            if idx != CL - (pivot_CL+1)] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+1])
                            if idx != CL - (pivot_CL+1)] + [string_CU]

    elif CL > pivot_UE and UE is None:
        if CU <= pivot_UE:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE+1]
        elif most_teams > CU > pivot_UE:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            if CL != CU:
                teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[CU-1]]
            elif CL == CU == pivot_UE+1:
                teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[pivot_UE+1]]
            elif CL == CU:
                teams_for_UE = teams[pivot_CL:pivot_UE] + [teams[pivot_UE]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]]
            teams_for_UE = teams[pivot_CL:pivot_UE] + [string_CU]

    elif CL <= pivot_CL and UE <= pivot_CL:                             # CL <= 5, UE <= 5, pivot_CL == 5, pivot_UE == 9
        if CU <= pivot_UE+2:
            teams_for_CL = teams[:pivot_CL+2]
            teams_for_UE = teams[pivot_CL+2:pivot_UE+3]
        elif most_teams > CU > pivot_UE+2:
            teams_for_CL = teams[:pivot_CL+2]
            teams_for_UE = teams[pivot_CL+2:pivot_UE+2] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL+2]
            teams_for_UE = teams[pivot_CL+2:pivot_UE+2] + [string_CU]

    elif CL <= pivot_CL < UE <= pivot_UE:                           # CL <= 5, 5 < UE <= 9, pivot_CL == 5, pivot_UE == 9
        if CU <= pivot_UE+2:
            if UE == pivot_CL+1:
                teams_for_CL = teams[:pivot_CL+2]
                teams_for_UE = teams[pivot_CL+2:pivot_UE+3]
            elif pivot_CL+1 < UE <= pivot_UE:
                teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
                teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+3]) if
                                idx != UE - (pivot_CL+2)]
        elif most_teams > CU > pivot_UE+2:
            if UE == pivot_CL+1:
                teams_for_CL = teams[:pivot_CL+2]
                teams_for_UE = teams[pivot_CL+2:pivot_UE+2] + [teams[CU-1]]
            elif pivot_CL+1 < UE <= pivot_UE:
                teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
                teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+2]) if
                                idx != UE - (pivot_CL+2)] + [teams[CU-1]]
        elif CU == most_teams:
            if UE == pivot_CL+1:
                teams_for_CL = teams[:pivot_CL+2]
                teams_for_UE = teams[pivot_CL+2:pivot_UE+2] + [string_CU]
            elif pivot_CL+1 < UE <= pivot_UE:
                teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
                teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+2]) if
                                idx != UE - (pivot_CL+2)] + [string_CU]

    elif CL <= pivot_CL and UE > pivot_UE:                              # CL <= 5, UE > 10, pivot_CL == 5, pivot_UE == 9
        if CU <= pivot_UE+2:
            teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+3]) if idx != UE - (pivot_CL+2)] \
                if pivot_UE < UE <= pivot_UE+3 else teams[pivot_CL+1:pivot_UE+2]
        elif most_teams > CU > pivot_UE+2:
            if UE != CU:
                teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
                teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [teams[CU-1]]
            elif UE == CU:
                teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
                teams_for_UE = teams[pivot_CL+1:pivot_UE+2]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL+1] + [teams[UE-1]]
            teams_for_UE = teams[pivot_CL+1:pivot_UE+1] + [string_CU]

    elif UE <= pivot_CL < CL <= pivot_UE:                           # 5 < CL <= 9, UE <= 5, pivot_CL == 5, pivot_UE == 9
        if CU <= pivot_UE+3 and CL == pivot_CL+1:
            teams_for_CL = teams[:pivot_CL+2]
            teams_for_UE = teams[pivot_CL+2:pivot_UE+3]
        elif CU <= pivot_UE+3 and pivot_CL+1 < CL <= pivot_UE:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+3]) if idx != CL - (pivot_CL+2)]
        elif most_teams > CU > pivot_UE+3 and CL == pivot_CL+1:
            teams_for_CL = teams[:pivot_CL+2]
            teams_for_UE = teams[pivot_CL+2:pivot_UE+2] + [teams[CU-1]]
        elif most_teams > CU > pivot_UE+3 and pivot_CL+1 < CL <= pivot_UE:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+2]) if idx != CL-(pivot_CL+2)] +\
                           [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+2]) if idx != CL-(pivot_CL+2)] + \
                           [string_CU]
    # 5 < CL <= 12, 5< UE <= 9, pivot_CL == 5, pivot_UE == 9
    elif pivot_CL < CL <= pivot_UE and pivot_CL < UE <= pivot_UE and CL != UE:
        if CU <= pivot_UE+3:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+3])
                            if idx != CL - (pivot_CL+1) and idx != UE - (pivot_CL+1)]
        elif most_teams > CU > pivot_UE+3:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+2])
                            if idx != CL - (pivot_CL+1) and idx != UE - (pivot_CL+1)] + [teams[CU-1]]
        elif CU == most_teams:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+2])
                            if idx != CL - (pivot_CL+1) and idx != UE - (pivot_CL+1)] + [string_CU]

    elif pivot_UE > CL > pivot_CL > CU:                # 5 < CL <= 9, 10 <= UE ... CU <= 5, pivot_CL == 5, pivot_UE == 9
        if pivot_UE < UE <= pivot_UE+3:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+3])
                            if idx != CL - (pivot_CL+1) and idx != UE - (pivot_CL+1)]
        elif UE > pivot_UE+3:
            teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
            teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL:pivot_UE+2])
                            if idx != CL - (pivot_CL+1)]
# am înlocuit 4 cu var_UE ....
    elif pivot_CL < CL <= pivot_UE < UE and CU < most_teams:        # 5 < CL <= 9, 10 > UE, pivot_CL == 5, pivot_UE == 9
        teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
        x = [team for team in teams if team not in teams_for_CL]
        teams_for_UE = (x[:var_UE] + [teams[CU-1]]) if teams[CU-1] not in (teams_for_CL + x[:var_UE]) else x[:var_UE+1]
    elif pivot_CL < CL <= pivot_UE < UE and CU == most_teams:
        teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
        x = [team for team in teams if team not in teams_for_CL]
        teams_for_UE = x[:var_UE] + [string_CU]

    elif CL > pivot_UE and UE <= pivot_CL and CU == most_teams:         # CL > 9, UE <= 5, pivot_CL == 5, pivot_UE == 9
        teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
        x = [team for team in teams if team not in teams_for_CL]
        teams_for_UE = x[:var_UE] + [string_CU]
    elif CL > pivot_UE and UE <= pivot_CL:
        if CU <= pivot_UE:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            if pivot_UE < CL <= pivot_UE+3:
                teams_for_UE = [team for idx, team in enumerate(teams[pivot_CL+1:pivot_UE+3]) if idx != CL-(pivot_CL+2)]
            elif CL > pivot_UE+3:
                teams_for_UE = teams[pivot_CL+1:pivot_UE+2]
        elif most_teams > CL != CU > pivot_UE:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            x = [team for team in teams if team not in teams_for_CL]
            teams_for_UE =\
                (x[:var_UE] + [teams[CU-1]]) if teams[CU-1] not in (teams_for_CL + x[:var_UE]) else x[:var_UE+1]
        elif most_teams > CL == CU > pivot_UE:
            teams_for_CL = teams[:pivot_CL+1] + [teams[CL-1]]
            x = [team for team in teams if team not in teams_for_CL]
            teams_for_UE =\
                (x[:var_UE] + [teams[CU-1]]) if teams[CU-1] not in (teams_for_CL + x[:var_UE]) else x[:var_UE+1]

    elif most_teams > CL > pivot_UE and CU < most_teams:           # CL > = 10 ... UE > 6, pivot_CL == 5, pivot_UE == 9
        teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
        x = [team for team in teams if team not in teams_for_CL]
        teams_for_UE = (x[:var_UE] + [teams[CU-1]]) if teams[CU-1] not in (teams_for_CL + x[:4]) else x[:var_UE+1]
    elif CU == most_teams and most_teams > CL > pivot_UE:
        teams_for_CL = teams[:pivot_CL] + [teams[CL-1]] + [teams[UE-1]]
        x = [team for team in teams if team not in teams_for_CL]
        teams_for_UE = x[:var_UE] + [string_CU]
# ......... până aici

    return teams_for_CL, teams_for_UE


def import_data_and_writing_teams_of_CL_by_country():
    countries_folder = 'countries'                                                # Folderul care conține fișierele .py
    output_file = 'teams_of_CL_by_country.py'                               # Fișierul în care se vor scrie rezultatele

    with open(output_file, 'w', encoding='utf-8') as file:
        file.write('teams_of_CL_by_country = {\n')
        for filename in os.listdir(countries_folder):
            if filename.endswith('.py') and filename != '__init__.py':
                country_name = filename[:-3]                                                        # Fără extensia .py
                module_name = f'countries.{country_name}'
                module = importlib.import_module(module_name)                                          # Importă modulul

                # Verifică dacă fișierul conține variabilele necesare
                if hasattr(module, 'teams') and hasattr(module, 'var_CL') and hasattr(module, 'var_UE') \
                        and hasattr(module, 'CL') and hasattr(module, 'UE') and hasattr(module, 'CU') \
                        and hasattr(module, 'string_CU'):

                    # Destructurarea variabilelor
                    global teams, var_CL, var_UE, CL, UE, CU, string_CU
                    teams = module.teams
                    var_CL = module.var_CL
                    var_UE = module.var_UE
                    CL = module.CL
                    UE = module.UE
                    CU = module.CU
                    string_CU = module.string_CU

                    teams_for_CL, teams_for_UE = picking_teams_for_European_cups()                   # Apelează funcția

                    formatted_country_name = country_name.title() \
                        .replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                    formatted_teams_for_CL = [f"'{team}'" for team in teams_for_CL]
                    middle_index = len(formatted_teams_for_CL) // 2
                    first_row = formatted_teams_for_CL[:middle_index]
                    second_row = formatted_teams_for_CL[middle_index:]
                    if len(f"      '{formatted_country_name}': [{', '.join(formatted_teams_for_CL)}], \n") > 120:
                        file.write(f"      '{formatted_country_name}': [{', '.join(first_row)},\n"
                                   f"                  {', '.join(second_row)}],\n")
                    else:
                        file.write(f"      '{formatted_country_name}': [{', '.join(formatted_teams_for_CL)}], \n")
                else:
                    formatted_country_name = country_name.title() \
                        .replace('_', ' ').replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                    file.write(f"{formatted_country_name:<25} - Fișierul {filename} nu conține variabilele necesare.\n")
        file.write('}\n')


def import_data_and_writing_teams_of_UE_by_country():
    countries_folder = 'countries'                                                # Folderul care conține fișierele .py
    output_file = 'teams_of_UE_by_country.py'                               # Fișierul în care se vor scrie rezultatele

    with open(output_file, 'w', encoding='utf-8') as file:
        file.write('teams_of_UE_by_country = {\n')
        for filename in os.listdir(countries_folder):
            if filename.endswith('.py') and filename != '__init__.py':
                country_name = filename[:-3]                                                        # Fără extensia .py
                module_name = f'countries.{country_name}'
                module = importlib.import_module(module_name)                                          # Importă modulul

                # Verifică dacă fișierul conține variabilele necesare
                if hasattr(module, 'teams') and hasattr(module, 'var_CL') and hasattr(module, 'var_UE') \
                        and hasattr(module, 'CL') and hasattr(module, 'UE') and hasattr(module, 'CU') \
                        and hasattr(module, 'string_CU'):

                    # Destructurarea variabilelor
                    global teams, var_CL, var_UE, CL, UE, CU, string_CU
                    teams = module.teams
                    var_CL = module.var_CL
                    var_UE = module.var_UE
                    CL = module.CL
                    UE = module.UE
                    CU = module.CU
                    string_CU = module.string_CU

                    teams_for_CL, teams_for_UE = picking_teams_for_European_cups()                   # Apelează funcția

                    formatted_country_name = country_name.title() \
                        .replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                    formatted_teams_for_UE = [f"\"{team}\"" for team in teams_for_UE]
                    middle_index = len(formatted_teams_for_UE) // 2
                    first_row = formatted_teams_for_UE[:middle_index]
                    second_row = formatted_teams_for_UE[middle_index:]
                    if len(f"      '{formatted_country_name}': [{', '.join(formatted_teams_for_UE)}], \n") > 120:
                        file.write(f"      '{formatted_country_name}': [{', '.join(first_row)},\n"
                                   f"                  {', '.join(second_row)}],\n")
                    else:
                        file.write(f"      '{formatted_country_name}': [{', '.join(formatted_teams_for_UE)}], \n")
                else:
                    formatted_country_name = country_name.title() \
                        .replace('_', ' ').replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                    file.write(f"{formatted_country_name:<25} - Fișierul {filename} nu conține variabilele necesare.\n")
        file.write('}\n')


def import_data_and_printing_the_teams_for_every_country():                     # var_CL, var_UE, CL, UE, CU, string_CU
    countries_folder = 'countries'                                                # Folderul care conține fișierele .py
    print("\n\033[32m8. Echipele participante\033[0m")

    for filename in os.listdir(countries_folder):
        if filename.endswith('.py') and filename != '__init__.py':
            country_name = filename[:-3]                                                            # Fără extensia .py
            module_name = f'countries.{country_name}'
            module = importlib.import_module(module_name)                                             # Importă modulul

            if hasattr(module, 'teams') and hasattr(module, 'var_CL') and hasattr(module, 'var_UE') \
                    and hasattr(module, 'CL') and hasattr(module, 'UE') and hasattr(module, 'CU') \
                    and hasattr(module, 'string_CU'):
                # Destructurarea variabilelor
                global teams, var_CL, var_UE, CL, UE, CU, string_CU
                teams = module.teams
                var_CL = module.var_CL
                var_UE = module.var_UE
                CL = module.CL
                UE = module.UE
                CU = module.CU
                string_CU = module.string_CU

                # Apelează funcția
                teams_for_CL, teams_for_UE = picking_teams_for_European_cups()

                pivot_CL = var_CL
                pivot_UE = var_CL + var_UE - 1

                formatted_country_name = country_name.title() \
                    .replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                print(f"{formatted_country_name:<22} - teams_for_CL "
                      f"- {', '.join(teams_for_CL):<101} <= pivot CL - {pivot_CL} ")
                print(f"{formatted_country_name:<22} - teams_for_UE "
                      f"- {', '.join(teams_for_UE):<101} <= pivot UE - {pivot_UE}")

            else:
                formatted_country_name = country_name.title()\
                    .replace('_', ' ').replace('And', 'and').replace('Of', 'of').replace('andorra', 'Andorra')
                print(f"{formatted_country_name:<22} - Fișierul {filename} nu conține variabilele necesare.")


def creating_teams_of_CL():
    teams_of_CL = {}

    for country_9, teams_9 in teams_of_CL_by_country.items():             # parcurge dicționarul teams_of_CL_by_country
        if country_9 in clasament_country_coefficients:    # verifică existența țării în clasament_country_coefficients
            position = clasament_country_coefficients[country_9]['position']                    # extrage poziția țării
            teams_of_CL[country_9] = {'teams': teams_9, 'position': position}  # crearea și atribuirea unei noi intrări
            # în dicționar      În dicționarul teams_of_CL, se adaugă o nouă intrare cu cheia country_9 (numele țării).
            #                              Valoarea asociată acestei chei este un nou dicționar care conține două chei:
            # 'teams':                                care are ca valoare lista de echipe teams_9 asociată acelei țări.
            # 'position':                                           care are ca valoare poziția țării extrasă anterior.
    sorted_teams_of_CL = dict(sorted(teams_of_CL.items(), key=lambda item: item[1]['position']))  # sortăm după poziție
    '''
    # Creăm fișierul teams_of_CL.py
    with open('teams_of_CL.py', 'w', encoding='utf-8') as file_9:
        file_9.write('teams_of_CL = {\n')
        for country_9, data_9 in sorted_teams_of_CL.items():
            file_9.write(f"    '{country_9}': {{'teams': {data_9['teams']}, 'position': {data_9['position']}}},\n")
        file_9.write('}\n')
    '''
    with open('teams_of_CL.py', 'w', encoding='utf-8') as file_9:
        file_9.write('teams_of_CL = {\n')
        for country_9, data_9 in sorted_teams_of_CL.items():
            team_list = data_9["teams"]
            first_line = ', '.join(f'"{team}"' for team in team_list[:len(team_list) // 2])
            if len(f'    "{country_9}": {{"teams": {data_9["teams"]}, "position": {data_9["position"]}}},\n') > 120:
                file_9.write(f'    "{country_9}": {{"teams": [{first_line},\n                         ')
                for team in team_list[len(team_list) // 2:]:
                    file_9.write(f'"{team}", ')
                file_9.seek(file_9.tell() - 2)  # elimină ultima virgulă; cursorul se mută cu două spații înapoi
                file_9.write(f'], "position": {data_9["position"]}}},\n')
            else:
                file_9.write(f'    "{country_9}": {{"teams": {data_9["teams"]}, "position": {data_9["position"]}}},\n')
        file_9.write('}\n')
    return teams_of_CL


def creating_teams_of_UE():
    teams_of_UE = {}
    for country_10, teams_10 in teams_of_UE_by_country.items():
        if country_10 in clasament_country_coefficients:
            position = clasament_country_coefficients[country_10]['position']
            teams_of_UE[country_10] = {'teams': teams_10, 'position': position}
    sorted_teams_of_UE = dict(sorted(teams_of_UE.items(), key=lambda item: item[1]['position']))

    with open('teams_of_UE.py', 'w', encoding='utf-8') as file_10:
        file_10.write('teams_of_UE = {\n')
        for country_10, data_10 in sorted_teams_of_UE.items():
            team_list = data_10["teams"]
            first_line = ', '.join(f'"{team}"' for team in team_list[:len(team_list) // 2])
            if len(f'    "{country_10}": {{"teams": {data_10["teams"]}, "position": {data_10["position"]}}},\n') > 120:
                file_10.write(f'    "{country_10}": {{"teams": [{first_line},\n                         ')
                for team in team_list[len(team_list) // 2:]:
                    file_10.write(f'"{team}", ')
                file_10.seek(file_10.tell() - 2)  # elimină ultima virgulă; cursorul se mută cu două spații înapoi
                file_10.write(f'], "position": {data_10["position"]}}},\n')
            else:
                file_10.write(f'    "{country_10}": {{"teams": {data_10["teams"]},'
                            f' "position": {data_10["position"]}}},\n')
        file_10.write('}\n')
    return teams_of_UE


def simulate_matches(team1, team2, is_runners_up=False):
    qualified_team = None
    if is_runners_up:
        team1_name, team1_coef = team1, 0
        team2_name, team2_coef = team2, 0
    else:
        try:
            team1_name = team1['team']
            team1_coef = float(team1['coefficient'])
            team2_name = team2['team']
            team2_coef = float(team2['coefficient'])
        except (ValueError, KeyError) as e:
            print(f"Error converting data for Team1: {team1}, Team2: {team2}. Exception: {e}")
            return "Invalid team data."

    # Funcție pentru a simula scorul cu probabilități ajustate
    def choosing_score(home_coef, away_coef):
        coef_diff = home_coef - away_coef
        if coef_diff > 50:
            home_score = random.choices([3, 4, 5, 6], [0.05, 0.05, 0.45, 0.45])[0]
            away_score = random.randint(0, 1)
        elif coef_diff > 30:
            home_score = random.choices([2, 3, 4], [0.1, 0.25, 0.65])[0]
            away_score = random.randint(0, 2)
        else:
            home_score = random.randint(0, 3)
            away_score = random.randint(0, 3)
        return home_score, away_score

    # Simulăm meciurile tur și retur
    score1_tur, score2_tur = choosing_score(team1_coef, team2_coef)
    score1_retur, score2_retur = choosing_score(team2_coef, team1_coef)

    result_tur = f"{team1_name:<30} vs {team2_name:<30} - {score1_tur}:{score2_tur}"
    result_retur = f"{score1_retur}:{score2_retur}"

    # Calculăm scorul total
    total_score1 = score1_tur + score1_retur
    total_score2 = score2_tur + score2_retur

    # Verificăm câștigătorul după scorul total
    if total_score1 > total_score2:
        qualified_team = team1_name
    elif total_score2 > total_score1:
        qualified_team = team2_name
    elif total_score1 == total_score2 and score1_tur == score2_tur and score1_retur < score1_tur:
        qualified_team = team2_name
    elif total_score1 == total_score2 and score1_tur == score2_tur and score1_retur > score1_tur:
        qualified_team = team1_name
    elif total_score1 == total_score2 and score1_tur == score2_retur and score1_retur == score2_tur:
        score1_et = random.randint(0, 2)
        score2_et = random.randint(0, 2)

        total_score1_et = score1_retur + score1_et
        total_score2_et = score2_retur + score2_et

        if total_score1_et - score1_retur > total_score2_et - score2_retur:
            qualified_team = team1_name
        elif total_score1_et - score1_retur < total_score2_et - score2_retur:
            qualified_team = team2_name
        else:
            # penalty shoot-out, lovituri de departajare
            while True:
                score1_pen = random.randint(1, 6)
                score2_pen = random.randint(1, 6)

                # if 5 >= score1_pen != score2_pen <= 5:
                #     if abs(score1_pen - score2_pen) <= 2:
                #         break
                # else:
                #     if abs(score1_pen - score2_pen) == 1:
                #         break
                # if (5 >= score1_pen and score2_pen <= 5) and abs(score1_pen - score2_pen) <= 2:
                #     if score1_pen > score2_pen:
                #         qualified_team = team1_name
                #         break
                #     else:
                #         qualified_team = team2_name
                #         break
                # elif score1_pen > 5 and score2_pen > 5 and abs(score1_pen - score2_pen) == 1:
                #     if score1_pen > score2_pen:
                #         qualified_team = team1_name
                #         break
                #     else:
                #         qualified_team = team2_name
                #         break
                # elif score1_pen == score2_pen:
                #     continue
                # Verificăm dacă scorurile penaltiurilor sunt valabile și diferite
                if score1_pen != score2_pen:
                    if (score1_pen <= 5 and score2_pen <= 5) and abs(score1_pen - score2_pen) <= 2:
                        if score1_pen > score2_pen:
                            qualified_team = team1_name
                            break
                        else:
                            qualified_team = team2_name
                            break
                    elif score1_pen > 5 and score2_pen > 5 and abs(score1_pen - score2_pen) == 1:
                        if score1_pen > score2_pen:
                            qualified_team = team1_name
                            break
                        else:
                            qualified_team = team2_name
                            break
                # Dacă scorurile sunt egale, reluăm bucla și generăm alte scoruri
                else:
                    continue
            return f"{result_tur}  {result_retur} ({total_score1_et}:{total_score2_et}, " \
                   f"{score1_pen}:{score2_pen}) Qualified team: {qualified_team}"

        return f"{result_tur}  {result_retur} ({total_score1_et}:{total_score2_et})     " \
               f" Qualified team: {qualified_team}"
    elif total_score1 == total_score2 and score1_retur > score2_tur:
        qualified_team = team1_name
    else:
        qualified_team = team2_name
    return f"{result_tur}  {result_retur}            Qualified team: {qualified_team}"
