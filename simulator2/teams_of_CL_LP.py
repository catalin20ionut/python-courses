teams_of_CL_LP = [
    {"team": "Manchester City", "coefficient": 152.0, "country": "England", "position": 1},
    {"team": "Liverpool", "coefficient": 146.0, "country": "England", "position": 2},
    {"team": "Real Madrid", "coefficient": 145.0, "country": "Spain", "position": 3},
    {"team": "FC Barcelona", "coefficient": 135.0, "country": "Spain", "position": 4},
    {"team": "Bayern Munich", "coefficient": 134.0, "country": "Germany", "position": 5},
    {"team": "Paris Saint-Germain", "coefficient": 132.0, "country": "France", "position": 6},
    {"team": "Atlético Madrid", "coefficient": 130.0, "country": "Spain", "position": 7},
    {"team": "Inter Milan", "coefficient": 130.0, "country": "Italy", "position": 8},
    {"team": "Juventus Torino", "coefficient": 121.0, "country": "Italy", "position": 9},
    {"team": "Chelsea", "coefficient": 113.0, "country": "England", "position": 10},
    {"team": "Arsenal", "coefficient": 112.0, "country": "England", "position": 11},
    {"team": "Borussia Dortmund", "coefficient": 110.0, "country": "Germany", "position": 12},
    {"team": "Tottenham Hotspur", "coefficient": 104.0, "country": "England", "position": 13},
    {"team": "Manchester United", "coefficient": 102.0, "country": "England", "position": 14},
    {"team": "Ajax Amsterdam", "coefficient": 100.0, "country": "Netherlands", "position": 15},
    {"team": "AS Monaco", "coefficient": 94.0, "country": "France", "position": 16},
    {"team": "AC Milan", "coefficient": 93.0, "country": "Italy", "position": 17},
    {"team": "PSV Eindhoven", "coefficient": 90.5, "country": "Netherlands", "position": 18},
    {"team": "FC Porto", "coefficient": 85.0, "country": "Portugal", "position": 19},
    {"team": "Shakhtar Donetsk", "coefficient": 85.0, "country": "Ukraine", "position": 20},
    {"team": "Benfica", "coefficient": 82.0, "country": "Portugal", "position": 21},
    {"team": "Red Bull Salzburg", "coefficient": 81.0, "country": "Austria", "position": 22},
    {"team": "Villarreal", "coefficient": 80.0, "country": "Spain", "position": 23},
    {"team": "Olympique Lyonnais", "coefficient": 76.0, "country": "France", "position": 24},
    {"team": "Olympiacos Piraeus", "coefficient": 73.0, "country": "Greece", "position": 25},
    {"team": "AS Roma", "coefficient": 73.0, "country": "Italy", "position": 26},
    {"team": "Bayer Leverkusen", "coefficient": 72.0, "country": "Germany", "position": 27},
    {"team": "Dinamo Zagreb", "coefficient": 62.5, "country": "Croatia", "position": 28},
    {"team": "Zenit Saint Petersburg", "coefficient": 60.0, "country": "Russia", "position": 29},
    {"team": "Galatasaray", "coefficient": 55.0, "country": "Turkey", "position": 30},
    {"team": "Braga", "coefficient": 53.0, "country": "Portugal", "position": 31},
    {"team": "Crvena Zvezda", "coefficient": 50.0, "country": "Serbia", "position": 32},
    {"team": "FC Basel", "coefficient": 50.0, "country": "Switzerland", "position": 33},
    {"team": "Fenerbahçe", "coefficient": 50.0, "country": "Turkey", "position": 34},
    {"team": "Standard Liège", "coefficient": 41.0, "country": "Belgium", "position": 35},
    {"team": "Lokomotiv Moscova", "coefficient": 40.0, "country": "Russia", "position": 36},
]
