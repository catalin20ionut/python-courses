import os
import importlib.util
import random
import time
import pandas as pd

from simulator2.clasament_country_coefficients import clasament_country_coefficients
from simulator2.country_coefficients import country_coefficients
from simulator2.fun import import_teams_from_file, writing_comments, writing_variables_in_the_countries_files, \
    import_data_and_writing_teams_of_CL_by_country, import_data_and_writing_teams_of_UE_by_country, \
    import_data_and_printing_the_teams_for_every_country, creating_teams_of_CL, creating_teams_of_UE, simulate_matches
from simulator2.teams_of_CL import teams_of_CL
from simulator2.teams_of_Co_wk import conference_coefficients_wild_card
from simulator2.teams_of_UE import teams_of_UE
from simulator2.uefa_coefficients import uefa_coefficients


# ---------------------------------------------------------------------------------------------------------------------
# Punctul 1 - generează fișierul clasament_country_coefficients.py
def write_clasament_country_coefficients(coefficients):
    # Sortăm țările după coeficient în ordine descrescătoare
    sorted_countries = sorted(coefficients.items(), key=lambda x: x[1], reverse=True)

    output_file = 'clasament_country_coefficients.py'
    try:
        with open(output_file, 'w', encoding='utf-8') as file:
            file.write('clasament_country_coefficients = {\n')
            for position, (country, uefa_coefficient) in enumerate(sorted_countries, start=1):
                country_str = f"'{country}':"
                coefficient_str = f"{uefa_coefficient:.3f}"
                position_str = f"{position}"
                country_padding = ' ' * (26 - len(country_str))
                coefficient_padding = ' ' * (8 - len(coefficient_str))
                position_padding = ' ' * (3 - len(position_str))
                file.write(f"    {country_str}{country_padding}"
                           f"{{'coefficient':{coefficient_padding}{coefficient_str}, "
                           f"'position':{position_padding}{position_str}}},\n")
            file.write('}\n')
        print(f" 1. Datele au fost scrise cu succes în fișierul {output_file}.")
    except IOError as e:
        print(f" 1. Eroare: Nu s-a putut scrie în fișierul {output_file}. Detalii: {e}")

if __name__ == "__main__":
    write_clasament_country_coefficients(country_coefficients)
# ---------------------------------------------------------------------------------------------------------------------
print("\n Clasament uefa coeficienți")


# Punctul 2 - afișează clasament clasamentul țărilor după coeficienții uefa
def print_country_coefficients(coefficients_dict):
    for country_2, data in sorted(coefficients_dict.items(), key=lambda x: x[1]['position']):
        print(f"{data['position']:2d}. {country_2:22} {data['coefficient']:7.3f}")
print_country_coefficients(clasament_country_coefficients)
# ---------------------------------------------------------------------------------------------------------------------


# Punctul 3
def checking_the_items():
    folder_path = r'C:\Users\home\PycharmProjects\pythonProject\Python_Courses\simulator2\countries'
    wild_cards_file_path = r'C:\Users\home\PycharmProjects\pythonProject\Python_Courses\simulator2\teams_of_Co_wk.py'
    differences_found = False

    wild_cards = import_teams_from_file(wild_cards_file_path)
    wild_cards_duplicates = [team for team in wild_cards if list(wild_cards).count(team) > 1]
    if wild_cards_duplicates:
        print(f"3e. În fișierul teams_of_Co_wk.py, următoarele "
              f"echipe sunt duplicate: {', '.join(wild_cards_duplicates)}.")
        differences_found = True

    countries_folder_teams = set()
    for filename in os.listdir(folder_path):
        if filename.endswith('.py') and filename != '__init__.py':
            file_path = os.path.join(folder_path, filename)

            teams = import_teams_from_file(file_path)
            duplicates = set([team for team in teams if teams.count(team) > 1])
            if duplicates:
                print(f"3a. În fișierul {filename}, următoarele echipe sunt duplicate: {', '.join(duplicates)}.")
                differences_found = True

            countries_folder_teams.update(teams)                              # colectează echipele din toate fișierele
            for team in teams:             # verifică dacă echipele din folderul countries sunt în uefa_coefficients.py
                if team not in uefa_coefficients:
                    print(f"3b. Echipa {team} din {filename} lipsește din uefa_coefficients.py.")
                    differences_found = True

    # verifică dacă echipele din uefa_coefficients.py lipsesc din countries și din fișierul teams_of_Co_wk.py
    # echipele din uefa_coefficients sunt unice pentru că sunt cheile dicționarului
    for team in uefa_coefficients:
        if team not in countries_folder_teams and team not in conference_coefficients_wild_card:
            print(f"3c. Echipa {team} din uefa_coefficients.py lipsește din fișierele din folderul countries sau din "
                  f"fișierul teams_of_Co_wk.py")
            differences_found = True
    if not differences_found:
        print("3d. Nu există diferențe.")

if __name__ == "__main__":
    checking_the_items()


# se face o verificare suplimentară; dacă numerele sunt diferite atunci există multiplicări în listele din countries
def count_teams_in_countries(folder_path):         # numără câte echipe sunt în fișierele .py din folderul countries
    total_teams = 0
    for filename in os.listdir(folder_path):
        if filename.endswith('.py') and filename != '__init__.py':
            file_path = os.path.join(folder_path, filename)
            teams_from_folder = import_teams_from_file(file_path)
            total_teams += len(teams_from_folder)
    return total_teams

folder_address = r'C:\Users\home\PycharmProjects\pythonProject\Python_Courses\simulator2\countries'
total_teams_in_countries = count_teams_in_countries(folder_address)

print(f"3e. În folderul countries sunt {total_teams_in_countries} "
      f"echipe \ncu {len(conference_coefficients_wild_card)} echipe din wild_cards.py "
      f"fac {total_teams_in_countries + len(conference_coefficients_wild_card)} echipe.")
print(f"3e. În fișierul uefa_coefficients.py sunt {len(uefa_coefficients)} echipe.")


# ---------------------------------------------------------------------------------------------------------------------
# push_enter = input("4. Apasă enter pentru a merge mai departe:")
if __name__ == "__main__":
    writing_comments()                                                                                     # Punctul  4
    writing_variables_in_the_countries_files()                                                             # Punctul  5
    import_data_and_writing_teams_of_CL_by_country()                                                       # Punctul  6
    import_data_and_writing_teams_of_UE_by_country()                                                       # Punctul  7
    import_data_and_printing_the_teams_for_every_country()                                                 # Punctul  8
    creating_teams_of_CL()                                                                                 # Punctul  9
    creating_teams_of_UE()                                                                                 # Punctul 10
# Punctul  4 scrie comentarii în fișierele ___.py din folderul countries ----------------------------------------------
# Punctul  5 scrie variabilele în fișierele ___.py din folderul countries ---------------------------------------------
# Punctul  6 scrie în _teams_of_CL_by_country.py țările cu echipele participante numai în CL --------------------------
# Punctul  7 scrie în _teams_of_UE_by_country.py țările cu echipele participante numai în UE --------------------------
# Punctul  8 printează țările în ordine crescătoare cu echipele care participă în cupele CL și UE ---------------------
# Punctul  9 creează fișierului teams_of_CL.py în funcție de poziția țării în clasament_country_coefficients ----------
# Punctul 10 creează fișierului teams_of_UE.py în funcție de poziția țării în clasament_country_coefficients ----------
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 11 - numărăm și afișăm echipele din Champions League
total_teams_11 = 0
for country_11, info_11 in teams_of_CL.items():
    total_teams_11 += len(info_11['teams'])

print(f"\033[32m\n Echipe participante în Liga Campionilor - {total_teams_11} de echipe.\033[0m")
for index_11, (country_11, info_11) in enumerate(teams_of_CL.items(), start=1):
    teams_11 = ', '.join(info_11['teams'])
    if index_11 <= 9:
        print(f" {index_11}. {country_11.ljust(29)}: {teams_11}")
    else:
        print(f"{index_11}. {country_11.ljust(29)}: {teams_11}")
print(f"\033[32m11. În Liga Campionilor participă: {total_teams_11} de echipe.\033[0m")
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 12 - numărăm și afișăm echipele din UEFA League
total_teams_12 = 0
for country_12, info_12 in teams_of_UE.items():
    total_teams_12 += len(info_12['teams'])

print(f"\033[32m\n Echipe participante în UEFA League - {total_teams_12} de echipe.\033[0m")
for index_12, (country_12, info_12) in enumerate(teams_of_UE.items(), start=1):
    teams_12 = ', '.join(info_12['teams'])
    if index_12 <= 9:
        print(f" {index_12}. {country_12.ljust(29)}: {teams_12}")
    else:
        print(f"{index_12}. {country_12.ljust(29)}: {teams_12}")
print(f"\033[32m12. UEFA League participă        : {total_teams_12} de echipe.\033[0m")
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 12 --------------------------------------------------- selectare echipelor pentru fazele din Champions League
# --------------------------------------------- bloc pentru selectare echipelor din Champions League - league phase (LP)
countries_directory = r'C:\Users\home\PycharmProjects\pythonProject\Python_Courses\simulator2\countries'


def finding_CL_and_UE_winning_teams():
    team_that_won_CL = None
    team_that_won_UE = None

    for filename in os.listdir(countries_directory):     # iterăm prin toate fișierele Python din directorul specificat
        if filename.endswith(".py"):
            filepath = os.path.join(countries_directory, filename)
            try:
                teams_13a = import_teams_from_file(filepath)
                                                                        #       Obținem CL și UE din modulele importate
                module_name = filename[:-3]                             # Eliminăm extensia .py pentru numele modulului
                module = importlib.import_module(f"countries.{module_name}")
                place_in_championship_for_CL_winning_team = getattr(module, 'CL', None)
                place_in_championship_for_UE_winning_team = getattr(module, 'UE', None)

                if place_in_championship_for_CL_winning_team is not None \
                        and place_in_championship_for_CL_winning_team <= len(teams_13a):
                    team_that_won_CL = teams_13a[place_in_championship_for_CL_winning_team-1]
                if place_in_championship_for_UE_winning_team is not None \
                        and place_in_championship_for_UE_winning_team <= len(teams_13a):
                    team_that_won_UE = teams_13a[place_in_championship_for_UE_winning_team-1]
            except FileNotFoundError as e_13a:
                print(f"Eroare de atribut: {e_13a}")
                continue
    return team_that_won_CL, team_that_won_UE
winner_of_CL_en_titre, winner_of_UE_en_titre = finding_CL_and_UE_winning_teams()
print(f"\n13a. Echipa CL: {winner_of_CL_en_titre}")
print(f"13a. Echipa UE: {winner_of_UE_en_titre}")


def excluding_CL_and_UE_winning_teams(local_cl_teams, local_ue_teams):
    filtered_teams_dict = {}
    for country_13b1, data_13b1 in teams_of_CL.items():
        filtered_teams = [team for team in data_13b1['teams'] if team != local_cl_teams and team != local_ue_teams]
        filtered_teams_dict[country_13b1] = {'teams': filtered_teams, 'position': data_13b1['position']}
    return filtered_teams_dict
teams_of_CL_without_CL_and_UE_winners = excluding_CL_and_UE_winning_teams(winner_of_CL_en_titre, winner_of_UE_en_titre)

print("\n13b. Echipele excluzând echipele CL și UE:")
for country_13b2, data_13b2 in teams_of_CL_without_CL_and_UE_winners.items():
    teams_str = ', '.join(data_13b2['teams'])
    print(f"13b. {country_13b2:<22} - {teams_str:<110} --- position: {data_13b2['position']:>3}")


def selecting_teams_for_CL_LP(local_teams, local_position):
    if local_position in [1, 2]:
        return local_teams[:3]
    elif local_position in [3, 4, 5]:
        return local_teams[:2]
    elif local_position == 6:
        return local_teams[:1]
    return []

teams_for_CL_LP = []
for country_13c1, data_13c1 in teams_of_CL_without_CL_and_UE_winners.items():
    teams_13c1 = data_13c1['teams']
    position_13c1 = data_13c1['position']
    selected_teams = selecting_teams_for_CL_LP(teams_13c1, position_13c1)

    for team_13c1 in selected_teams:
        if team_13c1 in uefa_coefficients:
            coefficient = uefa_coefficients[team_13c1]['coefficient']
            teams_for_CL_LP.append((position_13c1, team_13c1, coefficient, country_13c1))

if winner_of_CL_en_titre and winner_of_CL_en_titre in uefa_coefficients:
    cl_country = uefa_coefficients[winner_of_CL_en_titre]['country']
    cl_coefficient = uefa_coefficients[winner_of_CL_en_titre]['coefficient']
    teams_for_CL_LP.append((0, winner_of_CL_en_titre, cl_coefficient, cl_country))

if winner_of_UE_en_titre and winner_of_UE_en_titre in uefa_coefficients:
    ue_country = uefa_coefficients[winner_of_UE_en_titre]['country']
    ue_coefficient = uefa_coefficients[winner_of_UE_en_titre]['coefficient']
    teams_for_CL_LP.append((0, winner_of_UE_en_titre, ue_coefficient, ue_country))

sorted_teams_for_CL_LP = sorted(teams_for_CL_LP, key=lambda x: x[2], reverse=True)

print("\n Champions League:                                             The league round")
nr = 1
for position_13c2, team_13c2, coefficient, country_13c2 in sorted_teams_for_CL_LP:
    print(f"{nr:2} {team_13c2:<30} - {country_13c2:<23}- coefficient: {coefficient:>5} ")
    nr += 1
# -------------------------------------------------------------------- pentru turul 1 preliminarii Champions Path CP_T1
teams_for_CP_T1 = []
print("\n Champions League:                             The first round - Champions Path")
for country_13d, info in teams_of_CL_without_CL_and_UE_winners.items():
    if info["position"] >= 19 and info["teams"]:  # Verificăm dacă există echipe
        first_team = info["teams"][0]
        if first_team in uefa_coefficients:
            coefficient = uefa_coefficients[first_team]['coefficient']
            teams_for_CP_T1.append((info['position'], first_team, coefficient, country_13d))

# Sortăm echipele după coeficient în ordine descrescătoare
sorted_teams_for_CP_T1 = sorted(teams_for_CP_T1, key=lambda x: x[2], reverse=True)

crt = 1
for position_13d, team_13d, coefficient, country_13d in sorted_teams_for_CP_T1:
    print(f"{crt:>2} {team_13d:<30} - {country_13d:<23}- coefficient: {coefficient:>5} ")
    crt += 1
# -------------------------------------------------------------------- pentru turul 2 preliminarii Champions Path CP_T2
teams_for_CP_T2 = []
print("\n Champions League:                            The second round - Champions Path")
for country_13e, info in teams_of_CL_without_CL_and_UE_winners.items():
    if 13 <= info["position"] <= 18 and info["teams"]:
        first_team = info["teams"][0]
        if first_team in uefa_coefficients:
            coefficient = uefa_coefficients[first_team]['coefficient']
            teams_for_CP_T2.append((info['position'], first_team, coefficient, country_13e))

# Sortăm echipele după coeficient în ordine descrescătoare
sorted_teams_for_CP_T2 = sorted(teams_for_CP_T2, key=lambda x: x[2], reverse=True)

crt = 1
for position_13e, team_13e, coefficient, country_13e in sorted_teams_for_CP_T2:
    print(f"{crt:>2} {team_13e:<30} - {country_13e:<23}- coefficient: {coefficient:>5} ", )
    crt += 1
# -------------------------------------------------------------------- pentru turul 3 preliminarii Champions Path CP_T3
teams_for_CP_T3 = []
print("\n Champions League:                             The third round - Champions Path")
for country_13f, info in teams_of_CL_without_CL_and_UE_winners.items():
    if 7 <= info["position"] <= 12 and info["teams"]:
        first_team = info["teams"][0]
        if first_team in uefa_coefficients:
            coefficient = uefa_coefficients[first_team]['coefficient']
            teams_for_CP_T3.append((info['position'], first_team, coefficient, country_13f))

# Sortăm echipele după coeficient în ordine descrescătoare
sorted_teams_for_CP_T3 = sorted(teams_for_CP_T3, key=lambda x: x[2], reverse=True)

crt = 1
for position_13f, team_13f, coefficient, country_13f in sorted_teams_for_CP_T3:
    print(f"{crt:>2} {team_13f:<30} - {country_13f:<23}- coefficient: {coefficient:>5} ")
    crt += 1
# ---------------------------------------------------------------------- pentru turul 1 preliminarii Runners Path RP_T1
teams_for_RP_T1 = []
print("\n Champions League:                               The first round - Runners Path")
for country_13g, info in teams_of_CL_without_CL_and_UE_winners.items():
    if 13 <= info["position"] <= 34 and info["teams"]:
        second_team = info["teams"][1]
        if second_team in uefa_coefficients:
            coefficient = uefa_coefficients[second_team]['coefficient']
            teams_for_RP_T1.append((info['position'], second_team, coefficient, country_13g))

# Sortăm echipele după coeficient în ordine descrescătoare
sorted_teams_for_RP_T1 = sorted(teams_for_RP_T1, key=lambda x: x[2], reverse=True)

crt = 1
for position_13g, team_13g, coefficient, country_13g in sorted_teams_for_RP_T1:
    print(f"{crt:>2} {team_13g:<30} - {country_13g:<23}- coefficient: {coefficient:>5} ")
    crt += 1
# ---------------------------------------------------------------------- pentru turul 2 preliminarii Runners Path RP_T2
teams_for_RP_T2 = []
print("\n Champions League:                               The second round - Runners Path")
for country_13h, info in teams_of_CL_without_CL_and_UE_winners.items():
    if 2 <= info["position"] <= 12 and info["teams"]:
        second_team = info["teams"][-1]
        if second_team in uefa_coefficients:
            coefficient = uefa_coefficients[second_team]['coefficient']
            teams_for_RP_T2.append((info['position'], second_team, coefficient, country_13h))

# Sortăm echipele după coeficient în ordine descrescătoare
sorted_teams_for_RP_T2 = sorted(teams_for_RP_T2, key=lambda x: x[2], reverse=True)

crt = 1
for position_13h, team_13h, coefficient, country_13h in sorted_teams_for_RP_T2:
    print(f"{crt:>2} {team_13h:<30} - {country_13h:<23} - coefficient: {coefficient:>5} ")
    crt += 1
# ---------------------------------------------------------------------- pentru turul 3 preliminarii Runners Path RP_T3
selected_teams = set([team for _, team, _, _ in sorted_teams_for_CL_LP] +
                     [team for _, team, _, _ in sorted_teams_for_CP_T1] +
                     [team for _, team, _, _ in sorted_teams_for_CP_T2] +
                     [team for _, team, _, _ in sorted_teams_for_CP_T3] +
                     [team for _, team, _, _ in sorted_teams_for_RP_T1] +
                     [team for _, team, _, _ in sorted_teams_for_RP_T2])

# Creăm o listă pentru echipele rămase
teams_for_RP_T3 = []

for country_13i, info in teams_of_CL_without_CL_and_UE_winners.items():
    for team in info["teams"]:
        if team not in selected_teams and team in uefa_coefficients:
            coefficient = uefa_coefficients[team]['coefficient']
            teams_for_RP_T3.append((info['position'], team, coefficient, country_13i))
sorted_teams_for_RP_T3 = sorted(teams_for_RP_T3, key=lambda x: x[2], reverse=True)

print("\n Champions League:                                The third round - Runners Path")
crt = 1
for position_13i, team, coefficient, country_13i in sorted_teams_for_RP_T3:
    print(f"{crt:>2} {team:<30} - {country_13i:<23} - coefficient: {coefficient:>5}")
    crt += 1
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 14 ------------------------------------- meciurile din turul 1 preliminarii Champions League - Champions Path
qualified_teams_for_CP_T2 = []


def run_CP_T1():
    urna1_CP_T1 = sorted_teams_for_CP_T1[:len(sorted_teams_for_CP_T1) // 2]
    urna2_CP_T1 = sorted_teams_for_CP_T1[len(sorted_teams_for_CP_T1) // 2:]
    random.shuffle(urna1_CP_T1)
    random.shuffle(urna2_CP_T1)

    matches = []
    for team1 in urna1_CP_T1:
        while True:
            team2 = random.choice(urna2_CP_T1)
            if team1[3] != team2[3]:  # verificăm dacă echipele sunt din țări diferite
                matches.append((team1, team2))
                urna2_CP_T1.remove(team2)
                break

    print("\n\033[31m Champions League - the first qualifying round results - the champions path \033[0m")
    for index_14b, (team1, team2) in enumerate(matches, start=1):
        # convertim tuplele în dicționare pentru compatibilitate cu simulate_matches
        team1_dict = {
            'team': team1[1],
            'coefficient': team1[2],
            'country': team1[3]
        }
        team2_dict = {
            'team': team2[1],
            'coefficient': team2[2],
            'country': team2[3]
        }

        result = simulate_matches(team1_dict, team2_dict)
        print(f"{index_14b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_teams_for_CP_T2.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_for_CP_T2.append("Error processing match result")
    return qualified_teams_for_CP_T2


if __name__ == "__main__":
    qualified_teams_for_CP_T2 = run_CP_T1()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 15 ---------------------------------- meciurile din turul 1 preliminarii Champions League - Runners Path (RP)
qualified_teams_for_RP_T2 = []


def run_RP_T1():
    urna1_RP_T1 = sorted_teams_for_RP_T1[:len(sorted_teams_for_RP_T1) // 2]
    urna2_RP_T1 = sorted_teams_for_RP_T1[len(sorted_teams_for_RP_T1) // 2:]
    random.shuffle(urna1_RP_T1)
    random.shuffle(urna2_RP_T1)

    matches = []
    for team1 in urna1_RP_T1:
        while True:
            team2 = random.choice(urna2_RP_T1)
            if team1[3] != team2[3]:                                   # verificăm dacă echipele sunt din țări diferite
                matches.append((team1, team2))
                urna2_RP_T1.remove(team2)
                break

    print("\n\033[31m Champions League - the first qualifying round results - the runners path \033[0m")
    for index_15b, (team1, team2) in enumerate(matches, start=1):
        # convertim tuplele în dicționare pentru compatibilitate cu simulate_matches
        team1_dict = {
            'team': team1[1],
            'coefficient': team1[2],
            'country': team1[3]
        }
        team2_dict = {
            'team': team2[1],
            'coefficient': team2[2],
            'country': team2[3]
        }
        result = simulate_matches(team1_dict, team2_dict)
        print(f"{index_15b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_teams_for_RP_T2.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_for_RP_T2.append("Error processing match result")
    return qualified_teams_for_RP_T2

if __name__ == "__main__":
    qualified_teams_for_RP_T2 = run_RP_T1()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 16 ----------------------------------------------------------- meciurile din turul 1 preliminarii UEFA League
teams_of_UE_T1 = []
for country_16a, country_info in teams_of_UE.items():
    for team in country_info['teams']:
        if team in uefa_coefficients:
            teams_of_UE_T1.append({
                'team': team,
                'coefficient': uefa_coefficients[team]['coefficient'],
                'country': country_16a
            })
sorted_teams_of_UE_T1 = sorted(teams_of_UE_T1, key=lambda x: x['coefficient'], reverse=True)
print("\n\n  Europa League                                                   the first round")
for index_16a, team_info in enumerate(sorted_teams_of_UE_T1, start=1):
    print(f"{index_16a:>3}. {team_info['team']:<29} "
          f"- {team_info['country']:<23} - coefficient: {team_info['coefficient']:>5}")

teams_of_UE_T2 = []


def run_UE_T1():
    urna1_UE_T1 = sorted_teams_of_UE_T1[:len(sorted_teams_of_UE_T1) // 2]
    urna2_UE_T1 = sorted_teams_of_UE_T1[len(sorted_teams_of_UE_T1) // 2:]
    random.shuffle(urna1_UE_T1)
    random.shuffle(urna2_UE_T1)

    matches = []
    for team1 in urna1_UE_T1:
        while True:  # buclă până când sunt găsite echipe din țări diferite
            team2 = random.choice(urna2_UE_T1)
            if team1['country'] != team2['country']:
                matches.append((team1, team2))
                urna2_UE_T1.remove(team2)  # se scoate echipa din urna2 după ce a fost folosită
                break  # se iese din bucla while când se găsește o pereche validă

    print("\n\033[31m UEFA League - the first qualifying round results\033[0m")
    for index_16b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_16b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            teams_of_UE_T2.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            teams_of_UE_T2.append("Error processing match result")
    return teams_of_UE_T2

if __name__ == "__main__":
    teams_of_UE_T2 = run_UE_T1()
# ------------------------------------------------------------------------------------------------------------------
# Punctul 17 -------------------------------------------------- meciurile din turul 1 preliminarii Conference League
# echipele care nu s-au calificat din primul tur al Champions League (Champions Path, Runners Path) și Europa League
non_qualified_teams_CP_T1 = [team[1] for team in sorted_teams_for_CP_T1 if team[1] not in qualified_teams_for_CP_T2]
non_qualified_teams_RP_T1 = [team[1] for team in sorted_teams_for_RP_T1 if team[1] not in qualified_teams_for_RP_T2]
non_qualified_teams_UE_T1 = [team_info['team'] for team_info in sorted_teams_of_UE_T1
                             if team_info['team'] not in teams_of_UE_T2]
wild_card_teams = \
    sorted(conference_coefficients_wild_card.items(), key=lambda x: x[1]['coefficient'], reverse=True)[-3:]

teams_of_Co_T1 = []
for team_name in non_qualified_teams_CP_T1:                # adăugăm echipele din Champions Path care nu s-au calificat
    if team_name in uefa_coefficients:
        teams_of_Co_T1.append({
            'team': team_name,
            'coefficient': uefa_coefficients[team_name]['coefficient'],
            'country': uefa_coefficients[team_name]['country']
        })

for team_name in non_qualified_teams_RP_T1:                  # adăugăm echipele din Runners Path care nu s-au calificat
    if team_name in uefa_coefficients:
        teams_of_Co_T1.append({
            'team': team_name,
            'coefficient': uefa_coefficients[team_name]['coefficient'],
            'country': uefa_coefficients[team_name]['country']
        })

for team_name in non_qualified_teams_UE_T1:                 # adăugăm echipele din Europa League care nu s-au calificat
    if team_name in uefa_coefficients:
        teams_of_Co_T1.append({
            'team': team_name,
            'coefficient': uefa_coefficients[team_name]['coefficient'],
            'country': uefa_coefficients[team_name]['country']
        })

for team_name, team_info in wild_card_teams:                                               # adăugăm echipele wild card
    teams_of_Co_T1.append({
        'team': team_name,
        'coefficient': team_info['coefficient'],
        'country': team_info['country']
    })

sorted_teams_of_Co_T1 = sorted(teams_of_Co_T1, key=lambda x: x['coefficient'], reverse=True)
print("\n\n  Conference League                                               the first round")
for index_17a, team_info in enumerate(sorted_teams_of_Co_T1, start=1):
    print(f"{index_17a:>3}. {team_info['team']:<29} "
          f"- {team_info['country']:<23} - coefficient: {team_info['coefficient']:>5}")


qualified_teams_of_Co_T2 = []


def run_Co_T1():
    urna1_Co_T1 = sorted_teams_of_Co_T1[:len(sorted_teams_of_Co_T1) // 2]
    urna2_Co_T1 = sorted_teams_of_Co_T1[len(sorted_teams_of_Co_T1) // 2:]
    random.shuffle(urna1_Co_T1)
    random.shuffle(urna2_Co_T1)

    matches = []
    for team1 in urna1_Co_T1:
        while True:
            team2 = random.choice(urna2_Co_T1)
            if team1['country'] != team2['country']:
                matches.append((team1, team2))
                urna2_Co_T1.remove(team2)
                break

    print("\n\033[31m Conference League - the first qualifying round results\033[0m")
    for index_17b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_17b:>2}. {result}")
        try:
            qualified_team_name = result.split("Qualified team: ")[1].strip()
            for team in sorted_teams_of_Co_T1:
                if team['team'] == qualified_team_name:
                    qualified_teams_of_Co_T2.append(team)
                    break
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_of_Co_T2.append("Error processing match result")
    return qualified_teams_of_Co_T2

if __name__ == "__main__":
    qualified_teams_of_Co_T2 = run_Co_T1()
# sorted_qualified_teams_for_Co_T2 = sorted(qualified_teams_of_Co_T2, key=lambda x: x['coefficient'], reverse=True)
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 18 ------------------------------------- meciurile din turul 2 preliminarii Champions League - Champions Path
teams_of_CP_T2 = []

# Procesăm echipele din `qualified_teams_for_CP_T2` (presupunând că sunt nume de echipe)
for team_name in qualified_teams_for_CP_T2:
    if team_name in uefa_coefficients:
        coefficient = uefa_coefficients[team_name]['coefficient']
        country_18a1 = uefa_coefficients[team_name]['country']
        teams_of_CP_T2.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country_18a1
        })

for position_18a2, team_name, coefficient, country_18a2 in teams_for_CP_T2:
    if team_name in uefa_coefficients:
        teams_of_CP_T2.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country_18a2
        })

# Sortăm echipele pentru CP_T2 după coeficient
sorted_teams_of_CP_T2 = sorted(teams_of_CP_T2, key=lambda x: x['coefficient'], reverse=True)

qualified_teams_for_CP_T3 = []


def run_CP_T2():
    urna1_CP_T2 = sorted_teams_of_CP_T2[:len(sorted_teams_of_CP_T2) // 2]
    urna2_CP_T2 = sorted_teams_of_CP_T2[len(sorted_teams_of_CP_T2) // 2:]
    random.shuffle(urna1_CP_T2)
    random.shuffle(urna2_CP_T2)

    matches = []
    for team1 in urna1_CP_T2:
        while True:
            team2 = random.choice(urna2_CP_T2)
            if team1['country'] != team2['country']:                   # verificăm dacă echipele sunt din țări diferite
                matches.append((team1, team2))
                urna2_CP_T2.remove(team2)
                break

    print("\n\033[31m Champions League - the second qualifying round results - the champions path \033[0m")
    for index_18b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_18b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_teams_for_CP_T3.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_for_CP_T3.append("Error processing match result")
    return qualified_teams_for_CP_T3

if __name__ == "__main__":
    qualified_teams_for_CP_T3 = run_CP_T2()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 19 --------------------------------------- meciurile din turul 2 preliminarii Champions League - Runners Path
teams_of_RP_T2 = []

# Procesăm echipele din `qualified_teams_for_RP_T2` (presupunând că sunt nume de echipe)
for team_name in qualified_teams_for_RP_T2:
    if team_name in uefa_coefficients:
        coefficient = uefa_coefficients[team_name]['coefficient']
        country_19a1 = uefa_coefficients[team_name]['country']
        teams_of_RP_T2.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country_19a1
        })

for position_19a2, team_name, coefficient, country_19a2 in teams_for_RP_T2:
    if team_name in uefa_coefficients:
        teams_of_RP_T2.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country_19a2
        })

# Sortăm echipele pentru RP_T2 după coeficient
sorted_teams_of_RP_T2 = sorted(teams_of_RP_T2, key=lambda x: x['coefficient'], reverse=True)

qualified_teams_for_RP_T3 = []


def run_RP_T2():
    urna1_RP_T2 = sorted_teams_of_RP_T2[:len(sorted_teams_of_RP_T2) // 2]
    urna2_RP_T2 = sorted_teams_of_RP_T2[len(sorted_teams_of_RP_T2) // 2:]
    random.shuffle(urna1_RP_T2)
    random.shuffle(urna2_RP_T2)

    matches = []
    for team1 in urna1_RP_T2:
        while True:
            team2 = random.choice(urna2_RP_T2)
            if team1['country'] != team2['country']:  # verificăm dacă echipele sunt din țări diferite
                matches.append((team1, team2))
                urna2_RP_T2.remove(team2)
                break

    print("\n\033[31m Champions League - the second qualifying round results - the champions path \033[0m")
    for index_19b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_19b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_teams_for_RP_T3.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_for_RP_T3.append("Error processing match result")
    return qualified_teams_for_RP_T3

if __name__ == "__main__":
    qualified_teams_for_RP_T3 = run_RP_T2()
# --------------------------------------------------------------------------------------------------------------------
# Punctul 20 ----------------------------------------------------------- meciurile din turul 2 preliminarii UEFA League
qualified_teams_of_UE_T2 = []
for team_name in teams_of_UE_T2:
    if team_name in uefa_coefficients:
        coefficient = uefa_coefficients[team_name]['coefficient']
        country_20a = uefa_coefficients[team_name]['country']
        qualified_teams_of_UE_T2.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country_20a
        })

sorted_qualified_teams_of_UE_T2 = sorted(qualified_teams_of_UE_T2, key=lambda x: x['coefficient'], reverse=True)
print("\n  Europa League                                                  the second round")
for index_20a, team_info in enumerate(sorted_qualified_teams_of_UE_T2, start=1):
    print(f"{index_20a:>3}. {team_info['team']:<29} "
          f"- {team_info['country']:<23} - coefficient: {team_info['coefficient']:>5}")

teams_of_UE_T3 = []


def run_UE_T2():
    urna1_UE_T2 = teams_of_UE_T2[:len(teams_of_UE_T2) // 2]
    urna2_UE_T2 = teams_of_UE_T2[len(teams_of_UE_T2) // 2:]
    random.shuffle(urna1_UE_T2)
    random.shuffle(urna2_UE_T2)

    matches = []
    for team1_name in urna1_UE_T2:
        while True:
            team2_name = random.choice(urna2_UE_T2)
            if team1_name != team2_name:
                team1 = next(team for team in sorted_teams_of_UE_T1 if team['team'] == team1_name)
                team2 = next(team for team in sorted_teams_of_UE_T1 if team['team'] == team2_name)
                matches.append((team1, team2))
                urna2_UE_T2.remove(team2_name)
                break

    print("\n\033[31m UEFA League - the second qualifying round results\033[0m")
    for index_20b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_20b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            teams_of_UE_T3.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            teams_of_UE_T3.append("Error processing match result")
    return teams_of_UE_T3

if __name__ == "__main__":
    teams_of_UE_T3 = run_UE_T2()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 21 ----------------------------------------------------- meciurile din turul 2 preliminarii Conference League
non_qualified_teams_UE_T2 = [team_name for team_name in teams_of_UE_T2 if team_name not in teams_of_UE_T3]

# Construim lista echipele necalificate din turul 2 Europa League cu detalii suplimentare
non_qualified_teams_UE_T2_details = [
    {
        'team': team_name,
        'coefficient': uefa_coefficients[team_name]['coefficient'],
        'country': uefa_coefficients[team_name]['country']
    }
    for team_name in non_qualified_teams_UE_T2 if team_name in uefa_coefficients
]
teams_of_Co_T2 = qualified_teams_of_Co_T2 + non_qualified_teams_UE_T2_details

sorted_teams_of_Co_T2 = sorted(teams_of_Co_T2, key=lambda x: x['coefficient'], reverse=True)

print("\n\n  Conference League                                              the second round")
for index_21a, team_info in enumerate(sorted_teams_of_Co_T2, start=1):
    print(f"{index_21a:>3}. {team_info['team']:<29} "
          f"- {team_info['country']:<23} - coefficient: {team_info['coefficient']:>5}")


qualified_teams_of_Co_T3 = []


def run_Co_T2():
    urna1_Co_T2 = sorted_teams_of_Co_T2[:len(sorted_teams_of_Co_T2) // 2]
    urna2_Co_T2 = sorted_teams_of_Co_T2[len(sorted_teams_of_Co_T2) // 2:]
    random.shuffle(urna1_Co_T2)
    random.shuffle(urna2_Co_T2)

    matches = []
    for team1 in urna1_Co_T2:
        while True:
            team2 = random.choice(urna2_Co_T2)
            if team1['country'] != team2['country']:
                matches.append((team1, team2))
                urna2_Co_T2.remove(team2)
                break

    print("\n\033[31m Conference League - the second qualifying round results\033[0m")
    for index_21b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_21b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_teams_of_Co_T3.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_of_Co_T3.append("Error processing match result")
    return qualified_teams_of_Co_T3

if __name__ == "__main__":
    qualified_teams_of_Co_T3 = run_Co_T2()

# Sortăm echipele descrescător după coeficient
sorted_teams_of_Co_T2 = sorted(qualified_teams_of_Co_T2, key=lambda x: x['coefficient'], reverse=True)


# with open('winners_in_Co_T2.py', 'w', encoding='utf-8') as file:
#     file.write("winners_in_Co_T2 = {\n")
#     for team in sorted_teams_of_Co_T2:
#         file.write(
#              f'    "{team["team"]}": {{"coefficient": {team["coefficient"]}, "country": "{team["country"]}"}},\n')
#     file.write("}\n")
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 22 ------------------------------------- meciurile din turul 3 preliminarii Champions League - Champions Path
teams_of_CP_T3 = []

for team_name in qualified_teams_for_CP_T3:
    if team_name in uefa_coefficients:
        coefficient = uefa_coefficients[team_name]['coefficient']
        country_22a1 = uefa_coefficients[team_name]['country']
        teams_of_CP_T3.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country_22a1
        })

for position_22a2, team_name, coefficient, country_22a2 in teams_for_CP_T3:
    if team_name in uefa_coefficients:
        teams_of_CP_T3.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country_22a2
        })

# Sortăm echipele pentru CP_T2 după coeficient
sorted_teams_of_CP_T3 = sorted(teams_of_CP_T3, key=lambda x: x['coefficient'], reverse=True)

qualified_teams_for_CL_LP_cp = []


def run_CP_T3():
    urna1_CP_T3 = sorted_teams_of_CP_T3[:len(sorted_teams_of_CP_T3) // 2]
    urna2_CP_T3 = sorted_teams_of_CP_T3[len(sorted_teams_of_CP_T3) // 2:]
    random.shuffle(urna1_CP_T3)
    random.shuffle(urna2_CP_T3)

    matches = []
    for team1 in urna1_CP_T3:
        while True:
            team2 = random.choice(urna2_CP_T3)
            if team1['country'] != team2['country']:  # verificăm dacă echipele sunt din țări diferite
                matches.append((team1, team2))
                urna2_CP_T3.remove(team2)
                break

    print("\n\033[31m Champions League - the third qualifying round results - the champions path \033[0m")
    for index_22b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_22b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_teams_for_CL_LP_cp.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_for_CL_LP_cp.append("Error processing match result")
    return qualified_teams_for_CL_LP_cp

if __name__ == "__main__":
    qualified_teams_for_CL_LP_cp = run_CP_T3()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 23 --------------------------------------- meciurile din turul 3 preliminarii Champions League - Runners Path
teams_of_RP_T3 = []

for team_name in qualified_teams_for_RP_T3:
    if team_name in uefa_coefficients:
        coefficient = uefa_coefficients[team_name]['coefficient']
        country_23a1 = uefa_coefficients[team_name]['country']
        teams_of_RP_T3.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country_23a1
        })

for position_23a2, team_name, coefficient, country_23a2 in teams_for_RP_T3:
    if team_name in uefa_coefficients:
        teams_of_RP_T3.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country_23a2
        })

# Sortăm echipele pentru RP_T3 după coeficient
sorted_teams_of_RP_T3 = sorted(teams_of_RP_T3, key=lambda x: x['coefficient'], reverse=True)

qualified_teams_for_CL_LP_rp = []


def run_RP_T3():
    urna1_RP_T3 = sorted_teams_of_RP_T3[:len(sorted_teams_of_RP_T3) // 2]
    urna2_RP_T3 = sorted_teams_of_RP_T3[len(sorted_teams_of_RP_T3) // 2:]
    random.shuffle(urna1_RP_T3)
    random.shuffle(urna2_RP_T3)

    matches = []
    for team1 in urna1_RP_T3:
        while True:
            team2 = random.choice(urna2_RP_T3)
            if team1['country'] != team2['country']:  # verificăm dacă echipele sunt din țări diferite
                matches.append((team1, team2))
                urna2_RP_T3.remove(team2)
                break

    print("\n\033[31m Champions League - the third qualifying round results - the runners path \033[0m")
    for index_23b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_23b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_teams_for_CL_LP_rp.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_for_CL_LP_rp.append("Error processing match result")
    return qualified_teams_for_CL_LP_rp

if __name__ == "__main__":
    qualified_teams_for_CL_LP_rp = run_RP_T3()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 24 ----------------------------------------------------------- meciurile din turul 3 preliminarii UEFA League
qualified_teams_of_UE_T3 = []
for team_name in teams_of_UE_T3:
    if team_name in uefa_coefficients:
        coefficient = uefa_coefficients[team_name]['coefficient']
        country_24a = uefa_coefficients[team_name]['country']
        qualified_teams_of_UE_T3.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country_24a
        })
sorted_qualified_teams_of_UE_T3 = sorted(qualified_teams_of_UE_T3, key=lambda x: x['coefficient'], reverse=True)
print("\n  Europa League                                                   the third round")
for index_24a, team_info in enumerate(sorted_qualified_teams_of_UE_T3, start=1):
    print(f"{index_24a:>3}. {team_info['team']:<29} "
          f"- {team_info['country']:<23} - coefficient: {team_info['coefficient']:>5}")

qualified_teams_of_UE_T4 = []


def run_UE_T3():
    urna1_UE_T3 = teams_of_UE_T3[:len(teams_of_UE_T3) // 2]
    urna2_UE_T3 = teams_of_UE_T3[len(teams_of_UE_T3) // 2:]
    random.shuffle(urna1_UE_T3)
    random.shuffle(urna2_UE_T3)

    matches = []
    for team1_name in urna1_UE_T3:
        while True:
            team2_name = random.choice(urna2_UE_T3)
            if team1_name != team2_name:
                team1 = next(team for team in sorted_teams_of_UE_T1 if team['team'] == team1_name)
                team2 = next(team for team in sorted_teams_of_UE_T1 if team['team'] == team2_name)
                matches.append((team1, team2))
                urna2_UE_T3.remove(team2_name)
                break

    print("\n\033[31m UEFA League - the third qualifying round results\033[0m")
    for index_24b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_24b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_teams_of_UE_T4.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_of_UE_T4.append({
                'team': "Error processing match result",
                'coefficient': 0,
                'country': 'Unknown'
            })
    return qualified_teams_of_UE_T4


if __name__ == "__main__":
    qualified_teams_of_UE_T4 = run_UE_T3()
#  --------------------------------------------------------------------------------------------------------------------
# Punctul 25 ----------------------------------------------------- meciurile din turul 3 preliminarii Conference League
teams_of_Co_T3 = []
for team_name in qualified_teams_of_Co_T3:
    if team_name in uefa_coefficients:
        teams_of_Co_T3.append({
            'team': team_name,
            'coefficient': uefa_coefficients[team_name]['coefficient'],
            'country': uefa_coefficients[team_name]['country']
        })

sorted_teams_of_Co_T3 = sorted(teams_of_Co_T3, key=lambda x: x['coefficient'], reverse=True)
print("\n\n  Conference League                                               the third round")
for index_25a, team_info in enumerate(sorted_teams_of_Co_T3, start=1):
    print(f"{index_25a:>3}. {team_info['team']:<29} "
          f"- {team_info['country']:<23} - coefficient: {team_info['coefficient']:>5}")


qualified_teams_of_Co_T4 = []


def run_Co_T3():
    urna1_Co_T3 = sorted_teams_of_Co_T3[:len(sorted_teams_of_Co_T3) // 2]
    urna2_Co_T3 = sorted_teams_of_Co_T3[len(sorted_teams_of_Co_T3) // 2:]
    random.shuffle(urna1_Co_T3)
    random.shuffle(urna2_Co_T3)

    matches = []
    for team1 in urna1_Co_T3:
        while True:
            team2 = random.choice(urna2_Co_T3)
            if team1['country'] != team2['country']:
                matches.append((team1, team2))
                urna2_Co_T3.remove(team2)
                break

    print("\n\033[31m Conference League - the third qualifying round results\033[0m")
    for index_25b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_25b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_teams_of_Co_T4.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_of_Co_T4.append("Error processing match result")
    return qualified_teams_of_Co_T4

if __name__ == "__main__":
    qualified_teams_of_Co_T4 = run_Co_T3()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 26 --------------------------------------------------------- meciurile din turul 4 preliminarii Europa League


def get_team_info_26(team_26):
    if team_26 in uefa_coefficients:
        return {
            'team': team_26,
            'coefficient': uefa_coefficients[team_26]['coefficient'],
            'country': uefa_coefficients[team_26]['country']
        }
    else:
        print(f"Eroare: Nu s-au găsit date pentru echipa {team_26}")
        return None

non_qualified_teams_CP_T2 = [get_team_info_26(team['team']) for team in teams_of_CP_T2
                             if team['team'] not in qualified_teams_for_CP_T3]
non_qualified_teams_RP_T2 = [get_team_info_26(team['team']) for team in teams_of_RP_T2
                             if team['team'] not in qualified_teams_for_RP_T3]
qualified_teams_of_UE_T4 = [team if isinstance(team, dict) else get_team_info_26(team)
                            for team in qualified_teams_of_UE_T4]
teams_of_UE_T4 = non_qualified_teams_CP_T2 + non_qualified_teams_RP_T2 + qualified_teams_of_UE_T4
sorted_teams_of_UE_T4 = sorted(teams_of_UE_T4, key=lambda x: x['coefficient'], reverse=True)

print("\n\n  Europa League                                                  the fourth round")
for index_26a, team_info in enumerate(sorted_teams_of_UE_T4, start=1):
    print(f"{index_26a:>3}. {team_info['team']:<29} "
          f"- {team_info['country']:<23} - coefficient: {team_info['coefficient']:>5}")


qualified_teams_of_UE_T5 = []


def run_UE_T4():
    urna1_UE_T4 = sorted_teams_of_UE_T4[:len(sorted_teams_of_UE_T4) // 2]
    urna2_UE_T4 = sorted_teams_of_UE_T4[len(sorted_teams_of_UE_T4) // 2:]
    random.shuffle(urna1_UE_T4)
    random.shuffle(urna2_UE_T4)

    matches = []
    for team1 in urna1_UE_T4:
        while True:
            team2 = random.choice(urna2_UE_T4)
            if team1['country'] != team2['country']:
                matches.append((team1, team2))
                urna2_UE_T4.remove(team2)
                break

    print("\n\033[31m UEFA League - the fourth qualifying round results\033[0m")
    for index_26b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_26b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_teams_of_UE_T5.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_of_UE_T5.append({
                'team': 'Unknown',
                'coefficient': 0,
                'country': 'Unknown'
            })
    return qualified_teams_of_UE_T5

if __name__ == "__main__":
    qualified_teams_of_UE_T5 = run_UE_T4()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 27 --------------------------------------------------------- meciurile din turul 5 preliminarii Europa League
teams_of_UE_T5_dict = []
for team in qualified_teams_of_UE_T5:
    if team in uefa_coefficients:
        team_info = uefa_coefficients[team]
        team_of_UE_T5_dict = {
            "team": team,
            "coefficient": team_info['coefficient'],
            "country": team_info['country']
        }
        teams_of_UE_T5_dict.append(team_of_UE_T5_dict)
    else:
        print(f"Information for team '{team}' not found in uefa_coefficients.")
sorted_teams_of_UE_T5 = sorted(teams_of_UE_T5_dict, key=lambda x: x['coefficient'], reverse=True)
qualified_teams_of_UE_GP = []


def run_UE_T5():
    urna1_UE_T5 = sorted_teams_of_UE_T5[:len(sorted_teams_of_UE_T5) // 2]
    urna2_UE_T5 = sorted_teams_of_UE_T5[len(sorted_teams_of_UE_T5) // 2:]
    random.shuffle(urna1_UE_T5)
    random.shuffle(urna2_UE_T5)

    matches = []
    for team1 in urna1_UE_T5:
        while True:
            team2 = random.choice(urna2_UE_T5)
            if team1['country'] != team2['country']:
                matches.append((team1, team2))
                urna2_UE_T5.remove(team2)
                break

    print("\n\033[31m UEFA League - the fifth qualifying round results\033[0m")
    for index, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_team_dict = next((item for item in sorted_teams_of_UE_T5 if item['team'] == qualified_team), None)
            if qualified_team_dict:
                qualified_teams_of_UE_GP.append(qualified_team_dict)
            else:
                print(f"Error: Qualified team '{qualified_team}' not found in sorted_teams_of_UE_T5.")
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_of_UE_GP.append({
                'team': 'Unknown',
                'coefficient': 0,
                'country': 'Unknown'
            })
    return qualified_teams_of_UE_GP

if __name__ == "__main__":
    qualified_teams_of_UE_GP = run_UE_T5()
#  --------------------------------------------------------------------------------------------------------------------
# Punctul 27 ----------------------------------------------------- meciurile din turul 4 preliminarii Conference League


def get_team_info_27(team_name_27):
    if team_name_27 in uefa_coefficients:
        return {
            'team': team_name_27,
            'coefficient': uefa_coefficients[team_name_27]['coefficient'],
            'country': uefa_coefficients[team_name_27]['country']
        }
    else:
        print(f"Eroare: Nu s-au găsit date pentru echipa {team_name_27}")
        return None

non_qualified_teams_UE_T3 = [team['team'] for team in sorted_teams_of_UE_T4
                             if team['team'] not in qualified_teams_of_UE_T5]
non_qualified_teams_UE_T4_details = [get_team_info_27(team_name) for team_name in non_qualified_teams_UE_T3
                                     if team_name in uefa_coefficients]
qualified_teams_of_Co_T4_details = [get_team_info_27(team_name) for team_name in qualified_teams_of_Co_T4
                                    if team_name in uefa_coefficients]  # bun 25
# Extrage echipa wild_card din lista de echipe
wild_card_team_name, wild_card_team_info = sorted(conference_coefficients_wild_card.items(),
                                                  key=lambda x: x[1]['coefficient'], reverse=True)[10]

# Creează un dicționar pentru wild_card_team pentru a-l adăuga în lista echipelor
wild_card_team = {
    'team': wild_card_team_name,
    'coefficient': wild_card_team_info['coefficient'],
    'country': wild_card_team_info['country']
}
teams_of_Co_T4 = qualified_teams_of_Co_T4_details + non_qualified_teams_UE_T4_details + [wild_card_team]
teams_of_Co_T4 = [team for team in teams_of_Co_T4 if team is not None]
sorted_teams_of_Co_T4 = sorted(teams_of_Co_T4, key=lambda x: x['coefficient'], reverse=True)

print("\n\n  Conference League                                              the fourth round")
for index_27a, team_info in enumerate(sorted_teams_of_Co_T4, start=1):
    print(f"{index_27a:>3}. {team_info['team']:<29} "
          f"- {team_info['country']:<23} - coefficient: {team_info['coefficient']:>5}")


qualified_teams_of_Co_T5 = []


def run_Co_T4():
    urna1_Co_T4 = sorted_teams_of_Co_T4[:len(sorted_teams_of_Co_T4) // 2]
    urna2_Co_T4 = sorted_teams_of_Co_T4[len(sorted_teams_of_Co_T4) // 2:]
    random.shuffle(urna1_Co_T4)
    random.shuffle(urna2_Co_T4)

    matches = []
    for team1 in urna1_Co_T4:
        while True:
            team2 = random.choice(urna2_Co_T4)
            if team1['country'] != team2['country']:
                matches.append((team1, team2))
                urna2_Co_T4.remove(team2)
                break

    print("\n\033[31m Conference League - the fourth qualifying round results\033[0m")
    for index_27b, (team1, team2) in enumerate(matches, start=1):
        result = simulate_matches(team1, team2)
        print(f"{index_27b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            qualified_teams_of_Co_T5.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            qualified_teams_of_Co_T5.append("Error processing match result")
    return qualified_teams_of_Co_T5

if __name__ == "__main__":
    qualified_teams_of_Co_T5 = run_Co_T4()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 28 --------------------------------- meciurile din turul 4 preliminarii Conference League for wild card teams
teams_of_Co_WK = sorted(conference_coefficients_wild_card.items(), key=lambda x: x[1]['coefficient'], reverse=True)[:10]
teams_of_Co_WK_GP = []


def run_Co_WK():
    urna1_Co_WK = teams_of_Co_WK[:len(teams_of_Co_WK) // 2]
    urna2_Co_WK = teams_of_Co_WK[len(teams_of_Co_WK) // 2:]

    random.shuffle(urna1_Co_WK)
    random.shuffle(urna2_Co_WK)

    matches = []
    for team1 in urna1_Co_WK:
        while True:
            team2 = random.choice(urna2_Co_WK)
            matches.append((team1, team2))
            urna2_Co_WK.remove(team2)
            break

    print("\n\033[31m Conference League - Wild Card Round results\033[0m")
    for index_28b, (team1, team2) in enumerate(matches, start=1):

        team1_dict = {'team': team1[0], **team1[1]}
        team2_dict = {'team': team2[0], **team2[1]}

        result = simulate_matches(team1_dict, team2_dict)
        print(f"{index_28b:>2}. {result}")
        try:
            qualified_team = result.split("Qualified team: ")[1].strip()
            teams_of_Co_WK_GP.append(qualified_team)
        except IndexError:
            print(f"Error processing match result: {result}")
            teams_of_Co_WK_GP.append("Error processing match result")
    return teams_of_Co_WK_GP


if __name__ == "__main__":
    teams_of_Co_WK_GP = run_Co_WK()
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 29 -------------------------------------------------- scriere, afișare echipe Champions League - League Phase
teams_for_CL_LP_dict = [
    {'team': team_name, 'coefficient': coefficient, 'country': country, 'position': country}
    for _, team_name, coefficient, country in teams_for_CL_LP
]

teams_of_CL_LP = []
for team_name in qualified_teams_for_CL_LP_cp:
    if team_name in uefa_coefficients:
        coefficient = uefa_coefficients[team_name]['coefficient']
        country = uefa_coefficients[team_name]['country']
        teams_of_CL_LP.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country
        })

for team_name in qualified_teams_for_CL_LP_rp:
    if team_name in uefa_coefficients:
        coefficient = uefa_coefficients[team_name]['coefficient']
        country = uefa_coefficients[team_name]['country']
        teams_of_CL_LP.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country
        })

for team in teams_for_CL_LP_dict:
    team_name = team['team']
    if team_name in uefa_coefficients:
        coefficient = uefa_coefficients[team_name]['coefficient']
        country = uefa_coefficients[team_name]['country']
        teams_of_CL_LP.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country
        })

sorted_teams_of_CL_LP = sorted(teams_of_CL_LP, key=lambda x: x['coefficient'], reverse=True)
with open('teams_of_CL_LP.py', 'w', encoding='utf-8') as file_29:
    file_29.write('teams_of_CL_LP = [\n')
    for position_29, team_data in enumerate(sorted_teams_of_CL_LP, start=1):
        team_name = team_data['team']
        coefficient = team_data['coefficient']
        country = team_data['country']
        file_29.write(f'    {{"team": "{team_name}", "coefficient": {coefficient}, '
                      f'"country": "{country}", "position": {position_29}}},\n')
    file_29.write(']\n')

print("\n\n  Champions League                                                  League Phase")
for index_29, team_info in enumerate(sorted_teams_of_CL_LP, start=1):
    print(f"{index_29:>2}. {team_info['team']:<29} "
          f"- {team_info['country']:<23} - coefficient: {team_info['coefficient']:>5}")
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 30 -------------------------------------------------------- scriere, afișare echipe UEFA League - Group Phase


def get_team_info_30(team_30):
    if team_30 in uefa_coefficients:
        return {
            'team': team_30,
            'coefficient': uefa_coefficients[team_30]['coefficient'],
            'country': uefa_coefficients[team_30]['country']
        }
    else:
        print(f"Eroare: Nu s-au găsit date pentru echipa {team_30}")
        return None

non_qualified_teams_CP_T3 = [get_team_info_30(team['team']) for team in teams_of_CP_T3
                             if team['team'] not in qualified_teams_for_CL_LP_cp]
non_qualified_teams_RP_T3 = [get_team_info_30(team['team']) for team in teams_of_RP_T3
                             if team['team'] not in qualified_teams_for_CL_LP_rp]
teams_for_UE_GP_dict = [{'team': team[0], 'coefficient': team[1], 'country': team[2]} for team in teams_for_CL_LP]

teams_of_UE_GP = []
for team in qualified_teams_of_UE_GP:
    if isinstance(team, dict):
        team_name = team['team']  # Extragem numele echipei din dicționar
    else:
        team_name = team  # Dacă nu este dicționar, folosim valoarea directă

    if team_name in uefa_coefficients:
        coefficient = uefa_coefficients[team_name]['coefficient']
        country = uefa_coefficients[team_name]['country']
        teams_of_UE_GP.append({
            'team': team_name,
            'coefficient': coefficient,
            'country': country
        })

for team_info in non_qualified_teams_CP_T3:
    if team_info and team_info['team'] in uefa_coefficients:
        teams_of_UE_GP.append(team_info)
for team_info in non_qualified_teams_RP_T3:
    if team_info and team_info['team'] in uefa_coefficients:
        teams_of_UE_GP.append(team_info)
sorted_teams_of_UE_GP = sorted(teams_of_UE_GP, key=lambda x: x['coefficient'], reverse=True)

# with open('teams_of_UE_GP.py', 'w', encoding='utf-8') as file_30:
#     file_30.write('teams_of_UE_GP = {\n')
#     for position_30, team_data in enumerate(sorted_teams_of_UE_GP, start=1):
#         team_name = team_data['team']
#         coefficient = team_data['coefficient']
#         country = team_data['country']
#         file_30.write(f'    "{team_name}": '
#                       f'{{"coefficient": {coefficient}, "country": "{country}", "position": {position_30}}},\n')
#     file_30.write('}\n')

with open('teams_of_UE_GP.py', 'w', encoding='utf-8') as file_30:
    file_30.write('teams_of_UE_GP = [\n')
    for position_30, team_data in enumerate(sorted_teams_of_UE_GP, start=1):
        team_name = team_data['team']
        coefficient = team_data['coefficient']
        country = team_data['country']
        file_30.write(f'    {{"team": "{team_name}", "coefficient": {coefficient}, '
                      f'"country": "{country}", "position": {position_30}}},\n')
    file_30.write(']\n')

print("\n\n Europa League                                                       Group Phase")
for index_30, team_info in enumerate(sorted_teams_of_UE_GP, start=1):
    print(f"{index_30:>2}. {team_info['team']:<29} "
          f"- {team_info['country']:<23} - coefficient: {team_info['coefficient']:>5}")
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 31 -------------------------------------------------- scriere, afișare echipe Conference League - Group Phase


def get_team_info_31(team_31):
    if team_31 in uefa_coefficients:
        return {
            'team': team_31,
            'coefficient': uefa_coefficients[team_31]['coefficient'],
            'country': uefa_coefficients[team_31]['country']
        }
    else:
        print(f"Eroare: Nu s-au găsit date pentru echipa {team_31}")
        return None

dict_teams_of_Co_WK_GP = [get_team_info_31(team) or {'team': team, 'coefficient': 0.0, 'country': 'Unknown'} for team in
                 teams_of_Co_WK_GP]
dict_qualified_teams_of_Co_T5 = [get_team_info_31(team) or {'team': team, 'coefficient': 0.0, 'country': 'Unknown'}
                                 for team in qualified_teams_of_Co_T5]
qualified_team_names_UE_GP = [team['team'] for team in qualified_teams_of_UE_GP]
non_qualified_teams_UE_T5 = [team for team in sorted_teams_of_UE_T5 if team['team'] not in qualified_team_names_UE_GP]
teams_of_Co_GP = non_qualified_teams_UE_T5 + dict_teams_of_Co_WK_GP + dict_qualified_teams_of_Co_T5
valid_teams_of_Co_GP = [team for team in teams_of_Co_GP if isinstance(team, dict) and 'coefficient' in team]
sorted_teams_of_Co_GP = sorted(valid_teams_of_Co_GP, key=lambda x: x['coefficient'], reverse=True)

# Scriem rezultatele într-un fișier
with open('teams_of_Co_GP.py', 'w', encoding='utf-8') as file_31:
    file_31.write('teams_of_Co_GP = [\n')
    for position_31, team_data in enumerate(sorted_teams_of_Co_GP, start=1):
        team_name = team_data['team']
        coefficient = team_data['coefficient']
        country = team_data['country']
        file_31.write(f'    {{"team": "{team_name}", "coefficient": {coefficient}, '
                      f'"country": "{country}", "position": {position_31}}},\n')
    file_31.write(']\n')

print("\n\n Conference League                                                       Group Phase")
for index_31, team_info in enumerate(sorted_teams_of_Co_GP, start=1):
    print(f"{index_31:>2}. {team_info['team']:<29} "
          f"- {team_info['country']:<23} - coefficient: {team_info['coefficient']:>5}")
# ---------------------------------------------------------------------------------------------------------------------
# Punctul 32 ------------------------------------------------------------------------- extragere grupe Champions League
'''
# fiindcă position nu este definit în listă teams_of_CL_LP nu pot să pun echipele de la position 1 - 4 în aceeași urnă"
num_pots = 9
num_groups = 4
total_teams_in_LP = len(teams_of_CL_LP)
teams_per_pot = total_teams_in_LP // num_pots
pots = [teams_of_CL_LP[i * teams_per_pot:(i + 1) * teams_per_pot] for i in range(num_pots)]        # crearea pot-urilor
groups = [[] for _ in range(num_groups)]                                                        # crearea grupei finale

for i, pot in enumerate(pots, start=1):                                                             # afișează poturile
    print(f"Pot {i}:")
    for team in pot:
        print(f"  {team['team']:<29} - Coefficient: {team['coefficient']:>5.1f}")
    print()
    
# extrage aleatoriu o echipă din fiecare pot și adaugă în grupele finale
for pot in pots:                                                                                   # pentru fiecare pot
    for group in groups:                                             # fiecare grupă primește o echipă din potul curent
        if pot:                                                                       # verifică dacă potul nu este gol
            team = random.choice(pot)                                                # alege o echipă aleatorie din pot
            pot.remove(team)                                                            # elimină echipa aleasă din pot
            group.append(team)                                                                 # adaugă echipa în grupă

# Crearea DataFrame-ului cu denumirile grupei și echipele
df = pd.DataFrame({f"Grupa {i+1}": [f"{team['team']:<29}" for team in group] for i, group in enumerate(groups)})

print()
# afișează denumirile grupei cu ajustarea distanței dintre ele
header = [f"Grupa {i+1}".ljust(35) for i in range(num_groups)]
print("".join(header))

# afișează echipele una câte una cu un delay de 2 secunde între afișări
for i in range(len(df)):
    for col in df.columns:
        print(df[col][i].ljust(35), end='')                                           # ajustare distanță pentru echipe
        time.sleep(2)                                                                # delay de 2 secunde între afișări
    print()                                                                 # linie de separare între rândurile afișate
'''
# fiindcă position nu este definit în listă teams_of_CL_LP nu pot să pun echipele de la position 1 - 4 în aceeași urnă"
num_pots = 9
num_groups = 4
total_teams_in_LP = len(sorted_teams_of_CL_LP)
teams_per_pot = total_teams_in_LP // num_pots
pots = [sorted_teams_of_CL_LP[i * teams_per_pot:(i + 1) * teams_per_pot] for i in range(num_pots)]  # creează pot-urile
groups = [[] for _ in range(num_groups)]                                                        # crearea grupei finale

print()
for i, pot in enumerate(pots, start=1):                                                             # afișează poturile
    print(f"Pot {i}:")
    for team in pot:
        print(f"{team['team']:<29} - Coefficient: {team['coefficient']:>5.1f}")
    print()

# extrage aleatoriu o echipă din fiecare pot și adaugă în grupele finale
for pot in pots:                                                                                   # pentru fiecare pot
    for group in groups:                                             # fiecare grupă primește o echipă din potul curent
        if pot:                                                                       # verifică dacă potul nu este gol
            team = random.choice(pot)                                                # alege o echipă aleatorie din pot
            pot.remove(team)                                                            # elimină echipa aleasă din pot
            group.append(team)                                                                 # adaugă echipa în grupă

# Crearea DataFrame-ului cu denumirile grupei și echipele
df = pd.DataFrame({f"Grupa {i+1}": [f"{team['team']:<29}" for team in group] for i, group in enumerate(groups)})

print()
# afișează denumirile grupei cu ajustarea distanței dintre ele
header = [f"Grupa {i+1}".ljust(35) for i in range(num_groups)]
print("".join(header))

# afișează echipele una câte una cu un delay de 2 secunde între afișări
for i in range(len(df)):
    for col in df.columns:
        print(df[col][i].ljust(35), end='')                                           # ajustare distanță pentru echipe
        time.sleep(2)                                                                # delay de 2 secunde între afișări
    print()                                                                 # linie de separare între rândurile afișate

# ---------------------------------------------------------------------------------------------------------------------
